import type { GetStaticPaths, GetStaticProps, NextPage } from 'next'
import React from 'react'
import { MainLayout } from 'layout/MainLayout'
import axios from 'axios'
import { ThemedSection } from '@/components/ThemedSection'
import { portfolioApi } from '@/features/portfolio/api'
import { IPortfolioPageResponse } from '@/features/portfolio/api/types'
import { HeaderSection } from '@/features/portfolio/HeaderSection'
import { CasesSection } from '@/features/portfolio/CasesSection'
import { getSsrLocale } from '@/utils/localization'
import { BaseSeo } from '@/seo/BaseSeo'
import { revalidateTime } from '@/constants/revalidate'
import { ISocialData } from '@/requests/socials/types'
import { socialApi } from '@/requests/socials'
import { getRouteLocale } from '@/utils/get-route-locale'
import { ROUTES } from '@/routes'
import { locationsApi } from '@/requests/locations'
import { ILocation } from '@/requests/locations/types'
import { hasCapitalLetters } from '@/utils/hasCapitalLetters'
import { servicesApi } from '@/requests/services'
import { TServiceCategories } from '@/requests/services/types'
import { useBodyTheme } from '@/hooks/use-body-theme'

export const getStaticPaths: GetStaticPaths = async () => {
  const lang = getSsrLocale()
  const casesResponse = await portfolioApi.getCases(lang)

  const subCasesPromises = casesResponse.data.data.tags.map((tag) =>
    portfolioApi.getCase(lang, tag.uri),
  )

  const subCasesResponse = await Promise.allSettled(subCasesPromises)

  const amountPage = Math.ceil(
    casesResponse.data.data.pager.limit / casesResponse.data.data.pager.total,
  )

  const casesPaths = new Array(amountPage).fill(0).map((_, index) => ({
    params: {
      props: [(index + 1).toString()],
    },
  }))

  const subCasesPaths = subCasesResponse.map((x, index) => {
    if (x.status === 'rejected') {
      return null
    }

    return {
      params: {
        props: [casesResponse.data.data.tags[index].uri],
      },
    }
  })

  return {
    paths: [
      {
        params: { props: [] },
      },
      ...casesPaths,
      ...(subCasesPaths.filter(Boolean) as { params: { props: string[] } }[]),
    ],
    fallback: 'blocking',
  }
}

export const getStaticProps: GetStaticProps<
  Record<string, unknown>,
  { props: string[] }
> = async ({ params, locale }) => {
  try {
    const lang = getSsrLocale(locale)

    const props = params?.props ?? []

    if (props.length > 2) {
      return {
        notFound: true,
      }
    }

    if (hasCapitalLetters(props[0]))
      return {
        redirect: {
          destination: `${getRouteLocale(locale)}${
            ROUTES.CASES
          }/${props[0].toLowerCase()}${props[1] ? `/${props[1]}` : ''}`,
          statusCode: 301,
        },
        revalidate: revalidateTime,
      }

    const response = await Promise.all([
      socialApi.getSocials(),
      locationsApi.getLocations(lang),
      servicesApi.getServicesCategories(lang),
    ])

    // /cases page without params
    if (props.length === 0) {
      const casesResponse = await portfolioApi.getCases(lang)
      return {
        props: {
          response: casesResponse.data,
          socialLinks: response[0].data.data,
          locations: response[1].data.data,
          services: response[2].data.data,
        },
        revalidate: revalidateTime,
      }
    }

    // /cases/1 page  with pagination
    if (!Number.isNaN(+props[0])) {
      const casesResponse = await portfolioApi.getCases(lang, encodeURI(props[0]))
      return {
        props: {
          response: casesResponse.data,
          socialLinks: response[0].data.data,
          locations: response[1].data.data,
          services: response[2].data.data,
        },
        revalidate: revalidateTime,
      }
    }

    // /cases/cripta page with subCase
    if (Number.isNaN(+props[0])) {
      const caseResponse = await portfolioApi.getCase(lang, encodeURI(props[0]), props[1])
      return {
        props: {
          response: caseResponse.data,
          socialLinks: response[0].data.data,
          locations: response[1].data.data,
          services: response[2].data.data,
        },
        revalidate: revalidateTime,
      }
    }
    // fallback /cases
    const casesResponse = await portfolioApi.getCases(lang)
    return {
      props: {
        response: casesResponse.data,
        socialLinks: response[0].data.data,
        locations: response[1].data.data,
        services: response[2].data.data,
      },
      revalidate: revalidateTime,
    }
  } catch (e) {
    if (axios.isAxiosError(e) && e.response) {
      if (e.response.status === 404) {
        return {
          redirect: {
            destination: `${getRouteLocale(locale)}${ROUTES.CASES}`,
            statusCode: 301,
          },
          revalidate: revalidateTime,
        }
      }
    }

    throw new Error(`Failed to generate /cases page, received status ${e}`)
  }
}

interface ICasesPageProps {
  response: IPortfolioPageResponse
  socialLinks: ISocialData[]
  locations: ILocation[]
  services: TServiceCategories
}

const Case: NextPage<ICasesPageProps> = ({
  response,
  socialLinks,
  locations,
  services,
}) => {
  useBodyTheme('dark')

  return (
    <MainLayout socialLinks={socialLinks} locations={locations} services={services}>
      <BaseSeo
        meta={response.data.content.meta}
        microdata={response.data.microdata}
        noindex={response.data.noindex}
      />
      <ThemedSection theme="dark">
        <HeaderSection title={response.data.content.header} />
        <CasesSection
          cases={response.data.cases}
          tags={response.data.tags}
          pager={response.data.pager}
        />
      </ThemedSection>
    </MainLayout>
  )
}

export default Case
