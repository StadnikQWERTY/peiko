import { GetStaticPaths, GetStaticProps, NextPage } from 'next'
import React from 'react'
import axios from 'axios'
import { MainLayout } from '@/layout/MainLayout'
import { SectionContainer } from '@/components/sections/SectionContainer'
import { ThemedSection } from '@/components/ThemedSection'
import { getSsrLocale } from '@/utils/localization'
import { ROUTES } from '@/routes'
import { revalidateTime } from '@/constants/revalidate'
import { vacanciesApi } from '@/requests/vacancies'
import { IVacancyPageResponse } from '@/requests/vacancies/types'
import { BaseSeo } from '@/seo/BaseSeo'
import VacancyInfo from '@/features/vacancies/VacancyInfo'
import { socialApi } from '@/requests/socials'
import { ISocialData } from '@/requests/socials/types'
import { getRouteLocale } from '@/utils/get-route-locale'
import { hasCapitalLetters } from '@/utils/hasCapitalLetters'
import { locationsApi } from '@/requests/locations'
import { ILocation } from '@/requests/locations/types'
import { servicesApi } from '@/requests/services'
import { TServiceCategories } from '@/requests/services/types'
import { useBodyTheme } from '@/hooks/use-body-theme'

export const getStaticPaths: GetStaticPaths = async () => {
  const result = await vacanciesApi.getVacanciesUris()

  const paths = result.data.data.map((vacancy) => ({
    params: { vacancy: vacancy.uri },
    locale: vacancy.locale,
  }))

  return {
    paths,
    fallback: 'blocking',
  }
}

export const getStaticProps: GetStaticProps<OwnProps> = async (context) => {
  const { locale } = context

  try {
    const lang = getSsrLocale(locale)

    if (hasCapitalLetters(context?.params?.vacancy as string))
      return {
        redirect: {
          destination: `${getRouteLocale(locale)}${ROUTES.VACANCY}/${(
            context?.params?.vacancy as string
          ).toLowerCase()}`,
          statusCode: 301,
        },
        revalidate: revalidateTime,
      }

    const result = await Promise.all([
      vacanciesApi.getVacancy({
        locale: lang as string,
        vacancy: encodeURI(context?.params?.vacancy as string),
      }),
      socialApi.getSocials(),
      locationsApi.getLocations(lang),
      servicesApi.getServicesCategories(lang),
    ])

    return {
      props: {
        response: result[0].data.data,
        socialLinks: result[1].data.data,
        locations: result[2].data.data,
        services: result[3].data.data,
      },
      revalidate: revalidateTime,
    }
  } catch (e) {
    if (axios.isAxiosError(e) && e.response) {
      if (e.response.status === 404) {
        return {
          redirect: {
            destination: `${getRouteLocale(locale)}${ROUTES.VACANCIES}`,
            statusCode: 301,
          },
          revalidate: revalidateTime,
        }
      }
    }

    throw new Error(`Failed to generate /vacancies page, received status ${e}`)
  }
}

type OwnProps = {
  response: IVacancyPageResponse
  socialLinks: ISocialData[]
  locations: ILocation[]
  services: TServiceCategories
}

const Vacancy: NextPage<OwnProps> = (props) => {
  const { response, socialLinks, locations, services } = props
  useBodyTheme('light')

  return (
    <MainLayout socialLinks={socialLinks} locations={locations} services={services}>
      <BaseSeo meta={response.meta} noindex nofollow />
      <ThemedSection theme="light">
        <SectionContainer>
          <VacancyInfo vacancy={response} />
        </SectionContainer>
      </ThemedSection>
    </MainLayout>
  )
}

export default Vacancy
