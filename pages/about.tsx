import type { GetStaticProps, NextPage } from 'next'
import React from 'react'
import { ThemedSection } from '@/components/ThemedSection'
import { GallerySection } from '@/features/about/components/GallerySection'
import { OurPartners } from '@/components/sections/OurPartners'
import { aboutApi } from '@/features/about/api'
import { IAboutPageResponse } from '@/features/about/api/types'
import { PlatformSection } from '@/features/about/components/PlatformSection'
import { TaskSection } from '@/features/about/components/TaskSection'
import { VideoSection } from '@/features/about/components/VideoSection'
import { getSsrLocale } from '@/utils/localization'
import { TestimonialsSection } from '@/components/sections/TestimonialsSection'
import { BaseSeo } from '@/seo/BaseSeo'
import { MainLayout } from '@/layout/MainLayout'
import { VacanciesSection } from '@/components/sections/VacanciesSection'
import { socialApi } from '@/requests/socials'
import { ISocialData } from '@/requests/socials/types'
import { locationsApi } from '@/requests/locations'
import { ILocation } from '@/requests/locations/types'
import { revalidateTime } from '@/constants/revalidate'
import { servicesApi } from '@/requests/services'
import { TServiceCategories } from '@/requests/services/types'
import { useBodyTheme } from '@/hooks/use-body-theme'

export const getStaticProps: GetStaticProps<IAboutPageProps> = async ({ locale }) => {
  try {
    const lang = getSsrLocale(locale)
    const result = await Promise.all([
      aboutApi.getAbout(lang),
      socialApi.getSocials(),
      locationsApi.getLocations(lang),
      servicesApi.getServicesCategories(lang),
    ])

    return {
      props: {
        response: result[0].data,
        socialLinks: result[1].data.data,
        locations: result[2].data.data,
        services: result[3].data.data,
      },
      revalidate: revalidateTime,
    }
  } catch (e) {
    throw new Error(`Failed to generate /about page, received status ${e}`)
  }
}

interface IAboutPageProps {
  response: IAboutPageResponse
  socialLinks: ISocialData[]
  locations: ILocation[]
  services: TServiceCategories
}

const About: NextPage<IAboutPageProps> = ({
  response,
  socialLinks,
  locations,
  services,
}) => {
  useBodyTheme('dark')

  return (
    <MainLayout socialLinks={socialLinks} locations={locations} services={services}>
      <BaseSeo
        meta={response.data.content.meta}
        microdata={response.data.microdata}
        noindex={response.data.noindex}
      />

      <ThemedSection theme="dark">
        <GallerySection data={response.data.gallery} content={response.data.content} />
        <PlatformSection data={response.data.awards} />
        <TaskSection data={response.data.tasks} />
        <OurPartners data={response.data.partners} />
        <VideoSection data={response.data.video} />
        <TestimonialsSection data={response.data.reviews} />
        <VacanciesSection data={response.data.vacancies} />
      </ThemedSection>
    </MainLayout>
  )
}

export default About
