import React from 'react'
import { GetStaticPaths, GetStaticProps, NextPage } from 'next'
import styled from 'styled-components'
import useTranslation from 'next-translate/useTranslation'
import axios from 'axios'
import { casesApi } from '@/requests/cases'
import { ICasePageResponse } from '@/requests/cases/types'
import { CaseSection } from '@/features/cases/CaseSection/CaseSection'
import BackgroundSection from '@/features/cases/BackgroundSection/BackgroundSection'
import { ThemedSection } from '@/components/ThemedSection'
import { SectionContainer } from '@/components/sections/SectionContainer'
import { SectionTitles } from '@/components/sections/SectionTitles'
import TasksAndChallengesSection from '@/features/cases/TasksAndChallengesSection/TasksAndChallengesSection'
import ScreensSection from '@/features/cases/ScreensSection/ScreensSection'
import SolutionSection from '@/features/cases/SolutionSection/SolutionSection'
import WhatTheClientGotSection from '@/features/cases/WhatTheClientGotSection/WhatTheClientGotSection'
import TestimonialSection from '@/features/cases/TestimonialSection/TestimonialSection'
import { Spacer } from '@/components/Spacer/Spacer'
import { getSsrLocale } from '@/utils/localization'
import { revalidateTime } from '@/constants/revalidate'
import { getRouteLocale } from '@/utils/get-route-locale'
import { BaseSeo } from '@/seo/BaseSeo'
import { MainLayout } from '@/layout/MainLayout'
import { ROUTES } from '@/routes'
import ShareYourIdea from '@/components/ShareYourIdea'
import { socialApi } from '@/requests/socials'
import { ISocialData } from '@/requests/socials/types'
import { hasCapitalLetters } from '@/utils/hasCapitalLetters'
import { PageSwitcher } from '@/components/PageSwitcher/PageSwitcher'
import { locationsApi } from '@/requests/locations'
import { ILocation } from '@/requests/locations/types'
import { servicesApi } from '@/requests/services'
import { TServiceCategories } from '@/requests/services/types'
import { useBodyTheme } from '@/hooks/use-body-theme'

export const getStaticPaths: GetStaticPaths = async () => {
  const result = await casesApi.getCasesUris()

  const paths = result.data.data.map((service) => ({
    params: { case: service.uri },
    locale: service.locale,
  }))

  return {
    paths,
    fallback: 'blocking',
  }
}

export const getStaticProps: GetStaticProps<TCase> = async (context) => {
  const { locale } = context

  if (hasCapitalLetters(context?.params?.case as string))
    return {
      redirect: {
        destination: `${getRouteLocale(locale)}${ROUTES.CASE}/${(
          context?.params?.case as string
        ).toLowerCase()}`,
        statusCode: 301,
      },
      revalidate: revalidateTime,
    }

  try {
    const lang = getSsrLocale(locale)

    const result = await Promise.all([
      casesApi.getCase({
        locale: lang as string,
        name: encodeURI(context?.params?.case as string),
      }),
      socialApi.getSocials(),
      locationsApi.getLocations(lang),
      servicesApi.getServicesCategories(lang),
    ])

    return {
      props: {
        response: result[0].data.data,
        socialLinks: result[1].data.data,
        locations: result[2].data.data,
        services: result[3].data.data,
      },
      revalidate: revalidateTime,
    }
  } catch (e) {
    if (axios.isAxiosError(e) && e.response) {
      if (e.response.status === 404) {
        return {
          redirect: {
            destination: `${getRouteLocale(locale)}${ROUTES.CASES}`,
            statusCode: 301,
          },
          revalidate: revalidateTime,
        }
      }
    }

    throw new Error(`Failed to generate /case page, received status ${e}`)
  }
}

const StyledSectionTitles = styled(SectionTitles)`
  margin: 0 0 20px 0;
  padding: 0;
`

const ThemedContainer = styled.div`
  height: 100vh;
  display: flex;
  flex-direction: column;

  div:last-of-type {
    margin-top: auto;
  }
`

type TCase = {
  response: ICasePageResponse
  socialLinks: ISocialData[]
  locations: ILocation[]
  services: TServiceCategories
}

const Case: NextPage<TCase> = (props) => {
  const { response, socialLinks, locations, services } = props
  const { slider } = response
  const { t: tCommon } = useTranslation('common')
  useBodyTheme('dark')

  return (
    <MainLayout socialLinks={socialLinks} locations={locations} services={services}>
      <BaseSeo
        meta={response.meta}
        microdata={response.microdata}
        noindex={response.noindex}
      />

      <ThemedSection theme="dark">
        <CaseSection caseData={response} />
      </ThemedSection>
      <ThemedSection theme="light">
        <SectionContainer>
          <Spacer lg={{ padding: '100px 0' }}>
            <StyledSectionTitles tag="h2" name={tCommon('background')} header={<></>} />
            <BackgroundSection caseData={response} />
          </Spacer>
          <StyledSectionTitles name={tCommon('tasksAndChallenges')} header={<></>} />
          <Spacer lg={{ margin: '40px 0' }}>
            <TasksAndChallengesSection caseData={response} />
          </Spacer>
        </SectionContainer>
        <Spacer lg={{ padding: '100px 0' }}>
          <SectionContainer>
            <SectionTitles tag="h2" name={tCommon('screens')} header={<></>} />
          </SectionContainer>
          <ScreensSection slider={slider} />
        </Spacer>
      </ThemedSection>
      <ThemedSection theme="dark">
        <SectionContainer>
          <Spacer lg={{ padding: '100px 0' }}>
            <SectionTitles name={tCommon('solution')} header={<></>} />
            <SolutionSection caseData={response} />
          </Spacer>
          {(response.result.checklist || response.result.description) && (
            <>
              <SectionTitles
                name={tCommon('clientGot')}
                header={tCommon('results')}
                tagHeader="h2"
              />
              <WhatTheClientGotSection caseData={response} />
            </>
          )}
          <br />
          <Spacer lg={{ padding: '100px 0' }}>
            <TestimonialSection community={response.community} review={response.review} />
          </Spacer>
        </SectionContainer>
      </ThemedSection>
      <ThemedSection theme="light">
        <ThemedContainer>
          <Spacer md={{ padding: '0', margin: 'auto 0' }} xs={{ padding: '180px 0' }}>
            <ShareYourIdea />
          </Spacer>

          <Spacer lg={{ padding: '75px 0 150px 0' }} xs={{ padding: '60px 0 120px 0' }}>
            <SectionContainer>
              <PageSwitcher
                prev={{
                  name: response.prev.name,
                  url: `${ROUTES.CASE}/${response.prev.uri}`,
                }}
                next={{
                  name: response.next.name,
                  url: `${ROUTES.CASE}/${response.next.uri}`,
                }}
              />
            </SectionContainer>
          </Spacer>
        </ThemedContainer>
      </ThemedSection>
    </MainLayout>
  )
}

export default Case
