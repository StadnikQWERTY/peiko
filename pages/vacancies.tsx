import { GetStaticProps, NextPage } from 'next'
import useTranslation from 'next-translate/useTranslation'
import { useRouter } from 'next/router'
import React, { useState } from 'react'
import { MainLayout } from '@/layout/MainLayout'
import { SectionContainer } from '@/components/sections/SectionContainer'
import { ThemedSection } from '@/components/ThemedSection'
import { getSsrLocale } from '@/utils/localization'
import { vacanciesApi } from '@/requests/vacancies'
import { IVacanciesPageResponse } from '@/requests/vacancies/types'
import Content from '@/features/vacancies/Content'
import { ROUTES } from '@/routes'
import { BaseSeo } from '@/seo/BaseSeo'
import { Spacer } from '@/components/Spacer/Spacer'
import { Breadcrumbs } from '@/components/Breadcrumbs'
import { VacanciesSection } from '@/components/sections/VacanciesSection'
import { Text } from '@/components/Text'
import LongButton from '@/components/LongButton/LongButton'
import Popup from '@/features/vacancies/Popup'
import ApplyToPositionForm from '@/features/vacancies/forms/ApplyToPositionForm'
import { socialApi } from '@/requests/socials'
import { ISocialData } from '@/requests/socials/types'
import { revalidateTime } from '@/constants/revalidate'
import { PageSwitcher } from '@/components/PageSwitcher/PageSwitcher'
import { locationsApi } from '@/requests/locations'
import { ILocation } from '@/requests/locations/types'
import { servicesApi } from '@/requests/services'
import { TServiceCategories } from '@/requests/services/types'
import { useBodyTheme } from '@/hooks/use-body-theme'

export const getStaticProps: GetStaticProps<OwnProps> = async ({ locale }) => {
  try {
    const lang = getSsrLocale(locale)
    const result = await Promise.all([
      vacanciesApi.getVacancies({ locale: lang }),
      socialApi.getSocials(),
      locationsApi.getLocations(lang),
      servicesApi.getServicesCategories(lang),
    ])

    return {
      props: {
        response: result[0].data.data,
        socialLinks: result[1].data.data,
        locations: result[2].data.data,
        services: result[3].data.data,
      },
      revalidate: revalidateTime,
    }
  } catch (e) {
    throw new Error(`Failed to generate /vacancies page, received status ${e}`)
  }
}

interface OwnProps {
  response: IVacanciesPageResponse
  socialLinks: ISocialData[]
  locations: ILocation[]
  services: TServiceCategories
}

const Vacancies: NextPage<OwnProps> = (props) => {
  const { response, socialLinks, locations, services } = props
  const { content } = response
  useBodyTheme('light')

  const { t: tCommon } = useTranslation('common')
  const { asPath } = useRouter()
  const [isApplyToPositionPopup, setApplyToPositionPopup] = useState<boolean>(false)

  const breadcrumbs = [
    { name: tCommon('home'), href: ROUTES.HOME },
    { name: tCommon('vacancies'), href: asPath },
  ]

  return (
    <MainLayout socialLinks={socialLinks} locations={locations} services={services}>
      <BaseSeo meta={response.content.meta} noindex nofollow />
      <ThemedSection theme="light">
        <SectionContainer>
          <Spacer lg={{ padding: '66px 0 0 0' }} xs={{ padding: '88px 0 0 0' }}>
            <Breadcrumbs breadcrumbs={breadcrumbs} />
          </Spacer>
          <Spacer lg={{ padding: '26px 0 0 0' }} xs={{ padding: '16px 0 0 0' }}>
            <Content content={content} />
          </Spacer>
        </SectionContainer>
        <VacanciesSection data={response.vacancies} withoutSeeAll />
        <Text variant="h3" tag="h3" align="center" margin="20px 24px">
          {tCommon('haventFoundVacancy')}
        </Text>
        <SectionContainer>
          <Popup
            onClose={() => setApplyToPositionPopup(false)}
            open={isApplyToPositionPopup}
          >
            <div
              style={{
                maxWidth: '598px',
                margin: '0 auto',
                padding: '0 24px',
                width: '100%',
              }}
            >
              <Text variant="h2" tag="h2">
                {tCommon('submitYourCV')}
              </Text>
              <Text variant="paragraph">{tCommon('soonText')}</Text>
              <Spacer xs={{ margin: '16px 0 0' }}>
                <ApplyToPositionForm vacancyId={0} />
              </Spacer>
            </div>
          </Popup>
        </SectionContainer>
        <Spacer lg={{ padding: '0 0 120px 0' }} xs={{ padding: '0 0 60px 0' }}>
          <LongButton
            xs={{ minWidth: 'auto', paddingLeft: '24px', paddingRight: '24px' }}
            onClick={() => setApplyToPositionPopup(true)}
          >
            {tCommon('writeUs')}
          </LongButton>
        </Spacer>
        <SectionContainer>
          <Spacer lg={{ padding: '174px 0 222px 0' }} xs={{ padding: '66px 0 120px 0' }}>
            <PageSwitcher prev={{ url: ROUTES.ABOUT_US, name: tCommon('aboutUs') }} />
          </Spacer>
        </SectionContainer>
      </ThemedSection>
    </MainLayout>
  )
}

export default Vacancies
