import React from 'react'
import type { GetStaticProps, NextPage } from 'next'
import { AwardsSection } from '@/components/sections/AwardsSection'
import { MainSection } from '@/features/home/MainSection'
import { ServicesSection } from '@/features/home/ServicesSection'
import { AboutUs } from '@/features/home/AboutUs'
import { Cases } from '@/components/sections/Cases'
import { ThemedSection } from '@/components/ThemedSection'
import { Technology } from '@/components/sections/Technology'
import { OurPartners } from '@/components/sections/OurPartners'
import { Spacer } from '@/components/Spacer/Spacer'
import { getSsrLocale } from '@/utils/localization'
import { homeApi } from '@/features/home'
import { IMainPageResponse } from '@/features/home/api/types'
import { BaseSeo } from '@/seo/BaseSeo'
import { socialApi } from '@/requests/socials'
import { ISocialData } from '@/requests/socials/types'
import { revalidateTime } from '@/constants/revalidate'
import { locationsApi } from '@/requests/locations'
import { servicesApi } from '@/requests/services'
import { TServiceCategories } from '@/requests/services/types'
import { ILocation } from '@/requests/locations/types'
import { useBodyTheme } from '@/hooks/use-body-theme'
import { SectionContainer } from '@/components/sections/SectionContainer'
import { MainLayout } from '../src/layout/MainLayout'

export const getStaticProps: GetStaticProps<THome> = async ({ locale }) => {
  try {
    const lang = getSsrLocale(locale)
    const result = await Promise.all([
      homeApi.getHome(lang),
      socialApi.getSocials(),
      locationsApi.getLocations(lang),
      servicesApi.getServicesCategories(lang),
    ])

    return {
      props: {
        response: result[0].data,
        socialLinks: result[1].data.data,
        locations: result[2].data.data,
        services: result[3].data.data,
      },
      revalidate: revalidateTime,
    }
  } catch (e) {
    throw new Error(`Failed to generate / page, received status ${e}`)
  }
}

type THome = {
  response: IMainPageResponse
  socialLinks: ISocialData[]
  locations: ILocation[]
  services: TServiceCategories
}

const Home: NextPage<THome> = (props) => {
  const { response, socialLinks, locations } = props
  const { data } = response
  useBodyTheme('dark')

  return (
    <>
      <MainLayout
        socialLinks={socialLinks}
        locations={locations}
        services={data.services}
      >
        <BaseSeo meta={data.content.meta} microdata={data.microdata} />

        <ThemedSection theme="dark">
          <MainSection
            content={data.content}
            // awards={data.awards}
          />
        </ThemedSection>

        <ThemedSection theme="light">
          {response.data.rewards.length > 0 && (
            <Spacer xs={{ padding: '40px 0 0' }} md={{ padding: '98px 0 0' }}>
              <SectionContainer>
                <AwardsSection data={response.data.rewards} header animate />
              </SectionContainer>
            </Spacer>
          )}

          <Spacer xs={{ padding: '128px 0 120px' }} md={{ padding: '134px 0 120px' }}>
            <ServicesSection services={data.services} />
          </Spacer>
        </ThemedSection>

        <ThemedSection theme="dark">
          <Spacer xs={{ padding: '64px 0' }} md={{ padding: '140px 0' }}>
            <Cases cases={data.cases} />
          </Spacer>
        </ThemedSection>

        <ThemedSection theme="light">
          <Spacer xs={{ padding: '60px 0 0 0' }} md={{ padding: '120px 0 0 0 ' }}>
            <Technology technologies={data.technologies} />
          </Spacer>
        </ThemedSection>

        <ThemedSection theme="light">
          <Spacer xs={{ padding: '60px 0 0' }} md={{ padding: '120px 0 0' }}>
            <AboutUs data={data.about_us} />
          </Spacer>
          <OurPartners data={data.partners} />
        </ThemedSection>
      </MainLayout>
    </>
  )
}

export default Home
