import Document, { Html, Head, Main, NextScript, DocumentContext } from 'next/document'
import React from 'react'
import { ServerStyleSheet } from 'styled-components'
import { Favicon } from '@/seo/Favicon'
import { GoogleAnalytics } from '@/seo/GoogleAnalytics'
import { GoogleTagManager, GoogleTagManagerNoScript } from '@/seo/GoogleTagManager'
import { Smartlook } from '@/seo/Smartlook'

class MyDocument extends Document {
  static async getInitialProps(ctx: DocumentContext) {
    // https://styled-components.com/docs/advanced#server-side-rendering
    const sheet = new ServerStyleSheet()
    const originalRenderPage = ctx.renderPage

    try {
      // eslint-disable-next-line no-param-reassign
      ctx.renderPage = () =>
        originalRenderPage({
          enhanceApp: (App) => (props) => sheet.collectStyles(<App {...props} />),
        })

      const initialProps = await Document.getInitialProps(ctx)
      return {
        ...initialProps,
        styles: (
          <>
            {initialProps.styles}
            {sheet.getStyleElement()}
          </>
        ),
      }
    } finally {
      sheet.seal()
    }
  }

  render() {
    return (
      <Html>
        <Head>
          <link rel="preconnect" href="https://fonts.googleapis.com" />
          <link
            href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;0,700;1,600&display=auto"
            rel="stylesheet"
          />
          <Favicon />
          <Smartlook />

          <GoogleTagManager />
          <GoogleAnalytics />
          <script
            src="https://cdnjs.cloudflare.com/ajax/libs/three.js/r70/three.min.js"
            id="threeJS"
            async
          />
        </Head>
        <body>
          <GoogleTagManagerNoScript />
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

export default MyDocument
