import React from 'react'
import { GetStaticPaths, GetStaticProps, NextPage } from 'next'
import SyntaxHighlighter from 'react-syntax-highlighter'
import { useRouter } from 'next/router'
import useTranslation from 'next-translate/useTranslation'
import styled from 'styled-components'
import axios from 'axios'
import { ROUTES } from '@/routes'
import { getSsrLocale } from '@/utils/localization'
import { blogApi } from '@/requests/blog'
import { Breadcrumbs } from '@/components/Breadcrumbs'
import { ThemedSection } from '@/components/ThemedSection'
import { IArticlePageResponse } from '@/requests/blog/types'
import { Text } from '@/components/Text'
import { SectionContainer } from '@/components/sections/SectionContainer'
import DateAndReadInfo from '@/components/DateAndReadInfo'
import { BaseImage } from '@/components/BaseImage'
import List from '@/components/List/List'
import BlogCards from '@/features/blog/BlogCards'
import { SectionTitles } from '@/components/sections/SectionTitles'
import { MainLayout } from '@/layout/MainLayout'
import ShareYourIdea from '@/components/ShareYourIdea'
import { Spacer } from '@/components/Spacer/Spacer'
import LongButton from '@/components/LongButton/LongButton'
import { useSetContactUs } from '@/features/contact-us'
import { Description } from '@/features/blog/Description'
import { QuoteBlock } from '@/features/blog/QuoteBlock/QuoteBlock'
import { ComparisonBlock } from '@/components/ComparisonBlock'
import { SliderBlock } from '@/features/blog/SliderBlock/SliderBlock'
import { TableComparisonBlock } from '@/features/blog/TableComparisonBlock'
import { TableBlock } from '@/features/blog/TableBlock'
import { DevelopmentServices } from '@/components/DevelopmentServices/DevelopmentServices'
import { BaseSeo } from '@/seo/BaseSeo'
import { socialApi } from '@/requests/socials'
import { ISocialData } from '@/requests/socials/types'
import { revalidateTime } from '@/constants/revalidate'
import { getRouteLocale } from '@/utils/get-route-locale'
import { customParser } from '@/utils/customParser'
import { hasCapitalLetters } from '@/utils/hasCapitalLetters'
import { locationsApi } from '@/requests/locations'
import { ILocation } from '@/requests/locations/types'
import { useViews } from '@/hooks/use-views'
import { servicesApi } from '@/requests/services'
import { TServiceCategories } from '@/requests/services/types'
import { useBodyTheme } from '@/hooks/use-body-theme'

export const getStaticProps: GetStaticProps<OwnProps> = async ({ locale, params }) => {
  try {
    const lang = getSsrLocale(locale)

    if (hasCapitalLetters(params?.article as string))
      return {
        redirect: {
          destination: `${getRouteLocale(locale)}${ROUTES.BLOG}${ROUTES.ARTICLE}/${(
            params?.article as string
          ).toLowerCase()}`,
          statusCode: 301,
          revalidate: revalidateTime,
        },
      }

    const response = await Promise.all([
      blogApi.getArticle({
        locale: lang,
        uri: encodeURI(params?.article as string),
      }),
      socialApi.getSocials(),
      locationsApi.getLocations(lang),
      servicesApi.getServicesCategories(lang),
    ])

    return {
      props: {
        response: response[0].data.data,
        socialLinks: response[1].data.data,
        locations: response[2].data.data,
        services: response[3].data.data,
      },
      revalidate: revalidateTime,
    }
  } catch (e) {
    if (axios.isAxiosError(e) && e.response) {
      if (e.response.status === 404) {
        return {
          redirect: {
            destination: `${getRouteLocale(locale)}${ROUTES.BLOG}`,
            statusCode: 301,
          },
          revalidate: revalidateTime,
        }
      }
    }

    throw new Error(`Failed to generate /blog page, received status ${e}`)
  }
}

export const getStaticPaths: GetStaticPaths = async () => {
  const { data } = await blogApi.getBlogUris()

  return {
    paths: data.data.map((blog) => ({
      locale: blog.locale,
      params: { article: blog.uri },
    })),
    fallback: 'blocking',
  }
}

const HeaderImage = styled.div<{ background: string }>`
  ${(props) => props.background};
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 72px;
`

const BlogContent = styled.div`
  max-width: 1000px;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  gap: 24px;
  h3 {
    margin: 0;
  }
`

const CodeBlock = styled.div`
  border: 1px solid black;
  margin: 0 !important;
  padding: 0 !important;

  pre {
    margin: 0 !important;
    padding: 15px !important;
  }
`

interface OwnProps {
  response: IArticlePageResponse
  socialLinks: ISocialData[]
  locations: ILocation[]
  services: TServiceCategories
}

const Article: NextPage<OwnProps> = (props) => {
  const router = useRouter()
  const { asPath } = router
  const { response, socialLinks, locations, services } = props
  const { t } = useTranslation('common')
  const { setContactUsVisibility } = useSetContactUs()
  const { views, fetchingViews } = useViews({ articleSlug: router.query.article })
  useBodyTheme('light')

  const breadcrumbs = [
    { name: t('home'), href: ROUTES.HOME },
    { name: t('blog'), href: ROUTES.BLOG },
    { name: response.name, href: asPath },
  ]

  return (
    <MainLayout socialLinks={socialLinks} locations={locations} services={services}>
      <BaseSeo
        meta={response.meta}
        microdata={response.microdata}
        serverLocales={response.locales}
        noindex={response.noindex}
      />
      <ThemedSection theme="light">
        <SectionContainer>
          <Spacer lg={{ padding: '66px 0 18px 0' }} xs={{ padding: '88px 0 16px 0' }}>
            <Breadcrumbs breadcrumbs={breadcrumbs} justifyContent="start" />
          </Spacer>
          <Text tag="h1" variant="h1" margin="0 0 24px 0" lg={{ fontSize: '56px' }}>
            {response.name}
          </Text>
          <DateAndReadInfo
            date={response.date}
            views={views[0] || response.views}
            fetching={fetchingViews}
          />
          <Spacer lg={{ padding: '48px 0' }} xs={{ padding: '24px 0' }}>
            <HeaderImage background={response.gradient}>
              <BaseImage
                src={response.photo}
                alt={response.name}
                width={288}
                height={200}
                objectFit="cover"
              />
            </HeaderImage>
          </Spacer>
          <Spacer lg={{ padding: '0 0 144px 0' }} xs={{ padding: '0 0 120px 0' }}>
            <BlogContent>
              {response.blocks.map((block, blockIndex) => {
                switch (block.component) {
                  case 'description':
                    return (
                      // eslint-disable-next-line react/no-array-index-key
                      <Description block={block} key={blockIndex} />
                    )

                  case 'quote':
                    return (
                      // eslint-disable-next-line react/no-array-index-key
                      <QuoteBlock block={block} key={blockIndex} />
                    )

                  case 'list': {
                    const formatList =
                      (Array.isArray(block?.list_items) &&
                        block.list_items?.map((item) => ({ name: item }))) ||
                      []

                    return (
                      // eslint-disable-next-line react/no-array-index-key
                      <React.Fragment key={blockIndex}>
                        {block.header?.content && (
                          <Text
                            variant="h3ForBlog"
                            tag={
                              block.header?.tag as keyof JSX.IntrinsicElements | undefined
                            }
                          >
                            {block.header?.content}
                          </Text>
                        )}
                        {block.before_list_text && (
                          <Text variant="paragraphForBlog">
                            {customParser(block.before_list_text)}
                          </Text>
                        )}
                        <List list={formatList} listTag={block.list_tag} />
                        {block.after_list_text && (
                          <Text variant="paragraphForBlog">
                            {customParser(block.after_list_text)}
                          </Text>
                        )}
                      </React.Fragment>
                    )
                  }

                  case 'comparison': {
                    return (
                      <ComparisonBlock
                        // eslint-disable-next-line react/no-array-index-key
                        key={blockIndex}
                        items={block.items || []}
                        header={block.header?.content || ''}
                        headerTag={block.header?.tag || ''}
                        afterDesc={block.after_desc}
                        beforeDesc={block.before_desc}
                      />
                    )
                  }

                  case 'slider': {
                    // eslint-disable-next-line react/no-array-index-key
                    return <SliderBlock block={block} key={blockIndex} />
                  }

                  case 'table-comparison': {
                    // eslint-disable-next-line react/no-array-index-key
                    return <TableComparisonBlock block={block} key={blockIndex} />
                  }

                  case 'table': {
                    // eslint-disable-next-line react/no-array-index-key
                    return <TableBlock block={block} key={blockIndex} />
                  }

                  case 'button': {
                    return (
                      <LongButton
                        // eslint-disable-next-line react/no-array-index-key
                        key={blockIndex}
                        onClick={() => setContactUsVisibility(true)}
                      >
                        {t('contactUs')}
                      </LongButton>
                    )
                  }

                  case 'services': {
                    return (
                      // eslint-disable-next-line react/no-array-index-key
                      <React.Fragment key={blockIndex}>
                        {block.header?.content && (
                          <Text
                            variant="h3ForBlog"
                            tag={
                              block.header?.tag as keyof JSX.IntrinsicElements | undefined
                            }
                          >
                            {block.header?.content}
                          </Text>
                        )}
                        {(block?.services?.length || 0) > 0 && (
                          <DevelopmentServices services={block.services || []} />
                        )}
                      </React.Fragment>
                    )
                  }

                  case 'snippet': {
                    return (
                      // eslint-disable-next-line react/no-array-index-key
                      <CodeBlock key={blockIndex}>
                        <SyntaxHighlighter
                          language="javascript"
                          wrapLines
                          showLineNumbers
                        >
                          {customParser(block?.code || '') as string}
                        </SyntaxHighlighter>
                      </CodeBlock>
                    )
                  }

                  default:
                    // eslint-disable-next-line react/no-array-index-key
                    return <React.Fragment key={blockIndex} />
                }
              })}
            </BlogContent>
          </Spacer>
          {response.similar.length > 1 && (
            <>
              <SectionTitles header={t('relatedArticles')} name={t('blog')} />
              <BlogCards articles={response.similar} />
            </>
          )}
          <Spacer
            lg={{ padding: response.similar.length > 1 ? '288px 0' : '72px 0 288px' }}
            xs={{ padding: response.similar.length > 1 ? '120px 0' : '60px 0 180px' }}
          >
            <ShareYourIdea />
          </Spacer>
        </SectionContainer>
      </ThemedSection>
    </MainLayout>
  )
}

export default Article
