import { GetServerSideProps, NextPage } from 'next'
import React from 'react'
import { useRouter } from 'next/router'
import axios from 'axios'
import { getSsrLocale } from '@/utils/localization'
import { blogApi } from '@/requests/blog'
import { IBlogPageResponse } from '@/requests/blog/types'
import BlogPage from '@/layout/pages/BlogPage'
import { BaseSeo } from '@/seo/BaseSeo'
import { socialApi } from '@/requests/socials'
import { ISocialData } from '@/requests/socials/types'
import { ROUTES } from '@/routes'
import { BASE_APP_URL } from '@/constants/publicInfo'
import { getRouteLocale } from '@/utils/get-route-locale'
import { locationsApi } from '@/requests/locations'
import { ILocation } from '@/requests/locations/types'
import { hasCapitalLetters } from '@/utils/hasCapitalLetters'
import { servicesApi } from '@/requests/services'
import { TServiceCategories } from '@/requests/services/types'
import { useBodyTheme } from '@/hooks/use-body-theme'

export const getServerSideProps: GetServerSideProps<OwnProps> = async ({
  query,
  locale,
}) => {
  try {
    const lang = getSsrLocale(locale)
    const page = query.page as string[] | undefined

    if (page && page.length > 1) {
      return {
        notFound: true,
      }
    }

    if (hasCapitalLetters(query.tags as string))
      return {
        redirect: {
          destination: `${getRouteLocale(locale)}${ROUTES.BLOG}${ROUTES.TAG}/${(
            query.tags as string
          ).toLowerCase()}${page?.[0] ? `/${page[0]}` : ''}`,
          statusCode: 301,
        },
      }

    const response = await Promise.all([
      blogApi.getBlog({
        locale: lang,
        tags: query.tags as string,
        page: page && page[0],
      }),
      socialApi.getSocials(),
      locationsApi.getLocations(lang),
      servicesApi.getServicesCategories(lang),
    ])

    return {
      props: {
        response: response[0].data.data,
        socialLinks: response[1].data.data,
        locations: response[2].data.data,
        services: response[3].data.data,
      },
    }
  } catch (e) {
    if (axios.isAxiosError(e) && e.response) {
      if (e.response.status === 404) {
        return {
          redirect: {
            destination: `${getRouteLocale(locale)}${ROUTES.BLOG}`,
            statusCode: 301,
          },
        }
      }
    }

    throw new Error(`Failed to generate /blog page, received status ${e}`)
  }
}

interface OwnProps {
  response: IBlogPageResponse
  socialLinks: ISocialData[]
  locations: ILocation[]
  services: TServiceCategories
}

const Blog: NextPage<OwnProps> = (props) => {
  const { response } = props
  const router = useRouter()
  useBodyTheme('light')

  return (
    <>
      <BaseSeo
        meta={response.content.meta}
        noindex={response.noindex}
        canonical={`${BASE_APP_URL}${getRouteLocale(router.locale)}${ROUTES.BLOG}`}
      />
      <BlogPage {...props} />
    </>
  )
}

export default Blog
