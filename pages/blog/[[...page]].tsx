import { GetStaticPaths, GetStaticProps, NextPage } from 'next'
import React from 'react'
import axios from 'axios'
import { getSsrLocale } from '@/utils/localization'
import { blogApi } from '@/requests/blog'
import { IBlogPageResponse } from '@/requests/blog/types'
import BlogPage from '@/layout/pages/BlogPage'
import { BaseSeo } from '@/seo/BaseSeo'
import { socialApi } from '@/requests/socials'
import { ISocialData } from '@/requests/socials/types'
import { ROUTES } from '@/routes'
import { revalidateTime } from '@/constants/revalidate'
import { locationsApi } from '@/requests/locations'
import { getRouteLocale } from '@/utils/get-route-locale'
import { ILocation } from '@/requests/locations/types'
import { servicesApi } from '@/requests/services'
import { TServiceCategories } from '@/requests/services/types'
import { useBodyTheme } from '@/hooks/use-body-theme'
import i18nConfig from '../../i18n.js'

export const getStaticPaths: GetStaticPaths = async () => ({
  paths: i18nConfig.locales.map((locale) => ({
    locale,
    params: { page: undefined },
  })),
  fallback: 'blocking',
})

export const getStaticProps: GetStaticProps<OwnProps> = async ({ locale, params }) => {
  try {
    const lang = getSsrLocale(locale)
    const page = params?.page as string[] | undefined

    if (page && page.length > 1) {
      return {
        notFound: true,
      }
    }

    const response = await Promise.all([
      blogApi.getBlog({ locale: lang, page: page && page[0] }),
      socialApi.getSocials(),
      locationsApi.getLocations(lang),
      servicesApi.getServicesCategories(lang),
    ])

    return {
      props: {
        response: response[0].data.data,
        socialLinks: response[1].data.data,
        locations: response[2].data.data,
        services: response[3].data.data,
      },
      revalidate: revalidateTime,
    }
  } catch (e) {
    if (axios.isAxiosError(e) && e.response) {
      if (e.response.status === 404) {
        return {
          redirect: {
            destination: `${getRouteLocale(locale)}${ROUTES.BLOG}`,
            statusCode: 301,
          },
          revalidate: revalidateTime,
        }
      }
    }

    throw new Error(`Failed to generate /blog page, received status ${e}`)
  }
}

interface OwnProps {
  response: IBlogPageResponse
  socialLinks: ISocialData[]
  locations: ILocation[]
  services: TServiceCategories
}

const Page: NextPage<OwnProps> = (props) => {
  const { response, socialLinks, locations, services } = props
  useBodyTheme('light')

  return (
    <>
      <BaseSeo meta={response.content.meta} noindex={response.noindex} />
      <BlogPage
        socialLinks={socialLinks}
        response={response}
        locations={locations}
        services={services}
      />
    </>
  )
}

export default Page
