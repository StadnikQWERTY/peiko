import { GetStaticProps, NextPage } from 'next'
import { ErrorPage } from '@/layout/pages/ErrorPage'
import { ILocation } from '@/requests/locations/types'
import { servicesApi } from '@/requests/services'
import { getSsrLocale } from '@/utils/localization'
import { TServiceCategories } from '@/requests/services/types'
import { locationsApi } from '@/requests/locations'
import { revalidateTime } from '@/constants/revalidate'
import { useBodyTheme } from '@/hooks/use-body-theme'

interface OwnProps {
  locations: ILocation[]
  services: TServiceCategories
}

export const getStaticProps: GetStaticProps<OwnProps> = async ({ locale }) => {
  try {
    const lang = getSsrLocale(locale)
    const result = await Promise.all([
      locationsApi.getLocations(lang),
      servicesApi.getServicesCategories(lang),
    ])

    return {
      props: {
        locations: result[0].data.data,
        services: result[1].data.data,
      },
      revalidate: revalidateTime,
    }
  } catch (e) {
    throw new Error(`Failed to generate /vacancies page, received status ${e}`)
  }
}

const NotFoundPage: NextPage<OwnProps> = ({ locations, services }) => {
  useBodyTheme('dark')

  return <ErrorPage status={404} locations={locations} services={services} />
}

export default NotFoundPage
