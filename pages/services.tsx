import { GetStaticProps, NextPage } from 'next'
import React from 'react'
import { useRouter } from 'next/router'
import useTranslation from 'next-translate/useTranslation'
import { ThemedSection } from '@/components/ThemedSection'
import { IServicesPageResponse, TServiceCategories } from '@/requests/services/types'
import { servicesApi } from '@/requests/services'
import { ServicesSection } from '@/features/services/ServicesSection/ServicesSection'
import { Breadcrumbs } from '@/components/Breadcrumbs'
import { SectionContainer } from '@/components/sections/SectionContainer'
import { Spacer } from '@/components/Spacer/Spacer'
import { Text } from '@/components/Text'
import { Technology } from '@/components/sections/Technology'
import { ROUTES } from '@/routes'
import { getSsrLocale } from '@/utils/localization'
import { BaseSeo } from '@/seo/BaseSeo'
import ShareYourIdea from '@/components/ShareYourIdea'
import { MainLayout } from '@/layout/MainLayout'
import { ISocialData } from '@/requests/socials/types'
import { socialApi } from '@/requests/socials'
import { revalidateTime } from '@/constants/revalidate'
import { locationsApi } from '@/requests/locations'
import { ILocation } from '@/requests/locations/types'
import { PageSwitcher } from '@/components/PageSwitcher/PageSwitcher'
import { useBodyTheme } from '@/hooks/use-body-theme'

export const getStaticProps: GetStaticProps<TServices> = async ({ locale }) => {
  try {
    const lang = getSsrLocale(locale)
    const result = await Promise.all([
      servicesApi.getServices(lang),
      socialApi.getSocials(),
      locationsApi.getLocations(lang),
      servicesApi.getServicesCategories(lang),
    ])

    return {
      props: {
        response: result[0].data.data,
        socialLinks: result[1].data.data,
        locations: result[2].data.data,
        services: result[3].data.data,
      },
      revalidate: revalidateTime,
    }
  } catch (e) {
    throw new Error(`Failed to generate /services page, received status ${e}`)
  }
}

type TServices = {
  response: IServicesPageResponse
  socialLinks: ISocialData[]
  locations: ILocation[]
  services: TServiceCategories
}

const Services: NextPage<TServices> = (props) => {
  const { response, socialLinks, locations, services } = props
  const { asPath } = useRouter()
  useBodyTheme('light')
  const { t: tCommon } = useTranslation('common')

  const breadcrumbs = [
    { name: tCommon('home'), href: ROUTES.HOME },
    { name: tCommon('services'), href: asPath },
  ]

  return (
    <MainLayout socialLinks={socialLinks} locations={locations} services={services}>
      <BaseSeo
        meta={response.content.meta}
        microdata={response.microdata}
        noindex={response.noindex}
      />

      <ThemedSection theme="light">
        <SectionContainer>
          <Spacer lg={{ padding: '66px 0 9px 0' }} xs={{ padding: '88px 0 8px 0' }}>
            <Breadcrumbs breadcrumbs={breadcrumbs} />
          </Spacer>
          <Spacer lg={{ padding: '9px 0 40px 0' }} xs={{ padding: '8px 0 60px 0' }}>
            <Text variant="h1" tag="h1" align="center" lg={{ fontSize: '56px' }}>
              {response.content.header}
            </Text>
          </Spacer>
          <Spacer lg={{ padding: '40px 0 75px 0' }} xs={{ padding: '60px 0 60px 0' }}>
            <ServicesSection services={response.services} />
          </Spacer>
        </SectionContainer>
        <ShareYourIdea />
        <Spacer lg={{ padding: '75px 0', margin: '10px' }} xs={{ padding: '60px 0' }}>
          <Technology technologies={response.technologies} />
        </Spacer>
        <SectionContainer>
          <Spacer lg={{ padding: '75px 0 150px' }} xs={{ padding: '60px 0 120px' }}>
            <PageSwitcher
              next={{ name: tCommon('aboutUs'), url: '/about' }}
              prev={{ name: tCommon('cases'), url: '/cases' }}
            />
          </Spacer>
        </SectionContainer>
      </ThemedSection>
    </MainLayout>
  )
}

export default Services
