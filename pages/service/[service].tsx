import { GetStaticPaths, GetStaticProps, NextPage } from 'next'
import React from 'react'
import styled from 'styled-components'
import { useRouter } from 'next/router'
import useTranslation from 'next-translate/useTranslation'
import axios from 'axios'
import { ThemedSection } from '@/components/ThemedSection'
import { SectionContainer } from '@/components/sections/SectionContainer'
import { Breadcrumbs } from '@/components/Breadcrumbs'
import { Text } from '@/components/Text'
import { Description } from '@/features/services/Description/Description'
import Process from '@/features/services/Process/Process'
import { servicesApi } from '@/requests/services'
import { IServicePageResponse, TServiceCategories } from '@/requests/services/types'
import { Features } from '@/features/services/Features/Features'
import { Spacer } from '@/components/Spacer/Spacer'
import { SectionTitles } from '@/components/sections/SectionTitles'
import { PlatformSection } from '@/components/sections/PlatformSection'
import { Technology } from '@/components/sections/Technology'
import { ROUTES } from '@/routes'
import { getSsrLocale } from '@/utils/localization'
import TeamSection from '@/features/services/TeamSection/TeamSection'
import { Cases } from '@/components/sections/Cases'
import AdvantagesSection from '@/features/services/AdvantagesSection/AdvantagesSection'
import { TestimonialsSection } from '@/components/sections/TestimonialsSection'
import AccordionSection from '@/features/services/AccordionSection/AccordionSection'
import { revalidateTime } from '@/constants/revalidate'
import { BaseSeo } from '@/seo/BaseSeo'
import { MainLayout } from '@/layout/MainLayout'
import { useSetContactUs } from '@/features/contact-us'
import { MainSection } from '@/features/services/MainSection'
import { DevelopmentServices } from '@/components/DevelopmentServices/DevelopmentServices'
import { ComparisonBlock } from '@/components/ComparisonBlock'
import { IArticlePageItem } from '@/requests/blog/types'
import { socialApi } from '@/requests/socials'
import { ISocialData } from '@/requests/socials/types'
import { getRouteLocale } from '@/utils/get-route-locale'
import { locationsApi } from '@/requests/locations'
import { ILocation } from '@/requests/locations/types'
import { customParser } from '@/utils/customParser'
import { hasCapitalLetters } from '@/utils/hasCapitalLetters'
import { ServiceLD } from '@/seo/pages/ServiceLD'
import { WhyUs } from '@/components/sections/WhyUs'
import { Cooperation } from '@/components/sections/Cooperation'
import { KeyFeatures } from '@/components/sections/KeyFeatures'
import { deviceCssQuery } from '@/styling/breakpoints'
import { useBodyTheme } from '@/hooks/use-body-theme'
import { AwardsSection } from '@/components/sections/AwardsSection'

export const getStaticPaths: GetStaticPaths = async () => {
  const result = await servicesApi.getServicesUris()

  const paths = result.data.data.map((service) => ({
    params: { service: service.uri },
    locale: service.locale,
  }))

  return {
    paths,
    fallback: 'blocking',
  }
}

export const getStaticProps: GetStaticProps<TService> = async (context) => {
  const { locale } = context

  try {
    const lang = getSsrLocale(locale)

    if (hasCapitalLetters(context?.params?.service as string))
      return {
        redirect: {
          destination: `${getRouteLocale(locale)}${ROUTES.SERVICE}/${(
            context?.params?.service as string
          ).toLowerCase()}`,
          statusCode: 301,
        },
        revalidate: revalidateTime,
      }

    const result = await Promise.all([
      servicesApi.getService({
        locale: lang as string,
        name: encodeURI(context?.params?.service as string),
      }),
      socialApi.getSocials(),
      locationsApi.getLocations(lang),
      servicesApi.getServicesCategories(lang),
    ])

    return {
      props: {
        response: result[0].data.data,
        socialLinks: result[1].data.data,
        locations: result[2].data.data,
        servicesCategories: result[3].data.data,
      },
      revalidate: revalidateTime,
    }
  } catch (e) {
    if (axios.isAxiosError(e) && e.response) {
      if (e.response.status === 404) {
        return {
          redirect: {
            destination: `${getRouteLocale(locale)}${ROUTES.SERVICES}`,
            statusCode: 301,
          },
          revalidate: revalidateTime,
        }
      }
    }

    throw new Error(`Failed to generate /services page, received status ${e}`)
  }
}

const StyledAwardsSection = styled(PlatformSection)`
  margin: 0;
  padding: 0;
`

const StyledSectionTitles = styled(SectionTitles)`
  margin-bottom: 10px;
  padding: 0;
`

const AdvantagesTitles = styled(SectionTitles)`
  @media screen and ${deviceCssQuery.md} {
    margin-bottom: 64px;
    max-width: 800px;
  }
`

const CooperationTitles = styled(SectionTitles)`
  @media screen and ${deviceCssQuery.md} {
    margin-bottom: 74px;
  }
`

type TService = {
  response: IServicePageResponse
  socialLinks: ISocialData[]
  locations: ILocation[]
  servicesCategories: TServiceCategories
}

const Service: NextPage<TService> = (props) => {
  const { response, socialLinks, locations, servicesCategories } = props
  const faqData = response.blocks.find((item) => item.header === 'FAQ')?.items
  const { asPath } = useRouter()
  const { setContactUsVisibility } = useSetContactUs()
  useBodyTheme('dark')

  const { t: tCommon } = useTranslation('common')
  const { t: tServices } = useTranslation('services')

  const currentBreadCrumb = { name: response.name, href: asPath }
  const breadcrumbsWithParent = response.parent?.uri
    ? [
        { name: response.parent.name, href: `${ROUTES.SERVICE}/${response.parent?.uri}` },
        currentBreadCrumb,
      ]
    : [currentBreadCrumb]

  const breadcrumbs = [
    { name: tCommon('home'), href: ROUTES.HOME },
    { name: tCommon('services'), href: ROUTES.SERVICES },
    ...breadcrumbsWithParent,
  ]

  return (
    <>
      <MainLayout
        socialLinks={socialLinks}
        locations={locations}
        services={servicesCategories}
      >
        <BaseSeo
          meta={response.meta}
          microdata={response.microdata}
          serverLocales={response.locales}
          nofollow={response.noindex}
          noindex={response.noindex}
        />
        {faqData && <ServiceLD data={faqData} />}

        <ThemedSection theme="dark">
          <MainSection
            header={response.name}
            description={response.description}
            img={response.photo}
            breadcrumbs={
              <Breadcrumbs breadcrumbs={breadcrumbs} justifyContent="flex-start" />
            }
          />
        </ThemedSection>

        <ThemedSection theme="light">
          {response.blocks.map((block) => {
            switch (block.component) {
              case 'sub-services':
                return (
                  block.services && (
                    <SectionContainer>
                      <Spacer
                        key={block.component}
                        lg={{ padding: '75px 0' }}
                        xs={{ padding: '60px 0' }}
                      >
                        <SectionTitles
                          name={tServices('developmentServices')}
                          header={tServices('projectTypeText')}
                          tagHeader="h2"
                        />
                        <DevelopmentServices services={block.services} />
                      </Spacer>
                    </SectionContainer>
                  )
                )

              case 'why-us':
                return (
                  block.options && (
                    <SectionContainer>
                      <Spacer
                        key={block.component}
                        lg={{ padding: '75px 0' }}
                        xs={{ padding: '60px 0' }}
                      >
                        <SectionTitles header={block.header || ''} tagHeader="h2" />
                        <WhyUs data={block.options} />
                      </Spacer>
                    </SectionContainer>
                  )
                )

              case 'awards':
                return (
                  block.photos && (
                    <SectionContainer>
                      <Spacer
                        key={block.component}
                        lg={{ padding: '75px 0' }}
                        xs={{ padding: '60px 0' }}
                      >
                        <SectionTitles header={block.header || ''} tagHeader="h2" />
                        <AwardsSection data={block.photos} />
                      </Spacer>
                    </SectionContainer>
                  )
                )

              case 'cooperation':
                return (
                  block.cooperation && (
                    <SectionContainer>
                      <Spacer
                        key={block.component}
                        lg={{ padding: '75px 0 0' }}
                        xs={{ padding: '60px 0 0' }}
                      >
                        <CooperationTitles header={block.header} tagHeader="h2" />
                        <Cooperation data={block.cooperation} />
                      </Spacer>
                    </SectionContainer>
                  )
                )

              case 'key-features':
                return (
                  block.keyFeatures && (
                    <ThemedSection theme="dark">
                      <SectionContainer>
                        <Spacer
                          key={block.component}
                          xs={{ padding: '55px 0' }}
                          lg={{ padding: '70px 0' }}
                        >
                          <SectionTitles header={block.header || ''} tagHeader="h2" />
                          <KeyFeatures data={block.keyFeatures} />
                        </Spacer>
                      </SectionContainer>
                    </ThemedSection>
                  )
                )

              case 'description':
                if (!block.header && !block.description) {
                  return null
                }
                return (
                  <SectionContainer>
                    <Spacer
                      key={block.component}
                      lg={{ padding: '75px 0 0 0' }}
                      xs={{ padding: '60px 0 0 0' }}
                    >
                      <StyledSectionTitles
                        name={tServices('aboutServices')}
                        header={block.header || ''}
                        tagHeader="h2"
                      />
                      <Description description={block.description || ''} />
                    </Spacer>
                  </SectionContainer>
                )

              case 'team':
                return (
                  block.team && (
                    <SectionContainer>
                      <Spacer
                        key={block.component}
                        lg={{ padding: '0 0 75px 0' }}
                        xs={{ padding: '0 0 60px 0' }}
                      >
                        <TeamSection team={block.team} />
                      </Spacer>
                    </SectionContainer>
                  )
                )

              case 'advantages':
                return (
                  block.advantages && (
                    <SectionContainer>
                      <Spacer
                        key={block.component}
                        lg={{ padding: '75px 0' }}
                        xs={{ padding: '60px 0' }}
                      >
                        <AdvantagesTitles header={block.header || ''} tagHeader="h2" />
                        <AdvantagesSection advantages={block.advantages} />
                      </Spacer>
                    </SectionContainer>
                  )
                )

              case 'process':
                return (
                  <SectionContainer>
                    <Spacer
                      key={block.component}
                      lg={{ padding: '75px 0' }}
                      xs={{ padding: '60px 0' }}
                    >
                      <SectionTitles header={block.header || ''} tagHeader="h2" />
                      <Text variant="paragraph" tag="div">
                        {customParser(block.content || '')}
                      </Text>
                      {block.steps && <Process steps={block.steps} />}
                    </Spacer>
                  </SectionContainer>
                )

              case 'features':
                return (
                  block.content &&
                  block.features && (
                    <SectionContainer>
                      <Spacer
                        key={block.component}
                        lg={{ padding: '75px 0' }}
                        xs={{ padding: '60px 0' }}
                      >
                        <SectionTitles
                          name="Approach"
                          header={block.header || ''}
                          tagHeader="h2"
                        />
                        <Features content={block.content} features={block.features} />
                      </Spacer>
                    </SectionContainer>
                  )
                )

              case 'rate-services':
                return (
                  block.items && (
                    <SectionContainer>
                      <Spacer
                        key={block.component}
                        lg={{ padding: '75px 0' }}
                        xs={{ padding: '60px 0' }}
                      >
                        <StyledAwardsSection awards={block.items} />
                      </Spacer>
                    </SectionContainer>
                  )
                )

              case 'accordion':
                return (
                  block.items && (
                    <SectionContainer>
                      <Spacer lg={{ padding: '75px 0' }} xs={{ padding: '60px 0' }}>
                        <AccordionSection
                          accordion={block.items}
                          header={block.header || ''}
                        />
                      </Spacer>
                    </SectionContainer>
                  )
                )

              case 'reviews':
                return block.reviews && <TestimonialsSection data={block.reviews} />

              case 'comparison':
                return (
                  block.items && (
                    <SectionContainer>
                      <Spacer lg={{ padding: '75px 0' }} xs={{ padding: '60px 0' }}>
                        <ComparisonBlock
                          items={block.items as unknown as IArticlePageItem[]}
                          header={block.header}
                        />
                      </Spacer>
                    </SectionContainer>
                  )
                )

              case 'suggest-services':
                return (
                  (block.services?.length || 0) > 0 && (
                    <SectionContainer>
                      <Spacer
                        key={block.component}
                        lg={{ padding: '16px 0 75px 0' }}
                        xs={{ padding: '16px 0 60px 0' }}
                      >
                        <DevelopmentServices
                          onClick={() => {
                            setContactUsVisibility(true)
                          }}
                          services={block.services || []}
                        />
                      </Spacer>
                    </SectionContainer>
                  )
                )

              default:
                return <React.Fragment key={block.component} />
            }
          })}
        </ThemedSection>

        <ThemedSection theme="dark">
          {response.blocks.map((block) => {
            switch (block.component) {
              case 'cases':
                return (
                  block.cases && (
                    <Spacer
                      key={block.component}
                      lg={{ padding: '75px 0' }}
                      xs={{ padding: '60px 0' }}
                    >
                      <Cases cases={block.cases} />
                    </Spacer>
                  )
                )

              default:
                return <React.Fragment key={block.component} />
            }
          })}
        </ThemedSection>

        <ThemedSection theme="light">
          {response.blocks.map((block) => {
            switch (block.component) {
              case 'technologies':
                return (
                  block.technologies && (
                    <Spacer
                      key={block.component}
                      lg={{ padding: '75px 0' }}
                      xs={{ padding: '60px 0' }}
                    >
                      <Technology technologies={block.technologies} />
                    </Spacer>
                  )
                )

              case 'additional-services':
                return (
                  (block.services?.length || 0) > 0 && (
                    <SectionContainer>
                      <Spacer
                        key={block.component}
                        lg={{ padding: '75px 0' }}
                        xs={{ padding: '60px 0' }}
                      >
                        <SectionTitles header={block.header || ''} tagHeader="h2" />
                        <DevelopmentServices services={block.services || []} />
                      </Spacer>
                    </SectionContainer>
                  )
                )

              default:
                return <React.Fragment key={block.component} />
            }
          })}
        </ThemedSection>
        {
          // Was decided to remove the block. Maybe we'll return in the future
          //
          // <ThemedSection theme="light">
          //   <SectionContainer>
          //     <Spacer lg={{ padding: '75px 0 150px 0' }} xs={{ padding: '60px 0 120px 0' }}>
          //       <PageSwitcher
          //         next={
          //           response?.next?.name
          //             ? {
          //                 name: response?.next?.name,
          //                 url: `${ROUTES.SERVICE}/${response?.next?.uri}`,
          //               }
          //             : {
          //                 name: tCommon('services'),
          //                 url: `${ROUTES.SERVICES}`,
          //               }
          //         }
          //         prev={
          //           response?.prev?.name
          //             ? {
          //                 name: response?.prev?.name,
          //                 url: `${ROUTES.SERVICE}/${response?.prev?.uri} `,
          //               }
          //             : {
          //                 name: response?.parent?.name || '',
          //                 url: `${ROUTES.SERVICE}/${response?.parent?.uri} `,
          //               }
          //         }
          //       />
          //     </Spacer>
          //   </SectionContainer>
          // </ThemedSection>
        }
      </MainLayout>
    </>
  )
}

export default Service
