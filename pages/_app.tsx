import React from 'react'
import type { AppProps } from 'next/app'
import { Provider } from 'react-redux'
import store from 'store'
import { Normalize } from 'styled-normalize'
import { gsap } from 'gsap'
import { ScrollTrigger } from 'gsap/dist/ScrollTrigger'
import { DefaultSeo } from 'next-seo'
import { GlobalStyles } from '@/styling/global-styles.'
import { additionalMetaTags } from '@/seo/metaTags'
import { useWindowHeight } from '@/hooks/use-window-height'

// eslint-disable-next-line import/no-unresolved
import 'react-image-gallery/styles/css/image-gallery.css'

gsap.registerPlugin(ScrollTrigger)

const MyApp = ({ Component, pageProps }: AppProps) => {
  useWindowHeight()

  return (
    <>
      <DefaultSeo additionalMetaTags={additionalMetaTags} title="Peiko" />
      <Provider store={store}>
        <Normalize />
        <GlobalStyles />
        <Component {...pageProps} />
      </Provider>
    </>
  )
}

export default MyApp
