import { useEffect, useCallback } from 'react'

export const useWindowHeight = (): void => {
  const setHeightDimension = useCallback(() => {
    const { clientHeight } = document.documentElement
    const { innerHeight } = window

    const height = clientHeight < innerHeight ? innerHeight : clientHeight

    document.documentElement.style.setProperty('--window-height', `${height}px`)
  }, [])

  useEffect(() => {
    setHeightDimension()
    window.addEventListener('resize', setHeightDimension)

    return () => {
      window.removeEventListener('resize', setHeightDimension)
    }
  }, [])
}
