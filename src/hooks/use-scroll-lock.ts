import React, { useRef } from 'react'
import {
  disableBodyScroll,
  clearAllBodyScrollLocks,
  BodyScrollOptions,
} from 'body-scroll-lock'

type TRuturn = {
  setScrollLock: (x: boolean) => void
  scrollRef: React.MutableRefObject<null | HTMLDivElement>
}

type TUseScrollLock = () => TRuturn

const options: BodyScrollOptions = {
  reserveScrollBarGap: true,
}

export const useScrollLock: TUseScrollLock = () => {
  const scrollRef = useRef(null)
  const firstRender = useRef(true)

  const setScrollLock = (lock: boolean) => {
    if (!scrollRef.current) return

    if (!lock && !firstRender.current) {
      setTimeout(() => clearAllBodyScrollLocks(), 0)
      return
    }

    if (lock) {
      disableBodyScroll(scrollRef.current, options)
    }

    firstRender.current = false

    return () => {
      if (!scrollRef.current) return
      disableBodyScroll(scrollRef.current, options)
    }
  }

  return { setScrollLock, scrollRef }
}
