import { useEffect, useState } from 'react'

interface IUseScreenSize {
  windowWidth: number
  windowHeight: number
}

export function useScreenSize(): IUseScreenSize {
  const [windowWidth, setWindowWidth] = useState(0)
  const [windowHeight, setWindowHeight] = useState(0)

  useEffect(() => {
    function handleScreenResize() {
      const windowWidth = window.innerWidth
      const windowHeight = window.innerHeight

      setWindowWidth(windowWidth)
      setWindowHeight(windowHeight)
    }

    handleScreenResize()

    window.addEventListener('resize', handleScreenResize)

    return () => {
      window.removeEventListener('resize', handleScreenResize)
    }
  }, [])

  return {
    windowWidth,
    windowHeight,
  }
}
