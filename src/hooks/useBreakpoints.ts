import { useEffect, useMemo, useState } from 'react'
import { breakpoints } from '@/styling/breakpoints'

type TBreakpoints = keyof typeof breakpoints

interface IUseBreakpoints {
  currentBreakpoint: TBreakpoints
  isDesktop: boolean
  isMobile: boolean
  lgAndHigher: boolean
  mdAndHigher: boolean
}

export function useBreakpoints(): IUseBreakpoints {
  const [currentBreakpoint, setCurrentBreakpoint] = useState<TBreakpoints>('xs')

  useEffect(() => {
    const arrBreakpoints = Object.entries(breakpoints).reverse() as unknown as [
      TBreakpoints,
      string,
    ][]

    function handleScreenResize() {
      const windowWidth = window.innerWidth

      const foundBreakpoint = arrBreakpoints.find(
        ([, value]) => windowWidth > parseInt(value, 10),
      )

      if (foundBreakpoint) {
        setCurrentBreakpoint(foundBreakpoint[0])
      }
    }

    handleScreenResize()

    window.addEventListener('resize', handleScreenResize)

    return () => {
      window.removeEventListener('resize', handleScreenResize)
    }
  }, [])

  const breakPointValue = parseInt(breakpoints[currentBreakpoint], 10)

  const isDesktop = useMemo(
    () => breakPointValue >= parseInt(breakpoints.md, 10),
    [breakPointValue],
  )
  const isMobile = useMemo(
    () => breakPointValue < parseInt(breakpoints.md, 10),
    [breakPointValue],
  )

  const lgAndHigher = useMemo(
    () => breakPointValue >= parseInt(breakpoints.lg, 10),
    [currentBreakpoint],
  )

  const mdAndHigher = useMemo(
    () => breakPointValue >= parseInt(breakpoints.md, 10),
    [currentBreakpoint],
  )

  return {
    currentBreakpoint,
    isDesktop,
    isMobile,
    lgAndHigher,
    mdAndHigher,
  }
}
