import { MutableRefObject, useEffect, useRef } from 'react'

export function useMounting(): {
  mounted: MutableRefObject<boolean>
} {
  const mounted = useRef(false)

  useEffect(() => {
    mounted.current = true

    return () => {
      mounted.current = false
    }
  }, [])

  return {
    mounted,
  }
}
