import { useEffect } from 'react'

type TProps = {
  ref: React.MutableRefObject<HTMLElement | null>
  onClick: (e: HTMLElement | null) => void
}

type TUseClickOutside = (props: TProps) => void

export const useClickOutside: TUseClickOutside = ({ ref, onClick }) => {
  useEffect(() => {
    const listener = (e: MouseEvent) => {
      if (!ref.current) return
      const target = e.target as HTMLElement
      if (!ref.current.contains(target)) {
        onClick(target)
      }
    }

    document.addEventListener('mouseup', listener)

    return () => {
      document.removeEventListener('mouseup', listener)
    }
  }, [ref])
}
