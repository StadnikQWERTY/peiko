import { ErrorStatus } from '@/types/error'
import axios from 'axios'
import { notification } from '@/utils/notification'
import { FormikHelpers } from 'formik'
import { useState } from 'react'
import useTranslation from 'next-translate/useTranslation'

interface ISendForm<T> {
  notificationSuccessText?: string
  formik: FormikHelpers<T>
  formData: T
  formSendFunction: (params: T) => void
}

interface IUseSendForm {
  isSendForm: boolean
  sendForm: <T>(params: ISendForm<T>) => void
}

export function useSendForm(): IUseSendForm {
  const [isSendForm, setSendForm] = useState<boolean>(false)
  const { t: tCommon } = useTranslation('common')

  async function sendForm<T>(params: ISendForm<T>): Promise<void> {
    const { notificationSuccessText, formSendFunction, formData, formik } = params
    setSendForm(true)
    try {
      await formSendFunction(formData)
      setSendForm(false)
      formik.resetForm()
      notification({
        title: notificationSuccessText || tCommon('sendFormSuccess'),
        icon: 'success',
        position: 'top',
      })
    } catch (e) {
      setSendForm(false)
      if (!axios.isAxiosError(e) || !e.response) return
      const { status, data } = e.response

      if (status === ErrorStatus.Validation) {
        formik.setErrors(data.errors)
      }
    }
  }

  return {
    isSendForm,
    sendForm,
  }
}
