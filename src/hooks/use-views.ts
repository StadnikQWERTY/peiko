import { useState, useEffect } from 'react'
import { IBlogArticle } from '@/requests/blog/types'
import { blogApi } from '@/api/requests/blog'
import { useRouter } from 'next/router'
import { ParsedUrlQuery } from 'querystring'

type TViews = { [key: number]: number }

type TRuturn = {
  views: TViews
  fetchingViews: boolean
}

type Tprops = {
  articles?: IBlogArticle[]
  ids?: number[]
  articleSlug?: ParsedUrlQuery['URL']
}

type TUseViews = (props: Tprops) => TRuturn

export const useViews: TUseViews = ({ articles, ids, articleSlug }) => {
  const router = useRouter()
  const [views, setViews] = useState<TViews>({})
  const [fetchingViews, setFetchingViews] = useState(true)

  const getViews = async () => {
    try {
      let result: number[] = []

      if (ids) result = [...ids]

      if (articles) {
        result = articles.reduce<number[]>((prev, current) => [...prev, current.id], [])
      }

      const { data } = await blogApi.getViews(result)
      setViews(data.data)
      setFetchingViews(false)
    } catch (e) {
      setFetchingViews(false)
    }
  }

  const getArticleView = async (slug: Tprops['articleSlug']) => {
    try {
      if (typeof slug !== 'string') return

      const { data } = await blogApi.getArticle({
        locale: router.locale as string,
        uri: slug,
      })
      setViews([data.data.views])
      setFetchingViews(false)
    } catch (e) {
      setFetchingViews(false)
    }
  }

  useEffect(() => {
    if (articleSlug) {
      getArticleView(articleSlug)
    }

    if (!articleSlug) {
      getViews()
    }

    return () => {
      setFetchingViews(true)
    }
  }, [router])

  return { views, fetchingViews }
}
