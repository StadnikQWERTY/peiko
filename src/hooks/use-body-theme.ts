import { useIsomorphicLayoutEffect } from '@/hooks/use-isomorphic-layout-effect'
import { TTheme } from '@/styling/theme'

type TUseBodyTheme = (theme: TTheme) => void

export const useBodyTheme: TUseBodyTheme = (theme) => {
  useIsomorphicLayoutEffect(() => {
    document.body.setAttribute('data-theme', theme)
  }, [])

  return null
}
