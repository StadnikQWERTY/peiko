import * as yup from 'yup'

const required = yup.string().required('validation:required')

const date = yup.string().required('validation:required')

const email = yup.string().email('validation:invalid-email')

const phone = yup.string().required('validation:required')

export const validation = {
  required,
  date,
  email,
  phone,
}
