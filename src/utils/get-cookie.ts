type TGetCookie = (name: string) => string | null

export const getCookie: TGetCookie = (name) => {
  let nameEQ = `${name}=`
  let ca = document.cookie.split(';')
  for (let i = 0; i < ca.length; i += 1) {
    let c = ca[i]
    while (c.charAt(0) === ' ') c = c.substring(1, c.length)
    if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length, c.length)
  }
  return null
}
