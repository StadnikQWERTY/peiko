interface IRuntimeForm {
  [key: string]: any
}

export class RuntimeForm<T extends IRuntimeForm> {
  private readonly form: T

  constructor(form: T) {
    this.form = form
  }

  public getFormData(): FormData {
    const form = new FormData()

    for (const key in this.form) {
      if (this.form[key] !== undefined) {
        const field = this.form[key]

        if (Array.isArray(field)) {
          field.forEach((item: string | Blob) => {
            form.append(`${key}[]`, item)
          })
        } else {
          form.append(key, field)
        }
      }
    }

    return form
  }
}
