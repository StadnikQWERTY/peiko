interface ILoadParams {
  callback: () => void
}

interface ILibraryData {
  src: string
  async?: boolean
  defer?: boolean
}

export class ScriptLibraryLoader {
  private readonly tagId: string

  private readonly libraryData: ILibraryData

  constructor(tagId: string, libraryData: ILibraryData) {
    this.tagId = tagId
    this.libraryData = libraryData
  }

  loadLibrary(loadProps: ILoadParams): void {
    const existingScript = document.getElementById(this.tagId)
    const { callback } = loadProps

    if (!existingScript) {
      const script = document.createElement('script')
      script.src = this.libraryData.src
      script.id = this.tagId

      script.async = this.libraryData.async || false
      script.defer = this.libraryData.defer || false

      document.body.appendChild(script)
      script.onload = () => {
        if (callback) callback()
      }
    }
    if (existingScript && callback) callback()
  }
}
