import i18nConfig from '../../i18n.js'

type TGetRouteLocale = (locale?: string) => string

export const getRouteLocale: TGetRouteLocale = (locale) => {
  if (!locale) return ''
  return locale === i18nConfig.defaultLocale ? '' : `/${locale}`
}
