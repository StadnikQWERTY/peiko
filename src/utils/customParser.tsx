import ReactHtmlParser, { Transform } from 'react-html-parser'
import styled from 'styled-components'
import { CodeBlock } from 'react-code-blocks'

type HtmlParser = (string: string) => React.ReactNode

const StyledCode = styled.div`
  display: grid;
`

export const customParser: HtmlParser = (string: string) => {
  let code: string[] = []

  const transform: Transform = (node) => {
    if (node.name === 'code') {
      return (
        <StyledCode>
          <CodeBlock text={code.shift()} showLineNumbers={false} />
        </StyledCode>
      )
    }
  }

  // render code
  function getCode(str: string) {
    let start = -1
    let end = -1

    start = str.indexOf('<code>')
    end = str.indexOf('</code>')

    if (start === -1 || end === -1) return

    let codeString = str.slice(start + 6, end - 1)
    const firstLineIndex = codeString.indexOf('\n')

    if (firstLineIndex === 1) {
      codeString = codeString.slice(2)
    }

    code.push(codeString)

    getCode(str.slice(end + 7))
  }

  getCode(string)

  return ReactHtmlParser(string, { transform })
}
