import i18nConfig from '../../i18n'

export const getSsrLocale = (locale?: string): string =>
  locale || i18nConfig.defaultLocale
