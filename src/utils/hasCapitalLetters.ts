export const hasCapitalLetters = (string: string): boolean => /[A-Z]/.test(string)
