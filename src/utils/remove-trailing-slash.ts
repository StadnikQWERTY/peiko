type GetRouteLocale = (url: string) => string

export const removeTrailingSlash: GetRouteLocale = (url) => url.replace(/\/$/, '')
