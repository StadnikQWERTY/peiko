const padNumber = (number: number): string =>
  number < 10 ? `0${number.toString()}` : number.toString()

export default padNumber
