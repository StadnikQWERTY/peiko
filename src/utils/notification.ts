import Swal, { SweetAlertOptions } from 'sweetalert2'

export const notification = (props: SweetAlertOptions): void => {
  Swal.fire({
    toast: true,
    icon: 'error',
    position: 'bottom-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    ...props,
  })
}
