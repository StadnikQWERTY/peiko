export const PEIKO_CONTACTS = {
  PHONE: '+1 (650) 278-4458',
  PHONE_SECOND: '+44 (20) 3769-0654',
  EMAIL: 'hello@peiko.space',
}

export const PEIKO_LINK_CONTACTS = {
  PHONE: `tel: ${PEIKO_CONTACTS.PHONE}`,
  PHONE_SECOND: `tel: ${PEIKO_CONTACTS.PHONE_SECOND}`,
  EMAIL: `mailto: ${PEIKO_CONTACTS.EMAIL}`,
}

export const ABOUT_PEIKO = {
  DOU: 'https://jobs.dou.ua/companies/peiko/',
  CLUTCH: 'https://clutch.co/profile/peiko#summary',
  DESIGN_RUSH: 'https://www.designrush.com/agency/profile/peiko',
  GOOD_FIRMS: 'https://www.goodfirms.co/company/peiko',
  UPWORK: 'https://www.upwork.com/ag/peiko/',

  INSTAGRAM: 'https://www.instagram.com/peiko_space/',
  FACEBOOK: 'https://www.facebook.com/Peiko.space',
  LINKED_IN: 'https://ua.linkedin.com/company/peiko',
  BEHANCE: 'https://www.behance.net/Peiko',
  DRIBBLE: 'https://dribbble.com/peiko',
}

export const API_REST_TIMEOUT = 20000
export const BASE_API_URL = `${process.env.NEXT_PUBLIC_API_REST_URL}/api`
export const BASE_APP_URL = process.env.NEXT_PUBLIC_APP_URL
export const GOOGLE_MAPS_API_KEY = process.env.NEXT_PUBLIC_GOOGLE_MAPS_API_KEY
export const GOOGLE_ANALYTICS_ID = process.env.NEXT_PUBLIC_GOOGLE_ANALYTICS_ID
export const GOOGLE_TAG_MANAGER = process.env.NEXT_PUBLIC_GOOGLE_TAG_MANAGER === 'true'
export const NO_INDEX = process.env.NEXT_PUBLIC_NO_INDEX === 'true'
export const SMARTLOOK_ID = process.env.NEXT_PUBLIC_SMARTLOOK_ID
