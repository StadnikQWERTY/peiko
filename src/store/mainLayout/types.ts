import { TTheme } from '@/styling/theme'

export interface IInitialState {
  theme: TTheme
  currentThemedSectionId: string
  mobileMenuVisible: boolean
}
