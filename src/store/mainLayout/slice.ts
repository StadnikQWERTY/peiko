import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { IInitialState } from '@/store/mainLayout/types'
import { TTheme } from '@/styling/theme'

const initialState: IInitialState = {
  theme: 'dark',
  currentThemedSectionId: '',
  mobileMenuVisible: false,
}

export const mainLayoutSlice = createSlice({
  name: 'mainLayout',
  initialState,
  reducers: {
    setNewTheme(state, action: PayloadAction<TTheme>) {
      state.theme = action.payload
    },
    setCurrentThemedSectionId(state, action: PayloadAction<string>) {
      state.currentThemedSectionId = action.payload
    },
    setMobileMenuVisibility(state, action: PayloadAction<boolean>) {
      state.mobileMenuVisible = action.payload
    },
  },
})

export const mainLayoutActions = mainLayoutSlice.actions
