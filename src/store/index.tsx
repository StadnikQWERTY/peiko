import { configureStore, ThunkAction, AnyAction } from '@reduxjs/toolkit'
import { mainLayoutSlice } from '@/store/mainLayout/slice'
import { contactUsSlice } from '@/features/contact-us/store/contact-us'
import desktopHeaderMenu from '@/layout/store/desktop-header-menu'

const store = configureStore({
  reducer: {
    mainLayout: mainLayoutSlice.reducer,
    contactUs: contactUsSlice.reducer,
    desktopHeaderMenu,
  },
})

export default store

export type TStore = typeof store
export type TRootState = ReturnType<typeof store.getState>
export type TDispatch = typeof store.dispatch
export type TAsyncAction = ThunkAction<void, TRootState, unknown, AnyAction>
export type TSelector<P> = (s: TRootState) => P
