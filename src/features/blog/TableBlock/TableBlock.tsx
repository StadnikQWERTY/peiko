import React, { FC } from 'react'
import { Text } from '@/components/Text'

import styled from 'styled-components'
import { IArticlePageBlock } from '@/requests/blog/types'
import { customParser } from '@/utils/customParser'

const Table = styled.div`
  table {
    width: 100%;

    border-spacing: 20px 8px;

    th {
      text-align: start;
      padding: 8px;
      background: #e4e8f4;
      font-style: normal;
      font-weight: 600;
      font-size: 20px;
      line-height: 150%;
      letter-spacing: 0.03em;
      color: #14161d;

      &:first-of-type {
        background: #41b6e6;
        color: #ffffff;
      }
    }

    td {
      padding: 8px;
      background: #ffffff;
      font-style: italic;
      font-weight: 500;
      font-size: 16px;
      line-height: 190%;
      color: #14161d;

      &:first-of-type {
        background: #41b6e6;
        color: #ffffff;
      }
    }
  }
`

const Scroll = styled.div`
  overflow: scroll;
`

interface OwnProps {
  block: IArticlePageBlock
}

export const TableBlock: FC<OwnProps> = (props) => {
  const { block } = props
  return (
    <Scroll>
      {block?.header?.content && (
        <Text variant="h3ForBlog" tag="h2">
          {block?.header?.content}
        </Text>
      )}
      <Text variant="paragraphForBlog" tag="p">
        {customParser(block.description || '')}
      </Text>
      <Table>{customParser(block.table_html || '')}</Table>
    </Scroll>
  )
}
