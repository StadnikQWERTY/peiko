import React, { FC } from 'react'
import { IArticlePageBlock } from '@/requests/blog/types'
import styled from 'styled-components'

interface OwnProps {
  block: IArticlePageBlock
}

const Quote = styled.div`
  display: flex;
  flex-direction: column;
  gap: 24px;
  padding: 16px;
  background: #ffffff;
  border-left: 3px solid #41b6e6;
`

const QuoteText = styled.div`
  font-style: italic;
  font-weight: 500;
  font-size: 16px;
  line-height: 190%;
  color: #14161d;
`

const QuoteAuthor = styled.div`
  font-style: normal;
  font-weight: 700;
  font-size: 16px;
  line-height: 190%;
  text-align: right;
  color: #14161d;
`

export const QuoteBlock: FC<OwnProps> = (props) => {
  const { block } = props
  return (
    <>
      {(block?.content || block?.author) && (
        <Quote>
          {block?.content && <QuoteText>{block?.content}</QuoteText>}
          {block?.author && <QuoteAuthor>{block?.author}</QuoteAuthor>}
        </Quote>
      )}
    </>
  )
}
