import React, { FC } from 'react'
import { Text } from '@/components/Text'

import { IArticlePageBlock } from '@/requests/blog/types'
import styled from 'styled-components'
import { customParser } from '@/utils/customParser'

interface OwnProps {
  block: IArticlePageBlock
}

const StyledDescription = styled(Text)`
  em {
    display: inline-flex;
    font-style: normal !important;
    font-weight: 500 !important;
    font-size: 16px !important;
    padding: 0 4px !important;
  }

  .note {
    p {
      display: block;
      padding: 16px;
      background: #ffffff;
    }
  }

  .red {
    em {
      color: #e2702e;
      border: 2px solid #e2702e;
    }

    p {
      border: 2px solid #e2702e;
    }
  }

  .green {
    em {
      color: #60be79;
      border: 2px solid #60be79;
    }

    p {
      border: 2px solid #60be79;
    }
  }

  .yellow {
    em {
      color: #f6bd45;
      border: 2px solid #f6bd45;
    }

    p {
      border: 2px solid #f6bd45;
    }
  }

  h2 {
    margin: 0;
    padding: 0 0 8px 0;
    font-style: normal;
    font-weight: 600;
    font-size: 20px;
    line-height: 150%;
    letter-spacing: 0.03em;
    color: #000000;

    em {
      font-style: normal;
      font-weight: 600;
      font-size: 13px;
      line-height: 16px;
      text-transform: uppercase;
      background: #f4f6fa;
    }
  }

  p {
    margin: 0;
    padding: 0;
  }
`

const StyledText = styled(Text)`
  margin: 0;
  padding: 0;
`

export const Description: FC<OwnProps> = (props) => {
  const { block } = props

  return (
    <>
      {block?.header?.content && (
        <StyledText
          variant="h3ForBlog"
          tag={(block?.header?.tag as keyof JSX.IntrinsicElements | undefined) || 'h2'}
        >
          {block?.header?.content}
        </StyledText>
      )}
      {block?.description && (
        <StyledDescription variant="paragraphForBlog">
          {customParser(block?.description)}
        </StyledDescription>
      )}
    </>
  )
}
