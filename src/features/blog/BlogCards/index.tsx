import { IBlogArticle } from '@/requests/blog/types'
import styled from 'styled-components'
import { BaseImage } from '@/components/BaseImage'
import { Text } from '@/components/Text'
import { BaseLink } from '@/components/BaseLink'
import DateAndReadInfo from '@/components/DateAndReadInfo'
import { deviceCssQuery } from '@/styling/breakpoints'
import { useViews } from '@/hooks/use-views'

interface OwnProps {
  articles: IBlogArticle[]
}

const CardsContainer = styled.div`
  display: grid;

  @media screen and ${deviceCssQuery.xs} {
    grid-template-columns: 1fr;
    gap: 24px;
  }

  @media screen and ${deviceCssQuery.lg} {
    grid-template-columns: 1fr 1fr;
    gap: 20px;
  }
`

const Card = styled.div<{ background: string }>`
  padding: 32px;
  ${(props) => props.background};
  display: flex;
  flex-direction: column;
`

const ImageWrapper = styled.div`
  position: relative;
  margin: 10px auto 54px auto;
  display: flex;
  justify-content: center;
`

const StyledCategory = styled(Text)`
  color: #ffffff;
`

const StyledName = styled(Text)`
  margin: 16px 0 12px 0;
  color: #ffffff;

  @media screen and ${deviceCssQuery.lg} {
    overflow: hidden;
    display: -webkit-box;
    -webkit-line-clamp: 3;
    -webkit-box-orient: vertical;
    height: 80px;
  }
`

const BlogCards: React.FC<OwnProps> = (props) => {
  const { articles } = props
  const { views, fetchingViews } = useViews({ articles })

  return (
    <CardsContainer>
      {articles.map((article) => (
        <BaseLink key={article.id} href={`/blog/article/${article.uri}`}>
          <Card background={article.gradient}>
            <ImageWrapper>
              <BaseImage
                src={article.photo || '/'}
                alt={article.name}
                width={288}
                height={200}
              />
            </ImageWrapper>
            <StyledCategory variant="paragraph">{article.category}</StyledCategory>
            <StyledName variant="h3">{article.name}</StyledName>

            <DateAndReadInfo
              date={article.date}
              views={views[article.id] || article.views}
              color="white"
              fetching={fetchingViews}
            />
          </Card>
        </BaseLink>
      ))}
    </CardsContainer>
  )
}
export default BlogCards
