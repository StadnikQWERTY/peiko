import ImageGallery from 'react-image-gallery'
import React, { FC, MouseEventHandler } from 'react'
import { IArticlePageBlock } from '@/requests/blog/types'
import { Text } from '@/components/Text'
import useTranslation from 'next-translate/useTranslation'
import { useBreakpoints } from '@/hooks/useBreakpoints'

const LeftNav = React.memo(
  ({
    disabled,
    onClick,
  }: {
    disabled: boolean
    onClick: MouseEventHandler<HTMLButtonElement>
  }) => {
    const { t } = useTranslation('aria-label')

    return (
      <button
        style={{ margin: '0 40px', filter: 'none' }}
        type="button"
        className="image-gallery-icon image-gallery-left-nav"
        disabled={disabled}
        onClick={onClick}
        aria-label={t('previous-slide')}
      >
        <svg
          width="20"
          height="36"
          viewBox="0 0 20 36"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M18 34L2 18L18 2"
            stroke="#4A4E58"
            strokeWidth="1.5"
            strokeLinecap="square"
          />
        </svg>
      </button>
    )
  },
)

LeftNav.displayName = 'LeftNav'

const RightNav = React.memo(
  ({
    disabled,
    onClick,
  }: {
    disabled: boolean
    onClick: MouseEventHandler<HTMLButtonElement>
  }) => {
    const { t } = useTranslation('aria-label')
    return (
      <button
        style={{ margin: '0 40px', filter: 'none' }}
        type="button"
        className="image-gallery-icon image-gallery-right-nav"
        disabled={disabled}
        onClick={onClick}
        aria-label={t('next-slide')}
      >
        <svg
          width="20"
          height="36"
          viewBox="0 0 20 36"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M2 2L18 18L2 34"
            stroke="#4A4E58"
            strokeWidth="1.5"
            strokeLinecap="square"
          />
        </svg>
      </button>
    )
  },
)

RightNav.displayName = 'RightNav'

interface OwnProps {
  block: IArticlePageBlock
}

export const SliderBlock: FC<OwnProps> = (props) => {
  const { block } = props
  const { isMobile } = useBreakpoints()

  const formatImgData =
    block?.slides?.map((item) => ({
      original: item.url,
      originalHeight: isMobile ? 200 : 440,
      thumbnailHeight: isMobile ? 200 : 440,
      originalAlt: item.alt,
    })) || []
  return (
    <>
      <Text
        variant="h3ForBlog"
        tag={(block.header?.tag as keyof JSX.IntrinsicElements | undefined) || 'h2'}
      >
        {block?.header?.content}
      </Text>
      <ImageGallery
        items={formatImgData}
        showPlayButton={false}
        showFullscreenButton={false}
        renderLeftNav={(onClick, disabled) => (
          <LeftNav onClick={onClick} disabled={disabled} />
        )}
        renderRightNav={(onClick, disabled) => (
          <RightNav onClick={onClick} disabled={disabled} />
        )}
      />
    </>
  )
}
