import React, { FC } from 'react'
import { Text } from '@/components/Text'
import styled from 'styled-components'
import { IArticlePageBlock } from '@/requests/blog/types'
import { CheckIcon } from '@/components/icons/CheckIcon'

const TableComparison = styled.div<{ colSize: number }>`
  display: grid;
  grid-template-columns: ${({ colSize }) => `repeat(${colSize}, 1fr)`};
  gap: 8px 20px;
  margin: 0 20px;
`

const TableComparisonItem = styled.div<{ colSize: number }>`
  padding: 8px;
  background-color: white;
  &:nth-child(${({ colSize }) => colSize}n + 1) {
    background: #41b6e6;
    font-style: italic;
    font-weight: 500;
    font-size: 16px;
    line-height: 190%;
    color: #ffffff;
  }

  &:nth-child(-n + ${({ colSize }) => colSize}) {
    font-style: normal;
    font-weight: 600;
    font-size: 20px;
    line-height: 150%;
    letter-spacing: 0.03em;
    color: #ffffff;
  }

  &:not(:nth-child(${({ colSize }) => colSize}n + 1)) {
    color: black;
  }
`

const Scroll = styled.div`
  overflow: scroll;
`

interface OwnProps {
  block: IArticlePageBlock
}

export const TableComparisonBlock: FC<OwnProps> = (props) => {
  const { block } = props
  const colSize = block.table_rows?.[0].length || 1
  return (
    <Scroll>
      {block.header?.content && (
        <>
          <Text
            variant="h3ForBlog"
            tag={block.header?.tag as keyof JSX.IntrinsicElements | undefined}
          >
            {block.header?.content}
          </Text>
          <br />
        </>
      )}
      <TableComparison colSize={colSize}>
        {block.table_rows?.map((row, rowIndex) =>
          row.map((item, index) => {
            const isCheck = index > 0 && rowIndex > 0
            return (
              <TableComparisonItem key={item.content} colSize={colSize}>
                {isCheck ? <CheckIcon isCheck={item.content} /> : item.content}
              </TableComparisonItem>
            )
          }),
        )}
      </TableComparison>
    </Scroll>
  )
}
