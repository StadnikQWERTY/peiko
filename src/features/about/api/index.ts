import { TApiResponse, TLang } from '@/api/requests/types'
import { index } from '@/api/instances/main'
import { IAboutPageData } from './types'

const getAbout = (locale: TLang): TApiResponse<IAboutPageData> =>
  index.get('/about', {
    headers: {
      Language: locale,
    },
  })

export const aboutApi = {
  getAbout,
}
