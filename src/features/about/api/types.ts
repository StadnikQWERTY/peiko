import { IContent, IMicroData, IPartner, IReview, IVacancy } from '@/requests/types'

export interface IAboutPageResponse {
  data: IAboutPageData
}

export interface IAboutPageData {
  content: IContent
  gallery: IGallery[]
  awards: IAward[]
  partners: IPartner[]
  reviews: IReview[]
  vacancies: IVacancy[]
  tasks: ITask[]
  video: IVideo
  microdata: IMicroData
  noindex: boolean
}

export interface IGallery {
  title: string
  alt: string
  url: string
}

export interface IAward {
  id: number
  title: string
  photo: string
  url: string
  description: string
}

export interface ITask {
  id: number
  name: string
  description: string | null
}

export interface IVideo {
  link: string
  title: string
  header: string
  description: string
}
