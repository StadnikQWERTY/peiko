import { deviceCssQuery } from '@/styling/breakpoints'
import styled from 'styled-components'
import { Text } from '@/components/Text'

export const VideoContainer = styled.div`
  display: grid;

  @media screen and ${deviceCssQuery.xs} {
    grid-template-columns: 1fr;
    gap: 50px;
  }
  iframe {
    width: 100%;
    height: 250px;
    order: 1;
  }

  @media screen and ${deviceCssQuery.md} {
    grid-template-columns: 2fr 1fr;
    gap: 90px;

    iframe {
      width: 100%;
      min-height: 400px;
      height: auto;
      order: 0;
    }
  }
`

export const TextContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  @media screen and ${deviceCssQuery.xs} {
    order: 0;
    & > * {
      width: 100%;
    }
  }

  @media screen and ${deviceCssQuery.md} {
    order: 1;
    & > * {
      width: 100%;
      text-align: right;
    }
  }
`

export const StyledTitle = styled(Text)`
  color: ${(props) => props.theme.palette.secondaryText};
  text-transform: uppercase;
  letter-spacing: 5px;
`

export const StyledDescription = styled(Text)`
  margin-top: 10px;
  line-height: 27px;
`
