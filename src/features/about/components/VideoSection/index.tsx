import { SectionContainer } from '@/components/sections/SectionContainer'
import { IVideo } from '@/features/about/api/types'
import { Text } from '@/components/Text'
import React from 'react'
import * as Styled from './styled'

type TVideoSectionProps = {
  data: IVideo
}

export const VideoSection: React.FC<TVideoSectionProps> = ({ data }) => (
  <SectionContainer space={90}>
    <Styled.VideoContainer>
      <iframe
        id="ytplayer"
        title="ytplayer"
        src={data.link}
        frameBorder="0"
        width="100%"
        height="auto"
      />
      <Styled.TextContainer>
        <Styled.StyledTitle variant="h4">{data.title}</Styled.StyledTitle>
        <Text variant="h2" tag="h2" margin="0">
          {data.header}
        </Text>
        <Styled.StyledDescription variant="paragraph">
          {data.description}
        </Styled.StyledDescription>
      </Styled.TextContainer>
    </Styled.VideoContainer>
  </SectionContainer>
)
