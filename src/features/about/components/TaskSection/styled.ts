import { deviceCssQuery } from '@/styling/breakpoints'
import styled from 'styled-components'
import { Text } from '@/components/Text'

export const TaskContainer = styled.div`
  display: grid;
  gap: 60px;
  grid-template-columns: 1fr;

  strong {
    color: ${(p) => p.theme.palette.primary};
  }

  @media screen and ${deviceCssQuery.md} {
    gap: 200px;
    grid-template-columns: 1fr 1fr;
  }
`

export const Task = styled.div`
  @media screen and ${deviceCssQuery.md} {
    &:nth-of-type(2) {
      margin-top: 120px;
    }
  }
`

export const StyledTitle = styled(Text)`
  color: ${(props) => props.theme.palette.secondaryText};
  text-transform: uppercase;
  letter-spacing: 5px;
  margin-bottom: 8px;
`
