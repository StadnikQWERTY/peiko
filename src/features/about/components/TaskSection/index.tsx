import { SectionContainer } from '@/components/sections/SectionContainer'
import { Text } from '@/components/Text'
import { ITask } from '@/features/about/api/types'
import React from 'react'

import { customParser } from '@/utils/customParser'
import * as Styled from './styled'

type TTaskSectionProps = {
  data: ITask[]
}

export const TaskSection: React.FC<TTaskSectionProps> = ({ data }) => (
  <SectionContainer space={90}>
    <Styled.TaskContainer>
      {data.map((x) => (
        <Styled.Task key={x.id}>
          <Styled.StyledTitle variant="h4" tag="h2">
            {x.name}
          </Styled.StyledTitle>
          <Text variant="h2">{customParser(x.description || '')}</Text>
        </Styled.Task>
      ))}
    </Styled.TaskContainer>
  </SectionContainer>
)
