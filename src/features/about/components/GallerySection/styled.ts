import styled from 'styled-components'
import { deviceCssQuery } from '@/styling/breakpoints'
import { Text } from '@/components/Text'

export const Content = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`

export const Title = styled(Text)`
  opacity: 0;
`

export const StageContainer = styled.div`
  position: relative;
  width: 100%;

  @media screen and ${deviceCssQuery.xs} {
    min-height: 500px;
  }

  @media screen and ${deviceCssQuery.md} {
    min-height: 650px;
  }
`

export const Stage = styled.div`
  width: 100%;
  height: 100%;
  transform-style: preserve-3d;
  user-select: none;
  overflow: hidden;
  position: absolute;
`
export const Container = styled.div`
  perspective: 2000px;
  width: 400px;
  height: 500px;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  position: absolute;
`

export const Ring = styled.div`
  width: 100%;
  height: 100%;
  transform-style: preserve-3d;
  user-select: none;
  position: absolute;
`

export const Img = styled.div<{ img: string }>`
  width: 100%;
  height: 100%;
  transform-style: preserve-3d;
  user-select: none;
  background-image: url(${(p) => p.img});
  position: absolute;
  background-size: 750px 500px;
  background-repeat: no-repeat;
`

export const SubTitle = styled(Text)`
  font-size: 13px;
  text-transform: uppercase;
  width: 200px;
  text-align: center;
`
