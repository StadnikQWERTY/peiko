import React, { useEffect, useRef } from 'react'
import useTranslation from 'next-translate/useTranslation'
import { gsap } from 'gsap'
import { Text } from '@/components/Text'
import { Breadcrumbs } from '@/components/Breadcrumbs'
import { ROUTES } from '@/constants/routes'
import { IGallery } from '@/features/about/api/types'
import { IContent } from '@/requests/types'
import { Spacer } from '@/components/Spacer/Spacer'
import * as Styled from './styled'

type TGallerySectionProps = {
  data: IGallery[]
  content: IContent
}

function getBgPos(i: number, amount: number) {
  return `${
    100 -
    (gsap.utils.wrap(
      0,
      360,
      (gsap.getProperty('[data-ring]', 'rotationY') as number) - 180 - i * (360 / amount),
    ) /
      360) *
      500
  }px 0px`
}

export const GallerySection: React.FC<TGallerySectionProps> = ({ data, content }) => {
  const { t } = useTranslation('common')
  const isEventActive = useRef(false)
  const xPos = useRef(0)

  const drag = (
    e: React.MouseEvent<HTMLDivElement> | React.TouchEvent<HTMLDivElement>,
  ) => {
    if (!isEventActive.current) {
      return
    }
    let currentPos = 0

    if ('touches' in e) {
      currentPos = Math.round(e.touches[0].clientX)
    } else {
      currentPos = e.clientX
    }

    gsap.to('[data-ring]', {
      rotationY: `-=${(currentPos - xPos.current) % 360}`,
      onUpdate: () => {
        gsap.set('[data-img]', { backgroundPosition: (i) => getBgPos(i, data.length) })
      },
    })

    xPos.current = currentPos
  }

  const dragStart = (
    e: React.MouseEvent<HTMLDivElement> | React.TouchEvent<HTMLDivElement>,
  ) => {
    isEventActive.current = true

    let currentPos = 0

    if ('touches' in e) {
      currentPos = Math.round(e.touches[0].clientX)
    } else {
      currentPos = e.clientX
    }

    gsap.set('[data-ring]', { cursor: 'grabbing' })
    xPos.current = currentPos
  }

  const dragEnd = () => {
    isEventActive.current = false
    gsap.set('[data-ring]', { cursor: 'grab' })
  }

  useEffect(() => {
    gsap
      .timeline()
      .set('[data-ring]', { rotationY: 180, cursor: 'grab' }) // set initial rotationY so the parallax jump happens off screen
      .set('[data-img]', {
        // apply transform rotations to each image
        rotateY: (i) => i * ((360 / data.length) * -1),
        transformOrigin: '50% 50% 600px',
        z: -600,
        backgroundPosition: (i) => getBgPos(i, data.length),
        backfaceVisibility: 'hidden',
      })
      .from('[data-img]', {
        duration: 1.5,
        y: 200,
        opacity: 0,
        stagger: 0.1,
        ease: 'expo',
      })
  }, [data.length])

  return (
    <Spacer lg={{ padding: '66px 0 105px 0' }} xs={{ padding: '88px 0 64px 0' }}>
      <Styled.Content>
        <Breadcrumbs
          breadcrumbs={[
            {
              name: t('home'),
              href: ROUTES.HOME,
            },
            {
              name: t('aboutUs'),
              href: ROUTES.ABOUT_US,
            },
          ]}
        />
        <Text variant="h1" tag="h1" align="center">
          {content.header}
        </Text>
        <Styled.StageContainer>
          <Styled.Stage
            className="stage"
            onMouseDown={dragStart}
            onTouchStart={dragStart}
            onMouseUp={dragEnd}
            onTouchEnd={dragEnd}
            onMouseMove={drag}
            onTouchMove={drag}
          >
            <Styled.Container>
              <Styled.Ring data-ring>
                {data.map((x) => (
                  <Styled.Img key={x.alt} img={x.url} data-img />
                ))}
              </Styled.Ring>
            </Styled.Container>
          </Styled.Stage>
        </Styled.StageContainer>
      </Styled.Content>
    </Spacer>
  )
}
