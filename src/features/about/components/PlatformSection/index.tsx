import React from 'react'
import useTranslation from 'next-translate/useTranslation'
import { SectionContainer } from '@/components/sections/SectionContainer'
import { SectionTitles } from '@/components/sections/SectionTitles'
import { IAward } from '@/features/about/api/types'
import { Spacer } from '@/components/Spacer/Spacer'
import { Platform } from '@/components/Platform'

type TProps = {
  data: IAward[]
}

export const PlatformSection: React.FC<TProps> = ({ data }) => {
  const { t } = useTranslation('about')

  return (
    <Spacer lg={{ margin: '105px 0 100px 0' }} xs={{ margin: '64px 0 60px 0' }}>
      <SectionContainer>
        <SectionTitles header={t('media.header')} name={t('media.name')} tagHeader="h2" />
        <Platform data={data} />
      </SectionContainer>
    </Spacer>
  )
}
