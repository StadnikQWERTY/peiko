import styled from 'styled-components'
import { ExternalLink } from '@/components/ExternalLink'
import { deviceCssQuery } from '@/styling/breakpoints'

export const AwardsContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  flex-wrap: wrap;
  gap: 50px;

  @media screen and ${deviceCssQuery.xs} {
    gap: 20px;

    a {
      transform: scale(0.75);
    }
  }

  @media screen and ${deviceCssQuery.sm} {
    gap: 50px;

    a {
      transform: scale(1);
    }
  }
`

export const AwardContainer = styled(ExternalLink)`
  @media screen and ${deviceCssQuery.xs} {
    flex-basis: 153px;
    min-height: 45px;
  }

  @media screen and ${deviceCssQuery.md} {
    flex-basis: 200px;
    min-height: 60px;
  }
`
