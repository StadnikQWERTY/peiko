import useTranslation from 'next-translate/useTranslation'
import React from 'react'
import { Breadcrumbs } from '@/components/Breadcrumbs'
import { ROUTES } from '@/constants/routes'
import * as Styled from './styled'

type TProps = {
  title: string
}

export const HeaderSection: React.FC<TProps> = ({ title }) => {
  const { t } = useTranslation('common')

  return (
    <Styled.Content>
      <Breadcrumbs
        breadcrumbs={[
          {
            name: t('home'),
            href: ROUTES.HOME,
          },
          {
            name: t('cases'),
            href: ROUTES.CASES,
          },
        ]}
        justifyContent="flex-start"
      />
      <Styled.HiddenHeadader variant="h1" tag="h1">
        {title}
      </Styled.HiddenHeadader>
      <Styled.Header variant="h1" tag="h2">
        {t('portfolio')}
      </Styled.Header>
    </Styled.Content>
  )
}
