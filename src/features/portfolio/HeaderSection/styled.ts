import styled from 'styled-components'
import { SectionContainer } from '@/components/sections/SectionContainer'
import { Text } from '@/components/Text'

export const Content = styled(SectionContainer)`
  margin-top: 64px;
`

export const Header = styled(Text)`
  margin: 16px 0 0 0;
`

export const HiddenHeadader = styled(Text)`
  position: absolute;
  opacity: 0;
  z-index: -1;
`
