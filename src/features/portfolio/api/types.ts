import { ICase, IContent, ITag, IPager, IMicroData } from '../../../api/requests/types'

export interface IPortfolioPageResponse {
  data: IPortfolioPageData
}

export interface IPortfolioPageData {
  microdata: IMicroData
  cases: ICase[]
  tags: ITag[]
  content: IContent
  pager: IPager
  noindex: boolean
}
