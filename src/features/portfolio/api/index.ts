import { TApiResponse, TLang } from '@/api/requests/types'
import { IPortfolioPageData } from './types'
import { index } from '../../../api/instances/main'

const getCase = (
  locale: TLang,
  name: string,
  page?: string,
): TApiResponse<IPortfolioPageData> =>
  index.get(`/cases?tag=${name}${page ? `&page=${page}` : ''}`, {
    headers: {
      Language: locale,
    },
  })

const getCases = (locale: TLang, page?: string): TApiResponse<IPortfolioPageData> =>
  index.get(`/cases${page ? `?page=${page}` : ''}`, {
    headers: {
      Language: locale,
    },
  })

export const portfolioApi = {
  getCase,
  getCases,
}
