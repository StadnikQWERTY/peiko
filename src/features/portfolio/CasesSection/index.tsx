import React from 'react'
import { useRouter } from 'next/router'
import useTranslation from 'next-translate/useTranslation'
import { ICase, IPager, ITag } from '@/requests/types'
import { Pagination } from '@/components/Pagination'
import { SectionContainer } from '@/components/sections/SectionContainer'
import { CaseBlock } from '@/components/sections/Cases/components/CaseBlock'
import { ROUTES } from '@/constants/routes'
import Tags from '@/components/Tags'
import { Spacer } from '@/components/Spacer/Spacer'
import { useBreakpoints } from '@/hooks/useBreakpoints'
import * as Styled from './styled'

type TCasesSectionProps = {
  cases: ICase[]
  tags: ITag[]
  pager: IPager
}

export const CasesSection: React.FC<TCasesSectionProps> = ({ cases, tags, pager }) => {
  const router = useRouter()
  const { t } = useTranslation('common')
  const params = (router.query.props as string[]) ?? []
  const selectedCase = Number.isNaN(+params[0]) ? params[0] : undefined
  const { isMobile } = useBreakpoints()

  return (
    <SectionContainer>
      <Spacer xs={{ margin: '40px 0 0 0' }}>
        <Tags
          tags={tags}
          header={t('categories')}
          selectedTag={selectedCase}
          uri={ROUTES.CASES}
          toLowerCase
        />
      </Spacer>

      {cases.map((x) => (
        <Styled.CaseContainer key={x.id}>
          <CaseBlock portfolioCase={x} />
        </Styled.CaseContainer>
      ))}

      <Spacer xs={{ margin: '72px 0 0 0' }}>
        <Pagination
          currentPage={pager.page}
          getLinkTo={(page) => {
            if (selectedCase) {
              return `${ROUTES.CASES}/${selectedCase}${page}`
            }
            return `${ROUTES.CASES}${page}`
          }}
          lastPage={Math.ceil(pager.total / pager.limit)}
          justifyContent={isMobile ? 'center' : 'flex-end'}
        />
      </Spacer>
    </SectionContainer>
  )
}
