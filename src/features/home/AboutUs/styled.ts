import styled from 'styled-components'
import { deviceCssQuery } from '@/styling/breakpoints'
import { BaseLink } from '@/components/BaseLink'

export const NumberBlocks = styled.div`
  display: grid;
  justify-content: space-between;

  @media screen and ${deviceCssQuery.xs} {
    grid-template-columns: repeat(auto-fit, minmax(140px, 1fr));
    grid-gap: 32px 8px;
  }

  @media screen and ${deviceCssQuery.md} {
    grid-template-columns: repeat(auto-fit, minmax(230px, 1fr));
    grid-gap: 32px 20px;
  }

  @media screen and ${deviceCssQuery.lg} {
    grid-template-columns: repeat(auto-fit, minmax(265px, 1fr));
  }
`

export const SectionName = styled(BaseLink)`
  display: grid;
  grid-auto-flow: column;
  grid-gap: 9.33px;
  align-items: center;
  grid-auto-columns: max-content;
`

export const ContactUsContainer = styled.div`
  position: relative;
  text-align: center;
  padding-bottom: 15px;
  border-bottom: 1px solid #b1c1d1;

  @media screen and ${deviceCssQuery.md} {
    ::after {
      content: '';
      position: absolute;
      bottom: -1px;
      left: 50%;
      transform: translateX(-50%);
      width: 320px;
      height: 1px;
      background: #41b6e6;
    }
  }
`
