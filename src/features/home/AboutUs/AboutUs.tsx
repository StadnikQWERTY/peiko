import React from 'react'
import { SectionTitles } from '@/components/sections/SectionTitles'
import useTranslation from 'next-translate/useTranslation'
import { SectionContainer } from '@/components/sections/SectionContainer'
import { IAboutUs } from '@/features/home/api/types'
import { NumericBlock } from '@/features/home/AboutUs/components/NumericBlock'
import { ROUTES } from '@/routes'
import { ArrowIcon } from '@/components/icons/ArrowIcon'
import { SectionName } from '@/components/sections/SectionName'
import { Button } from '@/components/buttons/Button'
import { Spacer } from '@/components/Spacer/Spacer'
import { useSetContactUs } from '@/features/contact-us'
import * as Styled from './styled'

type TAboutUs = {
  data: IAboutUs[]
}

export const AboutUs: React.FC<TAboutUs> = (props) => {
  const { t } = useTranslation('home')
  const { data } = props
  const { setContactUsVisibility } = useSetContactUs()

  const sectionName = (
    <Styled.SectionName href={ROUTES.ABOUT_US}>
      <SectionName tag="span">{t('aboutUs')}</SectionName>
      <ArrowIcon />
    </Styled.SectionName>
  )

  return (
    <SectionContainer>
      <SectionTitles name={sectionName} header={t('whyUs')} tagHeader="h2" />
      <Spacer xs={{ margin: '0px 0px 60px 0px' }} md={{ margin: '0px 0px 120px 0px' }}>
        <Styled.NumberBlocks>
          {data.map((item) => (
            <NumericBlock key={item.name} data={item} />
          ))}
        </Styled.NumberBlocks>
      </Spacer>
      <Styled.ContactUsContainer>
        <Button
          onClick={() => {
            setContactUsVisibility(true)
          }}
        >
          Contact us
        </Button>
      </Styled.ContactUsContainer>
    </SectionContainer>
  )
}
