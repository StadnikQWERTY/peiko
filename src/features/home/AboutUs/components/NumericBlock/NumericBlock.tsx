import React, { useEffect, useRef } from 'react'
import { IAboutUs } from '@/features/home/api/types'
import styled from 'styled-components'
import { Text } from '@/components/Text'
import { deviceCssQuery } from '@/styling/breakpoints'
import { FontWeight } from '@/styling/fonts'
import { gsap } from 'gsap'

type TNumericBlock = {
  data: IAboutUs
}

const StyledNumber = styled(Text)`
  margin-bottom: 4px;
  font-weight: ${FontWeight.Medium};

  @media screen and ${deviceCssQuery.xs} {
    font-size: 48px;
    line-height: 58.51px;
  }

  @media screen and ${deviceCssQuery.md} {
    font-size: 56px;
    line-height: 68.26px;
  }
`

export const NumericBlock: React.FC<TNumericBlock> = (props) => {
  const { data } = props

  const numberDomRef = useRef<HTMLDivElement>(null)

  const countRef = useRef({
    value: 0,
  })

  useEffect(() => {
    gsap.to(countRef.current, 3.5, {
      roundProps: 'value',
      value: data.value,
      scrollTrigger: {
        trigger: numberDomRef.current,
        start: 'center bottom',
        toggleActions: 'play none none none',
      },
      onUpdate: () => {
        if (numberDomRef.current) {
          // innerHTML must be used in order to instantly update DOM
          numberDomRef.current.innerHTML = String(countRef.current.value)
        }
      },
    })
  }, [])

  return (
    <div>
      <StyledNumber align="center" ref={numberDomRef}>
        0
      </StyledNumber>
      <Text variant="paragraph" align="center">
        {data.name}
      </Text>
    </div>
  )
}
