import { TApiResponse, TLang } from '@/requests/types'
import { IMainPageData } from '@/features/home/api/types'
import { index } from '@/api/instances/main'

const getHome = (locale: TLang): TApiResponse<IMainPageData> =>
  index.get('/home', {
    headers: {
      Language: locale,
    },
  })

export const homeApi = {
  getHome,
}
