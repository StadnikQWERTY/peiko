import {
  IAward,
  IContent,
  IPartner,
  ITechnology,
  ICase,
  ITag,
  IMicroData,
  IReward,
} from '@/requests/types'

export interface IMainPageResponse {
  data: IMainPageData
}

export interface IMainPageData {
  microdata: IMicroData
  content: IContent
  awards: IAward[]
  services: IService[]
  technologies: ITechnology[]
  cases: ICase[]
  about_us: IAboutUs[]
  partners: IPartner[]
  rewards: IReward[]
}

export interface IService {
  id: number
  uri: string
  name: string
  preview: string
  photo: string
  tags: ITag[]
}

export interface IAboutUs {
  name: string
  value: number
}
