import styled from 'styled-components'
import { Text } from '@/components/Text'
import { HEADER_HEIGHT_DESKTOP } from '@/constants/menu'
import { deviceCssQuery } from '@/styling/breakpoints'
import { StarsBackground } from './components/StarsBackground'

const PADDING_TOP_XS = '130px'
const PADDING_BOTTOM_XS = '32px'

export const StarsBg = styled(StarsBackground)`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  height: 100%;
`

export const Container = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  box-sizing: border-box;
  padding: ${PADDING_TOP_XS} 0 ${PADDING_BOTTOM_XS};
  min-height: inherit;

  @media screen and ${deviceCssQuery.md} {
    padding: 70px 0 60px;
    height: calc(100vh - ${HEADER_HEIGHT_DESKTOP}px);
    overflow-y: hidden;
  }

  @media screen and ${deviceCssQuery.lg} {
    padding: 100px 0 60px;
  }
`

export const TopBlock = styled.section`
  display: flex;
  min-height: calc(100vh - (${PADDING_TOP_XS} + ${PADDING_BOTTOM_XS}));
  justify-content: space-between;

  & > div:first-child {
    display: flex;
    flex-direction: column;
    flex-basis: 100%;
  }
  & > div:last-child {
    width: 240px;
    flex-shrink: 0;
    display: none;
    margin-left: 64px;
  }

  @media screen and ${deviceCssQuery.md} {
    min-height: auto;
    & > div:first-child {
      flex-basis: 740px;
    }
    & > div:last-child {
      display: block;
    }
  }

  @media screen and ${deviceCssQuery.lg} {
    & > div:first-child {
      flex-basis: 900px;
    }
  }
`

export const TopText = styled(Text)`
  color: ${(props) => props.theme.palette.primary};
  text-align: center;
  @media screen and ${deviceCssQuery.md} {
    text-align: left;
  }
`

export const Header = styled(Text)`
  text-align: center;
  @media screen and ${deviceCssQuery.md} {
    text-align: left;
  }
`

export const SubtitleContainer = styled(Text)`
  max-width: 560px;
  text-align: center;
  margin: 0 auto;
  @media screen and ${deviceCssQuery.md} {
    text-align: left;
    margin: 0;
  }
`

export const Subtitle = styled(Text)`
  font-size: 16px;
  line-height: 27.2px;

  margin: 0;
  text-align: center;
  @media screen and ${deviceCssQuery.md} {
    text-align: left;
  }
`

export const ContactButtonWrapper = styled.div`
  margin-top: 16px;
  text-align: center;

  @media screen and ${deviceCssQuery.md} {
    margin-top: 40px;
    text-align: left;
  }
`
