// TODO: install npm package, format with typescript and fix errors later. Previous error - geometry.vertices is not defined
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck
import React, { useEffect, useRef } from 'react'
import { breakpoints } from '@/styling/breakpoints'
import { useBreakpoints } from '@/hooks/useBreakpoints'

const Stars: React.FC<{
  className?: string
}> = ({ className }) => {
  const rootRef = useRef<HTMLDivElement>(null)
  // const hasInit = useRef(false)

  useEffect(() => {
    let canvasElId

    let camera
    let renderer

    let windowHalfX
    let windowHalfY

    let mouseX
    let mouseY

    let aspectRatio
    let scene

    function onWindowResize() {
      // Everything should resize nicely if it needs to!
      let WIDTH = document.body.clientWidth
      let HEIGHT = window.innerHeight

      camera.aspect = aspectRatio
      camera.updateProjectionMatrix()
      renderer.setSize(WIDTH, HEIGHT)
    }

    function onMouseMove(e) {
      if (window.innerHeight > breakpoints.md) {
        mouseX = e.clientX - windowHalfX
        mouseY = e.clientY - windowHalfY
      }
    }

    function doFakeMovement() {
      mouseX = 10 - windowHalfX
      mouseY = 10 - windowHalfY
    }

    function execute() {
      /* 	'To actually be able to display anything with Three.js, we need three things:
        A scene, a camera, and a renderer so we can render the scene with the camera.'

             - https://threejs.org/docs/#Manual/Introduction/Creating_a_scene 		*/

      /* We need this stuff too */
      let HEIGHT
      let WIDTH
      let fieldOfView
      let nearPlane
      let farPlane
      let geometry
      let starStuff
      let materialOptions
      let stars

      init()
      animate()

      function init() {
        // container = document.createElement("div");
        // document.body.appendChild(container);
        // document.body.style.overflow = "hidden";

        HEIGHT = window.innerHeight
        WIDTH = document.body.clientWidth
        aspectRatio = WIDTH / HEIGHT
        fieldOfView = 75
        nearPlane = 1
        farPlane = 1000
        mouseX = 0
        mouseY = 0

        windowHalfX = WIDTH / 2
        windowHalfY = HEIGHT / 2

        /* 	fieldOfView — Camera frustum vertical field of view.
          aspectRatio — Camera frustum aspect ratio.
          nearPlane — Camera frustum near plane.
          farPlane — Camera frustum far plane.

          - https://threejs.org/docs/#Reference/Cameras/PerspectiveCamera

           In geometry, a frustum (plural: frusta or frustums)
           is the portion of a solid (normally a cone or pyramid)
           that lies between two parallel planes cutting it. - wikipedia.		*/

        camera = new THREE.PerspectiveCamera(
          fieldOfView,
          aspectRatio,
          nearPlane,
          farPlane,
        )

        // Z positioning of camera

        camera.position.z = farPlane / 2

        scene = new THREE.Scene({ antialias: true })
        scene.fog = new THREE.FogExp2(0x000000, 0.0003)

        // The wizard's about to get busy.
        starForge()

        renderer = new THREE.WebGLRenderer({ alpha: true })
        renderer.setClearColor(0x14161d, 1)
        renderer.setPixelRatio(window.devicePixelRatio)
        renderer.setSize(WIDTH, HEIGHT)
        canvasElId = rootRef.current?.appendChild(renderer.domElement)

        window.addEventListener('resize', onWindowResize, false)
        document.addEventListener('mousemove', onMouseMove, false)

        doFakeMovement()
      }

      function animate() {
        requestAnimationFrame(animate)
        render()
      }

      function render() {
        camera.position.x += (mouseX - camera.position.x) * 0.002
        camera.position.y += (-mouseY - camera.position.y) * 0.002
        camera.lookAt(scene.position)
        renderer.render(scene, camera)
      }

      function starForge() {
        /* 	Yep, it's a Star Wars: Knights of the Old Republic reference,
          are you really surprised at this point?
                              */
        let starQty = 30000
        geometry = new THREE.SphereGeometry(1000, 100, 50)

        materialOptions = {
          size: 0.6, // I know this is the default, it's for you.  Play with it if you want.
          // transparency: true,
          // opacity: 0.1,
          color: 0x2eb8e5,
        }

        starStuff = new THREE.PointCloudMaterial(materialOptions)

        // The wizard gaze became stern, his jaw set, he creates the cosmos with a wave of his arms

        for (let i = 0; i < starQty; i += 1) {
          let starVertex = new THREE.Vector3()
          starVertex.x = Math.random() * 2000 - 1000
          starVertex.y = Math.random() * 2000 - 1000
          starVertex.z = Math.random() * 2000 - 1000

          geometry.vertices.push(starVertex)
        }

        stars = new THREE.PointCloud(geometry, starStuff)
        scene.add(stars)
      }
    }
    if (rootRef.current && Boolean(window.THREE)) {
      if (!webGLSupport()) return
      execute()
    }

    return () => {
      if (canvasElId) {
        renderer.forceContextLoss()
        rootRef.current?.removeChild(canvasElId)
        rootRef.current = null
        window.removeEventListener('resize', onWindowResize, false)
        document.removeEventListener('mousemove', onMouseMove, false)
      }
    }
  }, [])

  return (
    <>
      {webGLSupport() && (
        <div className={className}>
          <div
            ref={rootRef}
            style={{ height: '100%', width: '100%', overflow: 'hidden' }}
          />
        </div>
      )}
    </>
  )
}

export const StarsBackground: React.FC<{
  className?: string
}> = (props) => {
  const { isMobile } = useBreakpoints()

  if (isMobile) return null

  return <Stars {...props} />
}

function webGLSupport() {
  /* 	The wizard of webGL only bestows his gifts of power
    to the worthy.  In this case, users with browsers who 'get it'.		*/

  try {
    let canvas = document.createElement('canvas')
    return !!(
      window.WebGLRenderingContext &&
      (canvas.getContext('webgl') || canvas.getContext('experimental-webgl'))
    )
  } catch (e) {
    // console.warn('Hey bro, for some reason we\'re not able to use webGL for this.  No biggie, we\'ll use canvas.');
    return false
  }
}
