import React from 'react'
import { IAward } from '@/requests/types'
import { BaseImage } from '@/components/BaseImage'
import { ExternalLink } from '@/components/ExternalLink'
import * as Styled from './styled'

type TAwardsSection = {
  awards: IAward[]
}

export const PlatformSection: React.FC<TAwardsSection> = ({ awards }) => (
  <Styled.Awards>
    {awards.map((award) => (
      <Styled.Award key={award.id}>
        <ExternalLink href={award.url}>
          <BaseImage
            src={award.photo}
            alt={award.title}
            title={award.title}
            width={133}
            height={48}
          />
        </ExternalLink>
      </Styled.Award>
    ))}
  </Styled.Awards>
)
