import styled from 'styled-components'
import { deviceCssQuery } from '@/styling/breakpoints'

export const Awards = styled.div`
  margin-top: auto;

  @media screen and ${deviceCssQuery.xs} {
    display: none;
  }

  @media screen and ${deviceCssQuery.md} {
    display: flex;
  }
`

export const Award = styled.div`
  margin-right: 20px;
`
