import React from 'react'
import useTranslation from 'next-translate/useTranslation'
import { IContent } from '@/requests/types'
import { ThreeJSContainer } from '@/components/ThreeJSContainer/ThreeJSContainer'
import { ClutchReviews } from '@/components/ClutchReviews'
import { ClutchAward } from '@/components/ClutchAward'
import { SectionContainer } from '@/components/sections/SectionContainer'
import { Button } from '@/components/buttons/Button'
import { Spacer } from '@/components/Spacer/Spacer'
import * as Styled from './styled'
import { useSetContactUs } from '../../contact-us'
// import { PlatformSection } from './components/PlatformSection'

type TGreetingSection = {
  content: IContent
  // awards: IAward[]
}

export const MainSection: React.FC<TGreetingSection> = (props) => {
  const { content } = props
  const { setContactUsVisibility } = useSetContactUs()

  const { t } = useTranslation('home')

  return (
    <SectionContainer>
      <ThreeJSContainer>
        <Styled.StarsBg />
      </ThreeJSContainer>

      <Styled.Container>
        <Styled.TopBlock>
          <div>
            <Styled.Header
              variant="h1"
              tag="h1"
              margin="20px 0 30px"
              lg={{ fontSize: '65px', lineHeight: '80px' }}
            >
              {content.header}
            </Styled.Header>
            <Styled.SubtitleContainer>
              <Styled.Subtitle tag="p">{content.sub_header}</Styled.Subtitle>
            </Styled.SubtitleContainer>
            <Styled.ContactButtonWrapper>
              <Button
                variant="secondary"
                onClick={() => {
                  setContactUsVisibility(true)
                }}
              >
                {t('common:letsTalk')}
              </Button>
            </Styled.ContactButtonWrapper>
          </div>
          <div>
            <ClutchReviews />
            <Spacer md={{ margin: '32px 0 0 0' }}>
              <ClutchAward variant="rectangular" />
            </Spacer>
          </div>
        </Styled.TopBlock>
        {
          // <PlatformSection awards={awards} />
        }
      </Styled.Container>
    </SectionContainer>
  )
}
