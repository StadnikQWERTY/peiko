import React, { useEffect, useRef, useState } from 'react'
import styled, { css, useTheme } from 'styled-components'
import { Text } from '@/components/Text'
import { ArrowIcon } from '@/components/icons/ArrowIcon'
// import { Hashtags } from '@/components/Hashtags'
import { gsap } from 'gsap'
import { useRouter } from 'next/router'
import { useBreakpoints } from '@/hooks/useBreakpoints'
import { ROUTES } from '@/routes'
import { IService } from '@/features/home/api/types'
import Link from 'next/link'
import { FontWeight } from '@/styling/fonts'
import { deviceCssQuery } from '@/styling/breakpoints'
import { BaseImage } from '@/components/BaseImage'
import * as Styled from './styled'

// const HashtagsWrapper = styled.div``

const StyledLink = styled.a(
  (props) => css`
    color: ${props.theme.palette.primaryText};
    font-weight: ${FontWeight.SemiBold};
    margin: 0 9.33px 0 0;
    text-transform: capitalize;
    @media screen and ${deviceCssQuery.xs} {
      font-size: 18px;
      line-height: 27px;
    }

    @media screen and ${deviceCssQuery.md} {
      font-size: 20px;
      line-height: 150%;
    }
  `,
)

type TServiceBlock = {
  service: IService
  className?: string
}

export const ServiceBlock: React.FC<TServiceBlock> = (props) => {
  const { service, className } = props
  const router = useRouter()

  const rootRef = useRef<HTMLButtonElement>(null)

  const theme = useTheme()

  const [arrowIconColor, setArrowIconColor] = useState(theme.palette.primary)
  const [hovered, setHovering] = useState(false)

  const handleMouseEnter = () => {
    setArrowIconColor('white')
    setHovering(true)
  }

  const handleMouseLeave = () => {
    setArrowIconColor(theme.palette.primary)
    setHovering(false)
  }

  const { isDesktop } = useBreakpoints()

  const tweenRef = useRef<gsap.core.Tween>()

  useEffect(() => {
    if (rootRef.current) {
      if (tweenRef.current) {
        tweenRef.current.kill()
      }

      tweenRef.current = gsap.fromTo(
        rootRef.current,
        {
          opacity: 0,
          y: 20,
        },
        {
          scrollTrigger: {
            trigger: rootRef.current,
            start: isDesktop ? 'center bottom' : '40px bottom',
            toggleActions: 'play none none none',
          },
          opacity: 1,
          y: 0,
          duration: 0.5,
        },
      )
    }
  }, [isDesktop])

  const link = `${ROUTES.SERVICE}/${service.uri}`

  const handleRootClick = (e: React.SyntheticEvent<HTMLButtonElement>) => {
    const parent = (e.target as HTMLButtonElement).parentNode as HTMLLinkElement
    if (parent.tagName === 'A') return
    router.push(link)
  }

  return (
    <Styled.RootLink
      ref={rootRef}
      className={className}
      onMouseEnter={handleMouseEnter}
      onMouseLeave={handleMouseLeave}
      onClick={handleRootClick}
      role="link"
    >
      {hovered && <Styled.VolumetricArrowIconContainer />}
      <Styled.ImageContainer>
        <BaseImage
          src={service.photo}
          alt={service.name}
          draggable={false}
          objectFit="cover"
          layout="fill"
          loading="lazy"
          title={service.name}
        />
      </Styled.ImageContainer>

      <Styled.Body>
        <Styled.BodyTitle>
          <Link href={link}>
            <StyledLink href={link} className="service-block-text">
              {service.name}
            </StyledLink>
          </Link>
          <Styled.TitleArrowContainer>
            <ArrowIcon color={arrowIconColor} />
          </Styled.TitleArrowContainer>
        </Styled.BodyTitle>
        <Text variant="secondaryParagraph" className="service-block-text" align="start">
          {service.preview}
        </Text>
        {
          // <HashtagsWrapper>
          //   <Hashtags generateLink={ROUTES.CASES} tags={service.tags} />
          // </HashtagsWrapper>
        }
      </Styled.Body>
    </Styled.RootLink>
  )
}
