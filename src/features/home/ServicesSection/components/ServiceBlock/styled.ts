import styled from 'styled-components'
import { deviceCssQuery } from '@/styling/breakpoints'
import { VolumetricArrowIcon } from '@/components/icons/VolumetricArrowIcon'
import { HashtagText } from '@/components/Hashtags/Hashtags'

export const Body = styled.div`
  display: grid;
  grid-gap: 8px;
  grid-auto-rows: max-content;
`

export const BodyTitle = styled.div`
  text-align: left;
  display: flex;
`

export const TitleArrowContainer = styled.div`
  margin-top: 1px;
`

export const ImageContainer = styled.div`
  position: relative;
  width: 80px;
  height: 80px;
  img {
    object-position: 0 -80px;
    user-select: none;
  }
`

export const RootLink = styled.button`
  position: relative;
  display: grid;
  align-content: flex-start;
  border: 1px solid ${(props) => props.theme.palette.secondary};
  &:hover {
    cursor: pointer;
  }

  @media screen and ${deviceCssQuery.xs} {
    padding: 28px 12px;
    grid-gap: 24px;
    grid-template-columns: 100%;
  }

  @media screen and ${deviceCssQuery.md} {
    padding: 32px;
    grid-gap: 32px;
    grid-template-columns: 80px auto;
  }

  @media (hover: hover) {
    &:hover {
      background: ${(props) => props.theme.palette.primary};
      transition: background-color 300ms;
    }

    &:hover .service-block-text {
      color: white;
      transition: color 300ms;
    }

    &:hover ${HashtagText} {
      color: white;
      transition: color 300ms;
    }

    &:hover ${ImageContainer} {
      img {
        object-position: 0 0px;
      }
    }
  }
`

export const VolumetricArrowIconContainer = styled(VolumetricArrowIcon)`
  position: absolute;
  bottom: 10px;
  right: 10px;

  @media screen and ${deviceCssQuery.xs} {
    max-height: 40px;
  }

  @media screen and ${deviceCssQuery.md} {
    max-height: unset;
  }
`
