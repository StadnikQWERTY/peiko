import React from 'react'
import { IService } from '@/features/home/api/types'
import styled from 'styled-components'
import { deviceCssQuery } from '@/styling/breakpoints'
import { ServiceBlock } from '@/features/home/ServicesSection/components/ServiceBlock'
import { SectionTitles } from '@/components/sections/SectionTitles'
import { SectionContainer } from '@/components/sections/SectionContainer'
import useTranslation from 'next-translate/useTranslation'

type TServicesSection = {
  services: IService[]
}

const Services = styled.section`
  display: grid;

  @media screen and ${deviceCssQuery.xs} {
    grid-template-columns: 100%;
  }

  @media screen and ${deviceCssQuery.lg} {
    grid-template-columns: 1fr 1fr;
    margin-left: unset;
  }
`

const ServiceBlockContainer = styled(ServiceBlock)`
  @media screen and ${deviceCssQuery.xs} {
    margin-bottom: -1px;
  }

  @media screen and ${deviceCssQuery.lg} {
    margin-left: -1px;
  }
`

export const ServicesSection: React.FC<TServicesSection> = (props) => {
  const { services } = props

  const { t: tHome } = useTranslation('home')
  const { t: tCommon } = useTranslation('common')

  return (
    <SectionContainer>
      <SectionTitles
        tagHeader="h2"
        name={tCommon('services')}
        header={tHome('thisWhatWeDoTheBest')}
      />
      <Services>
        {services.map((service) => (
          <ServiceBlockContainer key={service.id} service={service} />
        ))}
      </Services>
    </SectionContainer>
  )
}
