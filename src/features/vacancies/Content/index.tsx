import { FC } from 'react'

import { Text } from '@/components/Text'
import styled from 'styled-components'
import { IVacanciesPageContent } from '@/requests/vacancies/types'
import { deviceCssQuery } from '@/styling/breakpoints'
import { customParser } from '@/utils/customParser'

const TextItalic = styled(Text)`
  max-width: 850px;
  em {
    font-style: italic;
    font-weight: 500;
    color: ${(props) => props.theme.palette.primary};
  }

  @media screen and ${deviceCssQuery.xs} {
    margin: 64px 0 0 0;
  }

  @media screen and ${deviceCssQuery.lg} {
    margin: 72px 0 0 0;
  }
`

const Texts = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  text-align: center;
`

interface OwnProps {
  content: IVacanciesPageContent
}

const Content: FC<OwnProps> = (props) => {
  const { content } = props
  const { header, sub_header } = content
  return (
    <Texts>
      <Text variant="h1" tag="h1">
        {header}
      </Text>
      <TextItalic variant="h2" tag="h2">
        {customParser(sub_header)}
      </TextItalic>
    </Texts>
  )
}

export default Content
