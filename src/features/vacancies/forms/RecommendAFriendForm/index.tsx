// Core
import React from 'react'
import useTranslation from 'next-translate/useTranslation'

// Validation
import * as yup from 'yup'
import { useFormik } from 'formik'
import { validation } from '@/utils/validation'

// UI
import styled from 'styled-components'
import { deviceCssQuery } from '@/styling/breakpoints'
import { FormikInput } from '@/components/fields/FormikInput/FormikInput'
import { ArrowTextButton } from '@/components/buttons/ArrowTextButton'
import { FormikFileUploader } from '@/components/fields/InputFileUploader'
import { FormikPhoneInput } from '@/components/fields/FormikPhoneInput'

// Other
import { IRecommendAFriendSendReq } from '@/requests/vacancies/types'
import { useSendForm } from '@/hooks/useSendForm'
import { vacanciesApi } from '@/requests/vacancies'

const StyledForm = styled.form`
  display: grid;
  grid-gap: 16px;
  padding-bottom: 48px;
`

const StyledTwoFields = styled.div`
  display: grid;
  grid-gap: 16px;

  @media screen and ${deviceCssQuery.xs} {
    grid-template-columns: 1fr;
  }

  @media screen and ${deviceCssQuery.lg} {
    grid-template-columns: 1fr 1fr;
  }
`

const StyledSubmitButton = styled(ArrowTextButton)`
  @media screen and ${deviceCssQuery.xs} {
    max-width: 100%;
  }

  @media screen and ${deviceCssQuery.md} {
    max-width: 265px;
  }
`

const ContactUsForm: React.FC<{ vacancyId: number }> = ({ vacancyId }) => {
  const { t } = useTranslation('form')
  const { t: tCommon } = useTranslation('common')

  const { isSendForm, sendForm } = useSendForm()

  const formik = useFormik<IRecommendAFriendSendReq>({
    initialValues: {
      name: '',
      surname: '',
      email: '',
      phone: '',
      message: '',
      friend_name: '',
      friend_lastname: '',
      friend_email: '',
      friend_phone: '',
      vacancy_id: vacancyId,
      attachment: undefined,
    },
    validationSchema: yup.object().shape({
      email: validation.email,
    }),
    onSubmit: (formData) => {
      sendForm({
        formSendFunction: vacanciesApi.sendRecommendCv,
        formData,
        formik,
        notificationSuccessText: tCommon('sendFormSuccess'),
      })
    },
  })

  return (
    <StyledForm onSubmit={formik.handleSubmit}>
      <StyledTwoFields>
        <FormikInput formik={formik} label={tCommon('yourFirstName')} name="name" />
        <FormikInput formik={formik} label={tCommon('yourLastName')} name="surname" />
      </StyledTwoFields>
      <StyledTwoFields>
        <FormikInput formik={formik} label={tCommon('yourEmail')} name="email" />
        <FormikPhoneInput
          formik={formik}
          label={tCommon('yourPhoneNumber')}
          name="phone"
        />
      </StyledTwoFields>
      <StyledTwoFields>
        <FormikInput
          formik={formik}
          label={tCommon('friendsFirstName')}
          name="friend_name"
        />
        <FormikInput
          formik={formik}
          label={tCommon('friendsLastName')}
          name="friend_lastname"
        />
      </StyledTwoFields>
      <StyledTwoFields>
        <FormikInput
          formik={formik}
          label={tCommon('friendsEmail')}
          name="friend_email"
        />
        <FormikPhoneInput
          formik={formik}
          label={tCommon('friendsPhoneNumber')}
          name="friend_phone"
        />
      </StyledTwoFields>
      <FormikInput formik={formik} label={tCommon('coverLetter')} name="message" />
      <FormikFileUploader formik={formik} name="attachment" />
      <StyledSubmitButton lineAnimated loading={isSendForm} type="submit">
        {t('send')}
      </StyledSubmitButton>
    </StyledForm>
  )
}

export default ContactUsForm
