// Core
import React from 'react'
import useTranslation from 'next-translate/useTranslation'

// Validation
import * as yup from 'yup'
import { useFormik } from 'formik'
import { validation } from '@/utils/validation'

// UI
import styled from 'styled-components'
import { deviceCssQuery } from '@/styling/breakpoints'
import { FormikInput } from '@/components/fields/FormikInput/FormikInput'
import { ArrowTextButton } from '@/components/buttons/ArrowTextButton'
import { FormikFileUploader } from '@/components/fields/InputFileUploader'
import { FormikPhoneInput } from '@/components/fields/FormikPhoneInput'
import { IApplyToPositionSendReq } from '@/requests/vacancies/types'
import { vacanciesApi } from '@/requests/vacancies'
import { useSendForm } from '@/hooks/useSendForm'

const StyledForm = styled.form`
  display: grid;
  grid-gap: 16px;
  padding-bottom: 48px;
`

const StyledTwoFields = styled.div`
  display: grid;
  grid-gap: 16px;

  @media screen and ${deviceCssQuery.xs} {
    grid-template-columns: 1fr;
  }

  @media screen and ${deviceCssQuery.lg} {
    grid-template-columns: 1fr 1fr;
  }
`

const StyledSubmitButton = styled(ArrowTextButton)`
  @media screen and ${deviceCssQuery.xs} {
    max-width: 100%;
  }

  @media screen and ${deviceCssQuery.md} {
    max-width: 265px;
  }
`

const ContactUsForm: React.FC<{ vacancyId: number }> = ({ vacancyId }) => {
  const { t } = useTranslation('form')
  const { t: tCommon } = useTranslation('common')
  const { isSendForm, sendForm } = useSendForm()

  const formik = useFormik<IApplyToPositionSendReq>({
    initialValues: {
      name: '',
      surname: '',
      email: '',
      phone: '',
      message: '',
      vacancy_id: vacancyId,
      attachment: undefined,
    },
    validationSchema: yup.object().shape({
      email: validation.email,
    }),
    onSubmit: (formData) => {
      sendForm({
        notificationSuccessText: tCommon('sendFormSuccess'),
        formik,
        formSendFunction: vacanciesApi.sendCommonCv,
        formData,
      })
    },
  })

  return (
    <StyledForm onSubmit={formik.handleSubmit}>
      <StyledTwoFields>
        <FormikInput formik={formik} label={t('common:firstName')} name="name" />
        <FormikInput formik={formik} label={t('common:lastName')} name="surname" />
      </StyledTwoFields>
      <StyledTwoFields>
        <FormikInput formik={formik} label={t('email')} name="email" />
        <FormikPhoneInput formik={formik} label={t('phoneNumber')} name="phone" />
      </StyledTwoFields>
      <FormikInput formik={formik} label={t('common:coverLetter')} name="message" />
      <FormikFileUploader formik={formik} name="attachment" />
      <StyledSubmitButton lineAnimated loading={isSendForm} type="submit">
        {t('send')}
      </StyledSubmitButton>
    </StyledForm>
  )
}

export default ContactUsForm
