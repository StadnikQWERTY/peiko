import React, { FC, useState } from 'react'
import { Text } from '@/components/Text'
import { LocationIcon } from '@/components/icons/LocationIcon'
import styled from 'styled-components'
import { IVacancyPageResponse } from '@/requests/vacancies/types'
import { Spacer } from '@/components/Spacer/Spacer'
import { Breadcrumbs } from '@/components/Breadcrumbs'
import { useRouter } from 'next/router'
import { ROUTES } from '@/routes'
import useTranslation from 'next-translate/useTranslation'
import { useBreakpoints } from '@/hooks/useBreakpoints'
import { GoogleMapContainer } from '@/components/maps/GoogleMap/GoogleMapContainer'
import { deviceCssQuery } from '@/styling/breakpoints'
import ApplyToPositionForm from '@/features/vacancies/forms/ApplyToPositionForm'
import RecommendAFriendForm from '@/features/vacancies/forms/RecommendAFriendForm'
import { SocialLinks } from '@/components/SocialLinks'
import Trans from 'next-translate/Trans'
import { customParser } from '@/utils/customParser'
import { PageSwitcher } from '@/components/PageSwitcher/PageSwitcher'
import Popup from '../Popup'

const DotSvg = () => (
  <div>
    <svg
      width="8"
      height="9"
      viewBox="0 0 8 9"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <circle cx="4" cy="4.5" r="4" fill="#41B6E6" />
    </svg>
  </div>
)

const AboutVacancy = styled.div`
  display: flex;
  align-items: center;
  justify-content: start;
  gap: 40px;
`

const InfoContainer = styled.div`
  display: grid;
  gap: 115px;

  @media screen and ${deviceCssQuery.xs} {
    grid-template-columns: 1fr;
  }

  @media screen and ${deviceCssQuery.md} {
    grid-template-columns: 7fr 3fr;
  }
`

const InfoItem = styled.div`
  ul {
    padding: 0px;
    margin: 0;
  }

  li {
    list-style: none;
    margin: 8px 0;
  }
`

const HotVacancy = styled(Text)`
  margin: 0;
  letter-spacing: 0.2em;
  text-transform: uppercase;
  color: #41b6e6;
`

const StyledText = styled(Text)`
  margin: 0;
  color: ${(props) => props.theme.palette.secondaryText};
  path {
    fill: ${(props) => props.theme.palette.secondaryText};
  }
`

const StyledLocationText = styled(StyledText)`
  display: flex;
  align-items: center;
  justify-content: start;
  gap: 10px;
`

const StyledButton = styled.button`
  border-bottom: 1px solid #41b6e6;
  width: 100%;
  padding: 24px;
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  letter-spacing: 0.07em;
  text-transform: uppercase;
  color: #41b6e6;
`

const NavigationItem = styled.div`
  display: flex;
  flex-direction: column;
  gap: 15px;
`

const MapWrapper = styled.div`
  min-height: 240px;
`

const ListTittle = styled(Text)`
  margin: 48px 0 18px 0;
`

const EmployeeWrapper = styled.div`
  display: grid;
  grid-template-columns: 72px auto;
`

const EmployeeInfo = styled.div`
  display: flex;
  flex-direction: column;
  gap: 8px;
`

const EmployeeName = styled(Text)`
  letter-spacing: 0.2em;
  text-transform: uppercase;
  color: #14161d;
`

const EmployeeEmail = styled(Text)`
  color: #41b6e6;
`

const EmployeePosition = styled(Text)`
  color: #656c72;
`

const StyledSocialLinks = styled(SocialLinks)`
  path {
    fill: #b1c1d1 !important;
  }
`

const StyledLi = styled(Text)`
  display: flex;
  align-items: center;
  gap: 16px;
`

const StyledPosition = styled.span`
  color: #41b6e6;
`

interface OwnProps {
  vacancy: IVacancyPageResponse
}

const VacancyInfo: FC<OwnProps> = (props) => {
  const { vacancy } = props

  const { asPath } = useRouter()
  const { t: tCommon } = useTranslation('common')
  const { isMobile } = useBreakpoints()
  const [isApplyToPositionPopup, setApplyToPositionPopup] = useState<boolean>(false)
  const [isRecommendAFriendPopup, setRecommendAFriendPopup] = useState<boolean>(false)

  const breadcrumbs = [
    { name: tCommon('home'), href: ROUTES.HOME },
    { name: tCommon('vacancies'), href: ROUTES.VACANCIES },
    { name: vacancy.name, href: asPath },
  ]

  return (
    <>
      <Spacer lg={{ padding: '66px 0 26px 0' }} xs={{ padding: '88px 0 16px 0' }}>
        <Breadcrumbs justifyContent="start" breadcrumbs={breadcrumbs} />
      </Spacer>
      <Text variant="h1" tag="h1">
        {vacancy.name}
      </Text>
      {!isMobile && (
        <Spacer sm={{ padding: '24px 0 0 0' }} xs={{ padding: '0' }}>
          <AboutVacancy>
            {vacancy.is_hot && (
              <HotVacancy variant="h4" tag="span">
                Hot Vacancy 🔥
              </HotVacancy>
            )}
            <StyledLocationText variant="secondaryParagraph" tag="p">
              <LocationIcon /> {vacancy.location}
            </StyledLocationText>
            <StyledText variant="secondaryParagraph" tag="p">
              {vacancy.schedule}
            </StyledText>
          </AboutVacancy>
        </Spacer>
      )}
      <Spacer lg={{ padding: '24px 0 48px 0' }} xs={{ padding: '16px 0 48px 0' }}>
        <InfoContainer>
          <InfoItem>
            {customParser(vacancy.content)}
            {vacancy.lists.map((list) => (
              <div key={list.group}>
                <ListTittle variant="h3" tag="h2">
                  {list.group}
                </ListTittle>
                <ul>
                  {list.items.map((item) => (
                    <StyledLi key={item} variant="paragraph" tag="li">
                      <DotSvg />
                      <span>{item}</span>
                    </StyledLi>
                  ))}
                </ul>
              </div>
            ))}
          </InfoItem>
          {!isMobile && (
            <NavigationItem>
              <Popup
                onClose={() => setApplyToPositionPopup(false)}
                open={isApplyToPositionPopup}
              >
                <div
                  style={{
                    maxWidth: '598px',
                    margin: '0 auto',
                    padding: '0 24px',
                    width: '100%',
                  }}
                >
                  <Text variant="h2" tag="h2">
                    {tCommon('joinPeiko')}
                  </Text>
                  <Text variant="paragraph">
                    <Trans
                      i18nKey="common:applyingPosition"
                      components={{
                        styledPosition: <StyledPosition />,
                      }}
                      values={{ position: vacancy.name }}
                    />
                  </Text>
                  <Spacer xs={{ margin: '16px 0 0' }}>
                    <ApplyToPositionForm vacancyId={vacancy.id} />
                  </Spacer>
                </div>
              </Popup>
              <StyledButton onClick={() => setApplyToPositionPopup(true)}>
                {tCommon('applyToPosition')}
              </StyledButton>
              <Popup
                onClose={() => setRecommendAFriendPopup(false)}
                open={isRecommendAFriendPopup}
              >
                <div
                  style={{
                    maxWidth: '598px',
                    margin: '0 auto',
                    padding: '0 24px',
                    width: '100%',
                  }}
                >
                  <Text variant="h2" tag="h2">
                    {tCommon('recommendAFriend')}
                  </Text>
                  <Text variant="paragraph">
                    <Trans
                      i18nKey="common:recommendingFriendPosition"
                      components={{
                        styledPosition: <StyledPosition />,
                      }}
                      values={{ position: vacancy.name }}
                    />
                  </Text>
                  <Spacer xs={{ margin: '16px 0 0' }}>
                    <RecommendAFriendForm vacancyId={vacancy.id} />
                  </Spacer>
                </div>
              </Popup>
              <StyledButton onClick={() => setRecommendAFriendPopup(true)}>
                {tCommon('recommendAFriend')}
              </StyledButton>
              <br />
              <MapWrapper>
                <GoogleMapContainer />
              </MapWrapper>
              <Text variant="paragraph">
                {vacancy.map_location}
                <br />
                {vacancy.map_address}
              </Text>
              <Spacer lg={{ padding: '45px 0 0 0' }} xs={{ padding: '45px 0 0 0' }}>
                <StyledSocialLinks filter="invert(50%)" />
              </Spacer>
            </NavigationItem>
          )}
        </InfoContainer>
      </Spacer>
      <EmployeeWrapper>
        <img height={48} src={vacancy.employee.photo} alt={vacancy.employee.name} />
        <EmployeeInfo>
          <EmployeeName variant="h4">{vacancy.employee.name}</EmployeeName>
          <EmployeePosition variant="paragraph">
            {vacancy.employee.position}
          </EmployeePosition>
          <EmployeeEmail variant="paragraph">{vacancy.employee.email}</EmployeeEmail>
        </EmployeeInfo>
      </EmployeeWrapper>
      <Spacer lg={{ padding: '146px 0 194px 0' }} xs={{ padding: '66px 0 120px 0' }}>
        <PageSwitcher
          prev={{ url: ROUTES.VACANCIES, name: tCommon('allVacancies') }}
          next={{ url: ROUTES.ABOUT_US, name: tCommon('aboutUs') }}
        />
      </Spacer>
    </>
  )
}

export default VacancyInfo
