import React from 'react'
import { ThemedSection } from '@/components/ThemedSection'
import { FullScreenPopup } from '@/containers/FullScreenPopup'

interface OwnProps {
  onClose: (param: boolean) => void
  open: boolean
}

const Popup: React.FC<OwnProps> = ({ onClose, open, children }) => (
  <ThemedSection theme="dark">
    <FullScreenPopup isOpened={open} onChangeOpeningState={() => onClose(false)}>
      {open && children}
    </FullScreenPopup>
  </ThemedSection>
)

export default Popup
