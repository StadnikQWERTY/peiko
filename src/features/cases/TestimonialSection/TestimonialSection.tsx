import React from 'react'
import styled from 'styled-components'
import Testimonial from '@/components/Testimonial/Testimonial'
import { Text } from '@/components/Text'
import { deviceCssQuery } from '@/styling/breakpoints'
import { ArrowTextButton } from '@/components/buttons/ArrowTextButton'
import useTranslation from 'next-translate/useTranslation'
import { ICasePageCommunity } from '@/requests/cases/types'
import { IReview } from '@/requests/types'

const ClutchSvg = () => (
  <svg
    width="100"
    height="56"
    viewBox="0 0 100 56"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M26.0195 14H30.5722V42.454H26.0195V14ZM46.5064 33.3772C46.5064 37.7318 42.8449 38.0789 41.7557 38.0789C39.0344 38.0789 38.5393 35.5317 38.5393 33.9952V23.1053H33.9866V33.9724C33.9866 36.6699 34.8334 38.8961 36.3176 40.3803C37.6288 41.6926 39.5932 42.4096 41.7216 42.4096C43.2308 42.4096 45.3682 41.9396 46.5064 40.9004V42.454H51.059V23.1053H46.5064V33.3772ZM60.1643 16.2763H55.6117V23.1053H52.1972V27.6579H55.6117V42.454H60.1643V27.6579H63.5788V23.1053H60.1643V16.2763ZM77.9162 36.6687C76.9248 37.5599 75.6159 38.0539 74.1796 38.0539C71.0132 38.0539 68.688 35.7286 68.688 32.5361C68.688 29.3435 70.9393 27.1162 74.1796 27.1162C75.5909 27.1162 76.9248 27.5862 77.9412 28.4774L78.6321 29.0715L81.7017 26.003L80.9334 25.3099C79.1272 23.6789 76.7291 22.7638 74.1785 22.7638C68.4877 22.7638 64.355 26.8703 64.355 32.5122C64.355 38.1301 68.5867 42.3834 74.1785 42.3834C76.7769 42.3834 79.2 41.4684 80.9835 39.8112L81.7256 39.1181L78.6082 36.0519L77.9162 36.6687ZM97.9512 24.792C96.64 23.4809 95.1035 22.7638 92.9752 22.7638C91.466 22.7638 89.7564 23.2339 88.6183 24.2719V14H84.0656V42.454H88.6183V31.7951C88.6183 27.4405 91.7118 27.0945 92.801 27.0945C95.5223 27.0945 95.4472 29.6429 95.4472 31.1771V42.454H99.9999V31.201C99.9999 28.5036 99.4365 26.2773 97.9512 24.792Z"
      fill="#B1C1D1"
    />
    <path
      d="M74.0299 29.2944C74.9028 29.2944 75.7401 29.6412 76.3573 30.2585C76.9746 30.8758 77.3214 31.713 77.3214 32.586C77.3214 33.459 76.9746 34.2962 76.3573 34.9135C75.7401 35.5308 74.9028 35.8776 74.0299 35.8776C73.1569 35.8776 72.3196 35.5308 71.7024 34.9135C71.0851 34.2962 70.7383 33.459 70.7383 32.586C70.7383 31.713 71.0851 30.8758 71.7024 30.2585C72.3196 29.6412 73.1569 29.2944 74.0299 29.2944Z"
      fill="#CDD2E2"
    />
    <path
      d="M19.6458 35.3077C17.9146 37.0889 15.5632 38.054 13.0888 38.054C8.0172 38.054 4.32956 34.0705 4.32956 28.6028C4.32956 23.11 8.0172 19.1264 13.0888 19.1264C15.5393 19.1264 17.8646 20.0905 19.6219 21.8478L20.3139 22.5409L23.3573 19.4975L22.6892 18.8043C20.1659 16.2059 16.7514 14.7969 13.0888 14.7969C5.61682 14.7969 0 20.7347 0 28.6267C0 36.4948 5.64186 42.4086 13.0888 42.4086C16.7765 42.4086 20.191 40.9746 22.7143 38.3761L23.3824 37.683L20.364 34.5906L19.6458 35.3077Z"
      fill="#B1C1D1"
    />
  </svg>
)

const Container = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;

  @media screen and ${deviceCssQuery.xs} {
    gap: 50px;
    grid-template-columns: 1fr;
  }

  @media screen and ${deviceCssQuery.lg} {
    grid-template-columns: 1fr 1fr;
  }
`

const StyledText = styled(Text)`
  display: flex;
  align-items: center;
`

interface OwmProps {
  review: IReview
  community: ICasePageCommunity
}

const TestimonialSection: React.FC<OwmProps> = (props) => {
  const { review, community } = props
  const { t } = useTranslation('common')
  return (
    review && (
      <Container>
        {community.url && (
          <div>
            <ClutchSvg />
            <StyledText variant="paragraph">
              {t('readFull')}
              <a
                target="_blank"
                href={community.url || '/'}
                rel="noopener noreferrer nofollow"
              >
                <div>
                  <ArrowTextButton>{community.title}</ArrowTextButton>
                </div>
              </a>
            </StyledText>
          </div>
        )}
        <div>
          <Testimonial
            text={review.review}
            author={`${review.name}, ${review.position}`}
            company={review.case_name}
            externalLink={review.external_link || '/'}
          />
        </div>
      </Container>
    )
  )
}

export default TestimonialSection
