import React from 'react'
import styled from 'styled-components'
import { Text } from '@/components/Text'
import { ICasePageResponse } from '@/requests/cases/types'
import List from '@/components/List/List'

import { deviceCssQuery } from '@/styling/breakpoints'
import useTranslation from 'next-translate/useTranslation'
import { customParser } from '@/utils/customParser'

const Container = styled.div`
  display: grid;
  margin-top: 20px;

  @media screen and ${deviceCssQuery.xs} {
    grid-template-columns: 1fr;
    gap: 20px;
  }

  @media screen and ${deviceCssQuery.lg} {
    gap: 210px;
    grid-template-columns: 1fr 1fr;
  }
`

interface OwnProps {
  caseData: ICasePageResponse
}

const TasksAndChallengesSection: React.FC<OwnProps> = (props) => {
  const { caseData } = props
  const { task } = caseData
  const { description, checklist } = task
  const { t } = useTranslation('common')
  return (
    <>
      <Text variant="h3" tag="h2" margin={0}>
        {t('hadToDo')}
      </Text>
      <Container>
        <div>
          <List list={checklist} />
        </div>
        <div>
          <Text variant="paragraph">{customParser(description)}</Text>
        </div>
      </Container>
    </>
  )
}

export default TasksAndChallengesSection
