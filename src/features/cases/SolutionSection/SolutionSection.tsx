import React from 'react'
import { ICasePageResponse } from '@/requests/cases/types'
import styled from 'styled-components'
import { Text } from '@/components/Text'
import List from '@/components/List/List'

import { deviceCssQuery } from '@/styling/breakpoints'
import useTranslation from 'next-translate/useTranslation'
import { customParser } from '@/utils/customParser'

interface OwnProps {
  caseData: ICasePageResponse
}

const Container = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  gap: 210px;

  @media screen and ${deviceCssQuery.xs} {
    grid-template-columns: 1fr;
    gap: 20px;
  }

  @media screen and ${deviceCssQuery.lg} {
    gap: 210px;
    grid-template-columns: 1fr 1fr;
  }
`

const SolutionSection: React.FC<OwnProps> = (props) => {
  const { caseData } = props
  const { solution } = caseData
  const { t } = useTranslation('common')
  return (
    <>
      <Text variant="h3" tag="h2" margin={0}>
        {t('whatDone')}
      </Text>
      <Container>
        <div>
          <Text variant="paragraph">{customParser(solution.description)}</Text>
        </div>
        <div>
          <List list={solution.checklist} />
        </div>
      </Container>
    </>
  )
}

export default SolutionSection
