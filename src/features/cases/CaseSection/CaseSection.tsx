import React from 'react'
import { ICasePageResponse } from '@/requests/cases/types'
import styled from 'styled-components'
import { Text } from '@/components/Text'
// import { Hashtags } from '@/components/Hashtags'
import { BaseImage } from '@/components/BaseImage'
import googlePlayImg from '@/images/google-play.svg'
import appStoreImg from '@/images/app-store.svg'
import { Spacer } from '@/components/Spacer/Spacer'
import { Breadcrumbs } from '@/components/Breadcrumbs'
import { deviceCssQuery } from '@/styling/breakpoints'
import useTranslation from 'next-translate/useTranslation'
import { ROUTES } from '@/routes'
import { useRouter } from 'next/router'
import { ArrowTextButton } from '@/components/buttons/ArrowTextButton'
import { useBreakpoints } from '@/hooks/useBreakpoints'

// const HashtagsWrapper = styled.div``

const Container = styled.div`
  width: 100%;
  display: grid;

  @media screen and ${deviceCssQuery.xs} {
    height: auto;
    grid-template-columns: 1fr;
  }

  @media screen and ${deviceCssQuery.lg} {
    min-height: 100vh;
    grid-template-columns: 8fr 6fr;
  }
`

const MobileAppLinksContainer = styled.div`
  margin-top: 32px;
  display: grid;
  grid-template-columns: max-content max-content;
  grid-gap: 25px;
`

const ColorSpacer = styled.div<{ gradient: string; isMobile: boolean }>`
  ${(props) => props.gradient || ''};
  display: flex;
  align-items: center;
  justify-content: center;
  justify-content: ${(props) => (props.isMobile ? 'center' : 'flex-end')};
  overflow: hidden;

  @media screen and ${deviceCssQuery.xs} {
    padding: 33px 0;
    margin: 0;
  }

  @media screen and ${deviceCssQuery.lg} {
    padding: 0;
    margin: 0;
  }
`

const ImageContainer = styled.div<{ isMobile: boolean }>`
  position: relative;
  right: ${(props) => (props.isMobile ? '0%' : '-15%')};
`

const Table = styled.div`
  margin-top: auto;
  display: grid;
  border: 1px solid ${(props) => props.theme.palette.secondary};

  @media screen and ${deviceCssQuery.xs} {
    grid-template-columns: 1fr 1fr;
    &:last-child {
      border-bottom: none;
    }
    &:last-child {
      border-right: none;
    }
  }

  @media screen and ${deviceCssQuery.lg} {
    grid-template-columns: 1fr 1fr 1fr 1fr;
    &:last-child {
      border-right: none;
    }
  }
`

const TableItem = styled.div`
  padding: 18px;
  @media screen and ${deviceCssQuery.xs} {
    border-bottom: 1px solid ${(props) => props.theme.palette.secondary};
    border-right: 1px solid ${(props) => props.theme.palette.secondary};
  }

  @media screen and ${deviceCssQuery.lg} {
    border-right: 1px solid ${(props) => props.theme.palette.secondary};
  }
`

const Box = styled.div`
  height: 100%;
  display: flex;
  flex-direction: column;

  @media screen and ${deviceCssQuery.xs} {
    padding: 24px;
  }

  @media screen and ${deviceCssQuery.md} {
    padding: 24px 24px 24px 83px;
  }

  @media screen and ${deviceCssQuery.lg} {
    padding: 67px 20px 67px 160px;
  }
`

const StyledTableTitle = styled(Text)`
  color: ${(props) => props.theme.palette.secondaryLightText};
`

export const OpenCaseContainer = styled.div`
  display: flex;
  margin-bottom: 40px;
`

export const OpenCase = styled.div`
  display: flex;
  align-items: center;
`

interface OwnProps {
  caseData: ICasePageResponse
}

export const CaseSection: React.FC<OwnProps> = (props) => {
  const { caseData } = props
  const {
    name,
    header,
    preview,
    play_market_link,
    apple_store_link,
    options,
    gradient,
    photo,
    is_mobile,
  } = caseData

  const { t } = useTranslation('common')
  const { lgAndHigher, isMobile } = useBreakpoints()
  const { asPath } = useRouter()

  const breadcrumbs = [
    { name: t('home'), href: ROUTES.HOME },
    { name: t('case'), href: ROUTES.CASES },
    { name: t(name), href: asPath },
  ]

  const lgImgSize = {
    mobileCaseImageSize: {
      width: '314px',
      height: '636px',
    },
    desktopCaseImageSize: {
      width: '831px',
      height: '470px',
    },
  }

  const xsImgSize = {
    mobileCaseImageSize: {
      width: '244px',
      height: '476px',
    },
    desktopCaseImageSize: {
      width: '600px',
      height: '311px',
    },
  }

  const activeSize = lgAndHigher ? lgImgSize : xsImgSize
  const activeDevice = is_mobile
    ? activeSize.mobileCaseImageSize
    : activeSize.desktopCaseImageSize

  return (
    <Container>
      <Box>
        <Spacer lg={{ padding: '0' }} xs={{ padding: '60px 0 0 0' }}>
          <Breadcrumbs breadcrumbs={breadcrumbs} justifyContent="start" />
        </Spacer>

        <Spacer lg={{ padding: '18px 0' }}>
          <Text variant="h1" tag="div">
            {name}
          </Text>
        </Spacer>
        <Text variant="h3" tag="h1">
          {header}
        </Text>
        <Spacer lg={{ padding: '24px 120px 24px 0' }}>
          <Text variant="paragraph">{preview}</Text>
        </Spacer>
        {
          // <HashtagsWrapper
          //   onClick={(e) => {
          //     e.stopPropagation()
          //   }}
          // >
          //   <Hashtags generateLink={ROUTES.CASES} tags={tags} />
          // </HashtagsWrapper>
        }

        <MobileAppLinksContainer>
          {play_market_link && (
            <a
              href={play_market_link || ''}
              target="_blank"
              rel="noopener noreferrer nofollow"
            >
              <BaseImage alt={t('alt:google-play')} src={googlePlayImg} />
            </a>
          )}
          {apple_store_link && (
            <>
              <a
                href={apple_store_link || ''}
                target="_blank"
                rel="noopener noreferrer nofollow"
              >
                <BaseImage alt={t('alt:app-store')} src={appStoreImg} />
              </a>
              <br />
            </>
          )}
        </MobileAppLinksContainer>
        <OpenCaseContainer>
          {caseData.url && (
            <OpenCase>
              <Text variant="paragraph" tag="span">
                {t('open')}
              </Text>
              <a href={caseData.url} target="_blank" rel="noopener noreferrer nofollow">
                <ArrowTextButton>{name}</ArrowTextButton>
              </a>
            </OpenCase>
          )}
        </OpenCaseContainer>
        <Table>
          {options.map((option) => {
            const { param, title, value } = option

            return (
              <TableItem key={param}>
                <Spacer lg={{ margin: '0 0 8px 0' }}>
                  <StyledTableTitle variant="secondaryParagraph">
                    {title}
                  </StyledTableTitle>
                </Spacer>
                <Text variant="paragraph">{value}</Text>
              </TableItem>
            )
          })}
        </Table>
      </Box>
      <ColorSpacer gradient={gradient} isMobile={is_mobile}>
        <ImageContainer isMobile={is_mobile}>
          <BaseImage
            src={photo}
            alt={name}
            title={name}
            width={activeDevice.width}
            height={activeDevice.height}
            objectFit={isMobile ? 'scale-down' : 'initial'}
          />
        </ImageContainer>
      </ColorSpacer>
    </Container>
  )
}
