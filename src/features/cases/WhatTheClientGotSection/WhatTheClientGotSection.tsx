import React from 'react'
import { ICasePageResponse } from '@/requests/cases/types'
import styled from 'styled-components'
import { Text } from '@/components/Text'
import { customParser } from '@/utils/customParser'

const Table = styled.div`
  margin-top: 55px;
  display: grid;
  grid-template-columns: 1fr;
  border: 1px solid ${(props) => props.theme.palette.secondary};

  &:last-child {
    border-bottom: none;
  }
`

const StyledText = styled(Text)`
  margin: auto 0;
`

const TableItem = styled.div`
  display: flex;
  padding: 18px;
  border-bottom: 1px solid ${(props) => props.theme.palette.secondary};
  ${StyledText}:last-child {
    margin-left: auto;
    text-align: end;
  }
`

interface OwnProps {
  caseData: ICasePageResponse
}

const WhatTheClientGotSection: React.FC<OwnProps> = (props) => {
  const { caseData } = props
  return (
    <>
      <Text variant="paragraph" tag="div">
        {customParser(caseData.result.description)}
      </Text>
      {caseData.result.checklist.length && (
        <Table>
          {caseData.result.checklist.map((item) => {
            const { description, name } = item
            return (
              <TableItem key={name}>
                <StyledText variant="h2">{name}</StyledText>
                <StyledText variant="paragraph">{description}</StyledText>
              </TableItem>
            )
          })}
        </Table>
      )}
    </>
  )
}

export default WhatTheClientGotSection
