import React from 'react'
import styled from 'styled-components'
import { ICasePageSlider } from '@/requests/cases/types'
import Marquee from 'react-fast-marquee'
import { deviceCssQuery } from '@/styling/breakpoints'
import { ImgWithoutOptimization } from '@/components/ImgWithoutOptimization'
import { useBreakpoints } from '@/hooks/useBreakpoints'

const Container = styled.div`
  display: grid;
  gap: 48px;
`

const ImageWrapper = styled.div`
  margin: 0 30px;

  @media screen and ${deviceCssQuery.xs} {
    img {
      height: 240px;
    }
  }

  @media screen and ${deviceCssQuery.lg} {
    img {
      height: 400px;
    }
  }
`

interface OwnProps {
  slider: ICasePageSlider[]
}

const ScreensSection: React.FC<OwnProps> = (props) => {
  const { slider } = props
  const { isMobile } = useBreakpoints()

  return (
    <Container>
      <Marquee
        gradientWidth={0}
        delay={0}
        speed={20}
        loop={0}
        play
        pauseOnHover={false}
        pauseOnClick={false}
        direction="left"
      >
        {slider.map((item) => (
          <ImageWrapper key={item.alt}>
            <ImgWithoutOptimization
              src={item.photo}
              alt={item.alt}
              skeletonWidth={150}
              skeletonHeight={isMobile ? 240 : 400}
            />
          </ImageWrapper>
        ))}
      </Marquee>
      <Marquee
        gradientWidth={0}
        delay={0}
        speed={20}
        loop={0}
        play
        pauseOnHover={false}
        pauseOnClick={false}
        direction="right"
      >
        {slider.map((item) => (
          <ImageWrapper key={item.alt}>
            <ImgWithoutOptimization
              src={item.photo}
              alt={item.alt}
              skeletonWidth={150}
              skeletonHeight={isMobile ? 240 : 400}
            />
          </ImageWrapper>
        ))}
      </Marquee>
    </Container>
  )
}

export default React.memo(ScreensSection)
