import React from 'react'
import styled from 'styled-components'
import { ICasePageResponse } from '@/requests/cases/types'
import { Text } from '@/components/Text'

import { deviceCssQuery } from '@/styling/breakpoints'
import { customParser } from '@/utils/customParser'

const Container = styled.div`
  display: grid;

  @media screen and ${deviceCssQuery.xs} {
    grid-template-columns: 1fr;
    gap: 20px;
  }

  @media screen and ${deviceCssQuery.lg} {
    gap: 100px;
    grid-template-columns: 1fr 1fr;
  }
`

const Table = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  border: 1px solid ${(props) => props.theme.palette.secondary};

  &:last-child {
    border-right: none;
  }
`

const TableItem = styled.div`
  padding: 16px;
  border-right: 1px solid ${(props) => props.theme.palette.secondary};
`

const StyledText = styled(Text)`
  color: ${(props) => props.theme.palette.secondaryText};
`

interface OwnProps {
  caseData: ICasePageResponse
}

const BackgroundSection: React.FC<OwnProps> = (props) => {
  const { caseData } = props
  const { prologue, technologies, sprints } = caseData
  return (
    <Container>
      <Text variant="paragraph">{customParser(prologue)}</Text>
      <Table>
        <TableItem>
          <StyledText variant="secondaryParagraph">Tech Stack</StyledText>
          {technologies.map((technology) => (
            <Text variant="paragraph" key={technology.id}>
              {technology.name}{' '}
            </Text>
          ))}
        </TableItem>
        <TableItem>
          <StyledText variant="secondaryParagraph">Number of sprints</StyledText>
          <Text variant="paragraph">{sprints}</Text>
        </TableItem>
      </Table>
    </Container>
  )
}

export default BackgroundSection
