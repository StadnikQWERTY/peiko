import React from 'react'
import Trans from 'next-translate/Trans'
import { Text } from '@/components/Text'
import { BaseLink } from '@/components/BaseLink'
import styled from 'styled-components'
import { ROUTES } from '@/routes'

const StyledBaseLink = styled(BaseLink)`
  text-decoration: underline;
  color: unset;
`

const StyledText = styled(Text)`
  color: #aab1bc;
`

export const TermsAndConditionsNote: React.FC = () => (
  <Trans
    i18nKey="contact-us:pressingButtonTerms"
    components={{
      component: <StyledText variant="thirdParagraph" />,
      termsLink: <StyledBaseLink href={ROUTES.TERMS_CONDITIONS} />,
      privacyLink: <StyledBaseLink href={ROUTES.PRIVACY_POLICY} />,
    }}
  />
)
