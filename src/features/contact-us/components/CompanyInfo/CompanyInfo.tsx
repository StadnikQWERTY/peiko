import React from 'react'
import { Text } from '@/components/Text'
import { PhoneLink } from '@/components/PhoneLink'
import { EmailLink } from '@/components/EmailLink'
// import { SocialLinks } from '@/components/SocialLinks'
import { SectionContainer } from '@/components/sections/SectionContainer'
import { Divider } from '@/components/Divider'
import styled from 'styled-components'
import { deviceCssQuery } from '@/styling/breakpoints'
import useTranslation from 'next-translate/useTranslation'
import { useBreakpoints } from '@/hooks/useBreakpoints'
// import { ArrowIcon } from '@/components/icons/ArrowIcon'
// import { BaseLink } from '@/components/BaseLink'
// import { ROUTES } from '@/routes'
// import { useSetContactUs } from '@/features/contact-us'

const StyledOurContacts = styled.div`
  margin-top: auto;
  width: 100%;

  @media screen and ${deviceCssQuery.xs} {
    padding: 0;
  }

  @media screen and ${deviceCssQuery.md} {
    padding: 0 0 32px;
  }
`

const StyledOurContactsContent = styled.div`
  display: grid;
  grid-auto-flow: column;
  justify-content: space-between;

  @media screen and ${deviceCssQuery.xs} {
    grid-auto-flow: row;
    grid-auto-columns: 100%;
  }

  @media screen and ${deviceCssQuery.md} {
    grid-auto-flow: column;
    grid-auto-columns: max-content;
  }
`

const StyledContactInfoContainer = styled.div`
  display: flex;
  gap: 16px;
  flex-direction: column;
  margin-bottom: 46px;

  @media screen and ${deviceCssQuery.md} {
    flex-direction: row;
    margin-bottom: 80px;
    justify-content: center;
    justify-items: center;
  }
`

const MobileBlockContainer = styled.div`
  padding: 24px 0;
`

const DesktopContactsContainer = styled.div`
  padding: 32px 0;
`

// const CareersCountText = styled.sup`
//   color: ${(props) => props.theme.palette.primary};
//   font-size: 16px;
//   line-height: 20px;
//
//   margin-left: 4px;
//   margin-right: 10.33px;
// `

// const CareersInfoContainer = styled.div`
//   display: flex;
// `
const ContactInfoTitle = styled(Text)`
  @media screen and ${deviceCssQuery.md} {
    display: none;
  }
`

// const CareersInfoLink = styled(BaseLink)`
//   display: flex;
//   gap: 7px;

//   @media screen and ${deviceCssQuery.xs} {
//     margin-left: auto;
//   }

//   @media screen and ${deviceCssQuery.md} {
//     margin-left: unset;
//   }
// `

const AllRightReservedCont = styled.div`
  text-align: left;
  margin-bottom: 30px;

  @media screen and ${deviceCssQuery.md} {
    text-align: center;
  }
`

export const CompanyInfo: React.FC = () => {
  const { t } = useTranslation('common')
  // const { setContactUsVisibility } = useSetContactUs()
  const { mdAndHigher } = useBreakpoints()

  const currentYear = new Date().getFullYear()

  const AllRightsReserved = (
    <AllRightReservedCont>
      <Text variant="secondaryParagraph" tag="span">
        {currentYear}. {t('footer.allRightsReserved')}
      </Text>
    </AllRightReservedCont>
  )

  const contactInfo = (
    <StyledContactInfoContainer>
      <ContactInfoTitle variant="h3">{t('contactInfo')}</ContactInfoTitle>
      <PhoneLink
        phoneVariant="first"
        textProps={{ variant: 'paragraph', fontWeight: 500 }}
      />
      <PhoneLink
        phoneVariant="second"
        textProps={{ variant: 'paragraph', fontWeight: 500 }}
      />
      <EmailLink textProps={{ variant: 'paragraph' }} />
    </StyledContactInfoContainer>
  )

  // const subscribeInfo = (
  //   <div>
  //     <Text variant="h3" margin="0 0 18px 0">
  //       {t('subscribe')}
  //     </Text>
  //     <SocialLinks distance="29px" />
  //   </div>
  // )

  // const careersInfo = (
  //   <div>
  //     <CareersInfoContainer>
  //       <CareersInfoLink
  //         href={ROUTES.VACANCIES}
  //         onClick={() => setContactUsVisibility(false)}
  //       >
  //         <Text variant="mainButton">{t('careers')}</Text>
  //         {/* Todo: add this line later along with api */}
  //         {/* <CareersCountText>13</CareersCountText> */}
  //         <ArrowIcon color="white" />
  //       </CareersInfoLink>
  //     </CareersInfoContainer>
  //   </div>
  // )

  const desktopContactsInfo = (
    <DesktopContactsContainer>
      <SectionContainer desktopMenuExist={false}>{contactInfo}</SectionContainer>
      <SectionContainer>{AllRightsReserved}</SectionContainer>
    </DesktopContactsContainer>
  )

  const mobileContactsInfo = (
    <StyledOurContactsContent>
      <SectionContainer>
        <MobileBlockContainer>{contactInfo}</MobileBlockContainer>
      </SectionContainer>
      <SectionContainer>{AllRightsReserved}</SectionContainer>
    </StyledOurContactsContent>
  )

  return (
    <StyledOurContacts>
      <Divider />
      {mdAndHigher ? desktopContactsInfo : mobileContactsInfo}
    </StyledOurContacts>
  )
}
