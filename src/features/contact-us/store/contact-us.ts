import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { TAsyncAction, TSelector } from '@/store'
import { TFormPropsAsync } from '@/types/formik'
import { contactUsApi, IContactUsSendReq } from '@/features/contact-us/api'
import { ErrorStatus } from '@/types/error'
import { gmtEvents } from '@/seo/GoogleTagManager/gmt-events'
import axios from 'axios'
import { googleClientId } from '@/browserApi/googleId'

interface IInitialState {
  visible: boolean
  sendFormFetching: boolean
  showSuccessModal: boolean
  formData?: IContactUsSendReq
}

const initialState: IInitialState = {
  visible: false,
  sendFormFetching: false,
  showSuccessModal: false,
  formData: undefined,
}

export const contactUsSlice = createSlice({
  name: 'contactUs',
  initialState,
  reducers: {
    setVisibility(state, action: PayloadAction<boolean>) {
      state.visible = action.payload
      state.showSuccessModal = !action.payload
    },
    setFormFetching(state, action: PayloadAction<boolean>) {
      state.sendFormFetching = action.payload
    },
    setShowSuccessModal(state, action: PayloadAction<boolean>) {
      state.showSuccessModal = action.payload
    },
    setFormData(state, action: PayloadAction<IContactUsSendReq>) {
      state.formData = action.payload
    },
  },
})

export const sliceActions = contactUsSlice.actions
export const selectVisibility: TSelector<boolean> = (state) => state.contactUs.visible
export const selectFormFetching: TSelector<boolean> = (state) =>
  state.contactUs.sendFormFetching
export const selectShowSuccessModal: TSelector<boolean> = (state) =>
  state.contactUs.showSuccessModal

export const selectFormData: TSelector<IContactUsSendReq | undefined> = (state) =>
  state.contactUs.formData

export const sendForm =
  ({
    formData,
    formik,
  }: TFormPropsAsync<IContactUsSendReq> & {
    notificationSuccessText: string
  }): TAsyncAction =>
  async (dispatch) => {
    try {
      const googleId = googleClientId.get()
      dispatch(sliceActions.setFormFetching(true))
      await contactUsApi.contactsSend({
        ...formData,
        ...(googleId && { google_id: googleId }),
      })
      dispatch(sliceActions.setFormData(formData))
      formik.resetForm()
      dispatch(sliceActions.setShowSuccessModal(true))
      gmtEvents({ event: 'form_sent' })
    } catch (e) {
      if (!axios.isAxiosError(e) || !e.response) return
      const { status, data } = e.response

      if (status === ErrorStatus.Validation) {
        formik.setErrors(data.errors)
        return
      }
    } finally {
      dispatch(sliceActions.setFormFetching(false))
    }
  }
