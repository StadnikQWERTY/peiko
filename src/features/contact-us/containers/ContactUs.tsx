import React from 'react'
import { useRedux } from '@/hooks/useRedux'
import { selectShowSuccessModal } from '@/features/contact-us/store/contact-us'
import dynamic from 'next/dynamic'
import { Loader } from '@/components/Loader'

const ContactUsSection = dynamic(() => import('./ContactUsSection/ContactUsSection'), {
  loading: () => <Loader width="50px" height="50px" />,
})

const ContactUsSuccess = dynamic(() => import('./ContactUsSuccess/ContactUsSuccess'), {
  loading: () => null,
})

const ContactUs: React.FC = () => {
  const [useSelector] = useRedux()
  const showSuccessModal = useSelector(selectShowSuccessModal)

  return <>{showSuccessModal ? <ContactUsSuccess /> : <ContactUsSection />}</>
}

export default ContactUs
