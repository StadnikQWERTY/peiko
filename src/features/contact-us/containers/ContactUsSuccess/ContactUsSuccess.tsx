import React, { useEffect, useState } from 'react'
import { SectionContainer } from '@/components/sections/SectionContainer'
import { Text } from '@/components/Text'
import { Spacer } from '@/components/Spacer/Spacer'
import { Divider } from '@/components/Divider'
import { GoogleMapContainer } from '@/components/maps/GoogleMap/GoogleMapContainer'
import { PeikoLocation } from '@/components/PeikoLocation'
import { CompanyInfo } from '@/features/contact-us/components/CompanyInfo'
import styled from 'styled-components'
import { deviceCssQuery } from '@/styling/breakpoints'
import useTranslation from 'next-translate/useTranslation'
import { useBreakpoints } from '@/hooks/useBreakpoints'
import { selectFormData, sliceActions } from '@/features/contact-us/store/contact-us'
import { useRedux } from '@/hooks/useRedux'

const StyledBody = styled.div`
  display: grid;
  height: 100%;

  @media screen and ${deviceCssQuery.xs} {
    grid-template-columns: unset;
    grid-auto-columns: 1fr;
  }

  @media screen and ${deviceCssQuery.md} {
    grid-template-columns: minmax(45%, 55%) 1px minmax(35%, 45%);
    grid-gap: 48px;
  }
`

const StyledForm = styled.div`
  padding: 24px 0 0;
`

const StyledGoogleMapContainer = styled.div`
  max-height: 375px;
  height: 100%;
  width: 100%;
  margin-bottom: 16px;
`

const StyledProgress = styled.progress`
  appearance: none;
  width: 100%;
  height: 16px;
  border: 2px solid #343846;
  border-radius: 0;

  &::-webkit-progress-value {
    background: linear-gradient(109.88deg, #45c0eb 0%, #308fea 100%);
  }
`

const StyledCandidateInfo = styled(Spacer)`
  display: flex;
  flex-direction: column;
  gap: 8px;
`

const MODAL_CLOSE_SEC = 30

const ContactUsSuccess: React.FC = () => {
  const [time, setTime] = useState<number>(MODAL_CLOSE_SEC)
  const { t: tCommon } = useTranslation('common')
  const { t: tForm } = useTranslation('form')
  const [useSelector, dispatch] = useRedux()

  const formData = useSelector(selectFormData)

  const timer = () => {
    setTime((currentTime) => currentTime - 1)
  }

  useEffect(() => {
    setInterval(timer, 1000)
    return () => setTime(MODAL_CLOSE_SEC)
  }, [])

  useEffect(() => {
    if (time < 0) {
      dispatch(sliceActions.setVisibility(false))
    }
  }, [time])

  const { isMobile } = useBreakpoints()

  return (
    <>
      <SectionContainer desktopMenuExist={false}>
        <StyledBody>
          <Spacer xs={{ padding: '0 0 24px' }}>
            <StyledForm>
              <Text variant="h2" margin="0 0 12px">
                {tCommon('messageSendSuccess')}
              </Text>
              <Text variant="paragraph">
                {tCommon('messageSendSuccessText', { name: formData?.name })}
              </Text>
              <StyledCandidateInfo xs={{ margin: '16px 0 0' }}>
                <Text variant="secondaryParagraph">
                  {tForm('yourName')}: {formData?.name}
                </Text>
                <Text variant="secondaryParagraph">
                  {tForm('email')}: {formData?.email}
                </Text>
                <Text variant="secondaryParagraph">
                  {tForm('phoneNumber')}: {formData?.phone}
                </Text>
                <Text variant="secondaryParagraph">
                  {tForm('describeYourIdea')}: {formData?.message}
                </Text>
              </StyledCandidateInfo>
              <Spacer xs={{ padding: '24px 0 14px 0' }}>
                <Text variant="h3">{tCommon('hideMessageText', { time })}</Text>
              </Spacer>
              <StyledProgress
                id="load"
                max={MODAL_CLOSE_SEC}
                value={MODAL_CLOSE_SEC - time}
              />
            </StyledForm>
          </Spacer>

          {!isMobile && (
            <>
              <Divider vertical />

              <Spacer xs={{ padding: '24px 0' }}>
                <StyledGoogleMapContainer>
                  <GoogleMapContainer />
                </StyledGoogleMapContainer>

                <PeikoLocation />
              </Spacer>
            </>
          )}
        </StyledBody>
      </SectionContainer>

      <CompanyInfo />
    </>
  )
}

export default ContactUsSuccess
