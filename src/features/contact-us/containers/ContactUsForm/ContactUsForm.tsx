// Core
import React, { useEffect } from 'react'
import { useRedux } from '@/hooks/useRedux'
import useTranslation from 'next-translate/useTranslation'
import {
  selectFormFetching,
  sendForm,
  selectVisibility,
} from '@/features/contact-us/store/contact-us'

// Validation
import * as yup from 'yup'
import { useFormik } from 'formik'
import { validation } from '@/utils/validation'

// UI
import styled from 'styled-components'
import { deviceCssQuery } from '@/styling/breakpoints'
import { FormikInput } from '@/components/fields/FormikInput/FormikInput'
import { ArrowTextButton } from '@/components/buttons/ArrowTextButton'
import { FormikFileUploader } from '@/components/fields/InputFileUploader'
import { FormikPhoneInput } from '@/components/fields/FormikPhoneInput'

// Other
import { IContactUsSendReq } from '../../api'

const StyledForm = styled.form`
  display: grid;
  grid-gap: 16px;
`

const StyledTwoFields = styled.div`
  display: grid;
  grid-gap: 16px;

  @media screen and ${deviceCssQuery.xs} {
    grid-template-columns: 1fr;
  }

  @media screen and ${deviceCssQuery.lg} {
    grid-template-columns: 1fr 1fr;
  }
`

const StyledSubmitButton = styled(ArrowTextButton)`
  @media screen and ${deviceCssQuery.xs} {
    max-width: 100%;
  }

  @media screen and ${deviceCssQuery.md} {
    max-width: 265px;
  }
`

export const ContactUsForm: React.FC = () => {
  const { t } = useTranslation('form')
  const { t: tCommon } = useTranslation('common')

  const [useSelector, dispatch] = useRedux()
  const formFetching = useSelector(selectFormFetching)
  const visible = useSelector(selectVisibility)

  const formik = useFormik<IContactUsSendReq>({
    initialValues: {
      name: '',
      email: '',
      phone: '',
      message: '',
      attachment: undefined,
    },
    validationSchema: yup.object().shape({
      email: validation.email,
    }),
    onSubmit: (formData) => {
      dispatch(
        sendForm({
          formData,
          formik,
          notificationSuccessText: tCommon('sendFormSuccess'),
        }),
      )
    },
  })

  useEffect(() => {
    if (visible) return
    formik.resetForm()
  }, [visible])

  return (
    <StyledForm onSubmit={formik.handleSubmit}>
      <FormikInput formik={formik} label={t('yourName')} name="name" />
      <StyledTwoFields>
        <FormikInput formik={formik} label={t('email')} name="email" />
        <FormikPhoneInput formik={formik} label={t('phoneNumber')} name="phone" />
      </StyledTwoFields>
      <FormikInput formik={formik} label={t('describeYourIdea')} name="message" />
      <FormikFileUploader formik={formik} name="attachment" />
      <StyledSubmitButton lineAnimated loading={formFetching} type="submit">
        {t('send')}
      </StyledSubmitButton>
    </StyledForm>
  )
}
