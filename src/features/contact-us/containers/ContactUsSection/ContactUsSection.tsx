import React from 'react'
import { SectionContainer } from '@/components/sections/SectionContainer'
import { Text } from '@/components/Text'
import { Spacer } from '@/components/Spacer/Spacer'
import { ClutchReviews } from '@/components/ClutchReviews'
import { ContactUsForm } from '@/features/contact-us/containers/ContactUsForm'
import { Divider } from '@/components/Divider'
import { CompanyInfo } from '@/features/contact-us/components/CompanyInfo'
import styled, { css } from 'styled-components'
import { deviceCssQuery } from '@/styling/breakpoints'
import useTranslation from 'next-translate/useTranslation'
import { useBreakpoints } from '@/hooks/useBreakpoints'
import List from '@/components/List/List'

interface IInfoList {
  list: { start: string; number?: string; end?: string }[]
}

const StyledBody = styled.div`
  display: grid;
  height: 100%;

  @media screen and ${deviceCssQuery.xs} {
    grid-template-columns: unset;
    grid-auto-columns: 1fr;
  }

  @media screen and ${deviceCssQuery.md} {
    grid-template-columns: minmax(45%, 55%) 1px minmax(35%, 45%);
    grid-gap: 48px;
  }
`

const StyledForm = styled.div`
  padding: 24px 0 0;
`

const RightSection = styled.div`
  height: 100%;
  width: 100%;
`

const ClutchBlock = styled.div`
  max-width: 240px;
`

const InfoBlock = styled.div`
  margin-top: 30px;
`

const ListContainer = styled.div`
  margin-top: 18px;
`

const ListItem = styled(Text)<{ type?: 'blue'; marginLeft?: boolean }>(
  (props) => css`
    margin-right: 8px;
    margin-left: ${props.marginLeft ? '16px' : '0'};
    color: ${props.type === 'blue'
      ? props.theme.palette.primary
      : props.theme.palette.white};
  `,
)

const renderList = (list: IInfoList['list']) =>
  list.map((item) => {
    const el = (
      <>
        <ListItem variant="paragraph" tag="p" margin={0} marginLeft>
          {item.start}
        </ListItem>
        {item.number && (
          <ListItem variant="paragraph" tag="p" margin={0} type="blue">
            {item.number}
          </ListItem>
        )}
        {item.end && (
          <ListItem variant="paragraph" tag="p" margin={0}>
            {item.end}
          </ListItem>
        )}
      </>
    )

    return { component: el, id: item.start }
  })

const ContactUsSection: React.FC = () => {
  const { t } = useTranslation('common')
  const { isMobile } = useBreakpoints()

  const list = [
    {
      start: t('contact-us:info_list.1_start'),
      number: t('contact-us:info_list.1_number'),
      end: t('contact-us:info_list.1_end'),
    },
    {
      start: t('contact-us:info_list.2_start'),
      number: t('contact-us:info_list.2_number'),
      end: t('contact-us:info_list.2_end'),
    },
    { start: t('contact-us:info_list.3_start') },
    {
      start: t('contact-us:info_list.4_start'),
      number: t('contact-us:info_list.4_number'),
      end: t('contact-us:info_list.4_end'),
    },
  ]

  return (
    <>
      <SectionContainer desktopMenuExist={false}>
        <StyledBody>
          <Spacer xs={{ padding: '0 0 24px' }}>
            <StyledForm>
              <Text variant="h2" margin="0 0 12px">
                {t('contactUs')}
              </Text>
              <Text variant="paragraph">{t('contact-us:tellAboutYourIdea')}</Text>
              <Spacer xs={{ margin: '16px 0 0' }}>
                <ContactUsForm />
                {/* Todo: may be added in next development phase */}
                {/* <Spacer xs={{ margin: '24px 0 0' }}> */}
                {/*  <TermsAndConditionsNote /> */}
                {/* </Spacer> */}
              </Spacer>
            </StyledForm>
          </Spacer>

          {!isMobile && (
            <>
              <Divider vertical />

              <Spacer xs={{ padding: '30px 0' }}>
                <RightSection>
                  <ClutchBlock>
                    <ClutchReviews />
                  </ClutchBlock>
                  <InfoBlock>
                    <Text variant="h3">{t('contact-us:info_header')}</Text>
                    <ListContainer>
                      <List list={renderList(list)} />
                    </ListContainer>
                  </InfoBlock>
                </RightSection>
              </Spacer>
            </>
          )}
        </StyledBody>
      </SectionContainer>

      <CompanyInfo />
    </>
  )
}

export default ContactUsSection
