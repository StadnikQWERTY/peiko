import { useRedux } from '@/hooks/useRedux'
import { sliceActions, selectVisibility } from '../store/contact-us'

interface IUseSetContactUs {
  setContactUsVisibility: (state: boolean) => void
  visibility: boolean
}

export const useSetContactUs = (): IUseSetContactUs => {
  const [select, dispatch] = useRedux()

  const setContactUsVisibility = (state: boolean) =>
    dispatch(sliceActions.setVisibility(state))

  return {
    setContactUsVisibility,
    visibility: select(selectVisibility),
  }
}
