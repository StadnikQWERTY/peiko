import React from 'react'
import { ThemedSection } from '@/components/ThemedSection'
import { FullScreenPopup } from '@/containers/FullScreenPopup'
import { useRedux } from '@/hooks/useRedux'
import { selectVisibility } from '@/features/contact-us/store/contact-us'
import { useSetContactUs } from './utils/useSetContactUs'
import ContactUs from './containers/ContactUs'

export const ContactUsPopup: React.FC = () => {
  const [useSelector] = useRedux()
  const visible = useSelector(selectVisibility)
  const { setContactUsVisibility } = useSetContactUs()

  return (
    <ThemedSection theme="dark">
      <FullScreenPopup
        isOpened={visible}
        onChangeOpeningState={() => setContactUsVisibility(false)}
      >
        {visible && <ContactUs />}
      </FullScreenPopup>
    </ThemedSection>
  )
}
