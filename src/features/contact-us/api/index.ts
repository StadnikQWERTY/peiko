import { TApiResponse } from '@/requests/types'
import { index } from '@/api/instances/main'
import { RuntimeForm } from '@/utils/RuntimeForm'
import { IContactUsSendReq } from './types'

export * from './types'

const baseUrl = '/contacts'

type TReq = IContactUsSendReq & {
  google_id?: string
}

const contactsSend = (data: TReq): TApiResponse<unknown> =>
  index.post(`${baseUrl}/send`, new RuntimeForm(data).getFormData())

export const contactUsApi = {
  contactsSend,
}
