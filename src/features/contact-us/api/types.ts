export interface IContactUsSendReq {
  name: string
  email: string
  phone: string
  message: string
  attachment?: File | null
}
