import styled from 'styled-components'
import { rgba } from 'polished'
import { Text } from '@/components/Text'
import { HEADER_HEIGHT_DESKTOP } from '@/constants/menu'
import { deviceCssQuery } from '@/styling/breakpoints'
import { FontWeight } from '@/styling/fonts'
import { SectionContainer } from '@/components/sections/SectionContainer'

const PADDING_TOP_XS = '100px'
const PADDING_BOTTOM_XS = '32px'

export const Section = styled(SectionContainer)<{ img: string }>`
  background-size: cover;
  background-repeat: no-repeat;
  background-position: right;

  @media screen and ${deviceCssQuery.xs} {
    background-image: linear-gradient(
        45deg,
        ${(props) => rgba(props.theme.palette.primaryBg || '', 1)},
        ${(props) => rgba(props.theme.palette.primaryBg || '', 0.8)}
      ),
      url('${(props) => props.img}');
  }

  @media screen and ${deviceCssQuery.md} {
    background-image: url('${(props) => props.img}');
  }
`

export const Container = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  box-sizing: border-box;
  padding: ${PADDING_TOP_XS} 0 ${PADDING_BOTTOM_XS};
  min-height: 100vh;

  @media screen and ${deviceCssQuery.md} {
    padding: 80px 0 60px;
    min-height: calc(100vh - ${HEADER_HEIGHT_DESKTOP}px);
    overflow-y: hidden;
  }
`

export const HeaderContainer = styled.div`
  margin-top: 56px;
  text-align: center;

  @media screen and ${deviceCssQuery.md} {
    text-align: left;
    max-width: 650px;
  }
`

export const Header = styled(Text)`
  margin: 0;
`

export const SubtitleContainer = styled(Text)`
  text-align: center;
  margin-top: 40px;
  @media screen and ${deviceCssQuery.md} {
    max-width: 600px;
    text-align: left;
  }
`

export const Subtitle = styled(Text)`
  font-size: 18px;
  line-height: 27px;
  font-weight: ${FontWeight.Medium};
  color: ${(props) => props.theme.palette.white};
  margin: 0;
`

export const ContactButton = styled.div`
  margin-top: 40px;
  text-align: center;

  @media screen and ${deviceCssQuery.md} {
    text-align: left;
  }
`

export const SocialsCont = styled.div`
  display: none;

  @media screen and ${deviceCssQuery.md} {
    display: flex;
    margin-top: 80px;
    align-items: center;
  }
`

export const SocialsItem = styled.div`
  display: flex;
  flex-direction: column;
  margin-right: 60px;

  & > *:last-child {
    margin-top: 10px;
  }
`
