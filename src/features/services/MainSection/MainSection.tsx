import React from 'react'
import useTranslation from 'next-translate/useTranslation'
import { Text } from '@/components/Text'
import { BaseImage } from '@/components/BaseImage'
import { ClutchAward } from '@/components/ClutchAward'
import { Button } from '@/components/buttons/Button'
import * as Styled from './styled'
import { useSetContactUs } from '../../contact-us'

type TGreetingSection = {
  header: string
  description: string
  img: string
  breadcrumbs: JSX.Element
}

export const MainSection: React.FC<TGreetingSection> = (props) => {
  const { header, description, breadcrumbs, img } = props
  const { setContactUsVisibility } = useSetContactUs()
  const { t } = useTranslation('common')

  return (
    <Styled.Section img={img}>
      <Styled.Container>
        {breadcrumbs}

        <Styled.HeaderContainer>
          <Styled.Header
            variant="h1"
            tag="h1"
            margin="20px 0 30px"
            lg={{ fontSize: '65px', lineHeight: '80px' }}
          >
            {header}
          </Styled.Header>
        </Styled.HeaderContainer>

        <Styled.SubtitleContainer>
          <Styled.Subtitle tag="p">{description}</Styled.Subtitle>
        </Styled.SubtitleContainer>

        <Styled.ContactButton color="white">
          <Button
            onClick={() => {
              setContactUsVisibility(true)
            }}
          >
            {t('hireUs')}
          </Button>
        </Styled.ContactButton>

        <Styled.SocialsCont>
          <Styled.SocialsItem>
            <BaseImage
              alt="Clutch"
              src="/images/services/clutch.png"
              width={60}
              height={16}
            />
            <Text variant="thirdParagraph">{t('services:mainBlock.clutchReviews')}</Text>
          </Styled.SocialsItem>

          <Styled.SocialsItem>
            <BaseImage
              alt="Design Rush"
              src="/images/services/rush.png"
              width={109}
              height={16}
            />
            <Text variant="thirdParagraph">{t('services:mainBlock.rushReviews')}</Text>
          </Styled.SocialsItem>

          <ClutchAward variant="transparent" />
        </Styled.SocialsCont>
      </Styled.Container>
    </Styled.Section>
  )
}
