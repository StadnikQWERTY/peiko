import React from 'react'
import { IServicePageFeature } from '@/requests/services/types'
import styled from 'styled-components'
import { Text } from '@/components/Text'
import padNumber from '@/utils/padNumber'

import { deviceCssQuery } from '@/styling/breakpoints'
import { customParser } from '@/utils/customParser'

const Container = styled.div`
  display: grid;

  @media screen and ${deviceCssQuery.xs} {
    grid-template-columns: 1fr;
    gap: 48px;
  }

  @media screen and ${deviceCssQuery.lg} {
    grid-template-columns: 1fr 1fr;
    gap: 96px;
  }
`

const Item = styled.div``

const List = styled.div`
  display: flex;
  flex-direction: column;
  gap: 12px;
`

const ListItem = styled.div`
  display: grid;
  grid-template-columns: 48px 1fr;
  border: 1px solid ${(props) => props.theme.palette.secondary};
`
const ListItemName = styled(Text)`
  color: ${(props) => props.theme.palette.primary};
  letter-spacing: 0.2em;
  text-transform: uppercase;
`

const ListItemNumerable = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  background: ${(props) => props.theme.palette.primary};
  ${ListItemName} {
    color: white;
  }
`

const ListItemText = styled.div`
  padding: 24px;
`

interface OwnProps {
  features: IServicePageFeature[]
  content: string
}

export const Features: React.FC<OwnProps> = (props) => {
  const { features, content } = props
  return (
    <Container>
      <Item>
        <Text variant="paragraph" tag="p">
          {customParser(content)}
        </Text>
      </Item>
      <Item>
        <List>
          {features?.map((item, index) => (
            <ListItem key={item.name}>
              <ListItemNumerable>
                <ListItemName>{padNumber(index + 1)}</ListItemName>
              </ListItemNumerable>
              <ListItemText>
                <ListItemName variant="h4" tag="p">
                  {item.name}
                </ListItemName>
                <br />
                <Text variant="secondaryParagraph" tag="span">
                  {item.description}
                </Text>
              </ListItemText>
            </ListItem>
          ))}
        </List>
      </Item>
    </Container>
  )
}
