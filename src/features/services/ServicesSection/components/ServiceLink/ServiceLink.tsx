import React, { useState } from 'react'
import styled from 'styled-components'
import * as Styled from '@/features/services/ServicesSection/components/ServiceBlock/styled'
import { defaultColorConfig } from '@/styling/theme'
import { deviceCssQuery } from '@/styling/breakpoints'
import { useBreakpoints } from '@/hooks/useBreakpoints'
import useTranslation from 'next-translate/useTranslation'
import { useSetContactUs } from '@/features/contact-us'
import { ArrowIcon } from '@/components/icons/ArrowIcon'
import { Text } from '@/components/Text'

interface OwnProps {
  className?: string
}

const StyledText = styled(Text)`
  color: ${(props) => props.theme.palette.primary};
  margin-right: 7px;
`

const LinkWrapper = styled.button`
  cursor: pointer;
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  border: 1px solid ${(props) => props.theme.palette.secondary};

  @media screen and ${deviceCssQuery.xs} {
    height: 56px;
    background-color: ${(props) => props.theme.palette.primary};

    &:hover {
      background-color: ${(props) => props.theme.palette.primary};
      transition: background-color 300ms;
    }

    & ${StyledText} {
      color: white;
    }
  }

  @media screen and ${deviceCssQuery.lg} {
    height: auto;
    background-color: rgba(0, 0, 0, 0);

    &:hover {
      background-color: ${(props) => props.theme.palette.primary};
      transition: background-color 300ms;
    }

    & ${StyledText} {
      color: ${(props) => props.theme.palette.primary};
    }

    &:hover ${StyledText} {
      color: white;
    }
  }
`

export const ServiceLink: React.FC<OwnProps> = (props) => {
  const { className } = props
  const { currentBreakpoint } = useBreakpoints()
  const { t } = useTranslation('common')
  const { setContactUsVisibility } = useSetContactUs()

  const [hovered, setHovering] = useState(false)

  const handleMouseEnter = () => {
    setHovering(true)
  }

  const handleMouseLeave = () => {
    setHovering(false)
  }

  const arrowTextButtonHover = hovered ? 'white' : defaultColorConfig.primary
  const volumetricArrowIconContainerHover = hovered && (
    <Styled.VolumetricArrowIconContainer />
  )

  return (
    <LinkWrapper
      onClick={() => setContactUsVisibility(true)}
      onMouseEnter={handleMouseEnter}
      onMouseLeave={handleMouseLeave}
      className={className}
    >
      {currentBreakpoint === 'lg' && volumetricArrowIconContainerHover}
      <StyledText variant="mainButton">{t('hireUs')}</StyledText>
      <ArrowIcon color={currentBreakpoint === 'lg' ? arrowTextButtonHover : 'white'} />
    </LinkWrapper>
  )
}
