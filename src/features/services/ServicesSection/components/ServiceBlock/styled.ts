import styled from 'styled-components'
import { deviceCssQuery } from '@/styling/breakpoints'
import { VolumetricArrowIcon } from '@/components/icons/VolumetricArrowIcon'
import { Text } from '@/components/Text'

export const Body = styled.div`
  display: grid;
  grid-gap: 8px;
  grid-auto-rows: max-content;
`

export const StyledText = styled(Text)`
  @media screen and ${deviceCssQuery.xs} {
    width: 100%;
  }

  @media screen and ${deviceCssQuery.lg} {
    width: 80%;
  }
`

export const TextHover = styled(Text)``

export const BodyTitle = styled.div`
  display: flex;
`

export const TitleArrowContainer = styled.div`
  margin-top: 1px;
`

export const ImageContainer = styled.div`
  img {
    object-fit: none;
    object-position: 0 -80px;
    width: 80px;
    height: 80px;
  }

  @media screen and ${deviceCssQuery.sm} {
    margin: 0;
  }

  @media screen and ${deviceCssQuery.md} {
    margin: auto;
  }
`

export const Root = styled.button`
  cursor: pointer;
  position: relative;
  display: grid;
  align-content: flex-start;
  border: 1px solid ${(props) => props.theme.palette.secondary};
  text-align: left;

  @media screen and ${deviceCssQuery.xs} {
    padding: 28px 12px;
    grid-gap: 24px;
    grid-template-columns: 100%;
  }

  @media screen and ${deviceCssQuery.md} {
    padding: 32px;
    grid-gap: 32px;
    grid-template-columns: 80px auto;
  }

  & .service-block-text {
    transition: color 200ms;
  }

  &:hover ${TextHover}, &:hover .service-block-text {
    color: ${(props) => props.theme.palette.primary};
    transition: color 300ms;
  }
`

export const VolumetricArrowIconContainer = styled(VolumetricArrowIcon)`
  position: absolute;
  bottom: 10px;
  right: 10px;

  @media screen and ${deviceCssQuery.xs} {
    max-height: 40px;
  }

  @media screen and ${deviceCssQuery.md} {
    max-height: unset;
  }
`
