import React, { useState } from 'react'
import { IServicesPageService } from '@/api/requests/services/types'
import { ArrowIcon } from '@/components/icons/ArrowIcon'
// import { Hashtags } from '@/components/Hashtags'
import { useRouter } from 'next/router'
import styled, { css } from 'styled-components'
import { ROUTES } from '@/routes'
import { FontWeight } from '@/styling/fonts'
import { deviceCssQuery } from '@/styling/breakpoints'
import Link from 'next/link'
import * as Styled from './styled'

type TServiceBlock = {
  href: string
  service: IServicesPageService
  className?: string
}

// const HashtagsWrapper = styled.div``

const StyledLink = styled.a(
  (props) => css`
    color: ${props.theme.palette.primaryText};
    font-weight: ${FontWeight.SemiBold};
    margin: 0 9.33px 0 0;
    @media screen and ${deviceCssQuery.xs} {
      font-size: 18px;
      line-height: 27px;
    }

    @media screen and ${deviceCssQuery.md} {
      font-size: 20px;
      line-height: 150%;
    }
  `,
)

export const ServiceBlock: React.FC<TServiceBlock> = (props) => {
  const { service, className, href } = props
  const router = useRouter()
  const [hovered, setHovering] = useState(false)

  const handleMouseEnter = () => {
    setHovering(true)
  }

  const handleMouseLeave = () => {
    setHovering(false)
  }

  const link = `${ROUTES.SERVICE}/${href}`

  const handleRootClick = (e: React.SyntheticEvent<HTMLButtonElement>) => {
    const parent = (e.target as HTMLButtonElement).parentNode as HTMLLinkElement
    if (parent.tagName === 'A') return
    router.push(link)
  }

  return (
    <Styled.Root
      className={className}
      onMouseEnter={handleMouseEnter}
      onMouseLeave={handleMouseLeave}
      onClick={handleRootClick}
      role="link"
    >
      {hovered && <Styled.VolumetricArrowIconContainer opacity={1} />}
      <Styled.ImageContainer>
        <img src={service.photo} alt={service.name} title={service.name} />
      </Styled.ImageContainer>

      <Styled.Body>
        <Styled.BodyTitle>
          <Link href={link}>
            <StyledLink href={link} className="service-block-text">
              {service.name}
            </StyledLink>
          </Link>
          <Styled.TitleArrowContainer>
            <ArrowIcon />
          </Styled.TitleArrowContainer>
        </Styled.BodyTitle>
        <Styled.StyledText variant="secondaryParagraph" className="service-block-text">
          {service.preview}
        </Styled.StyledText>
        {
          // <HashtagsWrapper>
          //   <Hashtags generateLink={ROUTES.CASES} tags={service.tags} />
          // </HashtagsWrapper>
        }
      </Styled.Body>
    </Styled.Root>
  )
}
