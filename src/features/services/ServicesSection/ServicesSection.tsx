import React from 'react'
import { IServicesPageService } from '@/api/requests/services/types'
import styled from 'styled-components'
import { deviceCssQuery } from '@/styling/breakpoints'
import { ServiceBlock } from '@/features/services/ServicesSection/components/ServiceBlock/ServiceBlock'
import { ServiceLink } from '@/features/services/ServicesSection/components/ServiceLink/ServiceLink'

type TServicesSection = {
  services: IServicesPageService[]
}

const Services = styled.section`
  display: grid;

  @media screen and ${deviceCssQuery.xs} {
    grid-template-columns: 100%;
  }

  @media screen and ${deviceCssQuery.lg} {
    grid-template-columns: 2fr 1fr;
    margin-left: unset;
  }
`

const ServiceBlockContainer = styled(ServiceBlock)`
  @media screen and ${deviceCssQuery.xs} {
    margin-bottom: -1px;
  }

  @media screen and ${deviceCssQuery.lg} {
    margin-left: -1px;
  }
`

const ServiceLinkContainer = styled(ServiceLink)`
  @media screen and ${deviceCssQuery.xs} {
    margin-bottom: -1px;
  }

  @media screen and ${deviceCssQuery.lg} {
    margin-left: -1px;
  }
`

export const ServicesSection: React.FC<TServicesSection> = (props) => {
  const { services } = props

  return (
    <Services>
      {services.map((service) => (
        <React.Fragment key={service.id}>
          <ServiceBlockContainer service={service} href={service.uri} />
          <ServiceLinkContainer />
        </React.Fragment>
      ))}
    </Services>
  )
}
