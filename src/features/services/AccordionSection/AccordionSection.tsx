import React, { useState } from 'react'
import styled, { CSSProperties } from 'styled-components'
import { Text } from '@/components/Text'
import { SectionTitles } from '@/components/sections/SectionTitles'
import useTranslation from 'next-translate/useTranslation'
import { IServicePageItem } from '@/requests/services/types'

const PlusIcon = () => (
  <svg
    width="20"
    height="20"
    viewBox="0 0 20 20"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <rect x="18" y="9" width="1" height="16" transform="rotate(90 18 9)" fill="#41B6E6" />
    <rect
      x="11"
      y="18"
      width="1"
      height="16"
      transform="rotate(-180 11 18)"
      fill="#41B6E6"
    />
  </svg>
)

const MinusIcon = () => (
  <svg
    width="20"
    height="20"
    viewBox="0 0 20 20"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <rect x="18" y="9" width="1" height="16" transform="rotate(90 18 9)" fill="white" />
  </svg>
)

type TIsOpen = {
  isOpen?: boolean
}

const Container = styled.div<TIsOpen>`
  display: grid;
  grid-template-columns: 1fr;
  gap: 25px;
`

const Item = styled.div<TIsOpen>`
  background-color: ${(props) =>
    props.isOpen ? props.theme.palette.primary : props.theme.palette.primaryBg};
  width: 100%;
  display: flex;
  flex-direction: row;
  border: 1px solid ${(props) => props.theme.palette.secondary};
  padding: 32px;
  cursor: pointer;

  div {
    color: ${(props) => (props.isOpen ? 'white' : props.theme.palette.darkThird)};
  }
`

const Content = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
`

const StyledText = styled(Text)<{ display: CSSProperties['display'] }>`
  display: ${(props) => props.display || 'block'};
`

const CounterText = styled(Text)<TIsOpen>`
  color: ${(props) => (props.isOpen ? 'white' : props.theme.palette.primary)} !important;
`

const FaqItem = (props: { id: number; name: string; description: string }) => {
  const { id, description, name } = props
  const [showDescription, setShowDescription] = useState<boolean>(false)
  return (
    <Item onClick={() => setShowDescription(!showDescription)} isOpen={showDescription}>
      <Content>
        <CounterText variant="paragraph" isOpen={showDescription}>
          0{id}/
        </CounterText>
        <Text variant="h3">{name}</Text>
        <StyledText display={showDescription ? 'block' : 'none'} variant="paragraph">
          {description}
        </StyledText>
      </Content>
      {showDescription ? <MinusIcon /> : <PlusIcon />}
    </Item>
  )
}

interface OwnProps {
  accordion: IServicePageItem[]
  header: string
}

const AccordionSection: React.FC<OwnProps> = (props) => {
  const { accordion, header } = props
  const { t } = useTranslation('services')
  return (
    <>
      <SectionTitles header={header} name={t('findAnAnswer')} tagHeader="h2" />
      <Container>
        {accordion.map((item, index) => {
          const { name, description } = item
          return (
            <FaqItem key={name} description={description} id={index + 1} name={name} />
          )
        })}
      </Container>
    </>
  )
}

export default AccordionSection
