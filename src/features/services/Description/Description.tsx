import React from 'react'
import styled from 'styled-components'
import { Text } from '@/components/Text'
import { deviceCssQuery } from '@/styling/breakpoints'
import { customParser } from '@/utils/customParser'

const TextContainer = styled.div`
  display: grid;
  grid-gap: 200px;

  @media screen and ${deviceCssQuery.xs} {
    grid-template-columns: 100%;
  }

  @media screen and ${deviceCssQuery.lg} {
    grid-template-columns: 1fr;
  }
`

interface OwnProps {
  description: string
}

export const Description: React.FC<OwnProps> = (props) => {
  const { description } = props
  return (
    <TextContainer>
      <Text variant="paragraph">{customParser(description)}</Text>
    </TextContainer>
  )
}
