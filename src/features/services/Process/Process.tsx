import React from 'react'
import styled from 'styled-components'
import useTranslation from 'next-translate/useTranslation'
import { Text } from '@/components/Text'
import { BaseImage } from '@/components/BaseImage'
import { IServicePageStep } from '@/requests/services/types'
import { deviceCssQuery } from '@/styling/breakpoints'
import { ArrowTextButton } from '@/components/buttons/ArrowTextButton'
import { useSetContactUs } from '@/features/contact-us/utils/useSetContactUs'
import { customParser } from '@/utils/customParser'

const TextContainer = styled.div`
  display: grid;
  margin-top: 25px;
  grid-template-columns: repeat(auto-fit, minmax(280px, 1fr));

  @media screen and ${deviceCssQuery.xs} {
    gap: 24px;
  }

  @media screen and ${deviceCssQuery.lg} {
    gap: 56px;
  }
`

const Item = styled.div``

const IconCont = styled.div`
  margin-bottom: 10px;
`

// contact
const ContactContainer = styled.div`
  display: flex;
  flex-direction: column;
  position: relative;
  background: ${(props) => props.theme.palette.gradientBlue};
  border-radius: 2px;

  @media screen and ${deviceCssQuery.xs} {
    padding: 24px;
  }

  @media screen and ${deviceCssQuery.sm} {
    padding: 40px;
  }
`

const ContactHeader = styled(Text)`
  color: ${(props) => props.theme.palette.white};
  padding-right: 24px;
  margin-bottom: 8px;

  @media screen and ${deviceCssQuery.xs} {
    padding-right: 56px;
  }

  @media screen and ${deviceCssQuery.sm} {
    padding-right: 24px;
  }
`

const ContactDesc = styled(Text)`
  color: ${(props) => props.theme.palette.white};
  margin-bottom: 24px;

  @media screen and ${deviceCssQuery.xs} {
    padding-right: 56px;
  }

  @media screen and ${deviceCssQuery.sm} {
    padding-right: 24px;
  }
`

export const ContactButton = styled(ArrowTextButton)`
  margin-top: auto;
  padding: 16px 24px;
  width: fit-content;
  background: ${(props) => props.theme.palette.white};
`

export const ContactButtonText = styled(Text)`
  color: ${(props) => props.theme.palette.primary};
`

export const ContactIcon = styled.div`
  position: absolute;
  top: 20px;
  right: 0;
`

interface OwnProps {
  steps: IServicePageStep[]
}

const Process: React.FC<OwnProps> = (props) => {
  const { t } = useTranslation('services')
  const { steps } = props
  const { setContactUsVisibility } = useSetContactUs()

  const getImageLink = (n: number) => {
    const link = [
      '/images/cubes/cubes-1.png',
      '/images/cubes/cubes-2.png',
      '/images/cubes/cubes-3.png',
    ]

    const { length } = link
    const index = length - (Math.ceil(n / length) * length - n)

    return link[index - 1]
  }

  const renderContact = () => (
    <ContactContainer>
      <ContactHeader variant="h3">{t('process.contactHeader')}</ContactHeader>
      <ContactDesc variant="paragraph">{t('process.contactDesc')}</ContactDesc>
      <ContactButton
        onClick={() => {
          setContactUsVisibility(true)
        }}
      >
        <ContactButtonText>{t('common:getInTouch')}</ContactButtonText>
      </ContactButton>
      <ContactIcon>
        <BaseImage
          src="/images/services/process-contact-icon.png"
          width={63}
          height={94}
        />
      </ContactIcon>
    </ContactContainer>
  )

  return (
    <TextContainer>
      {steps?.map((step, index) => {
        const { description, name } = step

        return (
          <Item key={name}>
            <IconCont>
              <BaseImage src={getImageLink(index + 1)} width={60} height={60} />
            </IconCont>
            <Text variant="h3" tag="span">
              {name}
            </Text>
            <br />
            <Text variant="paragraph" tag="div">
              {customParser(description)}
            </Text>
          </Item>
        )
      })}
      {renderContact()}
    </TextContainer>
  )
}

export default Process
