import styled from 'styled-components'
import { Text } from '@/components/Text'
import React from 'react'
import { ITechnology } from '@/requests/types'
import useTranslation from 'next-translate/useTranslation'

const TeamContainer = styled.ul`
  padding: 0;
  list-style-type: none;
  text-align: end;

  li:first-child {
    color: ${(props) => props.theme.palette.secondaryLightText};
  }
`

const TeamItem = styled(Text)`
  margin: 16px 0;
  letter-spacing: 0.2em;
  text-transform: uppercase;
  font-weight: 600;
`

interface OwnProps {
  team: ITechnology[]
}

const TeamSection: React.FC<OwnProps> = (props) => {
  const { team } = props
  const { t } = useTranslation('services')
  return (
    <TeamContainer>
      <TeamItem variant="h4" tag="h2">
        {t('team')}
      </TeamItem>
      {team.map((item) => (
        <TeamItem tag="li" key={item.id}>
          {item.name}
        </TeamItem>
      ))}
    </TeamContainer>
  )
}

export default TeamSection
