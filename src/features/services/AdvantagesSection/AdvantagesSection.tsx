import React from 'react'
import styled from 'styled-components'
import { IServicePageAdvantage } from '@/requests/services/types'
import { FontStyle, FontWeight } from '@/styling/fonts'
import { defaultColorConfig } from '@/styling/theme'
import { Text } from '@/components/Text'
import { deviceCssQuery } from '@/styling/breakpoints'

type TNumberText = {
  color: string
}

const Container = styled.div`
  display: grid;
  gap: 72px;

  @media screen and ${deviceCssQuery.xs} {
    grid-template-columns: 1fr;
    gap: 36px;
  }

  @media screen and ${deviceCssQuery.sm} {
    grid-template-columns: 1fr 1fr;
    gap: 36px;
  }

  @media screen and ${deviceCssQuery.md} {
    grid-template-columns: 1fr 1fr 1fr;
    gap: 72px;
  }
`

const Item = styled.div`
  display: flex;
  flex-direction: column;
`

const NumberCont = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  background: ${(props) => props.theme.palette.white};

  @media screen and ${deviceCssQuery.xs} {
    width: 40px;
    height: 40px;
    border-radius: 20px;
  }

  @media screen and ${deviceCssQuery.md} {
    width: 60px;
    height: 60px;
    border-radius: 30px;
  }
`

const NumberText = styled(Text)<TNumberText>`
  ${(props) => `background: ${props.color}`};
  -webkit-background-clip: text;
  -moz-background-clip: text;
  -webkit-text-fill-color: transparent;
  -moz-text-fill-color: transparent;
  font-style: ${FontStyle.Normal};
  font-weight: ${FontWeight.Medium};

  @media screen and ${deviceCssQuery.xs} {
    font-size: 18px;
    line-height: 30px;
  }

  @media screen and ${deviceCssQuery.md} {
    font-size: 25px;
  }
`

const Header = styled(Text)`
  letter-spacing: 0.2em;
  text-transform: uppercase;
  color: ${(props) => props.theme.palette.primaryText};

  @media screen and ${deviceCssQuery.xs} {
    margin-top: 16px;
  }

  @media screen and ${deviceCssQuery.md} {
    margin-top: 24px;
  }
`

const Description = styled(Text)`
  margin-top: 16px;
`

interface OwnProps {
  advantages: IServicePageAdvantage[]
}

const AdvantagesSection: React.FC<OwnProps> = (props) => {
  const { advantages } = props

  const getNumber = (x: number) => (x < 10 ? `0${x}` : x)

  const getColor = (n: number) => {
    const colors = [
      defaultColorConfig.gradientBlue,
      defaultColorConfig.gradientPurple,
      defaultColorConfig.gradientOrange,
      defaultColorConfig.gradientGreen,
    ]

    const { length } = colors
    const index = length - (Math.ceil(n / length) * length - n)

    return colors[index - 1]
  }

  return (
    <Container>
      {advantages.map((advantage, index) => (
        <Item key={advantage.id}>
          <NumberCont>
            <NumberText variant="h4" tag="span" color={getColor(index + 1)}>
              {getNumber(index + 1)}
            </NumberText>
          </NumberCont>

          <Header variant="h4">{advantage.name}</Header>
          <Description variant="secondaryParagraph">{advantage.description}</Description>
        </Item>
      ))}
    </Container>
  )
}

export default AdvantagesSection
