import { HeaderMobile } from '@/components/HeaderMobile'
import { useIsomorphicLayoutEffect } from '@/hooks/use-isomorphic-layout-effect'
import styled from 'styled-components'
import { Z_INDEX } from '@/styling/z-index'
import { ANIMATE_DURATION } from '@/constants/menu'
import { useScrollLock } from '@/hooks/use-scroll-lock'

type TFullScreenPopup = {
  isOpened: boolean
  onChangeOpeningState: () => void
}

type TStyledRoot = {
  isOpened: boolean
}

const StyledRoot = styled.div<TStyledRoot>`
  position: fixed;
  z-index: ${Z_INDEX.FULLSCREEN_POPUP};
  width: 100%;
  left: 0;
  background: ${(props) => props.theme.palette.primaryBg};
  height: var(--window-height);
  transition: bottom ${ANIMATE_DURATION}s;
  bottom: ${(props) => (props.isOpened ? 0 : `calc(var(--window-height) * -1)`)};
  animation: ${(props) =>
    props.isOpened
      ? `open ${ANIMATE_DURATION}s forwards`
      : `close ${ANIMATE_DURATION}s forwards`};

  @keyframes open {
    0% {
      opacity: 0;
    }
    1%,
    100% {
      opacity: 1;
    }
  }

  @keyframes close {
    0%,
    99% {
      opacity: 1;
    }
    100% {
      opacity: 0;
    }
  }
`

const StyledBody = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
  overflow-y: auto;
  overflow-x: hidden;
  -webkit-overflow-scrolling: touch;
`

export const FullScreenPopup: React.FC<TFullScreenPopup> = ({
  isOpened,
  onChangeOpeningState,
  children,
}) => {
  const { scrollRef, setScrollLock } = useScrollLock()

  useIsomorphicLayoutEffect(() => {
    setScrollLock(isOpened)

    if (isOpened) {
      scrollRef.current?.scrollTo({ top: 0 })
    }
  }, [isOpened])

  return (
    <StyledRoot isOpened={isOpened}>
      <StyledBody ref={scrollRef}>
        <HeaderMobile isOpened onChangeOpeningState={onChangeOpeningState} />
        {children}
      </StyledBody>
    </StyledRoot>
  )
}
