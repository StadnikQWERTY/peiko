import React from 'react'
import { ProductJsonLd } from 'next-seo'
import { ICase } from '@/requests/types'

type TCasesLD = {
  data: ICase[]
}

export const CasesLD = ({ data }: TCasesLD): JSX.Element => (
  <>
    {data.map((item) => (
      <ProductJsonLd
        key={item.id}
        url={`${process.env.NEXT_PUBLIC_APP_URL}/${item.uri}`}
        sku={item.uri}
        productId={item.id}
        images={[item.photo]}
        productName={item.name}
        description={item.preview}
        brand="Peiko"
        keyOverride={item.id}
      />
    ))}
  </>
)
