import React from 'react'
import { IServicePageItem } from '@/requests/services/types'
import { FAQPageJsonLd } from 'next-seo'

type TServiceLD = {
  data: IServicePageItem[]
}

export const ServiceLD = ({ data }: TServiceLD): JSX.Element => (
  <FAQPageJsonLd
    mainEntity={data.map((item) => ({
      questionName: item.name,
      acceptedAnswerText: item.description,
    }))}
  />
)
