type TProps = {
  event: 'form_sent'
}

type TGtmEvents = (props: TProps) => void

export const gmtEvents: TGtmEvents = (props) => {
  if (!window.dataLayer) return

  window.dataLayer.push({ event: props.event })
}
