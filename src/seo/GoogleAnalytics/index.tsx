import { GOOGLE_ANALYTICS_ID } from '@/constants/publicInfo'

export const GoogleAnalytics = (): JSX.Element => {
  if (!GOOGLE_ANALYTICS_ID) return <></>

  return (
    <>
      <script
        async
        src={`https://www.googletagmanager.com/gtag/js?id=${GOOGLE_ANALYTICS_ID}`}
      />
      <script
        dangerouslySetInnerHTML={{
          __html: `
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', '${GOOGLE_ANALYTICS_ID}');
    `,
        }}
      />
    </>
  )
}
