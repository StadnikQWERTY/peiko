import React from 'react'
import { NextSeo, OrganizationJsonLd, WebPageJsonLd } from 'next-seo'
import { useRouter } from 'next/router'
import { MetaTag, OpenGraph } from 'next-seo/lib/types'
import useTranslation from 'next-translate/useTranslation'
import { BASE_APP_URL, NO_INDEX } from '@/constants/publicInfo'
import { IMeta, IMicroData, TLocales } from '@/requests/types'
import { getRouteLocale } from '@/utils/get-route-locale'
import { removeTrailingSlash } from '@/utils/remove-trailing-slash'
import { getCannonical } from '../utils/get-cannonical'
import { getAlternate } from '../utils/get-alternate'

type TBaseSeo = {
  meta: IMeta
  microdata?: IMicroData
  serverLocales?: TLocales
  noindex?: boolean
  nofollow?: boolean
  canonical?: string
}

export const BaseSeo: React.FC<TBaseSeo> = (props) => {
  const { meta, microdata, serverLocales, nofollow, noindex, canonical } = props
  const router = useRouter()
  const { lang } = useTranslation()

  const openGraphUrl = `${BASE_APP_URL}${getRouteLocale(lang)}${router.asPath}`

  const openGraph: OpenGraph = {
    type: 'website',
    title: meta.title,
    description: meta.description,
    images: [{ url: `${BASE_APP_URL}/images/peiko-social.png` }],
    locale: lang,
    url: removeTrailingSlash(openGraphUrl),
  }

  const additionalMetaTags: ReadonlyArray<MetaTag> = [
    {
      property: 'keywords',
      content: meta.keywords,
    },
  ]

  const MicroData = () => {
    if (microdata) {
      return (
        <>
          {microdata.organization && <OrganizationJsonLd {...microdata.organization} />}
          {microdata.webpage && <WebPageJsonLd {...microdata.webpage} />}
        </>
      )
    }
    return null
  }

  return (
    <>
      <NextSeo
        title={meta.title}
        description={meta.description}
        openGraph={openGraph}
        canonical={canonical || getCannonical({ lang, asPath: router.asPath })}
        additionalMetaTags={additionalMetaTags}
        noindex={NO_INDEX || noindex}
        nofollow={NO_INDEX || nofollow}
        languageAlternates={getAlternate({
          router,
          serverLocales,
        })}
      />
      <MicroData />
    </>
  )
}
