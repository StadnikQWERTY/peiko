import { MetaTag } from 'next-seo/lib/types'

export const additionalMetaTags: ReadonlyArray<MetaTag> = [
  {
    property: 'mobile-web-app-capable',
    content: 'yes',
  },
  {
    property: 'apple-mobile-web-app-capable',
    content: 'yes',
  },
  {
    property: 'application-name',
    content: 'Peiko',
  },
  {
    property: 'apple-mobile-web-app-title',
    content: 'Peiko',
  },
  {
    property: 'apple-mobile-web-app-capable',
    content: 'yes',
  },
]
