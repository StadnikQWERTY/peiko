import { BASE_APP_URL } from '@/constants/publicInfo'
import { getRouteLocale } from '@/utils/get-route-locale'
import { NextRouter } from 'next/router'
import { removeTrailingSlash } from '@/utils/remove-trailing-slash'
import { TLocales } from '@/requests/types'
import { ParsedUrlQuery } from 'querystring'

import i18nConfig from '../../../i18n.js'

type TProps = {
  serverLocales?: TLocales
  router: NextRouter
}

type TLangAlternates = {
  hrefLang: string
  href: string
}[]

type TGetAlternate = (props: TProps) => TLangAlternates

type TReplaceProps = {
  router: NextRouter
  slug: string
}

const isPaggination = (query: ParsedUrlQuery) => {
  if (query.page) return true
  if (!query.props || typeof query.props === 'string') return false
  const hasPage = query.props.some((prop) => !!Number(prop))
  return hasPage
}

const replaceSlug = ({ router, slug }: TReplaceProps) => {
  const { pathname } = router
  return pathname.replace(/\[.*]/, slug)
}

export const getAlternate: TGetAlternate = ({ serverLocales, router }) => {
  let configLocales = i18nConfig.locales
  const configDefaultLocale = i18nConfig.defaultLocale

  const result: TLangAlternates = []

  // **************
  // For paggination do not create alternate links
  // **************

  if (isPaggination(router.query)) {
    return result
  }

  // **************
  // If there are no slugs from the back
  // **************

  if (!serverLocales) {
    configLocales = ['x-default', ...configLocales]

    configLocales.forEach((lang) => {
      const pathLang = lang === 'x-default' ? configDefaultLocale : lang
      const link = {
        hrefLang: lang,
        href: removeTrailingSlash(
          `${BASE_APP_URL}${getRouteLocale(pathLang)}${router.asPath}`,
        ),
      }
      result.push(link)
    })

    return result
  }

  // **************
  // If there are slugs from the back we need replace slugs
  // **************

  if (Object.keys(serverLocales).length === 1) {
    return result
  }

  const serverDefaultSlug = serverLocales[configDefaultLocale]

  if (serverDefaultSlug) {
    configLocales = ['x-default', ...configLocales]
  }

  configLocales.forEach((lang) => {
    const serverSlug = lang === 'x-default' ? serverDefaultSlug : serverLocales[lang]

    if (!serverSlug) return

    const path = replaceSlug({ router, slug: serverSlug })
    const pathLang = lang === 'x-default' ? configDefaultLocale : lang

    const link = {
      hrefLang: lang,
      href: removeTrailingSlash(`${BASE_APP_URL}${getRouteLocale(pathLang)}${path}`),
    }
    result.push(link)
  })

  return result
}
