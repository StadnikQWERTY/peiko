import { BASE_APP_URL } from '@/constants/publicInfo'
import { getRouteLocale } from '@/utils/get-route-locale'
import { removeTrailingSlash } from '@/utils/remove-trailing-slash'

type TProps = {
  lang: string
  asPath: string
}

type TGetCannonical = (props: TProps) => string

export const getCannonical: TGetCannonical = ({ lang, asPath }) => {
  const getPath = () => {
    // remove params
    const withoutParams = asPath.split('?')[0]
    // get path props
    const pathProps = withoutParams.split('/')
    // remove paggination from path props if prop is last
    const withoutPaggination = pathProps.map((prop, index) => {
      if (index === pathProps.length - 1 && Number(prop)) return ''
      return prop
    })
    return withoutPaggination.join('/')
  }

  const path = getPath()

  const cannonical = `${BASE_APP_URL}${getRouteLocale(lang)}${path}`

  return removeTrailingSlash(cannonical)
}
