declare module '*.scss'

type StaticImageData = {
  src: string
  height: number
  width: number
  placeholder?: string
}

export declare interface google {
  maps: typeof google.maps
}

type NonUndefined<T> = T extends undefined ? never : T

declare global {
  interface Window {
    THREE: any
  }
}

declare global {
  interface Window {
    dataLayer: Record<string, any>[]
  }
}
