export interface ILocation {
  id: string
  location: string
  img: string
  phone: string
}
