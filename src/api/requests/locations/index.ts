import { TApiResponse } from '@/requests/types'
import { index } from '@/api/instances/main'
import { ILocation } from './types'

const getLocations = (locale: string): TApiResponse<ILocation[]> =>
  index.get(`/locations`, {
    headers: {
      Language: locale,
    },
  })

export const locationsApi = {
  getLocations,
}
