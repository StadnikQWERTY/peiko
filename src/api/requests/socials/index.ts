import { TApiResponse } from '@/requests/types'
import { index } from '@/api/instances/main'
import { ISocialData } from '@/requests/socials/types'

const getSocials = (): TApiResponse<ISocialData[]> => index.get(`/socials`)

export const socialApi = {
  getSocials,
}
