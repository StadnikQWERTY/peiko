export interface ISocialData {
  id: number
  url: string
  icon: string
  alt: string
}
