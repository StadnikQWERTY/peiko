import { IMeta, ISubService, TLocales, IContent, IMicroData } from '@/requests/types'

export interface IBlogPageResponse {
  content: IBlogContent
  articles: IBlogArticle[]
  pager: IBlogPager
  categories: IBlogCategory[]
  tags: IBlogCategory[]
  noindex: boolean
}

export interface IBlogArticle {
  id: number
  name: string
  uri: string
  category: string
  photo: string
  date: string
  views: number
  gradient: string
}

export interface IBlogCategory {
  id: number
  name: string
  uri: string
}

export interface IBlogContent {
  header: string
  sub_header: null
  meta: IMeta
}

export interface IBlogPager {
  total: number
  limit: number
  page: number
  offset: number
}

// article

export interface IArticlePageResponse {
  id: number
  name: string
  uri: string
  category: string
  photo: string
  date: string
  views: number
  gradient: string
  meta: IMeta
  blocks: IArticlePageBlock[]
  similar: IArticlePageSimilar[]
  locales: TLocales
  microdata: IMicroData
  content: IContent
  noindex: boolean
}

export interface IArticlePageBlock {
  component: string
  header?: IArticlePageHeader
  description?: string
  slides?: IArticlePageSlide[]
  content?: string
  author?: string
  list_items?: string[]
  list_tag?: string
  before_list_text?: string
  after_list_text?: string
  before_desc?: string
  items?: IArticlePageItem[]
  after_desc?: string
  table_rows?: Array<IArticlePageTableRow[]>
  table_html?: string
  services?: ISubService[]
  code?: string
}

export interface IArticlePageHeader {
  content: null | string
  tag: string
}

export interface IArticlePageItem {
  title: string
  theses: string[]
}

export interface IArticlePageSlide {
  id: number
  url: string
  alt: string
}

export interface IArticlePageTableRow {
  content: string
}

export interface IArticlePageSimilar {
  id: number
  name: string
  uri: string
  category: string
  photo: string
  date: string
  views: number
  gradient: string
}
