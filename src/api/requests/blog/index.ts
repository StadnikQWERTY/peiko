import { TApiResponse, TLang, IUri } from '@/requests/types'
import { index } from '@/api/instances/main'
import { IArticlePageResponse, IBlogPageResponse } from '@/requests/blog/types'
import qs from 'qs'

const getBlog = (props: {
  locale: TLang
  tags?: string
  page?: string
}): TApiResponse<IBlogPageResponse> =>
  index.get('/blog', {
    headers: {
      Language: props.locale,
    },
    params: {
      tags: props.tags?.split('_'),
      page: props.page,
    },
    paramsSerializer: (params) => qs.stringify(params),
  })

const getArticle = (props: {
  locale: TLang
  uri: string
}): TApiResponse<IArticlePageResponse> =>
  index.get(`/article/${props.uri}`, {
    headers: {
      Language: props.locale,
    },
  })

const getBlogUris = (): TApiResponse<IUri[]> => index.get('/blog/uris')

const getViews = (params: number[]): TApiResponse<{ [key: number]: number }> =>
  index.get('/blog/views', { params: { ids: params } })

export const blogApi = {
  getBlog,
  getArticle,
  getBlogUris,
  getViews,
}
