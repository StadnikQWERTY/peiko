import { IUri, TApiResponse, TLang } from '@/api/requests/types'
import {
  IServicePageResponse,
  IServicesPageResponse,
  TServiceCategories,
} from '@/api/requests/services/types'
import { index } from '../../instances/main'

const getServices = (locale: TLang): TApiResponse<IServicesPageResponse> =>
  index.get('/services', {
    headers: {
      Language: locale,
    },
  })

const getServicesUris = (): TApiResponse<IUri[]> => index.get('/services/uris')

const getService = (props: {
  locale: TLang
  name: string
}): TApiResponse<IServicePageResponse> =>
  index.get(`/service/${props.name}`, {
    headers: {
      Language: props.locale,
    },
  })

const getServicesCategories = (locale: TLang): TApiResponse<TServiceCategories> =>
  index.get('/services/categories', {
    headers: {
      Language: locale,
    },
  })

export const servicesApi = {
  getServices,
  getServicesUris,
  getService,
  getServicesCategories,
}
