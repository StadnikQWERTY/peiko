import {
  ICase,
  IContent,
  IMeta,
  IMicroData,
  IReview,
  ISubService,
  ISwitchPageLink,
  ITechnology,
  TLocales,
  IWhyUs,
  IKeyFeatures,
  ICooperation,
  IReward,
} from '@/requests/types'

export interface IServicesPageResponse {
  microdata: IMicroData
  content: IContent
  services: IServicesPageService[]
  technologies: IServicesPageTechnology[]
  noindex: boolean
}

export interface ITag {
  id: number
  uri: string
  name: string
}

export interface IServicesPageService {
  id: number
  uri: string
  name: string
  preview: string
  photo: string
  tags: ITag[]
}

export interface IServicesPageTechnology {
  id: number
  name: string
  photo: string
}

// main
export interface IServicePageResponse {
  microdata: IMicroData
  name: string
  photo: string
  noindex: boolean
  blocks: IServicePageBlock[]
  meta: IMeta
  prev: ISwitchPageLink
  next: ISwitchPageLink
  parent: IServicePageParent | null
  locales: TLocales
  description: string
}

export interface IServicePageParent {
  id: number
  name: string
  uri: string
}

export interface IServicePageFeature {
  name: string
  description: string
}

export interface IServicePageAdvantage {
  id: number
  photo: string
  name: string
  description: string
}

export interface IServicePageBlock {
  component:
    | 'sub-services'
    | 'description'
    | 'team'
    | 'rate-services'
    | 'process'
    | 'additional-services'
    | 'cases'
    | 'technologies'
    | 'advantages'
    | 'approach'
    | 'features'
    | 'accordion'
    | 'reviews'
    | 'comparison'
    | 'suggest-services'
    | 'why-us'
    | 'key-features'
    | 'cooperation'
    | 'awards'
  header?: string
  description?: string
  services?: ISubService[]
  team?: ITechnology[]
  items?: IServicePageItem[]
  content?: string
  steps?: IServicePageStep[]
  cases?: ICase[]
  technologies?: ITechnology[]
  advantages?: IServicePageAdvantage[]
  features?: IServicePageFeature[]
  reviews?: IReview[]
  options?: IWhyUs[]
  keyFeatures?: IKeyFeatures[]
  cooperation?: ICooperation[]
  photos?: IReward[]
}

export interface IServicePageItem {
  id: number
  title: string
  url: string
  photo: string
  // todo remove and create another interface
  name: string
  description: string
}

export interface IServicePageService {
  id: number
  name: string
  uri: string
  tags?: ITag[]
  description?: string
  icon?: string
}

export interface IServicePageStep {
  name: string
  description: string
}

export type TServiceCategories = {
  id: number
  uri: string
  name: string
}[]
