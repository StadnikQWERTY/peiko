import { IMeta, IMicroData, IReview, ISwitchPageLink, ITag } from '@/requests/types'

// cases page

export interface ICasesPageResponse {
  content: ICasesPageContent
  cases: ICasesPageCase[]
  tags: ITag[]
  pager: ICasesPagePager
}

export interface ICasesPageCase {
  id: number
  uri: string
  name: string
  preview: string
  short_preview: string
  photo: string
  tags: ITag[]
  apple_store_link: null | string
  play_market_link: null | string
}

export interface ICasesPageContent {
  header: string
  sub_header: string
  meta: IMeta
}

export interface ICasesPagePager {
  total: number
  limit: number
  page: number
  offset: number
}

// case page

export interface ICasePageResponse {
  url?: string
  microdata: IMicroData
  name: string
  header: string
  preview: string
  gradient: string
  prologue: string
  task: ICasePageSolution
  solution: ICasePageSolution
  result: ICasePageResult
  photo: string
  apple_store_link: string
  is_mobile: boolean
  play_market_link: string
  tags: ITag[]
  options: ICasePageOption[]
  sprints: string
  meta: IMeta
  technologies: ICasePageTechnology[]
  slider: ICasePageSlider[]
  review: IReview
  community: ICasePageCommunity
  prev: ISwitchPageLink
  next: ISwitchPageLink
  noindex: boolean
}

export interface ICasePageCommunity {
  id: number
  title: string
  description: string
  photo: string
  url: string
}

export interface ICasePageOption {
  param: string
  title: string
  value: string
}

export interface ICasePageResult {
  description: string
  checklist: ICasePageResultChecklist[]
}

export interface ICasePageResultChecklist {
  name: string
  description: string
}

export interface ICasePageSlider {
  photo: string
  alt: string
}

export interface ICasePageSolution {
  description: string
  checklist: ICasePageSolutionChecklist[]
}

export interface ICasePageSolutionChecklist {
  name: string
}

export interface ICasePageTechnology {
  id: number
  name: string
  photo: string
}
