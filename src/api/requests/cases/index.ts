import { IUri, TApiResponse, TLang } from '@/api/requests/types'
import { ICasePageResponse, ICasesPageResponse } from '@/requests/cases/types'
import { index } from '../../instances/main'

const getCases = (locale: TLang): TApiResponse<ICasesPageResponse> =>
  index.get('/cases', {
    headers: {
      Language: locale,
    },
  })

const getCasesUris = (): TApiResponse<IUri[]> => index.get('/cases/uris')

const getCase = (props: {
  locale: TLang
  name: string
}): TApiResponse<ICasePageResponse> =>
  index.get(`/case/${props.name}`, {
    headers: {
      Language: props.locale,
    },
  })

export const casesApi = {
  getCases,
  getCasesUris,
  getCase,
}
