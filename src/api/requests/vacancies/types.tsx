import { IMeta, IVacancy } from '@/requests/types'

// vacancies
export interface IVacanciesPageResponse {
  content: IVacanciesPageContent
  vacancies: IVacancy[]
}

export interface IVacanciesPageContent {
  header: string
  sub_header: string
  meta: IMeta
}

// vacancy
export interface IVacancyPageResponse {
  id: number
  name: string
  category: string
  uri: string
  location: string
  is_hot: boolean
  schedule: string
  content: string
  meta: IMeta
  lists: IVacancyPageList[]
  map_location: string
  map_address: string
  employee: IVacancyPageEmployee
}

export interface IVacancyPageEmployee {
  name: string
  position: string
  email: string
  photo: string
}

export interface IVacancyPageList {
  group: string
  items: string[]
}

// form

export interface IApplyToPositionSendReq {
  name: string
  surname: string
  email: string
  phone: string
  message: string
  vacancy_id: number
  attachment?: File | null
}

export interface IRecommendAFriendSendReq {
  name: string
  surname: string
  email: string
  phone: string
  message: string
  friend_name: string
  friend_lastname: string
  friend_email: string
  friend_phone: string
  vacancy_id: number
  attachment?: File | null
}
