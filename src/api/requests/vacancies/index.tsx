import { IUri, TApiResponse, TLang } from '@/requests/types'
import { index } from '@/api/instances/main'
import {
  IApplyToPositionSendReq,
  IRecommendAFriendSendReq,
  IVacanciesPageResponse,
  IVacancyPageResponse,
} from '@/requests/vacancies/types'
import { RuntimeForm } from '@/utils/RuntimeForm'

const getVacancies = (props: { locale: TLang }): TApiResponse<IVacanciesPageResponse> =>
  index.get('/vacancies', {
    headers: {
      Language: props.locale,
    },
  })

const getVacancy = (props: {
  locale: TLang
  vacancy: string
}): TApiResponse<IVacancyPageResponse> =>
  index.get(`/vacancy/${props.vacancy}`, {
    headers: {
      Language: props.locale,
    },
  })

const sendCommonCv = (
  params: IApplyToPositionSendReq,
): TApiResponse<IVacanciesPageResponse> =>
  index.post('/vacancies/common-cv', new RuntimeForm(params).getFormData())

const sendRecommendCv = (
  params: IRecommendAFriendSendReq,
): TApiResponse<IVacanciesPageResponse> =>
  index.post('/vacancies/recommend-cv', new RuntimeForm(params).getFormData())

const getVacanciesUris = (): TApiResponse<IUri[]> => index.get('/vacancies/uris')

export const vacanciesApi = {
  getVacancies,
  getVacancy,
  sendCommonCv,
  sendRecommendCv,
  getVacanciesUris,
}
