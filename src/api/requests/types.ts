import { AxiosResponse } from 'axios'
import { OrganizationJsonLdProps } from 'next-seo/lib/jsonld/organization'
import { WebPageJsonLdProps } from 'next-seo'

export type TAxiosResponse<T> = Promise<AxiosResponse<T>>
export type TApiResponse<T> = TAxiosResponse<{ data: T }>

export interface IReward {
  id: 1
  name: string
  photo: string
}

export interface ISubService {
  id: number
  name: string
  uri: string
  tags?: ITag[]
  description?: string
  icon?: string
}

export interface IWhyUs {
  id: number
  name: string
  content: string
  icon: string
}

export interface IKeyFeatures {
  name: string
  value: string
}

export interface ICooperation {
  id: number
  name: string
  items: {
    name: string
  }[]
  icon: string
}

export interface IReview {
  name: string
  review: string
  position: string
  photo: string
  external_link: null | string
  case_uri: string
  case_name: string
}

export interface IVacancy {
  id: number
  name: string
  category: string
  uri: string
  location: string
  is_hot: boolean
  schedule: string
}

export interface IMicroData {
  webpage?: WebPageJsonLdProps
  organization?: OrganizationJsonLdProps
}

export interface ISwitchPageLink {
  uri: string
  name: string
}

export interface IMeta {
  id: string
  title: string
  keywords: string
  description: string
}

export interface IAward {
  id: number
  title: string
  photo: string
  url: string
  description: string
}

export interface ITechnology {
  id: number
  name: string
  photo: string
}

export interface IPartner {
  id: number
  name: string
  photo: string
  photo_black: string
}

export interface IContent {
  header: string
  sub_header: string
  meta: IMeta
}

export interface ITag {
  id: number
  uri: string
  name: string
}

export interface IUri {
  locale: string
  uri: string
}

export interface ICase {
  id: number
  uri: string
  name: string
  preview: string
  short_preview: string
  photo: string
  photo_preview: string
  play_market_link: string | null
  apple_store_link: string | null
  tags: ITag[]
  is_mobile: boolean
  technologies: {
    id: number
    name: string
    photo: string
  }[]
}

export interface IPager {
  total: number
  limit: number
  page: number
  offset: number
}

export type TLocales = {
  [key: string]: string
}

export interface IClutchReviews {
  reiting: string
  rewiews: string
  descriptiopn: string
}

export type TLang = string
