import axios, { AxiosInstance } from 'axios'
import { isClient } from '@/utils/isClient'
import { ErrorStatus } from '@/types/error'
import { BASE_API_URL, API_REST_TIMEOUT } from '@/constants/publicInfo'
import router from 'next/router'
import { getSsrLocale } from '@/utils/localization'
import { notification } from '@/utils/notification'

const hasModalError = (status: number) => {
  const mainErrors = [
    ErrorStatus.Validation,
    ErrorStatus.Message,
    ErrorStatus.Unauthorized,
    ErrorStatus.NotFound,
  ]
  const hasServerError = status >= ErrorStatus.Server
  return !mainErrors.includes(status) && !hasServerError
}

export const index: AxiosInstance = axios.create({
  baseURL: BASE_API_URL,
  timeout: API_REST_TIMEOUT,
  headers: {
    'Content-Type': 'application/json',
  },
})

// request hendler
index.interceptors.request.use((request) => {
  const req = request
  if (req?.headers && isClient()) {
    req.headers.Language = getSsrLocale(router.locale)
  }

  return req
})

// response hendler
index.interceptors.response.use(
  (response) =>
    // do sometging
    response,
  (error) => {
    if (!error.response) {
      return Promise.reject({
        response: {
          status: null,
          data: {},
          canceled: axios.isCancel(error),
        },
      })
    }

    const { status, data } = error.response

    if (isClient() && status >= ErrorStatus.Server) {
      notification({ title: 'Server error' })
    }

    if (isClient() && hasModalError(status) && data.message) {
      notification({ title: data.message })
    }

    return Promise.reject(error)
  },
)
