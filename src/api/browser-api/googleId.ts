import { getCookie } from '@/utils/get-cookie'

export const googleClientId = {
  get(): string | null {
    return getCookie('_ga')
  },
}
