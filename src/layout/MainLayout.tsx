import styled from 'styled-components'
import { ThemedSection } from '@/components/ThemedSection'
import { ContactUsPopup } from '@/features/contact-us/ContactUsPopup'
import { ISocialData } from '@/requests/socials/types'
import { ILocation } from '@/requests/locations/types'
import { TServiceCategories } from '@/requests/services/types'
import { HEADER_HEIGHT_DESKTOP } from '@/constants/menu'
import { deviceCssQuery } from '@/styling/breakpoints'
import { Header } from './common/Header'
import { Footer } from './common/Footer'
import { SocialLinksContext } from '../context/SocialLinksContext'
import { LocationsContext } from '../context/LocationsContext'
import { ServicesContext } from '../context/ServicesContext'

export const Container = styled.div`
  overflow: hidden;

  @media screen and ${deviceCssQuery.md} {
    padding-top: ${HEADER_HEIGHT_DESKTOP}px;
  }
`

type TMainLayout = {
  isFooter?: boolean
  socialLinks?: ISocialData[]
  locations?: ILocation[]
  services?: TServiceCategories
}

export const MainLayout: React.FC<TMainLayout> = (props) => {
  const {
    children,
    isFooter = true,
    socialLinks = [],
    locations = [],
    services = [],
  } = props

  return (
    <SocialLinksContext.Provider value={socialLinks}>
      <LocationsContext.Provider value={locations}>
        <ServicesContext.Provider value={services}>
          <Container>
            <Header />
            {children}
            <ThemedSection theme="dark">{isFooter && <Footer />}</ThemedSection>
          </Container>
          <ContactUsPopup />
        </ServicesContext.Provider>
      </LocationsContext.Provider>
    </SocialLinksContext.Provider>
  )
}
