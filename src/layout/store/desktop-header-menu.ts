import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { TSelector, TAsyncAction, TDispatch } from '@/store'
import { MENU_TRANSITION_TIME_DESKTOP } from '@/layout/constants'

export type TInit = {
  animated: boolean
  open: '' | 'services' | 'phones'
}

export const init: TInit = {
  animated: false,
  open: '',
}

const desktopHeaderMenu = createSlice({
  name: 'desktop-header-menu',
  initialState: init,
  reducers: {
    setAnimated(state, action: PayloadAction<TInit['animated']>) {
      state.animated = action.payload
    },
    setOpen(state, action: PayloadAction<TInit['open']>) {
      state.open = action.payload
    },
    reset: () => init,
  },
})

export const { reset, setAnimated, setOpen } = desktopHeaderMenu.actions

// selectores
export const selectDesktopHeaderMenu: TSelector<TInit> = (state) =>
  state.desktopHeaderMenu

// reducer
export default desktopHeaderMenu.reducer

const removeAnimated = (dispatch: TDispatch, time: number) => {
  setTimeout(() => {
    dispatch(setAnimated(false))
  }, time)
}

export const setOpenMenu =
  (menuName: TInit['open']): TAsyncAction =>
  (dispatch, getState) => {
    const { open, animated } = getState().desktopHeaderMenu

    if (open === menuName) return
    if (animated) return

    dispatch(setAnimated(true))

    const replaceMenu = open !== '' && menuName !== ''

    if (replaceMenu) {
      dispatch(setOpen(''))
      setTimeout(() => {
        dispatch(setOpen(menuName))
      }, MENU_TRANSITION_TIME_DESKTOP)
      removeAnimated(dispatch, MENU_TRANSITION_TIME_DESKTOP * 2)
      return
    }

    dispatch(setOpen(menuName))
    removeAnimated(dispatch, MENU_TRANSITION_TIME_DESKTOP)
  }
