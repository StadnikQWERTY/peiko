import { useRedux } from '@/hooks/useRedux'
import { mainLayoutActions } from '@/store/mainLayout/slice'

interface IUseMobileMenu {
  mobileMenuVisible: boolean
  setMobileMenuVisibility: (state: boolean) => void
}

export const useMobileMenu = (): IUseMobileMenu => {
  const [useSelector, dispatch] = useRedux()

  const mobileMenuVisible = useSelector((state) => state.mainLayout.mobileMenuVisible)

  const setMobileMenuVisibility = (state: boolean) =>
    dispatch(mainLayoutActions.setMobileMenuVisibility(state))

  return {
    mobileMenuVisible,
    setMobileMenuVisibility,
  }
}
