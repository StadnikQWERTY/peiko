import React from 'react'
import Link from 'next/link'
import { TText } from '@/components/Text'
import { useRouter } from 'next/router'

import * as Styled from './styled'

type TMenuItem = {
  className?: string
  onClick?: () => void
  uppercase?: boolean
  variant?: TText['variant']
} & {
  link?: string
}

export const LinkButton: React.FC<TMenuItem> = ({
  children,
  className,
  link,
  onClick,
  uppercase,
  variant,
}) => {
  const router = useRouter()

  const isStringChildren = typeof children === 'string'

  const hasActiveLink = link ? router.asPath.indexOf(link) === 0 : false

  const MenuBodyDom = (
    <>
      {isStringChildren ? (
        <Styled.TextContainer>
          <Styled.StyledText variant={variant || 'link'}>{children}</Styled.StyledText>
          <Styled.ContactUsArrow />
        </Styled.TextContainer>
      ) : (
        children
      )}
    </>
  )

  return (
    <>
      {link ? (
        <Link href={link} prefetch={false}>
          <Styled.MenuItem
            as="a"
            active={hasActiveLink}
            href={link}
            className={className}
            uppercase={uppercase}
            onClick={onClick}
          >
            {MenuBodyDom}
          </Styled.MenuItem>
        </Link>
      ) : (
        <button onClick={onClick} type="button">
          <Styled.MenuItem
            as="span"
            active={hasActiveLink}
            uppercase={uppercase}
            className={className}
          >
            {MenuBodyDom}
          </Styled.MenuItem>
        </button>
      )}
    </>
  )
}
