import styled, { css } from 'styled-components'
import { Text } from '@/components/Text'
import { ArrowIcon } from '@/components/icons/ArrowIcon'

type TMenuItem = {
  active?: boolean
  uppercase?: boolean
}

type TTextContainer = {
  active?: boolean
}

const TRANSITION_DURATION = '350ms'

export const StyledText = styled(Text)``

export const ContactUsArrow = styled(ArrowIcon)`
  opacity: 0;
`

export const TextContainer = styled.div<TTextContainer>((props) => {
  const axtiveText = `
    color: ${props.theme.palette.primary};
    transition: ${TRANSITION_DURATION} color;
  `
  const activeArrow = `
    opacity: 1;
    transition: ${TRANSITION_DURATION} opacity;
  `

  return `
    display: grid;
    grid-auto-flow: column;
    grid-gap: 7.79px;
    align-items: center;

    ${
      props.active &&
      `${StyledText} {
      ${axtiveText}
    }
    ${ContactUsArrow} {
      ${activeArrow}
    }`
    }

    &:hover ${StyledText} {
      ${axtiveText}
    }

    & ${ContactUsArrow} {
      opacity: 0;
    }

    &:hover ${ContactUsArrow} {
      ${activeArrow}
    }
  `
})

export const MenuItem = styled.div<TMenuItem>((props) => {
  const axtiveText = `
    color: ${props.theme.palette.primary};
    transition: ${TRANSITION_DURATION} color;
  `
  const activeArrow = `
    opacity: 1;
    transition: ${TRANSITION_DURATION} opacity;
  `

  return css`
    display: flex;
    align-items: center;
    justify-content: center;
    ${props.uppercase && `text-transform: uppercase;`}
    // ${props.active && `pointer-events: none;`}
    white-space: nowrap;
    width: fit-content;

    ${props.active &&
    `${StyledText} {
      ${axtiveText}
    }
    ${ContactUsArrow} {
      ${activeArrow}
    }`}

    cursor: pointer;

    &:hover ${StyledText} {
      ${axtiveText}
    }

    &:hover ${ContactUsArrow} {
      ${activeArrow}
    }
  `
})
