import styled from 'styled-components'
import { Z_INDEX } from '@/styling/z-index'
import { HeaderMobile } from '@/components/HeaderMobile'
import { ANIMATE_DURATION } from '@/constants/menu'

type TOpenedSection = {
  isOpen: boolean
  init: boolean
}

export const Header = styled(HeaderMobile)`
  position: fixed;
`

export const MenuWrapper = styled.div<TOpenedSection>`
  position: fixed;
  z-index: ${Z_INDEX.MOBILE_MENU_CONTENT};
  width: 100%;
  height: var(--window-height);
  background: ${(props) => props.theme.palette.primaryBg};
  top: ${(props) => (props.isOpen ? 0 : `calc(var(--window-height) * -1)`)};
  display: ${(props) => (props.init ? 'block' : 'none')};
  transition: top ${ANIMATE_DURATION}s;
  animation: ${(props) =>
    props.isOpen
      ? `open ${ANIMATE_DURATION}s forwards`
      : `close ${ANIMATE_DURATION}s forwards`};

  @keyframes open {
    0% {
      opacity: 0;
    }
    1%,
    100% {
      opacity: 1;
    }
  }

  @keyframes close {
    0%,
    99% {
      opacity: 1;
    }
    100% {
      opacity: 0;
    }
  }
`
