import React, { useEffect, useState } from 'react'
import styled from 'styled-components'
import { ThemeAppProvider } from '@/styling/ThemeAppProvider'
import { useRedux } from '@/hooks/useRedux'
import { deviceCssQuery } from '@/styling/breakpoints'
import { useBreakpoints } from '@/hooks/useBreakpoints'
import { useMobileMenu } from '../../hooks/useMobileMenu'
import { MobileMenu } from '../MobileMenu'
import * as Styled from './styled'

const StyledContainer = styled.div`
  @media screen and ${deviceCssQuery.xs} {
    display: block;
  }

  @media screen and ${deviceCssQuery.md} {
    display: none;
  }
`

export const MobileHeader: React.FC = () => {
  const [useSelector] = useRedux()
  const theme = useSelector((state) => state.mainLayout.theme)
  const [init, setInit] = useState(false)

  const { mobileMenuVisible, setMobileMenuVisibility } = useMobileMenu()

  const { isMobile } = useBreakpoints()

  useEffect(() => {
    setInit(true)
  }, [])

  useEffect(() => {
    if (!isMobile && mobileMenuVisible) {
      setMobileMenuVisibility(false)
    }
  }, [isMobile, mobileMenuVisible])

  return (
    <ThemeAppProvider theme={theme}>
      <StyledContainer>
        <Styled.Header
          isOpened={mobileMenuVisible}
          onChangeOpeningState={() => setMobileMenuVisibility(!mobileMenuVisible)}
        />

        <Styled.MenuWrapper isOpen={mobileMenuVisible} init={init}>
          <MobileMenu isOpen={mobileMenuVisible} />
        </Styled.MenuWrapper>
      </StyledContainer>
    </ThemeAppProvider>
  )
}
