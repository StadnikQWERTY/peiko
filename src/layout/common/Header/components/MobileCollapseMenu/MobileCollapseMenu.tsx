import { useState } from 'react'
import { Collapse } from 'react-collapse'
import { useUpdateEffect } from 'react-use'
import { useRouter } from 'next/router'
import { TStates as TFoldingButtonProps } from '@/layout/common/Header/components/FoldingButton'
import { useMobileMenu } from '../../hooks/useMobileMenu'
import * as Styled from './styled'

type TContentProps = {
  onClick: () => void
}

type TProps = {
  target: (props: TFoldingButtonProps) => JSX.Element
  content: (props: TContentProps) => JSX.Element
  onTargetClick: () => void
  activeLink?: string
}

export const MobileCollapseMenu: React.FC<TProps> = ({
  target,
  content,
  onTargetClick,
  activeLink,
}) => {
  const router = useRouter()
  const [open, setOpen] = useState(false)
  const { mobileMenuVisible } = useMobileMenu()

  const onClickTarget = () => {
    setOpen((state) => !state)
  }

  // open by default
  useUpdateEffect(() => {
    if (!mobileMenuVisible || !activeLink) return
    if (router.asPath.indexOf(activeLink) === -1) return
    setOpen(true)
  }, [mobileMenuVisible])

  // close then mobile menu closed
  useUpdateEffect(() => {
    if (mobileMenuVisible) return
    setOpen(false)
  }, [mobileMenuVisible])

  return (
    <div>
      {target({ active: open, onClick: onClickTarget })}
      <Styled.Container>
        <Collapse
          isOpened={open}
          theme={{ collapse: 'ReactCollapse--collapse-mobile-header-folding-menu' }}
        >
          <Styled.Content>{content({ onClick: onTargetClick })}</Styled.Content>
        </Collapse>
      </Styled.Container>
    </div>
  )
}
