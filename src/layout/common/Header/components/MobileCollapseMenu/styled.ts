import styled from 'styled-components'
import { MENU_TRANSITION_TIME_MOBILE } from '@/layout/constants'

export const Container = styled.div`
  .ReactCollapse--collapse-mobile-header-folding-menu {
    transition: height ${MENU_TRANSITION_TIME_MOBILE}ms;
  }
`

export const Content = styled.div``
