import { useRef } from 'react'
import { Collapse } from 'react-collapse'
import { useClickOutside } from '@/hooks/use-click-outside'
import { useRedux } from '@/hooks/useRedux'
import {
  setOpenMenu,
  selectDesktopHeaderMenu,
  TInit,
} from '@/layout/store/desktop-header-menu'
import { TStates as TFoldingButtonProps } from '@/layout/common/Header/components/FoldingButton'
import { TContentProps } from './types'

import * as Styled from './styled'

type TProps = {
  target: (props: TFoldingButtonProps) => JSX.Element
  content: (props: TContentProps) => JSX.Element
  menuName: TInit['open']
}

const ID = 'desctop-menu-folding-button'

export const DesktopFoldingMenu: React.FC<TProps> = ({ target, content, menuName }) => {
  const [select, dispatch, redux] = useRedux()
  const containerRef = useRef<HTMLDivElement | null>(null)
  const { open, animated } = select(selectDesktopHeaderMenu)

  const handlClose = () => {
    dispatch(setOpenMenu(''))
  }

  const handlClickOutside = (eventTarget: HTMLElement | null) => {
    if (!eventTarget) return
    if (eventTarget.closest(`#${ID}`)) return
    handlClose()
  }

  const handlClickTarget = () => {
    const { open } = redux.getState().desktopHeaderMenu
    dispatch(setOpenMenu(open === menuName ? '' : menuName))
  }

  useClickOutside({ ref: containerRef, onClick: handlClickOutside })

  const hasOpen = open === menuName

  return (
    <>
      {target({ active: hasOpen, onClick: handlClickTarget, id: ID })}
      <Styled.Container id={ID} ref={containerRef} open={hasOpen} animated={animated}>
        <Collapse
          isOpened={hasOpen}
          theme={{ collapse: 'ReactCollapse--collapse-header-menu' }}
        >
          <Styled.Content>{content({ onClick: handlClose })}</Styled.Content>
        </Collapse>
      </Styled.Container>
    </>
  )
}
