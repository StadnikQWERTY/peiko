import styled from 'styled-components'
import { MENU_TRANSITION_TIME_DESKTOP } from '@/layout/constants'
import { HEADER_HEIGHT_DESKTOP } from '@/constants/menu'

type TContainer = {
  open: boolean
  animated: boolean
}

export const Container = styled.div<TContainer>`
  position: fixed;
  top: ${HEADER_HEIGHT_DESKTOP}px;
  left: 0;
  width: 100%;
  background: ${(props) => props.theme.palette.primaryBg};
  border-bottom: ${(props) =>
    props.open || props.animated ? `1px solid ${props.theme.palette.secondary}` : 'none'};

  .ReactCollapse--collapse-header-menu {
    transition: height ${MENU_TRANSITION_TIME_DESKTOP}ms;
  }
`

export const Content = styled.div`
  max-width: 1120px;
  margin: 0 auto;
`
