import styled from 'styled-components'
import { ArrowTextButton } from '@/components/buttons/ArrowTextButton'
import {
  GET_IN_TOUCH_FLOAT_BUTTON_HEIGHT,
  TOP_BAR_MENU_MOBILE_HEIGHT,
} from '@/constants/menu'

const contentPadding = '48px'

export const Root = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  justify-content: flex-start;
  padding-top: calc(${contentPadding} + ${TOP_BAR_MENU_MOBILE_HEIGHT}px);
  transition: 200ms;
  height: 100%;
  overflow-y: auto;
`

export const Buttons = styled.div`
  padding: 0 24px;
  padding-bottom: ${GET_IN_TOUCH_FLOAT_BUTTON_HEIGHT}px;

  & > * {
    margin-bottom: 32px;
  }
`

export const ButtonItem = styled.div`
  margin-bottom: 32px;
`

export const ServiceContent = styled.div`
  padding-top: 8px;

  & > * {
    margin-top: 24px;
  }
`

export const ContactUsButton = styled(ArrowTextButton)`
  margin-top: auto;
  display: flex;
  justify-content: center;
  padding: 22px 0;
  border-top: 1px solid ${(props) => props.theme.palette.secondary};
`
