import React, { useEffect, useContext } from 'react'
import useTranslation from 'next-translate/useTranslation'
import { ROUTES } from '@/routes'
import { useScrollLock } from '@/hooks/use-scroll-lock'
import { ServicesContext } from '@/context/ServicesContext'
import { LocationsContext } from '@/context/LocationsContext'
import { BaseImage } from '@/components/BaseImage'
import { useMobileMenu } from '../../hooks/useMobileMenu'
import { FoldingButton } from '../FoldingButton'
import { LinkButton } from '../LinkButton'
import { Locations } from '../Locations'
import { MobileCollapseMenu } from '../MobileCollapseMenu'
import * as Styled from './styled'

type TOpenedSection = {
  className?: string
  isOpen: boolean
}

export const MobileMenu: React.FC<TOpenedSection> = ({ className, isOpen }) => {
  const { t } = useTranslation('common')
  const { setMobileMenuVisibility } = useMobileMenu()
  const { scrollRef, setScrollLock } = useScrollLock()
  const services = useContext(ServicesContext)
  const locations = useContext(LocationsContext)

  useEffect(() => {
    setScrollLock(isOpen)

    if (isOpen) {
      scrollRef.current?.scrollTo({ top: 0 })
    }
  }, [isOpen])

  return (
    <Styled.Root className={className} ref={scrollRef}>
      <Styled.Buttons>
        <LinkButton
          uppercase
          onClick={() => setMobileMenuVisibility(false)}
          link={ROUTES.ABOUT_US}
          variant="paragraph"
        >
          {t('aboutUs')}
        </LinkButton>

        <LinkButton
          uppercase
          onClick={() => setMobileMenuVisibility(false)}
          link={ROUTES.CASES}
          variant="paragraph"
        >
          {t('cases')}
        </LinkButton>

        <LinkButton
          uppercase
          onClick={() => setMobileMenuVisibility(false)}
          link={ROUTES.BLOG}
          variant="paragraph"
        >
          {t('blog')}
        </LinkButton>

        <MobileCollapseMenu
          target={(props) => (
            <FoldingButton
              text={t('services')}
              variant="paragraph"
              activeLink={ROUTES.SERVICE}
              underline={false}
              {...props}
            />
          )}
          content={(props) => (
            <Styled.ServiceContent>
              {services.map((item) => (
                <LinkButton
                  variant="paragraph"
                  key={item.id}
                  link={`${ROUTES.SERVICE}/${item.uri}`}
                  {...props}
                >
                  {item.name}
                </LinkButton>
              ))}
            </Styled.ServiceContent>
          )}
          onTargetClick={() => setMobileMenuVisibility(false)}
          activeLink={ROUTES.SERVICE}
        />

        <MobileCollapseMenu
          target={(props) => (
            <FoldingButton
              icon={<BaseImage src={locations[0].img} height="22px" width="22px" />}
              text={locations[0].phone}
              underline={false}
              variant="paragraph"
              {...props}
            />
          )}
          content={(props) => <Locations mobile {...props} />}
          onTargetClick={() => setMobileMenuVisibility(false)}
        />
      </Styled.Buttons>
    </Styled.Root>
  )
}
