import { useContext } from 'react'
import useTranslation from 'next-translate/useTranslation'
import Trans from 'next-translate/Trans'
import { Text } from '@/components/Text'
import { Spacer } from '@/components/Spacer/Spacer'
import { useSetContactUs } from '@/features/contact-us'
import { ServicesContext } from '@/context/ServicesContext'
import { ROUTES } from '@/routes'
import { Button } from '@/components/buttons/Button'
import { TContentProps } from '../DesktopFoldingMenu/types'
import { LinkButton } from '../LinkButton'
import * as Styled from './styled'

export const DesktopServicesMenu: React.FC<TContentProps> = ({ onClick }) => {
  const { t } = useTranslation('common')
  const { setContactUsVisibility } = useSetContactUs()
  const services = useContext(ServicesContext)

  const handleClick = () => {
    setContactUsVisibility(true)
    if (!onClick) return
    onClick()
  }

  return (
    <Styled.Container>
      <Styled.Left>
        <Styled.Contact>
          <Trans
            i18nKey="header:services.contact-des"
            components={{
              component: <Text tag="span" variant="secondaryParagraph" />,
              color: <Styled.TextColor tag="span" variant="secondaryParagraph" />,
            }}
          />
          <Spacer xs={{ margin: '16px 0 0 0' }}>
            <Button onClick={handleClick}>{t('header:services.contact-button')}</Button>
          </Spacer>
        </Styled.Contact>
      </Styled.Left>
      <Styled.Right>
        {services.map((item) => (
          <LinkButton
            variant="paragraph"
            key={item.id}
            link={`${ROUTES.SERVICE}/${item.uri}`}
            onClick={onClick}
          >
            {item.name}
          </LinkButton>
        ))}
      </Styled.Right>
    </Styled.Container>
  )
}
