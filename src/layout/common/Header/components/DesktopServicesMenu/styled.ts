import styled from 'styled-components'
import { Text } from '@/components/Text'

export const Container = styled.div`
  display: flex;
  padding: 40px 0 40px 24px;
`

export const Left = styled.div`
  width: 31%;
  flex-shrink: 0;
`

export const Right = styled.div`
  width: 69%;
  flex-shrink: 0;
  display: grid;
  grid-auto-rows: min-content;
  grid-gap: 20px;
  grid-template-columns: repeat(auto-fit, minmax(320px, 1fr));
  align-items: flex-start;

  & > * {
    justify-content: flex-start;
  }
`

export const Contact = styled.div`
  width: 221px;
  margin-top: 33px;
`

export const TextColor = styled(Text)`
  color: ${(props) => props.theme.palette.primary};
`

export const LinkButton = styled(Text)`
  color: ${(props) => props.theme.palette.primary};
`
