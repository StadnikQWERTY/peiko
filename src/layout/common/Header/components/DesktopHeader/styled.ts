import styled from 'styled-components'
import { Z_INDEX } from '@/styling/z-index'
import { deviceCssQuery } from '@/styling/breakpoints'
import { WIDTH_SCROLLBAR } from '@/constants/scrollbar'
import { HEADER_HEIGHT_DESKTOP } from '@/constants/menu'
import { FoldingButton } from '../FoldingButton'
import { LinkButton } from '../LinkButton'

type THeader = {
  adjustWidth: boolean
}

export const Header = styled.div<THeader>`
  position: fixed;
  top: 0;
  left: 0;
  padding-right: ${(props) => (props.adjustWidth ? `${WIDTH_SCROLLBAR}px` : 0)};
  width: 100%;
  height: ${HEADER_HEIGHT_DESKTOP}px;
  z-index: ${Z_INDEX.MOBILE_MENU_CONTENT};
  background: ${(props) => props.theme.palette.primaryBg};
  border-bottom: 1px solid ${(props) => props.theme.palette.secondary};

  @media screen and ${deviceCssQuery.xs} {
    display: none;
  }

  @media screen and ${deviceCssQuery.md} {
    display: block;
  }
`
export const Content = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding-left: 24px;
  padding-right: 24px;
  height: 100%;
  max-width: 1120px;
  margin: 0 auto;
  border-left: 1px solid ${(props) => props.theme.palette.secondary};
  border-right: 1px solid ${(props) => props.theme.palette.secondary};
`

export const ButtonContainer = styled.div`
  display: flex;
  align-items: center;
  height: 100%;
`

export const Link = styled(LinkButton)`
  padding: 0 8px;
`

export const PhoneFoldingButton = styled(FoldingButton)`
  height: 100%;
  padding: 0 18px;
  margin-right: 18px;
`

export const PhoneMenuContainer = styled.div`
  padding: 40px 0 40px 80px;
`
