import { useContext } from 'react'
import useTranslation from 'next-translate/useTranslation'
import { ThemeAppProvider } from '@/styling/ThemeAppProvider'
import { ROUTES } from '@/routes'
import { BaseLink } from '@/components/BaseLink'
import { PeikoLogo } from '@/components/PeikoLogo'
import { useSetContactUs } from '@/features/contact-us'
import { LocationsContext } from '@/context/LocationsContext'
import { Button } from '@/components/buttons/Button'
import { BaseImage } from '@/components/BaseImage'
import { Locations } from '../Locations'
import { DesktopFoldingMenu } from '../DesktopFoldingMenu'
import { FoldingButton } from '../FoldingButton'
import { DesktopServicesMenu } from '../DesktopServicesMenu'
import * as Styled from './styled'

export const DesktopHeader: React.FC = () => {
  const { t } = useTranslation('common')
  const { visibility, setContactUsVisibility } = useSetContactUs()
  const locations = useContext(LocationsContext)

  return (
    <ThemeAppProvider theme="dark">
      <Styled.Header adjustWidth={visibility}>
        <Styled.Content>
          <BaseLink href={ROUTES.HOME}>
            <PeikoLogo width={60} height={69} />
          </BaseLink>
          <Styled.ButtonContainer>
            <Styled.Link link={ROUTES.CASES} uppercase>
              {t('cases')}
            </Styled.Link>
            <DesktopFoldingMenu
              target={(props) => (
                <FoldingButton
                  text={t('services')}
                  activeLink={ROUTES.SERVICE}
                  variant="link"
                  {...props}
                />
              )}
              content={(props) => <DesktopServicesMenu {...props} />}
              menuName="services"
            />
            <Styled.Link link={ROUTES.BLOG} uppercase>
              {t('blog')}
            </Styled.Link>
            <Styled.Link link={ROUTES.ABOUT_US} uppercase>
              {t('aboutUs')}
            </Styled.Link>
            <DesktopFoldingMenu
              target={(props) => (
                <Styled.PhoneFoldingButton
                  icon={<BaseImage src={locations[0].img} height="22px" width="22px" />}
                  text={locations[0].phone}
                  underline={false}
                  {...props}
                />
              )}
              content={(props) => (
                <Styled.PhoneMenuContainer>
                  <Locations {...props} />
                </Styled.PhoneMenuContainer>
              )}
              menuName="phones"
            />
            <Button
              size="small"
              waveAnimation
              onClick={() => {
                setContactUsVisibility(true)
              }}
            >
              {t('getInTouch')}
            </Button>
          </Styled.ButtonContainer>
        </Styled.Content>
      </Styled.Header>
    </ThemeAppProvider>
  )
}
