import React, { useContext } from 'react'
import styled, { css } from 'styled-components'
import { Text } from '@/components/Text'
import { PhoneLink } from '@/components/PhoneLink'
import { BaseImage } from '@/components/BaseImage'
import { LocationsContext } from '@/context/LocationsContext'
import { TContentProps } from './DesktopFoldingMenu/types'

type TRootProps = {
  mobile?: boolean
}

const List = styled.div`
  display: grid;
  grid-gap: 24px;
  grid-template-columns: repeat(auto-fit, minmax(240px, 1fr));
`

const Item = styled.div``

const LocationCont = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 8px;

  & > *:last-child {
    margin-left: 6px;
  }
`

const Root = styled.div<TRootProps>(
  (props) => css`
    ${props.mobile && 'margin-top: 43px;'}
    ${props.mobile && `${List} { grid-gap: 40px; }`}
    ${props.mobile && `${LocationCont} { margin-bottom: 16px; }`}
  `,
)

type TProps = TContentProps & {
  mobile?: boolean
}

export const Locations: React.FC<TProps> = ({ onClick, mobile }) => {
  const locations = useContext(LocationsContext)

  if (!locations.length) return null

  return (
    <Root mobile={mobile}>
      <List>
        {locations.map((item) => (
          <Item key={item.id}>
            <LocationCont>
              <BaseImage src={item.img} height="22px" width="22px" />
              <Text variant={mobile ? 'paragraph' : 'secondaryParagraph'} tag="span">
                {item.location}
              </Text>
            </LocationCont>
            <PhoneLink
              phone={item.phone}
              textProps={{ variant: mobile ? 'h3' : 'secondaryParagraph' }}
              onClick={onClick}
            />
          </Item>
        ))}
      </List>
    </Root>
  )
}
