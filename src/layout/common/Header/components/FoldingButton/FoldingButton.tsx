import React from 'react'
import { useRouter } from 'next/router'
import { TText } from '@/components/Text'
import * as Styled from './styled'

export type TStates = {
  active: boolean
  activeLink?: string
  onClick: () => void
  id?: string
  icon?: JSX.Element
  underline?: boolean
  className?: string
}

type TProps = TStates & {
  text: string
  variant?: TText['variant']
}

export const FoldingButton: React.FC<TProps> = ({
  text,
  active,
  onClick,
  id,
  icon,
  className,
  underline = true,
  activeLink,
  variant,
}) => {
  const router = useRouter()

  const hasActiveLink = activeLink ? router.asPath.indexOf(activeLink) === 0 : false

  return (
    <Styled.Container active={active} underline={underline} className={className}>
      <Styled.Button
        type="button"
        onClick={onClick}
        id={id}
        active={active}
        activeLink={hasActiveLink}
        underline={underline}
      >
        {icon && <Styled.Icon>{icon}</Styled.Icon>}
        <Styled.Text variant={variant}>{text}</Styled.Text>
        <Styled.Arrow />
      </Styled.Button>
    </Styled.Container>
  )
}
