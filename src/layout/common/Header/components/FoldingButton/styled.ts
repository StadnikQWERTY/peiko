import styled, { css } from 'styled-components'
import { Text as ParentText } from '@/components/Text'
import { ArrowDownIcon } from '@/components/icons/ArrowDownIcon'

type TContainer = {
  active?: boolean
  underline?: boolean
}

type TButton = {
  animated?: boolean
  active?: boolean
  underline?: boolean
  uppercase?: boolean
  activeLink?: boolean
}

const TRANSITION_DURATION = '350ms'

export const Container = styled.div<TContainer>`
  display: flex;
  align-items: center;
  position: relative;
  flex-shrink: 0;
  top: 1px;

  ${(props) =>
    props.underline &&
    `
    border-bottom: 1px solid ${props.theme.palette.secondary};
    height: 100%;
  `}

  transition: ${TRANSITION_DURATION};
  ${(props) =>
    props.active &&
    props.underline &&
    `border-bottom: 1px solid ${props.theme.palette.primary};`}
`

export const Text = styled(ParentText)``

export const Arrow = styled(ArrowDownIcon)`
  transform: rotate(0deg);
  transition: ${TRANSITION_DURATION} transform;
  position: relative;
  margin-left: 8px;
  top: -1px;
  & path {
    stroke: ${(props) => props.theme.palette.primaryText};
    transition: ${TRANSITION_DURATION} stroke;
  }
`

export const Button = styled.button<TButton>((props) => {
  const axtiveText = `
    color: ${props.theme.palette.primary};
    transition: ${TRANSITION_DURATION} color;
  `
  const arrowColorActive = `
    & path {
      stroke: ${props.theme.palette.primary};
      transition: ${TRANSITION_DURATION} stroke;
    }
  `

  const activeArrow = `
    transform: rotate(180deg);
    ${arrowColorActive}
  `

  return css`
    display: flex;
    align-items: center;
    text-transform: uppercase;
    white-space: nowrap;
    width: 100%;
    ${props.underline ? 'padding: 0 8px 0 12px;' : 'padding: 0;'}
    cursor: pointer;
    & > * {
      flex-shrink: 0;
    }

    ${props.activeLink &&
    `
      ${Text} {
        ${axtiveText}
      }
      ${Arrow} {
        ${arrowColorActive}
      }
    `}

    ${props.active &&
    `
    ${Text} {
      ${axtiveText}
    }
    ${Arrow} {
      ${activeArrow}
    }`}

    @media not all and (pointer: coarse) {
      &:hover ${Text} {
        @content;
        ${axtiveText}
      }
    }

    @media not all and (pointer: coarse) {
      &:hover ${Arrow} {
        @content;
        ${arrowColorActive}
      }
    }
  `
})

export const Icon = styled.div`
  display: flex;
  align-items: center;
  margin-right: 6px;
`
