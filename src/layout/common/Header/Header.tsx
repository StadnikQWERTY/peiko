import React from 'react'
import { DesktopHeader } from './components/DesktopHeader'
import { MobileHeader } from './components/MobileHeader'

export const Header: React.FC = () => (
  <>
    <DesktopHeader />
    <MobileHeader />
  </>
)
