import React from 'react'
import * as Styled from './styled'

export const ServicesHeader: React.FC = ({ children }) => (
  <Styled.ServicesHeader variant="h4" textTransform="uppercase">
    {children}
  </Styled.ServicesHeader>
)
