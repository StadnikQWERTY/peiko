import React from 'react'
import * as Styled from './styled'

type TLocalLinkText = {
  href: string
}

export const LocalLinkText: React.FC<TLocalLinkText> = (props) => {
  const { children, ...otherProps } = props

  return (
    <Styled.LocalLinkWrapper {...otherProps}>
      <Styled.TextWrapper variant="paragraph" tag="span">
        {children}
      </Styled.TextWrapper>
    </Styled.LocalLinkWrapper>
  )
}

type TAlienLinkText = React.AnchorHTMLAttributes<HTMLAnchorElement>
export const AlienLinkText: React.FC<TAlienLinkText> = (props) => {
  const { children, ...otherProps } = props

  return (
    <Styled.AlienLinkWrapper {...otherProps}>
      <Styled.TextWrapper variant="paragraph" tag="span">
        {children}
      </Styled.TextWrapper>
    </Styled.AlienLinkWrapper>
  )
}
