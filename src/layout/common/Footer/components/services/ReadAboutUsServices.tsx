import React from 'react'
import useTranslation from 'next-translate/useTranslation'
import { ABOUT_PEIKO } from '@/constants/publicInfo'
import * as Styled from './styled'
import { ServicesHeader } from './ServicesHeader'
import { AlienLinkText } from './LinkText'

export const ReadAboutUsServices: React.FC = () => {
  const { t } = useTranslation('common')
  const readAboutUsLinks = [
    {
      link: ABOUT_PEIKO.DOU,
      text: 'DOU',
    },
    {
      link: ABOUT_PEIKO.CLUTCH,
      text: 'Clutch',
    },
    {
      link: ABOUT_PEIKO.DESIGN_RUSH,
      text: 'DesignRush',
    },
    {
      link: ABOUT_PEIKO.GOOD_FIRMS,
      text: 'GoodFirms',
    },
    {
      link: ABOUT_PEIKO.UPWORK,
      text: 'Upwork',
    },
  ]

  return (
    <Styled.Services>
      <ServicesHeader>{t('footer.readAboutUs')}</ServicesHeader>
      {readAboutUsLinks.map((item) => (
        <AlienLinkText
          key={item.text}
          href={item.link}
          target="_blank"
          rel="noopener noreferrer nofollow"
        >
          {item.text}
        </AlienLinkText>
      ))}
    </Styled.Services>
  )
}
