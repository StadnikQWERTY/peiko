import React from 'react'
import useTranslation from 'next-translate/useTranslation'
import { ROUTES } from '@/routes'
import * as Styled from './styled'
import { LocalLinkText } from './LinkText'
import { ServicesHeader } from './ServicesHeader'

export const CompanyServices: React.FC = () => {
  const { t } = useTranslation('common')
  const companyLinks = [
    {
      link: ROUTES.SERVICES,
      text: t('services'),
    },
    {
      link: ROUTES.CASES,
      text: t('cases'),
    },
    {
      link: ROUTES.VACANCIES,
      text: t('careers'),
    },
    {
      link: ROUTES.ABOUT_US,
      text: t('aboutUs'),
    },
  ]

  return (
    <Styled.Services>
      <ServicesHeader>{t('company')}</ServicesHeader>
      {companyLinks.map((item) => (
        <LocalLinkText key={item.text} href={item.link}>
          {item.text}
        </LocalLinkText>
      ))}
    </Styled.Services>
  )
}
