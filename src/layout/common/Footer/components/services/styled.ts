import styled, { css } from 'styled-components'
import { Text } from '@/components/Text'
import { BaseLink } from '@/components/BaseLink'

export const TextWrapper = styled(Text)``

const linkStyles = css`
  @media (hover: hover) {
    &:hover ${TextWrapper} {
      color: ${(props) => props.theme.palette.primary};
    }
  }

  @media (hover: none) {
    &:active ${TextWrapper} {
      color: ${(props) => props.theme.palette.primary};
    }
  }
`
export const LocalLinkWrapper = styled(BaseLink)(() => linkStyles)

export const AlienLinkWrapper = styled.a(() => linkStyles)

export const Services = styled.div`
  display: grid;
  grid-auto-flow: row;
  grid-gap: 16px;
  grid-auto-rows: max-content;
`

export const ServicesHeader = styled(Text)`
  &&& {
    color: ${(props) => props.theme.palette.secondaryText};
  }
`
