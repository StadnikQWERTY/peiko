import React, { useContext } from 'react'
import styled from 'styled-components'
import { deviceCssQuery } from '@/styling/breakpoints'
import { SectionContainer } from '@/components/sections/SectionContainer'
import useTranslation from 'next-translate/useTranslation'
import { Text } from '@/components/Text'
import { PhoneLink } from '@/components/PhoneLink'
import { BaseImage } from '@/components/BaseImage'
import { LocationsContext } from '@/context/LocationsContext'
import { ServicesHeader } from './services/ServicesHeader'
import { GRID_MIN_MAX_MD } from '../constants'

const Root = styled.div`
  padding: 0 0 20px;
`

const List = styled.div`
  display: grid;
  color: white;
  margin-top: 16px;

  @media screen and ${deviceCssQuery.xs} {
    grid-template-columns: repeat(auto-fit, minmax(240px, 1fr));
  }

  @media screen and ${deviceCssQuery.md} {
    grid-template-columns: repeat(auto-fit, minmax(${GRID_MIN_MAX_MD}px, 1fr));
  }
`

const Item = styled.div`
  margin: 0 24px 44px 0;
`

const LocationCont = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 24px;

  & > *:first-child {
    margin-right: 6px;
  }
`

export const Locations: React.FC = () => {
  const { t } = useTranslation('common')
  const locations = useContext(LocationsContext)

  if (!locations.length) return null

  return (
    <SectionContainer>
      <Root>
        <ServicesHeader>{t('footer.ourLocations')}</ServicesHeader>
        <List>
          {locations.map((item) => (
            <Item key={item.id}>
              <LocationCont>
                <Text variant="paragraph" tag="span">
                  {item.location}
                </Text>
                <BaseImage src={item.img} height="22px" width="22px" />
              </LocationCont>
              <PhoneLink phone={item.phone} />
            </Item>
          ))}
        </List>
      </Root>
    </SectionContainer>
  )
}
