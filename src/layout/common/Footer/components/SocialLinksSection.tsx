import React from 'react'
import styled from 'styled-components'
import useTranslation from 'next-translate/useTranslation'
import { SocialLinks } from '@/components/SocialLinks'
import { ServicesHeader } from './services/ServicesHeader'

const SectionNameContainer = styled.div`
  margin-bottom: 18px;
`

export const SocialLinksSection: React.FC = () => {
  const { t } = useTranslation('common')

  return (
    <div>
      <SectionNameContainer>
        <ServicesHeader>{t('socialMedia')}</ServicesHeader>
      </SectionNameContainer>
      <SocialLinks autoFit distance="26px" />
    </div>
  )
}
