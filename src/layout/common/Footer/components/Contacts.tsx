import React from 'react'
import styled from 'styled-components'
import { PhoneLink } from '@/components/PhoneLink'
import { EmailLink } from '@/components/EmailLink'
import useTranslation from 'next-translate/useTranslation'
import { ServicesHeader } from './services/ServicesHeader'

const base = `
  display: grid;
  grid-auto-flow: row;
  grid-gap: 16px;
  grid-auto-rows: max-content;
`

const StyledRoot = styled.address`
  ${base}
  font-style: normal;
`

const StyledPhoneLinkContainer = styled.div`
  ${base}
  margin-top: 8px;
`

export const Contacts: React.FC = () => {
  const { t } = useTranslation('common')

  return (
    <StyledRoot>
      <ServicesHeader>{t('footer.contactUs')}</ServicesHeader>
      <EmailLink />
      <StyledPhoneLinkContainer>
        <PhoneLink phoneVariant="first" />
        <PhoneLink phoneVariant="second" />
      </StyledPhoneLinkContainer>
    </StyledRoot>
  )
}
