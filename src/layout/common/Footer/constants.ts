import { contentMaxWidth, smallPadding } from '@/components/sections/SectionContainer'

export const GRID_MIN_MAX_MD = (contentMaxWidth - smallPadding * 2) / 4
