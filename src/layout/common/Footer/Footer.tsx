import React from 'react'
import useTranslation from 'next-translate/useTranslation'
import { SectionContainer } from '@/components/sections/SectionContainer'
import { Text } from '@/components/Text'
import { useBreakpoints } from '@/hooks/useBreakpoints'
import { useSetContactUs } from '@/features/contact-us'
import { PeikoLogo } from '@/components/PeikoLogo'
import { Contacts } from './components/Contacts'
import { CompanyServices } from './components/services/CompanyServices'
import { ReadAboutUsServices } from './components/services/ReadAboutUsServices'
import { SocialLinksSection } from './components/SocialLinksSection'
import * as Styled from './styled'

export const Footer: React.FC = () => {
  const { t } = useTranslation('common')

  const { setContactUsVisibility } = useSetContactUs()

  const { isMobile } = useBreakpoints()

  const currentYear = new Date().getFullYear()

  const AllRightsReserved = (
    <Styled.AllRightReservedCont>
      <Text variant="secondaryParagraph" tag="span">
        {currentYear}. {t('footer.allRightsReserved')}
      </Text>
    </Styled.AllRightReservedCont>
  )

  const contactUsButton = (
    <Styled.ContactUsButton variant="float" onClick={() => setContactUsVisibility(true)}>
      {t('getInTouch')}
    </Styled.ContactUsButton>
  )

  return (
    <Styled.FooterContainer>
      <Styled.LogoContainer>
        <PeikoLogo />
      </Styled.LogoContainer>

      {
        // <Locations />
      }

      {!isMobile ? (
        <SectionContainer>
          <Styled.DesktopBody>
            <CompanyServices />
            <ReadAboutUsServices />
            <Contacts />
            <SocialLinksSection />
          </Styled.DesktopBody>
          {AllRightsReserved}
        </SectionContainer>
      ) : (
        <>
          <SectionContainer>
            <Styled.MobileBody>
              <Styled.MobileServicesContainer>
                <CompanyServices />
                <ReadAboutUsServices />
              </Styled.MobileServicesContainer>
              <Contacts />
              <SocialLinksSection />
            </Styled.MobileBody>
            {AllRightsReserved}
          </SectionContainer>
          {contactUsButton}
        </>
      )}
    </Styled.FooterContainer>
  )
}
