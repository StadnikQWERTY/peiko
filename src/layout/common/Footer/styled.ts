import styled from 'styled-components'
import { deviceCssQuery } from '@/styling/breakpoints'
import { ArrowTextButton } from '@/components/buttons/ArrowTextButton'
import { Button } from '@/components/buttons/Button'
import { GRID_MIN_MAX_MD } from './constants'

export const DesktopBody = styled.div`
  display: grid;
  color: white;
  margin-top: 16px;

  & > * {
    margin-bottom: 64px;
  }

  @media screen and ${deviceCssQuery.xs} {
    grid-template-columns: repeat(auto-fit, minmax(240px, 1fr));
  }

  @media screen and ${deviceCssQuery.md} {
    grid-template-columns: repeat(auto-fit, minmax(${GRID_MIN_MAX_MD}px, 1fr));
  }
`

export const MobileBody = styled.div`
  & > * {
    margin-bottom: 64px;
  }
`
export const MobileServicesContainer = styled.div`
  display: grid;
  grid-auto-flow: column;
`

export const FooterContainer = styled.footer`
  // min-height: 100vh;

  @media screen and ${deviceCssQuery.xs} {
    padding: 66px 0 0 0;
  }

  @media screen and ${deviceCssQuery.md} {
    padding: 98.67px 0 0 0;
  }

  @media screen and ${deviceCssQuery.lg} {
    padding: 98.67px 0 0 0;
    // min-height: calc(100vh - 98.67px);
  }
`

export const LogoContainer = styled.div`
  display: flex;
  justify-content: center;
  margin-bottom: 76px;
`

export const HireUsContainer = styled.div`
  position: relative;
  display: flex;
  justify-content: center;
  min-width: 300px;

  @media screen and ${deviceCssQuery.xs} {
    margin-bottom: 64px;
  }

  @media screen and ${deviceCssQuery.md} {
    margin-bottom: 144px;
  }
`

export const HireUsButtonWrapped = styled(ArrowTextButton)`
  @media screen and ${deviceCssQuery.xs} {
    min-width: 100%;
  }

  @media screen and ${deviceCssQuery.md} {
    min-width: 320px;
  }
`

export const HireUsLine = styled.div`
  position: absolute;
  height: 1px;
  width: 0;
  background: ${(props) => props.theme.palette.darkThird};
  bottom: 0;
`

export const ContactUsButton = styled(Button)`
  @media screen and ${deviceCssQuery.md} {
    display: none;
  }
`

export const AllRightReservedCont = styled.div`
  margin-bottom: 64px;
`
