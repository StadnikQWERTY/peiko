import React from 'react'
import styled from 'styled-components'
import useTranslation from 'next-translate/useTranslation'
import { useRouter } from 'next/router'
import { MainLayout } from '@/layout/MainLayout'
import { ThemedSection } from '@/components/ThemedSection'
import { ThreeJSContainer } from '@/components/ThreeJSContainer'
import * as Styled from '@/features/home/MainSection/styled'
import { deviceCssQuery } from '@/styling/breakpoints'
import { Text } from '@/components/Text/Text'
import { ArrowTextButton } from '@/components/buttons/ArrowTextButton'
import { ROUTES } from '@/routes'
import { BaseLink } from '@/components/BaseLink'
import { HEADER_HEIGHT_DESKTOP } from '@/constants/menu'
import { ILocation } from '@/requests/locations/types'
import { TServiceCategories } from '@/requests/services/types'

export const Root = styled.div`
  height: calc(100vh - ${HEADER_HEIGHT_DESKTOP}px);
`

export const Container = styled.section`
  display: flex;
  flex-direction: column;
  align-items: center;
  height: inherit;
  background: ${(props) => props.theme.palette.primaryBg};
  box-sizing: border-box;
  padding: 66px 24px;
`

export const Content = styled.div`
  margin-top: auto;
  margin-bottom: auto;
  display: flex;
  flex-direction: column;
  align-items: center;
  z-index: 1;

  @media screen and ${deviceCssQuery.xs} {
    max-width: 100%;
  }

  @media screen and ${deviceCssQuery.md} {
    max-width: 90%;
  }
`

export const Title = styled(Text)`
  margin-bottom: 16px;
  @media screen and ${deviceCssQuery.md} {
    margin-bottom: 32px;
  }
`

export const Description = styled(Text)`
  margin-bottom: 34px;
  font-size: 16px;
  @media screen and ${deviceCssQuery.md} {
    margin-bottom: 40px;
    font-size: 14px;
  }
`

export const LinkText = styled(Text)`
  padding: 0;
  margin: 0;
  color: ${(props) => props.theme.palette.primary} !important;
  cursor: pointer;

  &:first-of-type {
    margin-left: 0;
  }

  &:hover {
    color: ${(props) => props.theme.palette.secondaryLightText} !important;
  }

  @media screen and ${deviceCssQuery.md} {
    text-transform: uppercase;
  }
`

interface OwnProps {
  status: 404 | 500
  locations: ILocation[]
  services: TServiceCategories
}

export const ErrorPage: React.FC<OwnProps> = (props) => {
  const { status, locations, services } = props

  const { t } = useTranslation('error')

  const router = useRouter()

  return (
    <MainLayout isFooter={false} locations={locations} services={services}>
      <ThemedSection theme="dark">
        <Root>
          <ThreeJSContainer>
            <Styled.StarsBg />
          </ThreeJSContainer>
          <Container>
            <Content>
              <Title variant="h2">{t(`${status}`)}</Title>
              <Description align="center" variant="paragraph">
                {t(`${status}-desc`)}
                <LinkText onClick={() => router.back()} variant="paragraph" tag="button">
                  {t('prev-page')}
                </LinkText>
              </Description>
              <BaseLink href={ROUTES.HOME}>
                <ArrowTextButton>{t('common:home')}</ArrowTextButton>
              </BaseLink>
            </Content>
          </Container>
        </Root>
      </ThemedSection>
    </MainLayout>
  )
}
