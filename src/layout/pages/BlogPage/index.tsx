import { MainLayout } from '@/layout/MainLayout'
import { ThemedSection } from '@/components/ThemedSection'
import { SectionContainer } from '@/components/sections/SectionContainer'
import { Spacer } from '@/components/Spacer/Spacer'
import { Breadcrumbs } from '@/components/Breadcrumbs'
import { Text } from '@/components/Text'
import Tags from '@/components/Tags'
import { ROUTES } from '@/routes'
import BlogCards from '@/features/blog/BlogCards'
import { Pagination } from '@/components/Pagination'
import React, { FC } from 'react'
import { useRouter } from 'next/router'
import useTranslation from 'next-translate/useTranslation'
import { IBlogPageResponse } from '@/requests/blog/types'
import ShareYourIdea from '@/components/ShareYourIdea'
import { ISocialData } from '@/requests/socials/types'
import { ILocation } from '@/requests/locations/types'
import { TServiceCategories } from '@/requests/services/types'

interface OwnProps {
  response: IBlogPageResponse
  socialLinks: ISocialData[]
  locations: ILocation[]
  services: TServiceCategories
}

const BlogPage: FC<OwnProps> = (props) => {
  const { response, socialLinks, locations, services } = props
  const router = useRouter()
  const { asPath, query } = router
  const { t } = useTranslation('common')
  const params = (query.props as string[]) ?? []
  const selectedBlogPage = Number.isNaN(+params[0]) ? params[0] : undefined

  const breadcrumbs = [
    { name: t('home'), href: ROUTES.HOME },
    { name: t('blog'), href: asPath },
  ]

  return (
    <MainLayout socialLinks={socialLinks} locations={locations} services={services}>
      <ThemedSection theme="light">
        <SectionContainer>
          <Spacer lg={{ padding: '66px 0 18px 0' }} xs={{ padding: '88px 0 16px 0' }}>
            <Breadcrumbs breadcrumbs={breadcrumbs} />
          </Spacer>
          <Text variant="h1" tag="h1" align="center" lg={{ fontSize: '56px' }}>
            {response.content.header}
          </Text>
          <Spacer lg={{ padding: '72px 0 96px 0' }} xs={{ padding: '64px 0 32px 0' }}>
            <Tags
              tags={response.tags}
              header={t('relatedTags')}
              uri={ROUTES.BLOG + ROUTES.TAG}
              selectedTag={query.tags as string}
              showAllUri={ROUTES.BLOG}
              isMulti
            />
          </Spacer>
          <BlogCards articles={response.articles} />
          <Spacer lg={{ padding: '76px 0 288px 0' }} xs={{ padding: '64px 0 120px 0' }}>
            <Pagination
              currentPage={response.pager.page}
              getLinkTo={(page) => {
                if (selectedBlogPage) {
                  return `${ROUTES.BLOG}/${selectedBlogPage}${page}`
                }
                return `${ROUTES.BLOG}${page}`
              }}
              lastPage={Math.ceil(response.pager.total / response.pager.limit)}
              justifyContent="center"
            />
          </Spacer>
          <Spacer lg={{ padding: '0 0 192px 0' }} xs={{ padding: '0 0 120px 0' }}>
            <ShareYourIdea />
          </Spacer>
        </SectionContainer>
      </ThemedSection>
    </MainLayout>
  )
}

export default BlogPage
