import { createContext } from 'react'
import { ISocialData } from '@/requests/socials/types'

export const SocialLinksContext = createContext<ISocialData[]>([])
