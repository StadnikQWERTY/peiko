import { createContext } from 'react'
import { TServiceCategories } from '@/requests/services/types'

export const ServicesContext = createContext<TServiceCategories>([])
