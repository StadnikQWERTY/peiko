import { createContext } from 'react'
import { ILocation } from '@/requests/locations/types'

export const LocationsContext = createContext<ILocation[]>([])
