import { createGlobalStyle } from 'styled-components'
import { WIDTH_SCROLLBAR } from '@/constants/scrollbar'
import { theming } from './theme'

export const GlobalStyles = createGlobalStyle`
  body[data-theme='light'] {
    --background: ${theming.light.palette.primaryBg};
  }
  body[data-theme='dark'] {
    --background: ${theming.dark.palette.primaryBg};
  }

  body {
    background: var(--background);
  }


  * {
    font-family: 'Montserrat', sans-serif;
  }

  h1 {
    margin: 0;
  }

  menu {
    padding-inline-start: 0;
    margin-block-start: 0;
    margin-block-end: 0;
  }

  button {
    outline: none;
    background: transparent;
    border: none;
    cursor: pointer;
  }

  a {
    text-decoration: none;
  }

  div {
    box-sizing: border-box;
  }

  // Remove the blue highlight of button on mobile
  a:active, button:active {
    -webkit-tap-highlight-color: transparent;
  }

  // scrollbar
  /* total width */
  *::-webkit-scrollbar {
      background-color:#F4F6FA;
      width: ${WIDTH_SCROLLBAR}px;
      height: ${WIDTH_SCROLLBAR}px;
  }

  /* background of the scrollbar except button or resizer */
  *::-webkit-scrollbar-track {
      background-color:#F4F6FA
  }
  *::-webkit-scrollbar-track:hover {
      background-color:#f4f4f4
  }

  /* scrollbar itself */
  *::-webkit-scrollbar-thumb {
      background-color: #6e6e6e;
  }
  *::-webkit-scrollbar-thumb:hover {
      background-color: #626262;
  }

  /* set button(top and bottom of the scrollbar) */
  *::-webkit-scrollbar-button {display:none}
`
