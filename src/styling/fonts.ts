export enum FontWeight {
  Light = '300',
  Regular = '400',
  Medium = '500',
  SemiBold = '600',
  Bold = '700',
}

export enum FontStyle {
  Italic = 'italic',
  Oblique = 'oblique',
  Normal = 'normal',
  Inherit = 'inherit',
}
