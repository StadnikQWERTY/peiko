import React, { useMemo } from 'react'
import { ThemeProvider } from 'styled-components'
import { theming, TTheme } from '@/styling/theme'

type TThemedSection = {
  theme: TTheme
}

export const ThemeAppProvider: React.FC<TThemedSection> = (props) => {
  const { children, theme } = props

  const themeConfig = useMemo(() => {
    if (theme === 'light') {
      return theming.light
    }
    return theming.dark
  }, [theme])

  return <ThemeProvider theme={themeConfig}>{children}</ThemeProvider>
}
