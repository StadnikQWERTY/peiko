import 'styled-components'
import { CSSProperties } from 'react'

// Inspired by https://medium.com/rbi-tech/theme-with-styled-components-and-typescript-209244ec15a3

interface IPalette {
  currentTheme: 'dark' | 'light'
  primary: CSSProperties['color']
  secondary: CSSProperties['color']
  secondaryText: CSSProperties['color']
  secondaryLightText: CSSProperties['color']
  primaryText: CSSProperties['color']
  primaryBg: CSSProperties['color']
  scrollbarBg: CSSProperties['color']
  darkThird: CSSProperties['color']
  danger: CSSProperties['color']
  lightText: CSSProperties['color']
  grayIcon: CSSProperties['color']
  pagination: CSSProperties['color']
  white: CSSProperties['color']
  gradientBlue: CSSProperties['color']
  gradientPurple: CSSProperties['color']
  gradientOrange: CSSProperties['color']
  gradientGreen: CSSProperties['color']
}

declare module 'styled-components' {
  export interface DefaultTheme {
    palette: IPalette
  }
}
