export interface Breakpoints<T = string> {
  xs: T
  sm: T
  md: T
  lg: T
}

export const breakpoints: Breakpoints = {
  xs: '0px',
  sm: '720px',
  md: '900px',
  lg: '1366px',
}

export const deviceCssQuery: Breakpoints = {
  xs: `(min-width: ${breakpoints.xs})`,
  sm: `(min-width: ${breakpoints.sm})`,
  md: `(min-width: ${breakpoints.md})`,
  lg: `(min-width: ${breakpoints.lg})`,
}
