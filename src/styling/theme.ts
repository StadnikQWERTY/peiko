import { DefaultTheme } from 'styled-components'

export const defaultColorConfig = {
  primary: '#41B6E6',
  secondaryText: '#656C72',
  secondaryLightText: '#B1C1D1',
  darkThird: '#4A4E58',
  danger: '#DD0202',
  white: '#FFFFFF',
  scrollbarBg: '#E4E8F4',
  gradientBlue: 'linear-gradient(109.88deg, #45C0EB 0%, #308FEA 100%)',
  gradientPurple: 'linear-gradient(109.88deg, #A456DF 0%, #7222AF 100%)',
  gradientOrange: 'linear-gradient(109.88deg, #E2702E 0%, #DF4528 100%)',
  gradientGreen: 'linear-gradient(109.88deg, #60BE79 0%, #35934E 100%)',
}

export type TTheme = 'light' | 'dark'

export type TThemingConfig = Record<TTheme, DefaultTheme>

export const theming: TThemingConfig = {
  light: {
    palette: {
      ...defaultColorConfig,
      lightText: '#B1C1D1',
      secondary: '#E4E8F4',
      primaryText: '#14161D',
      primaryBg: '#F4F6FA',
      grayIcon: '#656C72',
      pagination: '#14161D',
      currentTheme: 'light',
    },
  },
  dark: {
    palette: {
      ...defaultColorConfig,
      lightText: '#656C72',
      secondary: '#343846',
      primaryText: '#FFFFFF',
      primaryBg: '#14161D',
      grayIcon: '#B1C1D1',
      pagination: '#FFFFFF',
      currentTheme: 'dark',
    },
  },
}
