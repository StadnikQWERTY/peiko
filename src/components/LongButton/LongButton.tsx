import React, { useEffect, useRef } from 'react'
import styled from 'styled-components'
import { deviceCssQuery } from '@/styling/breakpoints'
import { ArrowTextButton } from '@/components/buttons/ArrowTextButton'
import { gsap } from 'gsap'
import { ScrollTrigger } from 'gsap/dist/ScrollTrigger'

interface OwnProps {
  onClick?: () => void
  xs?: {
    minWidth?: string
    paddingLeft?: string
    paddingRight?: string
  }
}

export const HireUsContainer = styled.div`
  position: relative;
  display: flex;
  justify-content: center;
  min-width: 300px;
`

export const HireUsButtonWrapped = styled(ArrowTextButton)<OwnProps>`
  @media screen and ${deviceCssQuery.xs} {
    min-width: ${(props) => props.xs?.minWidth || '100%'};
    padding-left: ${(props) => props.xs?.paddingLeft || '0'};
    padding-right: ${(props) => props.xs?.paddingRight || '0'};
  }

  @media screen and ${deviceCssQuery.md} {
    min-width: 320px;
  }
`

export const HireUsLine = styled.div`
  position: absolute;
  height: 1px;
  width: 0;
  background: ${(props) => props.theme.palette.secondary};
  bottom: 0;
`

const LongButton: React.FC<OwnProps> = (props) => {
  const { children, onClick, ...otherProps } = props

  const hireUsLine = useRef<HTMLDivElement>(null)

  useEffect(() => {
    if (hireUsLine.current) {
      const animation = gsap.to(hireUsLine.current, {
        delay: 0.1,
        opacity: 1,
        width: '100%',
        duration: 1.3,
        ease: 'power2.inOut',
        paused: true,
      })

      ScrollTrigger.create({
        trigger: hireUsLine.current,
        start: '40px bottom',
        onEnter: () => animation.play(),
      })

      ScrollTrigger.create({
        trigger: hireUsLine.current,
        onLeaveBack: () => animation.pause(0),
      })
    }
  }, [])

  return (
    <HireUsContainer onClick={onClick}>
      <HireUsButtonWrapped {...otherProps} lineAnimated>
        {children}
      </HireUsButtonWrapped>
      <HireUsLine ref={hireUsLine} />
    </HireUsContainer>
  )
}

export default LongButton
