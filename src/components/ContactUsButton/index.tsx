import { FC } from 'react'
import styled from 'styled-components'
import useTranslation from 'next-translate/useTranslation'
import { useSetContactUs } from '@/features/contact-us'

const Container = styled.div`
  z-index: 1;
  position: fixed;
  height: 0;
  width: 100vw;
  bottom: 0;
  left: 0;
  circle {
    fill: ${({ theme }) => theme.palette.pagination};
  }
`

const Button = styled.button`
  position: absolute;
  right: 160px;
  bottom: 32px;
  circle {
    fill: ${({ theme }) => theme.palette.pagination};
  }
`

const ContactUsButton: FC = () => {
  const { t } = useTranslation('aria-label')
  const { setContactUsVisibility } = useSetContactUs()

  return (
    <Container>
      <Button
        onClick={() => setContactUsVisibility(true)}
        aria-label={t('contact-us-button')}
      >
        <svg
          width="64"
          height="64"
          viewBox="0 0 64 64"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <circle cx="32" cy="32" r="32" fill="#F4F6FA" />
          <path
            d="M20 24H44M20 24V40M20 24L25 29M44 24V40M44 24L39 29M44 40H20M44 40L39 29M20 40L25 29M25 29L26.6667 30.6667H37.3333L39 29"
            stroke="#21A0CA"
            strokeLinecap="round"
            strokeLinejoin="round"
          />
        </svg>
      </Button>
    </Container>
  )
}

export default ContactUsButton
