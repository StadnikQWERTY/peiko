import React from 'react'
import styled from 'styled-components'

type TSeoLink = {
  href: string
  content: string
}

const StyledLink = styled.a`
  position: absolute;
  right: 8000px;
`
// render hidden link for seo
export const SeoLink: React.FC<TSeoLink> = ({ content, href }) => (
  <StyledLink tabIndex={-1} href={href}>
    {content}
  </StyledLink>
)
