import useTranslation from 'next-translate/useTranslation'
import styled, { css } from 'styled-components'
import { Text } from '@/components/Text'
import { BaseImage } from '@/components/BaseImage'
import { deviceCssQuery } from '@/styling/breakpoints'

type TProps = {
  variant: 'rectangular' | 'transparent'
}

const Container = styled.div<TProps>`
  display: flex;

  ${(props) =>
    props.variant === 'rectangular' &&
    css`
      @media screen and ${deviceCssQuery.md} {
        flex-direction: column;
        align-items: center;
        width: 100%;
        padding: 24px;
        background: ${props.theme.palette.primary};
        text-align: center;
        border-radius: 2px;
      }
    `}

  ${(props) =>
    props.variant === 'transparent' &&
    css`
      align-items: center;
    `}
`

const Image = styled.div<TProps>`
  flex-shrink: 0;

  ${(props) =>
    props.variant === 'rectangular' &&
    css`
      width: 114px;
    `}

  ${(props) =>
    props.variant === 'transparent' &&
    css`
      width: 70px;
    `}
`

const DescriptionCont = styled.div<TProps>`
  @media screen and ${deviceCssQuery.xs} {
    text-align: center;
  }

  ${(props) =>
    props.variant === 'rectangular' &&
    css`
      margin-top: 16px;
    `}

  ${(props) =>
    props.variant === 'transparent' &&
    css`
      margin-left: 10px;
      width: 244px;
      @media screen and ${deviceCssQuery.md} {
        text-align: left;
      }
    `}
`

const Description = styled(Text)<{ var: TProps['variant'] }>`
  ${(props) =>
    props.var === 'transparent' &&
    css`
      line-height: 18px;
    `}
`

export const ClutchAward: React.FC<TProps> = ({ variant }) => {
  const { t } = useTranslation('clutch-award')

  return (
    <Container variant={variant}>
      <Image variant={variant}>
        <BaseImage
          src="/images/awards/clutch_top_1000_2022.png"
          alt="Clutch top 1000 companies 2022"
          height="93px"
          width="114px"
          priority
        />
      </Image>

      <DescriptionCont variant={variant}>
        <Description var={variant} variant="secondaryParagraph" tag="span">
          {t('description')}
        </Description>
      </DescriptionCont>
    </Container>
  )
}
