import React, { useEffect, useState, useRef } from 'react'

export const ThreeJSContainer: React.FC = ({ children }) => {
  const [loaded, setLoaded] = useState(false)
  const timer = useRef<ReturnType<typeof setInterval> | null>(null)

  useEffect(
    () => () => {
      if (timer.current) {
        clearInterval(timer.current)
      }
    },
    [],
  )

  useEffect(() => {
    timer.current = setInterval(() => {
      if (!window.THREE) return
      if (timer.current) {
        clearInterval(timer.current)
      }
      setLoaded(true)
    }, 10)
  }, [])

  return loaded ? <>{children}</> : <></>
}
