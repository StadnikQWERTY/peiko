import { ScriptLibraryLoader } from '@/utils/ScriptLibraryLoader'

export const threeJSLoader = new ScriptLibraryLoader('threeJS', {
  src: 'https://cdnjs.cloudflare.com/ajax/libs/three.js/r70/three.min.js',
})
