import React from 'react'
import styled from 'styled-components'

const StyledRoot = styled.svg`
  margin: auto;
  background: none;
  display: block;
  shape-rendering: auto;
`

type TLoader = {
  color?: string
  width?: string
  height?: string
}

export const Loader: React.FC<TLoader> = (props) => {
  const { width = '100%', height = '100%', color = '#41b6e6' } = props

  return (
    <StyledRoot
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 100 100"
      width={width}
      height={height}
      preserveAspectRatio="xMidYMid"
    >
      <circle
        cx="50"
        cy="50"
        fill="none"
        stroke={color}
        strokeWidth="1"
        r="35"
        strokeDasharray="164.93361431346415 56.97787143782138"
      >
        <animateTransform
          attributeName="transform"
          type="rotate"
          repeatCount="indefinite"
          dur="1s"
          values="0 50 50;360 50 50"
          keyTimes="0;1"
        />
      </circle>
    </StyledRoot>
  )
}
