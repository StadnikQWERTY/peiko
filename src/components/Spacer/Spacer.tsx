import React from 'react'
import styled, { css } from 'styled-components'
import { Breakpoints, deviceCssQuery } from '@/styling/breakpoints'

interface OwnProps {
  className?: string
}

interface BreakpointSpace {
  margin?: string
  padding?: string
}

const Space = styled.div<Partial<Breakpoints<BreakpointSpace>>>`
  display: block;

  ${({ xs, sm, md, lg }) => {
    // check if only one true
    const breakpoints = [xs, sm, md, lg]
    const mapBreakpoints = breakpoints.map((item) => item || false)
    const filteredBreakpoints = mapBreakpoints.filter(Boolean)

    if (filteredBreakpoints[0] && filteredBreakpoints.length === 1) {
      return css`
        margin: ${filteredBreakpoints[0]?.margin || 'unset'};
        padding: ${filteredBreakpoints[0]?.padding || 'unset'};
      `
    }
  }}

  ${({ xs }) => {
    if (!xs) {
      return null
    }

    return css`
      @media screen and ${deviceCssQuery.xs} {
        margin: ${xs?.margin || 'unset'};
        padding: ${xs?.padding || 'unset'};
      }
    `
  }}

  ${({ sm }) => {
    if (!sm) {
      return null
    }

    return css`
      @media screen and ${deviceCssQuery.sm} {
        margin: ${sm?.margin || 'unset'};
        padding: ${sm?.padding || 'unset'};
      }
    `
  }}

  ${({ md }) => {
    if (!md) {
      return null
    }

    return css`
      @media screen and ${deviceCssQuery.md} {
        margin: ${md?.margin || 'unset'};
        padding: ${md?.padding || 'unset'};
      }
    `
  }}

  ${({ lg }) => {
    if (!lg) {
      return null
    }

    return css`
      @media screen and ${deviceCssQuery.lg} {
        margin: ${lg?.margin || 'unset'};
        padding: ${lg?.padding || 'unset'};
      }
    `
  }}
`

export const Spacer: React.FC<OwnProps & Partial<Breakpoints<BreakpointSpace>>> = (
  props,
) => {
  const { children, ...others } = props

  return <Space {...others}>{children}</Space>
}
