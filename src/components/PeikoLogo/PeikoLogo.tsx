import React from 'react'
import { BaseImage } from '@/components/BaseImage'
import { ImageProps } from 'next/image'
import peikoImg from '@/images/peiko-logo.svg'

type TProps = Omit<ImageProps, 'src'>

export const PeikoLogo: React.FC<TProps> = (props) => (
  <BaseImage {...props} src={peikoImg} alt="Peiko logo" title="Peiko logo" priority />
)
