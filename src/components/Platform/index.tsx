import React from 'react'
import { IAward } from '@/features/about/api/types'
import { BaseImage } from '@/components/BaseImage'
import * as Styled from './styled'

type TProps = {
  data: IAward[]
}

export const Platform: React.FC<TProps> = ({ data }) => (
  <Styled.Root>
    {data.map((x) => (
      <Styled.Container key={x.id} href={x.url}>
        <BaseImage
          alt={x.title}
          title={x.title}
          src={x.photo}
          layout="fill"
          objectFit="contain"
        />
      </Styled.Container>
    ))}
  </Styled.Root>
)
