import Image, { ImageProps } from 'next/image'

type TBaseImage = ImageProps

export const BaseImage: React.FC<TBaseImage> = (props) => {
  const { src } = props

  return <>{src && <Image {...props} src={src} />}</>
}
