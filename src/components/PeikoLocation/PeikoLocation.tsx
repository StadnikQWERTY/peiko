import React from 'react'
import { Text, TText } from '@/components/Text'
import useTranslation from 'next-translate/useTranslation'

type TPeikoLocation = Omit<TText, 'children'>

export const PeikoLocation: React.FC<TPeikoLocation> = (props) => {
  const { t } = useTranslation('common')

  return (
    <Text variant="paragraph" {...props}>
      {t('peikoLocation.cityAndCountry')}
      <br />
      {t('peikoLocation.street')}
    </Text>
  )
}
