import React, { FC } from 'react'
import styled, { useTheme } from 'styled-components'
import { Text } from '@/components/Text'
import useTranslation from 'next-translate/useTranslation'
import EyeIcon from '@/components/icons/EyeIcon'

const BlogInfo = styled.div`
  display: flex;
  margin-top: auto;
  gap: 25px;
`

type Tcolor = 'white'

type TBlogInfoText = {
  color?: Tcolor
  fetching?: boolean
}

const BlogInfoText = styled(Text)<TBlogInfoText>`
  color: ${(props) =>
    props.color === 'white'
      ? props.theme.palette.white
      : props.theme.palette.primaryText};
  opacity: ${(props) => (props.fetching ? '0' : '1')};
`

const ViewsInfo = styled.div`
  display: flex;
  gap: 8px;
  align-items: center;
  justify-content: center;
`

interface OwnProps {
  date: string
  views?: number
  color?: Tcolor
  fetching?: boolean
}

const DateAndReadInfo: FC<OwnProps> = (props) => {
  const { t } = useTranslation('common')
  const { date, views, color, fetching } = props
  const theme = useTheme()

  return (
    <BlogInfo>
      <BlogInfoText color={color} variant="paragraph">
        {date}
      </BlogInfoText>
      <ViewsInfo>
        <EyeIcon
          color={color === 'white' ? theme.palette.white : theme.palette.primaryText}
        />
        {views && (
          <BlogInfoText color={color} variant="paragraph" fetching={fetching}>
            {t('minRead', { time: views })}
          </BlogInfoText>
        )}
      </ViewsInfo>
    </BlogInfo>
  )
}

export default DateAndReadInfo
