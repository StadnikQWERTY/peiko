import React from 'react'
import styled from 'styled-components'
import { Text, TText } from '@/components/Text'
import { BaseLink } from '@/components/BaseLink'
import { ITag } from '@/requests/types'

type THashtags = {
  tags: ITag[]
  hashtagVariant?: TText['variant']
  generateLink?: string
}

export const StyledHashtags = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  margin: -2px -5px;
`

export const HashtagContainer = styled.div`
  padding: 2px 5px;
`
export const Link = styled(BaseLink)`
  padding: 2px 5px;
`

export const HashtagText = styled(Text)`
  color: ${(props) => props.theme.palette.primary};
  cursor: pointer;
  font-size: 16px;

  &:hover {
    color: ${(props) => props.theme.palette.secondaryLightText} !important;
  }
`

export const Hashtags: React.FC<THashtags> = (props) => {
  const {
    generateLink,
    tags,
    hashtagVariant = 'secondaryParagraph',
    ...otherProps
  } = props

  return (
    <StyledHashtags {...otherProps}>
      {tags.map((tag) => {
        const url = `${generateLink}/${tag.uri}`

        return (
          <React.Fragment key={tag.id}>
            {generateLink ? (
              <Link href={url}>
                <HashtagText variant={hashtagVariant}>#{tag.name}</HashtagText>
              </Link>
            ) : (
              <HashtagContainer>
                <HashtagText variant={hashtagVariant}>#{tag.name}</HashtagText>
              </HashtagContainer>
            )}
          </React.Fragment>
        )
      })}
    </StyledHashtags>
  )
}
