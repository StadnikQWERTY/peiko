import { TPaginationData, TPaginationDataInput } from './types'

export const getPaginationData = ({
  currentPage,
  lastPage,
}: TPaginationDataInput): TPaginationData => {
  const paginationData: TPaginationData = {
    currentPage,
    pagesBefore: [],
    pagesAfter: [],
  }

  if (currentPage > 3) {
    paginationData.firstPage = 1
  }

  if (currentPage > 4) {
    paginationData.dotsBefore = true
  }

  if (currentPage > 1) {
    const pagesBefore = []
    for (let i = 2; i > 0; i -= 1) {
      if (currentPage - i !== 0) {
        pagesBefore.push(currentPage - i)
      }
    }
    paginationData.pagesBefore = pagesBefore
  }

  if (lastPage - currentPage > 0) {
    const pagesAfter = []
    for (let i = 1; i < 3; i += 1) {
      if (currentPage + i > lastPage) {
        break
      }
      pagesAfter.push(currentPage + i)
    }
    paginationData.pagesAfter = pagesAfter
  }

  if (lastPage - currentPage > 3) {
    paginationData.dotsAfter = true
  }

  if (lastPage - currentPage > 2) {
    paginationData.lastPage = lastPage
  }

  return paginationData
}
