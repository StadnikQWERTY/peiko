import styled, { css } from 'styled-components'
import { CSSProperties } from 'react'
import { deviceCssQuery } from '@/styling/breakpoints'
import { Text } from '../Text'

export const Container = styled.nav<{ justifyContent?: CSSProperties['justifyContent'] }>`
  display: flex;
  justify-content: ${(props) => props.justifyContent || 'flex-end'};
  width: 100%;
  flex-wrap: wrap;
`

export const Dots = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  position: relative;
  height: 40px;
  width: 40px;
  margin: 0 4px;

  span {
    position: relative;
    top: -4px;
    color: ${({ theme }) => theme.palette.pagination};
  }
`

export const PaginationButton = styled.div<{ $active: boolean }>`
  height: 30px;
  min-width: 30px;
  margin: 0 12px;
  border-bottom: 2px solid transparent;
  flex-shrink: 0;

  button,
  a {
    height: 100%;
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
    outline: none;
    background: rgba(0, 0, 0, 0);
    border: none;

    &:hover {
      cursor: pointer;
    }
  }

  span {
    color: ${({ theme }) => theme.palette.pagination};
  }

  ${(p) =>
    p.$active &&
    css`
      background-color: rgba(0, 0, 0, 0);
      background-clip: padding-box;
      border-color: ${p.theme.palette.primary};
      span {
        color: ${p.theme.palette.primary};
      }
      pointer-events: none;
    `}
`

export const Arrow = styled.div`
  height: 30px;
  min-width: 30px;

  @media screen and ${deviceCssQuery.xs} {
    margin-left: 0;
    margin-top: 25px;
  }

  @media screen and ${deviceCssQuery.sm} {
    margin-left: 48px;
    margin-top: 0;
  }

  display: flex;
  justify-content: center;
  align-items: center;
`

export const ArrowTitle = styled(Text)`
  color: ${(p) => p.theme.palette.primary};
  display: flex;
  gap: 5px;
`
