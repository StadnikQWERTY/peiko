export type TPaginationData = {
  currentPage: number
  pagesBefore: number[]
  pagesAfter: number[]
  firstPage?: number
  dotsBefore?: boolean
  dotsAfter?: boolean
  lastPage?: number
}

export type TPaginationDataInput = {
  currentPage: number
  lastPage: number
}
