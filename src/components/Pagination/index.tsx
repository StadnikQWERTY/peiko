import useTranslation from 'next-translate/useTranslation'
import { CSSProperties, FC } from 'react'
import { useBreakpoints } from '@/hooks/useBreakpoints'
import { BaseLink } from '../BaseLink'
import { getPaginationData } from './helper'
import * as Styled from './styled'

interface Pagination {
  currentPage: number
  lastPage: number
  getLinkTo?: (page?: string) => string
  onClick?: (page: number) => void
  justifyContent?: CSSProperties['justifyContent']
}

const dotsComponent = (
  <Styled.Dots>
    <span>...</span>
  </Styled.Dots>
)

const Arrow: FC<{ className?: string }> = (props) => {
  const { className } = props
  return (
    <svg
      width="20"
      height="20"
      viewBox="0 0 20 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      className={className}
    >
      <path
        d="M7.5 5L12.5 10L7.5 15"
        stroke="#41B6E6"
        strokeWidth="1.5"
        strokeLinecap="square"
      />
    </svg>
  )
}

export const Pagination: React.FC<Pagination> = ({
  currentPage,
  lastPage,
  onClick,
  getLinkTo,
  justifyContent,
}) => {
  const { isMobile } = useBreakpoints()
  const { t } = useTranslation('common')
  if (lastPage === 1) return <></>

  const paginationData = getPaginationData({ currentPage, lastPage })

  const renderButton = (page: number) => (
    <Styled.PaginationButton
      role="button"
      tabIndex={0}
      $active={page === currentPage}
      onClick={() => onClick?.(page)}
    >
      {getLinkTo ? (
        <BaseLink href={getLinkTo(page > 1 ? `/${page}` : '')}>
          <span>{page}</span>
        </BaseLink>
      ) : (
        <button type="button">
          <span>{page}</span>
        </button>
      )}
    </Styled.PaginationButton>
  )

  return (
    <Styled.Container justifyContent={justifyContent}>
      {paginationData.firstPage && renderButton(paginationData.firstPage)}
      {paginationData.dotsBefore && dotsComponent}
      {paginationData.pagesBefore[0] && renderButton(paginationData.pagesBefore[0])}
      {paginationData.pagesBefore[1] && renderButton(paginationData.pagesBefore[1])}
      {paginationData.currentPage && renderButton(paginationData.currentPage)}
      {paginationData.pagesAfter[0] && renderButton(paginationData.pagesAfter[0])}
      {paginationData.pagesAfter[1] && renderButton(paginationData.pagesAfter[1])}
      {paginationData.dotsAfter && dotsComponent}
      {paginationData.lastPage && renderButton(paginationData.lastPage)}
      {currentPage < lastPage && (
        <Styled.Arrow
          role="button"
          tabIndex={0}
          onClick={() => onClick?.(currentPage + 1)}
          style={{ flexBasis: isMobile ? '100%' : 'auto' }}
        >
          {getLinkTo ? (
            <BaseLink href={getLinkTo(currentPage + 1 > 1 ? `/${currentPage + 1}` : '')}>
              <Styled.ArrowTitle variant="mainButton">
                {t('next')} <Arrow />
              </Styled.ArrowTitle>
            </BaseLink>
          ) : (
            <Styled.ArrowTitle variant="mainButton">
              {t('next')} <Arrow />
            </Styled.ArrowTitle>
          )}
        </Styled.Arrow>
      )}
    </Styled.Container>
  )
}
