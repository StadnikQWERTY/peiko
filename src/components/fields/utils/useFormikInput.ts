import useTranslation from 'next-translate/useTranslation'
import { ChangeEvent } from 'react'
import { getFieldError } from '@/utils/getFieldError'
import { TFormik } from '@/types/formik'

interface IUseFormikInput {
  handleChange: (e: ChangeEvent) => void
  fieldError: string
}

export function useFormikInput(formik: TFormik, name: string): IUseFormikInput {
  const { t } = useTranslation('validation')
  const field = formik.getFieldProps(name)
  const { touched, error } = formik.getFieldMeta(name)

  const handleChange = (e: ChangeEvent) => {
    field.onChange(e)

    if (error) return
    formik.setFieldError(name, '')
  }

  const fieldError = getFieldError({ touched, error, t })

  return {
    handleChange,
    fieldError,
  }
}
