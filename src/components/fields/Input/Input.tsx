import React, { ChangeEvent } from 'react'
import styled, { css } from 'styled-components'
import { Text } from '@/components/Text'

const StyledInput = styled.input`
  margin-top: auto;
  padding-bottom: 12px;
  min-height: 16px;
  font-size: 16px;
  line-height: 27.2px;
  background: ${(props) => props.theme.palette.primaryBg};
  outline: none;
  border: unset;
  color: white;

  &:-webkit-autofill,
  &:-webkit-autofill:hover,
  &:-webkit-autofill:focus,
  &:-webkit-autofill:active {
    transition: 'color 9999s ease-out, background-color 9999s ease-out';
    transition-delay: 9999s;
  }
`

const StyledTitle = styled(Text)`
  position: absolute;
  pointer-events: none;
  transform-origin: top left;
  left: 0;
  top: 28px;
  transition: 200ms ease;
`

const labeledPlaceholder = css`
  transform: translate(0, 0px);
  font-size: 12px;
  line-height: 18px;
  top: 0;
  transition: 200ms ease;
  color: ${(props) => props.theme.palette.secondaryLightText};
`

const StyledError = styled(Text)`
  color: ${(props) => props.theme.palette.danger};
  font-size: 14px;
  line-height: 18px;
  text-align: right;
`

const StyledRoot = styled.div`
  position: relative;
`

const StyledInputContainer = styled.label<{ $error?: boolean; $filled: boolean }>`
  position: relative;
  display: flex;
  flex-direction: column;
  height: 60px;
  box-sizing: border-box;
  border-bottom: 1px solid ${(props) => props.theme.palette.secondaryText};
  transition: 200ms ease;
  
  ${(props) =>
    props.$error &&
    css`
      border-bottom: 1px solid ${(props) => props.theme.palette.danger};
    `}
  
  

  &:focus-within ${StyledTitle} {
    ${labeledPlaceholder}
    ${(props) =>
      props.$error && {
        color: props.theme.palette.danger,
      }}
  }

  & ${StyledTitle} {
    ${(props) => props.$filled && labeledPlaceholder}
    ${(props) =>
      props.$filled &&
      props.$error && {
        color: props.theme.palette.danger,
      }}

  &:focus-within {
    border-bottom: 1px solid ${(props) => props.theme.palette.primary};
  }
`

export type TInput = {
  label: string
  value?: string | number
  errorMsg?: string
  onChange?: (event: ChangeEvent) => void
  name: string
  type?: string
  disabled?: boolean
  isLabelFormat?: boolean
  inputComponent?: React.ReactNode
}

export const Input: React.FC<TInput> = (props) => {
  const {
    value,
    label,
    errorMsg,
    type,
    disabled,
    name,
    isLabelFormat = false,
    onChange,
    inputComponent,
  } = props

  const valueExist =
    value !== undefined && typeof value === 'string' ? Boolean(value.length) : true

  return (
    <StyledRoot>
      <StyledInputContainer
        $error={Boolean(errorMsg)}
        $filled={valueExist || isLabelFormat}
      >
        <StyledTitle tag="span" variant="secondaryParagraph">
          {label}
        </StyledTitle>

        {inputComponent || (
          <StyledInput
            value={value}
            type={type}
            disabled={disabled}
            name={name}
            onChange={onChange}
          />
        )}
      </StyledInputContainer>

      {errorMsg && <StyledError>{errorMsg}</StyledError>}
    </StyledRoot>
  )
}
