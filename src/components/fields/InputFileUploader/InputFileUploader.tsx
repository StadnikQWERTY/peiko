import React, { ChangeEvent, useEffect, useRef, useState } from 'react'
import { Text } from '@/components/Text'
import useTranslation from 'next-translate/useTranslation'
import { FileIcon } from '@/components/icons/FileIcon'
import { CheckMarkIcon } from '@/components/icons/CheckMarkIcon'
import * as Styled from './styled'

export type TInputFileUploader = {
  onChange?: (file?: File | null) => void
  name: string
  errorMsg?: string
  value: string
}

export const InputFileUploader: React.FC<TInputFileUploader> = (props) => {
  const { onChange, errorMsg, value } = props

  const { t } = useTranslation('form')
  const inputRef = useRef<HTMLInputElement>(null)
  const [file, setFile] = useState<File | null>()

  useEffect(() => {
    if (!value) {
      setFile(null)
    }
  }, [value])

  const handleClick = () => {
    if (inputRef.current) {
      inputRef.current.click()
    }
  }

  const handleChange = (event: ChangeEvent) => {
    if (onChange) {
      const target = event.target as HTMLInputElement

      if (target && target.files) {
        const file = target.files[0]
        setFile(file)
        onChange(file)
      }
    }
  }

  const handleRemoveFile = () => {
    setFile(null)
    if (onChange) {
      onChange()
    }
    if (inputRef.current) {
      inputRef.current.value = ''
    }
  }

  return (
    <Styled.Root>
      <Styled.Input ref={inputRef} onChange={handleChange} type="file" />

      <Styled.Field>
        <Styled.UploadingDescription onClick={handleClick}>
          {file ? (
            <>
              <CheckMarkIcon />
              <Text variant="secondaryButton">{t('fileUploaded')}</Text>
            </>
          ) : (
            <>
              <Styled.Arrow color="white" />
              <Text variant="secondaryButton">{t('uploadFile')}</Text>
            </>
          )}
        </Styled.UploadingDescription>
        {file && (
          <Styled.FileInfo>
            <FileIcon />
            <Styled.FileName>{file?.name}</Styled.FileName>
            <Styled.CloseIconWrapper onClick={handleRemoveFile} />
          </Styled.FileInfo>
        )}
      </Styled.Field>

      {errorMsg && <Styled.Error>{errorMsg}</Styled.Error>}
    </Styled.Root>
  )
}
