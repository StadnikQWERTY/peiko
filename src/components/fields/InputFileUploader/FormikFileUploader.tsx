import React from 'react'
import { TFormik } from '@/types/formik'
import { useFormikInput } from '@/components/fields/utils/useFormikInput'
import { InputFileUploader, TInputFileUploader } from './InputFileUploader'

export type TFormikInput = {
  formik: TFormik
} & Omit<TInputFileUploader, 'value'>

export const FormikFileUploader: React.FC<TFormikInput> = ({ formik, ...props }) => {
  const field = formik.getFieldProps(props.name)

  const { fieldError } = useFormikInput(formik, props.name)

  const handleChange = (file?: File | null) => {
    formik.setFieldValue(props.name, file)

    if (fieldError) return
    formik.setFieldError(props.name, '')
  }

  return (
    <InputFileUploader
      {...props}
      {...field}
      onChange={handleChange}
      errorMsg={fieldError}
    />
  )
}
