import styled from 'styled-components'
import { ArrowIcon } from '@/components/icons/ArrowIcon'
import { Text } from '@/components/Text'
import { CloseIcon } from '@/components/icons/CloseIcon'

export const Root = styled.div`
  display: flex;
  flex-direction: column;
  min-height: 16px;
`

export const Input = styled.input`
  display: none;
`

export const UploadingDescription = styled.div`
  display: grid;
  grid-auto-flow: column;
  grid-auto-columns: max-content;
  grid-gap: 7.67px;
  align-items: center;
  cursor: pointer;
`

export const FileInfo = styled.div`
  display: grid;
  grid-template-columns: 9.33px 1fr 8px;
  grid-gap: 8px;
  align-items: center;
`

export const Arrow = styled(ArrowIcon)`
  transform: rotate(-45deg);
`

export const Error = styled(Text)`
  color: ${(props) => props.theme.palette.danger};
  font-size: 14px;
  line-height: 18px;
`

export const Field = styled.div`
  display: flex;
  flex-wrap: wrap;
  gap: 17px;
  align-items: center;
`

export const CloseIconWrapper = styled(CloseIcon)`
  cursor: pointer;
  &:hover {
    path {
      stroke: #21a0ca;
    }
  }
`

export const FileName = styled(Text)`
  font-weight: 400;
  font-size: 14px;
`
