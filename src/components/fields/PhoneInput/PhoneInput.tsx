import ReactPhoneInput, { PhoneInputProps } from 'react-phone-input-2'
import 'react-phone-input-2/lib/style.css'
import styled from 'styled-components'
import { Input, TInput } from '@/components/fields/Input'
import { Z_INDEX } from '@/styling/z-index'

export type TPhoneInput = PhoneInputProps &
  Omit<TInput, 'onChange'> & {
    label: string
    name: string
  }

const StyledPhoneInputContainer = styled.div`
  display: flex;
  flex-direction: column;
  height: 60px;
  transition: 200ms ease;
  color: ${(props) => props.theme.palette.secondaryLightText};

  .react-tel-input .flag-dropdown.open {
    z-index: ${Z_INDEX.SELECT_DROPDOWN};
  }

  .react-tel-input .country-list .country-name {
    color: ${(props) => props.theme.palette.secondaryText};
  }

  .react-tel-input {
    margin-top: auto;
    width: 100%;

    .input {
      color: white;
      background: ${(props) => props.theme.palette.primaryBg};
      border: none;
      border-radius: 0;
      width: 100%;
      height: 39px;
      font-family: 'Montserrat', serif;
    }

    .button {
      background: ${(props) => props.theme.palette.primaryBg};
      border: none;

      .selected-flag {
        transition: opacity 150ms;
        border-radius: 0;

        &.open {
          background: ${(props) => props.theme.palette.primaryBg};
          border-radius: 0;
        }

        &:hover {
          background: ${(props) => props.theme.palette.primaryBg};
          opacity: 0.75;
          transition: opacity 150ms;
        }
      }
    }
  }
`

export const PhoneInput: React.FC<TPhoneInput> = (props) => {
  const { label, name, errorMsg, onChange, value, ...otherProps } = props

  return (
    <StyledPhoneInputContainer>
      <Input
        label={label}
        name={name}
        isLabelFormat
        errorMsg={errorMsg}
        inputComponent={
          <ReactPhoneInput
            value={value}
            country="us"
            preferredCountries={['us', 'ua']}
            excludeCountries={['ru']}
            inputClass="input"
            buttonClass="button"
            dropdownClass="dropdown"
            onChange={onChange}
            placeholder={label}
            inputProps={{
              name,
            }}
            {...otherProps}
          />
        }
      />
    </StyledPhoneInputContainer>
  )
}
