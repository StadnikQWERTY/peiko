import React, { ChangeEvent } from 'react'
import { TFormik } from '@/types/formik'
import { getFieldError } from '@/utils/getFieldError'
import useTranslation from 'next-translate/useTranslation'
import { Input, TInput } from '../Input'

type TFormikInput = {
  formik: TFormik
} & TInput

export const FormikInput: React.FC<TFormikInput> = ({ formik, ...props }) => {
  const { t } = useTranslation('validation')
  const field = formik.getFieldProps(props.name)
  const { touched, error } = formik.getFieldMeta(props.name)

  const handleChange = (e: ChangeEvent) => {
    field.onChange(e)

    if (error) return
    formik.setFieldError(props.name, '')
  }

  const fieldError = getFieldError({ touched, error, t })

  return <Input {...props} {...field} onChange={handleChange} errorMsg={fieldError} />
}
