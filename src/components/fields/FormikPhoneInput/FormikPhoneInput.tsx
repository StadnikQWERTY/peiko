import React from 'react'
import { TFormik } from '@/types/formik'
import { getFieldError } from '@/utils/getFieldError'
import useTranslation from 'next-translate/useTranslation'
import { PhoneInput } from '@/components/fields/PhoneInput'
import { TPhoneInput } from '@/components/fields/PhoneInput/PhoneInput'
import { NonUndefined } from '@/types/declaration'

export type TFormikPhoneInput = {
  formik: TFormik
} & TPhoneInput

type THandleChangeParams = Parameters<NonUndefined<TPhoneInput['onChange']>>

export const FormikPhoneInput: React.FC<TFormikPhoneInput> = ({ formik, ...props }) => {
  const { t } = useTranslation('validation')
  const { touched, error } = formik.getFieldMeta(props.name)

  const field = formik.getFieldProps(props.name)

  const handleChange = (...args: THandleChangeParams) => {
    const [value] = args

    formik.setFieldValue(props.name, value)

    if (error) return
    formik.setFieldError(props.name, '')
  }

  const fieldError = getFieldError({ touched, error, t })

  return (
    <PhoneInput {...props} {...field} onChange={handleChange} errorMsg={fieldError} />
  )
}
