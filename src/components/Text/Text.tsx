import React, { CSSProperties } from 'react'
import styled, { css } from 'styled-components'
import { FontStyle, FontWeight } from '@/styling/fonts'
import { deviceCssQuery } from '@/styling/breakpoints'

export type TText = {
  tag?: keyof JSX.IntrinsicElements
  variant?:
    | 'h1'
    | 'h1Italic'
    | 'h2'
    | 'h3ForBlog'
    | 'h3'
    | 'h4'
    | 'paragraph'
    | 'paragraphForBlog'
    | 'secondaryParagraph'
    | 'thirdParagraph'
    | 'link'
    | 'mainButton'
    | 'secondaryButton'
  children?: React.ReactNode
  align?: CSSProperties['textAlign']
  textTransform?: CSSProperties['textTransform']
  fontStyle?: CSSProperties['fontStyle']
  fontWeight?: CSSProperties['fontWeight']
  margin?: CSSProperties['margin']
  className?: string
  onClick?: () => void
  lg?: {
    fontSize?: CSSProperties['fontSize']
    lineHeight?: CSSProperties['lineHeight']
  }
}

export const StyledText = styled.div<TText>((props) => {
  const { palette } = props.theme

  const baseStyles = css`
    text-align: ${props.align};
    text-transform: ${props.textTransform};
    margin: ${props.margin};
    font-style: ${props.fontStyle};
    color: ${palette.primaryText};
    font-weight: ${FontWeight.Regular};
  `

  switch (props.variant) {
    case 'h1':
      return css`
        ${baseStyles};
        font-weight: ${props.fontWeight || FontWeight.Bold};

        @media screen and ${deviceCssQuery.xs} {
          font-size: 36px;
          line-height: 54px;
        }

        @media screen and ${deviceCssQuery.md} {
          font-size: 56px;
          line-height: 64px;
        }

        @media screen and ${deviceCssQuery.lg} {
          font-size: ${props.lg?.fontSize || '80px'};
          line-height: ${props.lg?.lineHeight || '97.52px'};
        }
      `

    case 'h1Italic':
      return css`
        ${baseStyles};
        font-weight: ${props.fontWeight || FontWeight.SemiBold};
        font-style: ${FontStyle.Italic};

        @media screen and ${deviceCssQuery.xs} {
          font-size: 36px;
          line-height: 54px;
        }

        @media screen and ${deviceCssQuery.md} {
          font-size: 56px;
          line-height: 64px;
        }

        @media screen and ${deviceCssQuery.lg} {
          font-size: 96px;
          line-height: 117.02px;
        }
      `

    case 'h2':
      return css`
        ${baseStyles};
        font-weight: ${props.fontWeight || FontWeight.Bold};

        @media screen and ${deviceCssQuery.xs} {
          font-size: 24px;
          line-height: 36px;
        }

        @media screen and ${deviceCssQuery.md} {
          font-size: 40px;
          line-height: 60px;
        }
      `

    case 'h3ForBlog':
      return css`
        ${baseStyles};
        font-style: normal;
        font-weight: ${props.fontWeight || '600'};
        letter-spacing: 0.03em;
        color: #14161d;

        @media screen and ${deviceCssQuery.xs} {
          font-size: 24px;
          line-height: 36px;
        }

        @media screen and ${deviceCssQuery.md} {
          font-size: 32px;
          line-height: 150%;
        }
      `

    case 'h3':
      return css`
        ${baseStyles};
        font-weight: ${props.fontWeight || FontWeight.SemiBold};

        @media screen and ${deviceCssQuery.xs} {
          font-size: 18px;
          line-height: 27px;
        }

        @media screen and ${deviceCssQuery.md} {
          font-size: 20px;
          line-height: 150%;
        }
      `

    case 'h4':
      return css`
        ${baseStyles};
        font-size: 14px;
        font-weight: ${props.fontWeight || FontWeight.SemiBold};
        line-height: 21px;
        color: ${palette.secondaryLightText};
      `

    case 'paragraph':
      return css`
        ${baseStyles};
        font-size: 16px;
        font-weight: ${props.fontWeight || FontWeight.Regular};
        line-height: 27.2px;
      `

    case 'paragraphForBlog':
      return css`
        ${baseStyles};
        font-style: normal;
        font-weight: ${props.fontWeight || '400'};
        font-size: 16px;
        line-height: 190%;
        color: #14161d;
      `

    case 'secondaryParagraph':
      return css`
        ${baseStyles};
        font-size: 14px;
        font-weight: ${props.fontWeight || FontWeight.Regular};
        line-height: 21px;
      `

    case 'thirdParagraph':
      return css`
        ${baseStyles};
        font-size: 12px;
        font-weight: ${props.fontWeight || FontWeight.Regular};
        line-height: 18px;
      `

    case 'link':
      return css`
        ${baseStyles};
        font-weight: ${props.fontWeight || FontWeight.Medium};

        @media screen and ${deviceCssQuery.xs} {
          font-size: 10px;
          line-height: 12.19px;
        }

        @media screen and ${deviceCssQuery.md} {
          font-size: 14px;
          line-height: 17.7px;
        }
      `

    case 'mainButton':
      return css`
        ${baseStyles};
        font-weight: ${props.fontWeight || FontWeight.Medium};
        text-transform: uppercase;
        font-size: 16px;
        line-height: 19.5px;
      `

    case 'secondaryButton':
      return css`
        ${baseStyles};
        font-weight: ${props.fontWeight || FontWeight.SemiBold};
        text-transform: uppercase;
        font-size: 13px;
        line-height: 16px;
      `

    default:
      return css`
        ${baseStyles};
        font-size: 14px;
        color: ${palette.primaryText};
      `
  }
})

export const Text = React.forwardRef<HTMLElement, TText>((props, ref) => {
  const { tag = 'div', children, className, ...otherProps } = props

  return (
    <>
      {children && (
        <StyledText as={tag} ref={ref} className={className} {...otherProps}>
          {children}
        </StyledText>
      )}
    </>
  )
})

Text.displayName = 'Text'
