import React from 'react'

type TExternalLinkProps = {
  href: string
  className?: string
}

export const ExternalLink: React.FC<TExternalLinkProps> = ({
  className,
  children,
  href,
}) => (
  <a className={className} target="_blank" rel="noopener noreferrer nofollow" href={href}>
    {children}
  </a>
)
