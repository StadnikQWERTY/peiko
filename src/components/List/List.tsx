import React, { CSSProperties, FC } from 'react'
import styled from 'styled-components'
import { Text } from '@/components/Text'
import { customParser } from '@/utils/customParser'

const StyledDot = styled.div`
  margin-top: 3px;
`

const Dot: FC<{ color?: CSSProperties['color'] }> = ({ color }) => (
  <StyledDot>
    <svg
      width="8"
      height="8"
      viewBox="0 0 8 8"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <circle cx="4" cy="4" r="4" fill={color || '#41B6E6'} />
    </svg>
  </StyledDot>
)

const ListContainer = styled.div`
  display: flex;
  flex-direction: column;
  gap: 16px;
`

const ListItem = styled.div`
  display: flex;
  align-items: flex-start;
`

const StyledText = styled(Text)`
  color: ${(props) => props.theme.palette.primary};
`

interface IListItem {
  id?: string | number
  name?: string
  component?: JSX.Element
}

interface OwnProps {
  list: IListItem[]
  listTag?: string
  dotColor?: CSSProperties['color']
}

const List: React.FC<OwnProps> = (props) => {
  const { list, dotColor, listTag = 'ul' } = props
  return (
    <ListContainer>
      {list.map((listItem, listIndex) => {
        const { name, component, id } = listItem
        return (
          <ListItem key={name || id}>
            {listTag === 'ul' ? (
              <Dot color={dotColor} />
            ) : (
              <StyledText variant="paragraph" tag="p" margin={0}>
                {listIndex + 1}
              </StyledText>
            )}
            {name && (
              <Text variant="paragraph" tag="p" margin="0 0 0 16px">
                {customParser(name)}
              </Text>
            )}
            {component && component}
          </ListItem>
        )
      })}
    </ListContainer>
  )
}

export default List
