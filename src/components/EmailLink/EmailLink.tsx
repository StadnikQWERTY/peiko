import React from 'react'
import { PEIKO_CONTACTS, PEIKO_LINK_CONTACTS } from '@/constants/publicInfo'
import { Text, TText } from '@/components/Text'

type TEmailLink = {
  className?: string
  textProps?: Omit<TText, 'children'>
}

export const EmailLink: React.FC<TEmailLink> = (props) => {
  const { className, textProps } = props

  return (
    <a href={PEIKO_LINK_CONTACTS.EMAIL} className={className}>
      <Text variant="h3" {...textProps}>
        {PEIKO_CONTACTS.EMAIL}
      </Text>
    </a>
  )
}
