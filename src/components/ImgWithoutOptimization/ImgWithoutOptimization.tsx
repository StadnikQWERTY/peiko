interface OwnProps {
  skeletonWidth?: number
  skeletonHeight?: number
}

export const ImgWithoutOptimization: React.FC<
  React.DetailedHTMLProps<React.ImgHTMLAttributes<HTMLImageElement>, HTMLImageElement> &
    OwnProps
> = (props) => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { alt, ...imgProps } = props

  return <img {...imgProps} alt={alt || ''} />
}
