import React from 'react'
import styled from 'styled-components'
import { aubergineTheme } from '@/components/maps/GoogleMap/utils/aubergineTheme'

const StyledMapWrapper = styled.div`
  height: 100%;
  width: 100%;
`

const peikoLatLng = { lat: 50.418116, lng: 30.4762092 } as const

export const GoogleMap: React.FC = () => {
  const ref = React.useRef<HTMLDivElement>(null)
  const [map, setMap] = React.useState<google.maps.Map>()

  React.useEffect(() => {
    if (ref.current && !map) {
      setMap(
        new window.google.maps.Map(ref.current, {
          center: peikoLatLng,
          zoom: 12,
          streetViewControl: false,
          mapTypeControl: false,
          styles: [...aubergineTheme],
        }),
      )
    }
  }, [ref, map])

  React.useEffect(() => {
    if (map) {
      new google.maps.Marker({
        position: peikoLatLng,
        title: 'Peiko',
        map,
      })
    }
  }, [map])

  return <StyledMapWrapper ref={ref} />
}
