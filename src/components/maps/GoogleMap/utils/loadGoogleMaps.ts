import { ScriptLibraryLoader } from '@/utils/ScriptLibraryLoader'
import { GOOGLE_MAPS_API_KEY } from '@/constants/publicInfo'

export const googleMapLoader = new ScriptLibraryLoader('googleMaps', {
  src: `https://maps.googleapis.com/maps/api/js?key=${GOOGLE_MAPS_API_KEY}`,
  async: true,
})
