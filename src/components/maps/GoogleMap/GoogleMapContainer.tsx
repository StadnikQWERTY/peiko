import React from 'react'
import { Wrapper, Status } from '@googlemaps/react-wrapper'
import { Loader } from '@/components/Loader'
import { GOOGLE_MAPS_API_KEY } from '@/constants/publicInfo'
import { GoogleMap } from './GoogleMap'

const render = (status: Status) => {
  switch (status) {
    case Status.LOADING:
      return <Loader width="80px" height="80px" />
    case Status.FAILURE:
      return <div>Error load google map</div>
    case Status.SUCCESS:
      return <GoogleMap />
    default:
      return <div>Error load google map</div>
  }
}

export const GoogleMapContainer: React.FC = () => (
  <Wrapper apiKey={GOOGLE_MAPS_API_KEY || ''} render={render} />
)
