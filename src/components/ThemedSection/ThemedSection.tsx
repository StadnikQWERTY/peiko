import React, { useEffect, useRef, useState } from 'react'
import styled from 'styled-components'
import { TTheme } from '@/styling/theme'
import { ThemeAppProvider } from '@/styling/ThemeAppProvider'
import { useBreakpoints } from '@/hooks/useBreakpoints'
import { mainLayoutActions } from '@/store/mainLayout/slice'
import { useRedux } from '@/hooks/useRedux'

type TThemedSection = {
  theme: TTheme
  className?: string
}

const StyledRoot = styled.div`
  background: ${(props) => props.theme.palette.primaryBg};
`

export const ThemedSection: React.FC<TThemedSection> = (props) => {
  const { children, theme, className } = props

  const { isDesktop } = useBreakpoints()

  const [sectionId] = useState(Math.random().toString())

  const desktopContainerRef = useRef<HTMLDivElement>(null)
  const rootRef = useRef<HTMLDivElement>(null)

  const [, dispatch] = useRedux()

  useEffect(() => {
    function handleScreenSizeChanges() {
      if (rootRef.current) {
        const elementDistances = rootRef.current.getBoundingClientRect()
        const canSwitchToThisSectionMobile =
          elementDistances.top <= 0 && elementDistances.bottom >= 0

        const canSwitchToThisSectionDesktop =
          elementDistances.top <= 100 &&
          elementDistances.bottom >= 0 &&
          elementDistances.bottom >= window.innerHeight - 100

        if (
          (isDesktop && canSwitchToThisSectionDesktop) ||
          (!isDesktop && canSwitchToThisSectionMobile)
        ) {
          dispatch(mainLayoutActions.setNewTheme(theme))
          dispatch(mainLayoutActions.setCurrentThemedSectionId(sectionId))

          if (isDesktop && desktopContainerRef.current && rootRef.current) {
            desktopContainerRef.current.style.height = `${rootRef.current.offsetHeight}px`
          }
        }
      }
    }

    handleScreenSizeChanges()

    window.addEventListener('scroll', handleScreenSizeChanges)
    window.addEventListener('resize', handleScreenSizeChanges)

    return () => {
      window.removeEventListener('scroll', handleScreenSizeChanges)
      window.removeEventListener('resize', handleScreenSizeChanges)
    }
  }, [isDesktop, sectionId])

  return (
    <ThemeAppProvider theme={theme}>
      <StyledRoot ref={rootRef} className={className}>
        {children}
      </StyledRoot>
    </ThemeAppProvider>
  )
}
