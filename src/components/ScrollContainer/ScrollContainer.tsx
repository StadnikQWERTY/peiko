import React, { useEffect, useRef } from 'react'
import { useIsomorphicLayoutEffect } from 'react-use'
import * as S from './styled'

export type ScrollContainerProps = {
  gap?: number
  children: React.ReactNode
}

export const ScrollContainer: React.FC<ScrollContainerProps> = ({
  gap = 20,
  children,
}) => {
  const container = useRef<HTMLDivElement>(null)
  const scrollContainer = useRef<HTMLDivElement | null>(null)
  const scrollbar = useRef<HTMLDivElement | null>(null)
  const thumb = useRef<HTMLDivElement | null>(null)

  const calcscrollContainerPosition = () => {
    if (!container.current || !scrollContainer.current) return

    const distanceBetweencontainerAndDocument =
      document.documentElement.clientWidth - container.current.clientWidth

    const marginDistance = distanceBetweencontainerAndDocument / 2

    // Set scrollContainer margin and padding
    scrollContainer.current.style.marginLeft = `-${marginDistance}px`
    scrollContainer.current.style.marginRight = `-${marginDistance}px`
    scrollContainer.current.style.paddingLeft = `${marginDistance}px`
    scrollContainer.current.style.paddingRight = `${marginDistance}px`
  }

  const calcScrollbarPosition = () => {
    if (!scrollContainer.current || !scrollbar.current || !thumb.current) return

    const { clientWidth, scrollWidth, scrollLeft } = scrollContainer.current

    const thumbWidth = (clientWidth / scrollWidth) * clientWidth - 40
    const thumbPosition = (scrollLeft / scrollWidth) * clientWidth

    thumb.current.style.width = `${thumbWidth}px`
    thumb.current.style.transform = `translateX(${thumbPosition}px)`

    // Hide scrollbar if not needed
    if (clientWidth >= scrollWidth) {
      scrollbar.current.style.display = 'none'
    } else {
      scrollbar.current.style.display = 'block'
    }
  }

  const handleScroll = () => {
    calcScrollbarPosition()
  }

  const handleResize = () => {
    calcscrollContainerPosition()
    calcScrollbarPosition()
  }

  useIsomorphicLayoutEffect(() => {
    handleResize()
  }, [])

  useEffect(() => {
    if (!scrollContainer.current) return

    const scrollContainerElement = scrollContainer.current

    scrollContainerElement.addEventListener('scroll', handleScroll)
    window.addEventListener('resize', handleResize)

    return () => {
      scrollContainerElement.removeEventListener('scroll', handleScroll)
      window.removeEventListener('resize', handleResize)
    }
  }, [])

  return (
    <div ref={container}>
      <S.Container gap={gap} innerRef={scrollContainer} nativeMobileScroll>
        {children}
      </S.Container>
      <S.ScrollBar ref={scrollbar}>
        <S.Thumb ref={thumb} />
      </S.ScrollBar>
    </div>
  )
}
