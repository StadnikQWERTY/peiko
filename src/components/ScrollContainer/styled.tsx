import styled from 'styled-components'
import ScrollContainer, { ScrollContainerProps } from 'react-indiana-drag-scroll'

type ContainerProps = ScrollContainerProps & { gap: number }

export const Container = styled(ScrollContainer)<ContainerProps>`
  display: flex;
  flex-wrap: nowrap;
  overflow-x: auto !important;
  gap: ${({ gap }) => `${gap}px`};
  -webkit-overflow-scrolling: touch; // Enable smooth scrolling on iOS devices
  -ms-overflow-style: none;
  scrollbar-width: none;

  &::-webkit-scrollbar {
    display: none;
  }

  & > * {
    flex: 0 0 auto;
  }

  & img {
    user-select: none;
  }
`

export const ScrollBar = styled.div`
  position: relative;
  height: 5px;
  margin-top: 48px;
  transform: scaleX(0.3);
  transform-origin: left center;
  background: ${({ theme }) => theme.palette.scrollbarBg};
`

export const Thumb = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  height: 5px;
  background: ${({ theme }) => theme.palette.primary};
`
