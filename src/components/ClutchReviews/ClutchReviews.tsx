import useTranslation from 'next-translate/useTranslation'
import styled from 'styled-components'
import { Text } from '@/components/Text'
import { FontWeight } from '@/styling/fonts'
import { BaseImage } from '@/components/BaseImage'
import { mockClutch } from './mock'

const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`

const TopBlock = styled.div`
  display: flex;
  align-items: center;
`

const Rating = styled(Text)`
  display: flex;
  font-size: 120px;
  line-height: 146px;
  margin-right: 12px;
`

const MiddleBlock = styled.div`
  display: flex;
  align-items: center;
`

const Reviews = styled(Text)`
  margin-right: 16px;
  font-size: 14px;
  font-weight: ${FontWeight.Medium};
`

const BottomBlock = styled.div`
  margin-top: 12px;
`

const Peiko = styled(Text)`
  font-size: 14px;
  color: ${(props) => props.theme.palette.primary};
`

const Description = styled(Text)`
  font-size: 14px;
`

export const ClutchReviews: React.FC = () => {
  const { t } = useTranslation('contact-us')

  return (
    <Container>
      <TopBlock>
        <Rating>{mockClutch.clutch.raiting}</Rating>
        <BaseImage
          src="/images/blue_star.svg"
          alt="Blue star"
          height="49px"
          width="49px"
          priority
        />
      </TopBlock>

      <MiddleBlock>
        <Reviews>{t(`${mockClutch.clutch.rewiews}`)}</Reviews>
        <BaseImage
          src="/images/clutch_text.svg"
          alt="Blue star"
          height="45px"
          width="71px"
          priority
        />
      </MiddleBlock>

      <BottomBlock>
        <Peiko tag="span">{`Peiko `}</Peiko>
        <Description variant="paragraph" tag="span">
          {t(`${mockClutch.clutch.descriptiopn}`)}
        </Description>
      </BottomBlock>
    </Container>
  )
}
