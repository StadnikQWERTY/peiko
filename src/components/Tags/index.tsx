import React, { FC } from 'react'
import styled, { css } from 'styled-components'
import useTranslation from 'next-translate/useTranslation'
import CrossIcon from '@/components/icons/CrossIcon'
import { BaseLink } from '@/components/BaseLink'
import { ITag } from '@/requests/types'
import { Text } from '@/components/Text'

const LinkBlock = styled.div`
  display: flex;
  align-items: center;
`

const StyledCloseButton = styled.div`
  margin-left: 12px;
  &:hover {
    cursor: pointer;
  }
`

export const TagsContainer = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  column-gap: 16px;
  gap: 12px;
`

export const HashtagText = styled(Text)<{ selected: boolean }>`
  color: ${(props) => props.theme.palette.primary} !important;
  cursor: pointer;
  border: 1px solid ${(props) => props.theme.palette.primary};
  padding: 3px 16px;
  border-radius: 20px;

  &:first-of-type {
    margin-left: 0;
  }

  &:hover {
    color: ${(props) => props.theme.palette.secondaryLightText} !important;
  }

  ${(p) =>
    p.selected &&
    css`
      color: ${p.theme.palette.secondaryLightText} !important;
      border: 1px solid ${p.theme.palette.secondaryLightText};
    `}
`

export const Container = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
`

export const Header = styled(Text)`
  color: ${(props) => props.theme.palette.lightText};
  text-transform: uppercase;
  letter-spacing: 5px;
  margin-bottom: 24px;
`

export const ShowAll = styled(Text)`
  color: ${(props) => props.theme.palette.primary};
  margin-left: 16px;
  font-weight: normal;
  cursor: pointer;
`

interface OwnProps {
  tags: ITag[]
  selectedTag?: string
  header: string
  uri?: string
  showAllUri?: string
  isMulti?: boolean
  toLowerCase?: boolean
}

const Tags: FC<OwnProps> = (props) => {
  const {
    tags,
    selectedTag,
    header,
    uri: tagsUri,
    showAllUri,
    isMulti,
    toLowerCase,
  } = props
  const { t } = useTranslation('common')

  return (
    <>
      <Container>
        <Header variant="h4">{header}</Header>
        {selectedTag && (
          <BaseLink href={`${showAllUri || tagsUri}`}>
            <ShowAll variant="h4">{t('showAll')}</ShowAll>
          </BaseLink>
        )}
      </Container>
      <TagsContainer>
        {tags.map((tag) => {
          const { uri, id, name } = tag
          const selected = selectedTag?.split('_').includes(uri) || false

          const fullLink = () => {
            const linkSort = `${selectedTag ? `${selectedTag}_` : ''}${uri}`
              .split('_')
              .sort()
              .join('_')
            const nextLink = `${tagsUri}/${linkSort}`

            if (isMulti) {
              return selected ? `${tagsUri}/${selectedTag}` : nextLink
            }
            return `${tagsUri}/${uri}`
          }

          const closeLink = () => {
            if (isMulti) {
              const closeTagLink = selectedTag
                ?.split('_')
                .sort()
                .filter((tag) => tag !== uri)
                .join('_')

              if (closeTagLink) {
                return `${tagsUri}/${closeTagLink}`
              }

              return `${showAllUri || tagsUri}`
            }

            return `${showAllUri || tagsUri}`
          }

          return (
            <>
              <LinkBlock>
                <BaseLink href={fullLink()} key={id}>
                  <HashtagText variant="paragraph" selected={selected}>
                    #{!toLowerCase ? name : name.toLowerCase()}
                  </HashtagText>
                </BaseLink>
                {selected && (
                  <BaseLink href={closeLink()}>
                    <StyledCloseButton>
                      <CrossIcon />
                    </StyledCloseButton>
                  </BaseLink>
                )}
              </LinkBlock>
            </>
          )
        })}
      </TagsContainer>
    </>
  )
}

export default Tags
