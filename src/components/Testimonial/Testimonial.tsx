import React from 'react'
import styled from 'styled-components'
import { Text } from '@/components/Text'
import { FontWeight } from '@/styling/fonts'
import { deviceCssQuery } from '@/styling/breakpoints'
import { QuotesIcon } from '@/components/icons/QuotesIcon'

interface ITestimonialProps {
  text: string
  author?: string
  company?: string
  externalLink?: string
}

const Container = styled.div`
  display: grid;
  width: 100%;

  @media screen and ${deviceCssQuery.xs} {
    grid-template-columns: 30px 1fr 30px;
  }

  @media screen and ${deviceCssQuery.md} {
    grid-template-columns: 44px 1fr 44px;
  }
`

const AuthorContainer = styled.div`
  grid-column: 1 / span 3;
  align-self: flex-end;
  justify-self: flex-end;
  margin-top: 24px;
`

const Author = styled(Text)`
  color: ${(p) => p.theme.palette.secondaryText};
  text-transform: uppercase;
  text-align: right;
  font-size: 13px;
`

const Company = styled(Text)`
  color: ${(p) => p.theme.palette.primary};
  text-transform: uppercase;
  text-align: right;
  font-size: 13px;
`

const Testimonial: React.FC<ITestimonialProps> = ({
  author,
  company,
  text,
  externalLink,
}) => (
  <Container>
    <QuotesIcon position="left" />
    <Text variant="h3" fontWeight={FontWeight.Regular}>
      {text}
    </Text>
    <QuotesIcon position="right" />
    <AuthorContainer>
      <Author variant="paragraph">{author}</Author>
      <a href={externalLink || '/'} target="_blank" rel="noopener noreferrer nofollow">
        <Company variant="paragraph">{company}</Company>
      </a>
    </AuthorContainer>
  </Container>
)

export default Testimonial
