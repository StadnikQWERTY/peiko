import React from 'react'
import { Text, TText } from '@/components/Text'
import styled from 'styled-components'

const StyledText = styled(Text)`
  margin: 0;
  padding: 0;
  color: ${(props) => props.theme.palette.lightText};
  text-transform: uppercase;
  letter-spacing: 5px;
`

type TSectionName = TText

export const SectionName: React.FC<TSectionName> = (props) => {
  const { children, ...otherProps } = props

  return (
    <StyledText variant="h4" tag={otherProps.tag || 'h4'} {...otherProps}>
      {children}
    </StyledText>
  )
}
