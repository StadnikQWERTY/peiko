import React from 'react'
import useTranslation from 'next-translate/useTranslation'
import styled from 'styled-components'
import { SectionTitles } from '@/components/sections/SectionTitles'
import { SectionContainer } from '@/components/sections/SectionContainer'
import { ROUTES } from '@/routes'
import { ArrowIcon } from '@/components/icons/ArrowIcon'
import { BaseLink } from '@/components/BaseLink'
import { SectionHeader } from '@/components/sections/SectionHeader'
import { SectionName } from '@/components/sections/SectionName'
import { CaseBlock } from '@/components/sections/Cases/components/CaseBlock'
import { ICase } from '@/requests/types'
import { deviceCssQuery } from '@/styling/breakpoints'

type TCases = {
  cases: ICase[]
}

const StyledSectionHeader = styled(SectionHeader)``

const StyleSectionName = styled(SectionName)`
  margin-bottom: 20px;
`

const StyledHeader = styled.div``

export const LinkedSectionHeader = styled(BaseLink)`
  display: grid;
  grid-auto-flow: column;
  grid-gap: 9.33px;
  align-items: flex-start;
  grid-auto-columns: max-content;

  & ${StyledSectionHeader} {
    transition: color 200ms;
  }

  &:hover ${StyledSectionHeader} {
    transition: color 200ms;
    color: ${(props) => props.theme.palette.primary};
  }
`

const StyledCaseContainer = styled.div`
  margin-bottom: 80px;
  display: inline-block;

  &:last-child {
    margin-bottom: 0;
  }

  @media screen and ${deviceCssQuery.lg} {
    margin-bottom: 64px;
  }
`

const StyledSectionTitles = styled(SectionTitles)`
  @media screen and ${deviceCssQuery.xs} {
    margin-bottom: 64px;
  }

  @media screen and ${deviceCssQuery.lg} {
    margin-bottom: 100px;
  }
`

export const Cases: React.FC<TCases> = (props) => {
  const { cases } = props

  const { t } = useTranslation('common')

  const sectionHeader = (
    <StyledHeader>
      <StyleSectionName>{t('casesSectionName')}</StyleSectionName>
      <LinkedSectionHeader href={ROUTES.CASES}>
        <StyledSectionHeader>{t('projects')}</StyledSectionHeader>
        <ArrowIcon />
      </LinkedSectionHeader>
    </StyledHeader>
  )

  return (
    <SectionContainer>
      <StyledSectionTitles header={sectionHeader} />
      {cases.map((item) => (
        <StyledCaseContainer key={item.id}>
          <CaseBlock portfolioCase={item} />
        </StyledCaseContainer>
      ))}
    </SectionContainer>
  )
}
