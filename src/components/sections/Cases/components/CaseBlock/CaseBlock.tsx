import React from 'react'
import useTranslation from 'next-translate/useTranslation'
import { FontStyle } from '@/styling/fonts'
import { BaseImage } from '@/components/BaseImage'
import { ICase } from '@/requests/types'
import { ROUTES } from '@/routes'
import { Button } from '@/components/buttons/Button'
import { BaseLink } from '@/components/BaseLink'
import * as Styled from './styled'

type TCaseBlock = {
  portfolioCase: ICase
}

export const CaseBlock: React.FC<TCaseBlock> = (props) => {
  const { portfolioCase } = props
  const { t } = useTranslation('common')

  const fullLink = `${ROUTES.CASE}/${portfolioCase.uri}`

  return (
    <Styled.Root>
      <div>
        <Styled.Header
          variant="h1Italic"
          tag="h3"
          margin={0}
          fontStyle={FontStyle.Italic}
        >
          {portfolioCase.name}
        </Styled.Header>

        <Styled.Description variant="h3" tag="p">
          {portfolioCase.short_preview}
        </Styled.Description>

        <Styled.StackCont>
          {portfolioCase.technologies.map((item) => (
            <Styled.StackItem key={item.id}>
              <Styled.StackImage>
                <BaseImage
                  src={item.photo}
                  alt={item.name}
                  title={item.name}
                  width={30}
                  height={60}
                />
              </Styled.StackImage>
            </Styled.StackItem>
          ))}
        </Styled.StackCont>

        <Styled.ButtonDesk>
          <BaseLink href={fullLink}>
            <Styled.ButtonContainer>
              <Button variant="primary">{t('caseStudy')}</Button>
            </Styled.ButtonContainer>
          </BaseLink>
        </Styled.ButtonDesk>
      </div>

      <div>
        <Styled.Image>
          <BaseLink href={fullLink}>
            <BaseImage
              src={portfolioCase.photo_preview}
              alt={portfolioCase.name}
              title={portfolioCase.name}
              width={520}
              height={480}
            />
          </BaseLink>
        </Styled.Image>

        <Styled.ButtonMob>
          <BaseLink href={fullLink}>
            <Styled.ButtonContainer>
              <Button variant="primary">{t('caseStudy')}</Button>
            </Styled.ButtonContainer>
          </BaseLink>
        </Styled.ButtonMob>
      </div>
    </Styled.Root>
  )
}
