import styled from 'styled-components'
import { deviceCssQuery } from '@/styling/breakpoints'
import { FontWeight } from '@/styling/fonts'
import { Text } from '@/components/Text'

export const Root = styled.div`
  display: grid;
  grid-column-gap: 64px;
  grid-row-gap: 32px;
  align-items: flex-start;
  width: 100%;

  @media screen and ${deviceCssQuery.xs} {
    grid-template-columns: 1fr;
  }

  @media screen and ${deviceCssQuery.md} {
    grid-auto-flow: row;
    grid-template-columns: 1fr 1fr;
  }
`

export const Header = styled(Text)`
  @media screen and ${deviceCssQuery.lg} {
    font-size: 60px;
    line-height: 90px;
    margin-top: 32px;
  }
`

export const Description = styled(Text)`
  font-weight: ${FontWeight.Medium};
  margin-top: 24px;
`

export const StackCont = styled.div`
  display: flex;
  flex-wrap: wrap;
`

export const StackItem = styled.div`
  height: 30px;
  overflow: hidden;
  margin: 24px 24px 0 0;
  flex-shrink: 0;
`

export const StackImage = styled.div`
  position: relative;
  bottom: 30px;

  &:hover {
    bottom: 0;
  }
`
export const ButtonDesk = styled.div`
  display: none;
  @media screen and ${deviceCssQuery.sm} {
    display: block;
  }
`

export const ButtonMob = styled.div`
  display: block;
  @media screen and ${deviceCssQuery.sm} {
    display: none;
  }
`

export const ButtonContainer = styled.div`
  margin-top: 16px;
  display: inline-block;

  @media screen and ${deviceCssQuery.sm} {
    margin-top: 32px;
  }
`

export const Image = styled.div`
  display: flex;
  justify-content: center;

  & img {
    border-radius: 3px;
  }
`
