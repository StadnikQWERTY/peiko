import useTranslation from 'next-translate/useTranslation'
import React from 'react'
import styled from 'styled-components'
import { SectionContainer } from '@/components/sections/SectionContainer'
import { SectionTitles } from '@/components/sections/SectionTitles'
import { IReview } from '@/requests/types'
import { deviceCssQuery } from '@/styling/breakpoints'
import { ROUTES } from '@/routes'
import { Testimonial } from './Testimonial'

const Container = styled.div`
  display: grid;
  width: 100%;
  gap: 80px;

  @media screen and ${deviceCssQuery.xs} {
    grid-template-columns: 1fr;
  }

  @media screen and ${deviceCssQuery.lg} {
    grid-template-columns: 1fr 1fr;
  }
`

type TTestimonialsSectionProps = {
  data: IReview[]
}

export const TestimonialsSection: React.FC<TTestimonialsSectionProps> = ({ data }) => {
  const { t } = useTranslation('common')
  return (
    <SectionContainer space={90}>
      <SectionTitles header={t('testimonials.header')} tagHeader="h2" />
      <Container>
        {data.map((x) => (
          <Testimonial
            key={x.name}
            text={x.review}
            author={`${x.name}, ${x.position}`}
            company={x.case_name}
            companyUri={x.case_uri && `${ROUTES.CASE}/${x.case_uri}`}
          />
        ))}
      </Container>
    </SectionContainer>
  )
}
