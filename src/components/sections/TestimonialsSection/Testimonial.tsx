import React from 'react'
import styled from 'styled-components'
import StarRatings from 'react-star-ratings'
import { FontWeight } from '@/styling/fonts'
import { Text } from '@/components/Text'
import { deviceCssQuery } from '@/styling/breakpoints'
import { QuotesIcon } from '@/components/icons/QuotesIcon'
import { BaseLink } from '@/components/BaseLink'
import { ClutchTextIcon } from '@/components/icons/ClutchTextIcon'

interface ITestimonialProps {
  text: string
  author?: string
  company?: string
  companyUri?: string
}

const Container = styled.div`
  display: grid;
  width: 100%;

  @media screen and ${deviceCssQuery.xs} {
    grid-template-columns: 30px 1fr;
  }

  @media screen and ${deviceCssQuery.md} {
    grid-template-columns: 44px 1fr;
  }
`

const Content = styled.div`
  display: flex;
  flex-direction: column;
`

const ReviewText = styled(Text)`
  font-weight: ${FontWeight.SemiBold};
  margin-bottom: 24px;
`

const Bottom = styled.div`
  margin-top: auto;
`

const PlatformCont = styled.div`
  display: flex;
  align-items: center;
`

const PlatformLogo = styled(ClutchTextIcon)`
  margin-right: 16px;
  position: relative;
  top: -1px;
`

const AuthorCont = styled.div`
  display: flex;
  margin-top: 24px;
`

const Author = styled(Text)`
  color: ${(p) => p.theme.palette.secondaryText};
  text-transform: uppercase;
  text-align: left;
  font-size: 13px;
`

const Company = styled(Text)`
  color: ${(p) => p.theme.palette.primary};
  text-transform: uppercase;
  margin-left: 16px;
  font-size: 13px;
  white-space: nowrap;
  flex-shrink: 0;
`

export const Testimonial: React.FC<ITestimonialProps> = ({
  author,
  company,
  text,
  companyUri,
}) => (
  <Container>
    <QuotesIcon position="left" />

    <Content>
      <ReviewText variant="h3">{text}</ReviewText>

      <Bottom>
        <PlatformCont>
          <PlatformLogo />
          <StarRatings
            rating={5}
            starDimension="16px"
            starSpacing="3px"
            starRatedColor="#FF3D2E"
          />
        </PlatformCont>
        <AuthorCont>
          <Author variant="paragraph">{author}</Author>
          <BaseLink href={companyUri}>
            <Company variant="paragraph">{company}</Company>
          </BaseLink>
        </AuthorCont>
      </Bottom>
    </Content>
  </Container>
)
