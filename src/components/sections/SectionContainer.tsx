import React, { CSSProperties } from 'react'
import styled, { css } from 'styled-components'
import { deviceCssQuery } from '@/styling/breakpoints'

const Root = styled.div<{ space?: number }>`
  display: flex;
  flex-direction: column;
  align-items: center;

  ${(p) =>
    p.space &&
    css`
      @media screen and ${deviceCssQuery.xs} {
        padding: ${p.space / 2}px 0;
      }

      @media screen and ${deviceCssQuery.md} {
        padding: ${p.space}px 0;
      }
    `}
`

export const contentMaxWidth = 1160
// export const mdContentSpace = 20
export const smallPadding = 20

const Content = styled.div<{ $desktopMenuExist?: boolean }>`
  max-width: ${contentMaxWidth}px;
  width: 100%;
  height: 100%;

  @media screen and ${deviceCssQuery.xs} {
    padding: 0 ${smallPadding}px;
  }

  @media screen and ${deviceCssQuery.md} {
    padding: 0 ${smallPadding}px;
  }
`

type TSectionContainer = {
  space?: number
  className?: string
  contentStyles?: CSSProperties
  desktopMenuExist?: boolean
  children: React.ReactNode
  contentRef?: React.MutableRefObject<HTMLDivElement | null>
  id?: string
}

export const SectionContainer = React.forwardRef<HTMLDivElement, TSectionContainer>(
  (props, ref) => {
    const {
      children,
      className,
      space,
      contentStyles = {},
      desktopMenuExist = true,
      contentRef,
      id,
    } = props

    return (
      <Root className={className} space={space} ref={ref} id={id}>
        <Content
          $desktopMenuExist={desktopMenuExist}
          style={contentStyles}
          ref={contentRef}
        >
          {children}
        </Content>
      </Root>
    )
  },
)

SectionContainer.displayName = 'SectionContainer'
