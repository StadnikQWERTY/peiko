import React, { useEffect, useRef, useState } from 'react'
import styled from 'styled-components'
import { gsap } from 'gsap'
import useTranslation from 'next-translate/useTranslation'
import { SectionContainer } from '@/components/sections/SectionContainer'
import { SectionTitles } from '@/components/sections/SectionTitles'
import { ITechnology } from '@/requests/types'
import { deviceCssQuery } from '@/styling/breakpoints'
import { useBreakpoints } from '@/hooks/useBreakpoints'
import { TechnologyBlock } from './components/TechnologyBlock'

const StyledTechnologies = styled.div`
  display: grid;
  justify-content: space-between;
  width: 100%;

  @media screen and ${deviceCssQuery.xs} {
    grid-gap: 15px;
    grid-template-columns: repeat(auto-fill, minmax(90px, 1fr));
  }

  @media screen and ${deviceCssQuery.sm} {
    grid-template-columns: repeat(4, 1fr);
  }

  @media screen and ${deviceCssQuery.md} {
    grid-gap: 65px;
    grid-template-columns: repeat(6, 1fr);
  }
`

type TTTechnology = {
  technologies: ITechnology[]
}

export const Technology: React.FC<TTTechnology> = (props) => {
  const { technologies } = props
  const { t } = useTranslation('common')

  const technologyRefs = useRef<HTMLDivElement[]>([])

  const addToRefs = (el: HTMLDivElement) => {
    if (el && !technologyRefs.current.includes(el)) {
      technologyRefs.current.push(el)
    }
  }

  useEffect(() => {
    const tween = gsap.fromTo(
      technologyRefs.current,
      {
        opacity: 0,
        y: 20,
      },
      {
        y: 0,
        delay: 0.01,
        duration: 0.5,
        opacity: 1,
        scrollTrigger: {
          trigger: technologyRefs.current,
          start: 'center bottom',
          toggleActions: 'play none none none',
        },
        stagger: {
          each: 0.03,
        },
      },
    )

    return () => {
      tween.kill()
    }
  }, [])

  const [clickedOnMobileIndex, setClickedOnMobileIndex] = useState<number | null>()
  const { isMobile } = useBreakpoints()

  useEffect(() => {
    if (!isMobile) {
      setClickedOnMobileIndex(null)
    }
  }, [isMobile])

  return (
    <SectionContainer>
      <SectionTitles
        tagHeader="h2"
        name={t('technology.name')}
        header={t('technology.weFindBestSolution')}
      />

      <StyledTechnologies>
        {technologies.map((technology, index) => (
          <TechnologyBlock
            key={technology.id}
            technology={technology}
            rootRef={addToRefs}
            clickedOnMobile={clickedOnMobileIndex === index}
            onClick={() => {
              if (isMobile) {
                setClickedOnMobileIndex((prev) => (prev === index ? null : index))
              }
            }}
          />
        ))}
      </StyledTechnologies>
    </SectionContainer>
  )
}
