import React from 'react'
import { ITechnology } from '@/requests/types'
import { BaseImage } from '@/components/BaseImage'
import * as Styled from './styled'

type TTechnologyBlock = {
  technology: ITechnology
  rootRef?: (el: HTMLDivElement) => void
  clickedOnMobile: boolean
  onClick: () => void
}

export const TechnologyBlock: React.FC<TTechnologyBlock> = (props) => {
  const { technology, rootRef, clickedOnMobile, ...otherProps } = props

  return (
    <Styled.Root ref={rootRef} $clickedOnMobile={clickedOnMobile} {...otherProps}>
      <Styled.ImageContainer>
        <BaseImage
          src={technology.photo}
          alt={technology.name}
          layout="fill"
          onContextMenu={(event) => {
            // It prevents context menu opening, when you touch on picture on mobile
            event.preventDefault()
            event.stopPropagation()
            return false
          }}
        />
      </Styled.ImageContainer>

      <Styled.TechnologyName variant="secondaryParagraph">
        {technology.name}
      </Styled.TechnologyName>
    </Styled.Root>
  )
}
