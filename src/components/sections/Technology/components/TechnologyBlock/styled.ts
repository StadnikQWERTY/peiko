import styled, { css } from 'styled-components'
import { Text } from '@/components/Text'
import { deviceCssQuery } from '@/styling/breakpoints'

export const ImageContainer = styled.div`
  position: relative;
  height: 40px;
  width: 40px;
`

export const TechnologyName = styled(Text)`
  opacity: 0;
  margin-top: 12px;
`

export const Root = styled.div<{ $clickedOnMobile: boolean }>((props) => {
  const hoveredImg = css`
    object-position: top;
  `

  const hoveredImgContainer = css`
    transform: scale(1.2);
    transition: 350ms;
  `

  const hoveredTechnologyName = css`
    opacity: 1;
    transition: 350ms;
  `

  const mobileStyle =
    props.$clickedOnMobile &&
    css`
      & img {
        ${hoveredImg};
      }

      & ${ImageContainer} {
        ${hoveredImgContainer};
      }

      & ${TechnologyName} {
        ${hoveredTechnologyName}
      }
    `

  return css`
    display: flex;
    flex-direction: column;
    align-items: center;
    position: relative;

    img {
      object-fit: cover;
      object-position: bottom;
    }

    @media screen and ${deviceCssQuery.xs} {
      ${mobileStyle};
    }

    @media screen and ${deviceCssQuery.md} {
      &:hover {
        & img {
          ${hoveredImg};
        }

        & ${ImageContainer} {
          ${hoveredImgContainer};
        }

        & ${TechnologyName} {
          ${hoveredTechnologyName}
        }
      }
    }
  `
})
