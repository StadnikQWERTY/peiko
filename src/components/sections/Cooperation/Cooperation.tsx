import React from 'react'
import styled from 'styled-components'
import { Text } from '@/components/Text'
import { BaseImage } from '@/components/BaseImage'
import { ICooperation } from '@/requests/types'

const Container = styled.section`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
`

const Column = styled.div`
  display: flex;
  flex-direction: column;
  min-width: 280px;
  margin-bottom: 60px;
`

const Header = styled.div`
  display: flex;
  align-items: center;
  & > *:last-child {
    margin-left: 16px;
  }
`

const TextHeader = styled(Text)`
  color: ${(p) => p.theme.palette.primaryText};
  text-transform: uppercase;
`

const List = styled.div`
  margin: 24px 0 0 8px;
`
const ListItem = styled.div`
  display: flex;
  margin-top: 12px;

  &:before {
    content: '';
    display: block;
    height: 4px;
    width: 4px;
    border-radius: 2px;
    background: ${(p) => p.theme.palette.primaryText};
    margin-right: 8px;
    margin-top: 9px;
  }
`

interface TProps {
  data: ICooperation[]
}

export const Cooperation: React.FC<TProps> = ({ data }) => (
  <Container>
    {data.map((column) => (
      <Column key={column.id}>
        <Header>
          <BaseImage src={column.icon} height={28} width={28} />
          <TextHeader variant="h4">{column.name}</TextHeader>
        </Header>
        <List>
          {column.items.map((listItem) => (
            <ListItem key={listItem.name}>
              <Text variant="secondaryParagraph">{listItem.name}</Text>
            </ListItem>
          ))}
        </List>
      </Column>
    ))}
  </Container>
)
