import styled from 'styled-components'
import { deviceCssQuery } from '@/styling/breakpoints'
import { Award } from './components/Award'

export const Root = styled.section`
  display: grid;
  grid-gap: 32px;
  grid-auto-flow: row;

  @media screen and ${deviceCssQuery.xs} {
    padding: 32px 0 0;
  }

  @media screen and ${deviceCssQuery.md} {
    padding: 64px 0 0;
  }
`

export const Awards = styled.div`
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
`

export const WrappedAward = styled(Award)`
  @media screen and ${deviceCssQuery.xs} {
    margin: 0 12px;
    flex-basis: 150px;
  }

  @media screen and ${deviceCssQuery.md} {
    margin: 0 60px;
    flex-basis: 200px;
  }
`
