import React, { useEffect, useRef } from 'react'
import { gsap } from 'gsap'
import useTranslation from 'next-translate/useTranslation'
import { IAward } from '@/requests/types'
import { SectionName } from '@/components/sections/SectionName'
import { Platform } from '@/components/Platform'
import * as Styled from './styled'

type TAwardsSection = {
  awards: IAward[]
  className?: string
}

export const PlatformSection: React.FC<TAwardsSection> = (props) => {
  const { awards, className } = props

  const awardsRef = useRef<HTMLDivElement>(null)

  useEffect(() => {
    if (awardsRef.current) {
      gsap.fromTo(
        awardsRef.current,
        {
          opacity: 0,
          y: 40,
        },
        {
          scrollTrigger: {
            trigger: awardsRef.current,
            start: 'center bottom',
            toggleActions: 'play none none none',
          },
          opacity: 1,
          y: 0,
          duration: 0.8,
        },
      )
    }
  }, [])

  const { t } = useTranslation('common')

  return (
    <Styled.Root ref={awardsRef} className={className}>
      <SectionName tag="h2" align="center">
        {t('awards')}
      </SectionName>
      <Platform data={awards} />
    </Styled.Root>
  )
}
