import React from 'react'
import styled from 'styled-components'
import { IAward } from '@/requests/types'
import { BaseImage } from '@/components/BaseImage'

type TAward = {
  award: IAward
  className?: string
}

const StyledRoot = styled.div`
  display: grid;
  grid-auto-rows: 64px auto;
  grid-gap: 16px;
`

const StyledLink = styled.a`
  position: relative;
`

export const Award: React.FC<TAward> = (props) => {
  const { award, className } = props

  return (
    <StyledRoot className={className}>
      <StyledLink href={award.url} target="_blank" rel="noopener noreferrer nofollow">
        <BaseImage
          src={award.photo}
          alt={award.title}
          title={award.title}
          layout="fill"
          objectFit="contain"
        />
      </StyledLink>
    </StyledRoot>
  )
}
