import React, { useEffect, useMemo, useRef, useState } from 'react'
import useTranslation from 'next-translate/useTranslation'
import { SectionContainer } from '@/components/sections/SectionContainer'
import { SectionTitles } from '@/components/sections/SectionTitles'
import { IPartner } from '@/requests/types'
import { Spacer } from '@/components/Spacer/Spacer'
import { PartnerBlock } from '@/components/sections/OurPartners/components/PartnerBlock'
import styled from 'styled-components'
import { deviceCssQuery } from '@/styling/breakpoints'
import { useBreakpoints } from '@/hooks/useBreakpoints'
import { useScreenSize } from '@/hooks/useScreenSize'
import { useMounting } from '@/hooks/useMounting'

type TOurPartners = {
  data: IPartner[]
}

const mobileColumnWidth = 120
const desktopColumnWidth = 185.6

const StyledPartnersContainer = styled.div`
  display: grid;
  grid-gap: 1px;

  @media screen and ${deviceCssQuery.xs} {
    grid-template-columns: repeat(auto-fit, minmax(${mobileColumnWidth}px, 1fr));
    grid-auto-rows: 120px;
  }

  @media screen and ${deviceCssQuery.sm} {
    grid-template-columns: repeat(auto-fit, minmax(${desktopColumnWidth}px, 1fr));
    grid-auto-rows: 168px;
  }
`

export const OurPartners: React.FC<TOurPartners> = (props) => {
  const { data } = props

  const { t } = useTranslation('it-partners')

  const contentRef = useRef<HTMLDivElement>(null)
  const partnerBlockRef = useRef<HTMLDivElement>(null)

  const [emptyBlocksNumber, setEmptyBlocksNumber] = useState(0)

  const { isMobile } = useBreakpoints()
  const { windowWidth } = useScreenSize()

  const partnerBlocks = useMemo(() => {
    const emptyBlocks = Array(emptyBlocksNumber).fill(undefined)
    return [...data, ...emptyBlocks]
  }, [data, emptyBlocksNumber])

  const { mounted } = useMounting()

  useEffect(() => {
    const containerDom = contentRef.current
    const partnerBlockDom = partnerBlockRef.current

    if (containerDom && data.length && partnerBlockDom && windowWidth && mounted) {
      const partnerBlockWidth = partnerBlockRef.current.offsetWidth // divider

      const blocksPerRow = Math.floor(containerDom.offsetWidth / partnerBlockWidth)
      const completelyFilledLinesCount = Math.floor(data.length / blocksPerRow)
      const existedColumnsOnLastRow = Math.abs(
        data.length - blocksPerRow * completelyFilledLinesCount,
      )

      const additionalColumns = existedColumnsOnLastRow
        ? blocksPerRow - existedColumnsOnLastRow
        : 0

      setEmptyBlocksNumber(additionalColumns)
    }
  }, [data, isMobile, windowWidth, mounted])

  return (
    <SectionContainer space={90}>
      <SectionTitles header={t('header')} tagHeader="h2" name={t('name')} />
      <Spacer xs={{ margin: '48px 0 0 0' }}>
        <StyledPartnersContainer ref={contentRef}>
          {partnerBlocks.map((partner, index) => {
            const blockRef = index === 0 ? partnerBlockRef : undefined
            // eslint-disable-next-line react/no-array-index-key
            return <PartnerBlock key={index} partner={partner} rootRef={blockRef} />
          })}
        </StyledPartnersContainer>
      </Spacer>
    </SectionContainer>
  )
}
