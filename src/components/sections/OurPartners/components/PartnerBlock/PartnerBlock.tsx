import React, { MutableRefObject } from 'react'
import styled, { useTheme } from 'styled-components'
import { IPartner } from '@/requests/types'
import { BaseImage } from '@/components/BaseImage'

const StyledRoot = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  outline: 1px solid ${(props) => props.theme.palette.secondary};
  box-sizing: border-box;
  padding: 17px;
`

const StyledImageContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 100%;

  filter: grayscale(1) contrast(0%);
  transition: filter 200ms;

  &:hover {
    filter: grayscale(0) contrast(100%);
    transition: filter 200ms;
  }
`

type TPartnerBlock = {
  partner?: IPartner
  className?: string
  rootRef?: MutableRefObject<HTMLDivElement | null>
}

export const PartnerBlock: React.FC<TPartnerBlock> = (props) => {
  const { partner, className, rootRef } = props

  const theme = useTheme()

  const imgTheme =
    theme.palette.currentTheme === 'dark' ? partner?.photo_black : partner?.photo

  return (
    <StyledRoot className={className} ref={rootRef}>
      {partner && (
        <StyledImageContainer>
          <BaseImage
            src={imgTheme || ''}
            alt={partner.name}
            width={120}
            height={64}
            draggable={false}
            loading="lazy"
          />
        </StyledImageContainer>
      )}
    </StyledRoot>
  )
}
