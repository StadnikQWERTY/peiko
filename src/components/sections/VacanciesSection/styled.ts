import { deviceCssQuery } from '@/styling/breakpoints'
import styled from 'styled-components'
import { BaseLink } from '@/components/BaseLink'

export const VacancyGrid = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  margin-top: 24px;

  @media screen and ${deviceCssQuery.md} {
    grid-template-columns: 2fr 1fr;
  }
  border-bottom: 1px solid ${(props) => props.theme.palette.secondary};
`

export const NoVacancy = styled.div`
  display: block;
  border: 1px solid ${(props) => props.theme.palette.secondary};
  @media screen and ${deviceCssQuery.xs} {
    padding: 24px;
  }

  @media screen and ${deviceCssQuery.md} {
    padding: 32px;
  }
`

export const Vacancy = styled(BaseLink)`
  display: block;
  border: 1px solid ${(props) => props.theme.palette.secondary};

  border-bottom: unset;

  @media screen and ${deviceCssQuery.xs} {
    padding: 24px;
  }

  @media screen and ${deviceCssQuery.md} {
    padding: 32px;
  }
`

export const LearnMore = styled(BaseLink)`
  cursor: pointer;
  display: none;

  border: 1px solid ${(props) => props.theme.palette.secondary};
  border-left: unset;
  border-bottom: unset;

  @media screen and ${deviceCssQuery.md} {
    display: flex;
    justify-content: center;
    align-items: center;
  }
`

export const Container = styled.div`
  display: flex;
  align-items: center;
  gap: 24px;
  margin-top: 16px;
`

export const LocationContainer = styled.div`
  display: flex;
  align-items: center;
  gap: 10px;
`

export const SectionNameContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  flex-wrap: wrap;

  & a:nth-of-type(2) {
    display: none;
  }

  @media screen and ${deviceCssQuery.md} {
    & a:nth-of-type(2) {
      display: block;
    }
  }
`
