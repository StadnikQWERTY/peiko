import { SectionContainer } from '@/components/sections/SectionContainer'
import { SectionName } from '@/components/sections/SectionName'
import { Text } from '@/components/Text'
import { ROUTES } from '@/routes'
import { IVacancy } from '@/requests/types'
import { LocationIcon } from '@/components/icons/LocationIcon'

import React from 'react'
import { BaseLink } from '@/components/BaseLink'
import useTranslation from 'next-translate/useTranslation'
import { ArrowTextButton } from '@/components/buttons/ArrowTextButton'
import styled from 'styled-components'
import * as Styled from './styled'

const StyledLocationIcon = styled(LocationIcon)`
  path {
    fill: ${(props) => props.theme.palette.grayIcon};
  }
`

const StyledText = styled(Text)`
  color: ${(props) => props.theme.palette.grayIcon};
`

const NoVacancies = styled(Text)`
  letter-spacing: 0.2em;
  text-transform: uppercase;
  text-align: center;
`

type TVacanciesSectionProps = {
  data: IVacancy[]
  withoutSeeAll?: boolean
}

export const VacanciesSection: React.FC<TVacanciesSectionProps> = (props) => {
  const { data, withoutSeeAll } = props

  const { t } = useTranslation('common')
  if (data.length === 0) {
    return (
      <SectionContainer space={90}>
        <Styled.NoVacancy>
          <NoVacancies variant="h4" tag="h4">
            There are no open vacancies
          </NoVacancies>
        </Styled.NoVacancy>
      </SectionContainer>
    )
  }

  return (
    <SectionContainer space={90}>
      <Styled.SectionNameContainer>
        <SectionName tag="h2">{t('vacanciesInfo', { amount: data.length })}</SectionName>

        {!withoutSeeAll && (
          <BaseLink href={ROUTES.VACANCIES}>
            <ArrowTextButton>{t('see-all')}</ArrowTextButton>
          </BaseLink>
        )}
      </Styled.SectionNameContainer>

      <Styled.VacancyGrid>
        {data.map((vacancy) => {
          const { id, name, is_hot, location, schedule, uri } = vacancy
          return (
            <React.Fragment key={id}>
              <Styled.Vacancy href={`${ROUTES.VACANCY}/${uri}`}>
                <Text variant="h3">
                  {name} {is_hot && '🔥'}
                </Text>
                <Styled.Container>
                  <Styled.LocationContainer>
                    <StyledLocationIcon />{' '}
                    <StyledText variant="secondaryParagraph">{location}</StyledText>
                  </Styled.LocationContainer>
                  <StyledText variant="secondaryParagraph">{schedule}</StyledText>
                </Styled.Container>
              </Styled.Vacancy>
              <Styled.LearnMore href={`${ROUTES.VACANCY}/${uri}`}>
                <ArrowTextButton>{t('common:learn-more')}</ArrowTextButton>
              </Styled.LearnMore>
            </React.Fragment>
          )
        })}
      </Styled.VacancyGrid>
    </SectionContainer>
  )
}
