import styled from 'styled-components'
import { deviceCssQuery } from '@/styling/breakpoints'
import { SectionTitles } from '@/components/sections/SectionTitles'

export const Root = styled.section``

export const Header = styled(SectionTitles)`
  text-align: center;
  @media screen and ${deviceCssQuery.xs} {
    margin-bottom: 20px;
  }

  @media screen and ${deviceCssQuery.md} {
    margin-bottom: 20px;
    text-align: left;
  }
`

export const ImageCont = styled.div`
  display: flex;
  justify-content: center;
`

export const AwardsBlock = styled.div`
  display: grid;

  @media screen and ${deviceCssQuery.xs} {
    grid-template-columns: repeat(auto-fit, minmax(140px, 1fr));
    row-gap: 10px;
    column-gap: 20px;
  }

  @media screen and ${deviceCssQuery.sm} {
    grid-template-columns: repeat(auto-fit, minmax(180px, min-content));
    row-gap: 20px;
    column-gap: 60px;
  }
`
