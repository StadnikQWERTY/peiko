import React, { useEffect, useRef } from 'react'
import { gsap } from 'gsap'
import useTranslation from 'next-translate/useTranslation'
import { BaseImage } from '@/components/BaseImage'
import { IReward } from '@/requests/types'
import * as Styled from './styled'

type TProps = {
  data: IReward[]
  header?: boolean
  animate?: boolean
}

export const AwardsSection: React.FC<TProps> = ({ data, header, animate }) => {
  const { t } = useTranslation('common')
  const awardsRef = useRef<HTMLDivElement>(null)

  useEffect(() => {
    if (awardsRef.current && animate) {
      gsap.fromTo(
        awardsRef.current,
        {
          opacity: 0,
          y: 40,
        },
        {
          scrollTrigger: {
            trigger: awardsRef.current,
            start: 'center bottom',
            toggleActions: 'play none none none',
          },
          opacity: 1,
          y: 0,
          duration: 0.8,
        },
      )
    }
  }, [])

  return (
    <div ref={awardsRef}>
      {header && (
        <Styled.Header
          tagHeader="h2"
          name={t('awardSectionName')}
          header={t('awardSectionHeader')}
        />
      )}
      <Styled.AwardsBlock>
        {data.map((item) => (
          <Styled.ImageCont key={item.id}>
            <BaseImage
              src={item.photo}
              alt={item.name}
              height="180px"
              width="180px"
              draggable={false}
              loading="lazy"
            />
          </Styled.ImageCont>
        ))}
      </Styled.AwardsBlock>
    </div>
  )
}
