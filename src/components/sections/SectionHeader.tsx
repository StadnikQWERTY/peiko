import React from 'react'
import { Text, TText } from '@/components/Text'
import styled from 'styled-components'

type TSectionName = TText

const StyledText = styled(Text)`
  margin: 0;
  padding: 0;
`

export const SectionHeader: React.FC<TSectionName> = (props) => {
  const { children, ...otherProps } = props

  return (
    <StyledText variant="h2" tag={otherProps.tag || 'h2'} {...otherProps}>
      {children}
    </StyledText>
  )
}
