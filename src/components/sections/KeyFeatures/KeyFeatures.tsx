import React from 'react'
import styled from 'styled-components'
import { deviceCssQuery } from '@/styling/breakpoints'
import { FontStyle, FontWeight } from '@/styling/fonts'
import { Text } from '@/components/Text'
import { IKeyFeatures } from '@/requests/types'

const Container = styled.section`
  display: grid;
  column-gap: 56px;
  row-gap: 24px;

  @media screen and ${deviceCssQuery.xs} {
    grid-template-columns: repeat(auto-fit, minmax(280px, 1fr));
  }

  @media screen and ${deviceCssQuery.sm} {
    row-gap: 40px;
    grid-template-columns: repeat(auto-fit, minmax(300px, 1fr));
  }
`

const Item = styled.div`
  display: flex;
  flex-direction: column;
`

const Value = styled(Text)`
  ${(props) => `background: ${props.theme.palette.gradientBlue}`};
  -webkit-background-clip: text;
  -moz-background-clip: text;
  -webkit-text-fill-color: transparent;
  -moz-text-fill-color: transparent;
  font-size: 45px;
  font-style: ${FontStyle.Normal};
  font-weight: ${FontWeight.SemiBold};
  line-height: 79px;
  margin-bottom: 10px;

  @media screen and ${deviceCssQuery.sm} {
    font-size: 65px;
    line-height: 79px;
  }
`

interface TProps {
  data: IKeyFeatures[]
}

export const KeyFeatures: React.FC<TProps> = ({ data }) => (
  <Container>
    {data.map((item) => (
      <Item key={item.name}>
        <Value tag="span">{item.value}</Value>
        <Text variant="h3" tag="span">
          {item.name}
        </Text>
      </Item>
    ))}
  </Container>
)
