import React from 'react'
import styled from 'styled-components'
import { SectionName } from '@/components/sections/SectionName'
import { deviceCssQuery } from '@/styling/breakpoints'
import { SectionHeader } from '@/components/sections/SectionHeader'

type TSectionTitles = {
  tag?: keyof JSX.IntrinsicElements
  tagHeader?: keyof JSX.IntrinsicElements
  name?: string | React.ReactElement
  header?: string | React.ReactElement
  className?: string
}

const StyledRoot = styled.div`
  display: grid;
  grid-auto-flow: row;
  grid-gap: 16px;

  @media screen and ${deviceCssQuery.xs} {
    margin-bottom: 32px;
  }

  @media screen and ${deviceCssQuery.md} {
    margin-bottom: 48px;
  }
`

export const SectionTitles: React.FC<TSectionTitles> = (props) => {
  const { name, header, className, tag, tagHeader } = props

  const parsedName =
    typeof name === 'string' ? <SectionName tag={tag}>{name}</SectionName> : name

  const parsedHeader =
    typeof header === 'string' ? (
      <SectionHeader tag={tagHeader}>{header}</SectionHeader>
    ) : (
      header
    )

  return (
    <StyledRoot className={className}>
      {parsedName && parsedName}
      {parsedHeader}
    </StyledRoot>
  )
}
