import React from 'react'
import styled from 'styled-components'
import { deviceCssQuery } from '@/styling/breakpoints'
import { Text } from '@/components/Text'
import { BaseImage } from '@/components/BaseImage'
import { IWhyUs } from '@/requests/types'

const Container = styled.section`
  display: grid;
  grid-gap: 32px;

  @media screen and ${deviceCssQuery.xs} {
    grid-template-columns: repeat(auto-fit, minmax(280px, 1fr));
  }

  @media screen and ${deviceCssQuery.sm} {
    grid-template-columns: repeat(auto-fit, minmax(440px, 1fr));
  }
`

const Item = styled.div`
  display: flex;
`
const ItemLeft = styled.div`
  display: flex;
  flex-shrink: 0;
  align-items: start;
  padding: 2px 12px 0 0;
`

const ItemRight = styled.div`
  display: flex;
  flex-direction: column;
`

const Header = styled(Text)`
  margin-bottom: 16px;
`

const Description = styled(Text)`
  margin: 0;
`

interface TProps {
  data: IWhyUs[]
}

export const WhyUs: React.FC<TProps> = ({ data }) => (
  <Container>
    {data.map((item) => (
      <Item key={item.id}>
        <ItemLeft>
          <BaseImage src={item.icon} width={26} height={26} />
        </ItemLeft>
        <ItemRight>
          <Header tag="span" variant="h3">
            {item.name}
          </Header>
          <Description tag="p" variant="paragraph">
            {item.content}
          </Description>
        </ItemRight>
      </Item>
    ))}
  </Container>
)
