import React, { AnchorHTMLAttributes } from 'react'
import Link, { LinkProps } from 'next/link'

type TLinkProps = Omit<LinkProps, 'href'> & {
  href?: string
}

export type TBaseLink = TLinkProps & {
  className?: string
  anchorRef?: React.MutableRefObject<HTMLAnchorElement | null>
} & AnchorHTMLAttributes<unknown>

export const BaseLink: React.FC<TBaseLink> = (props) => {
  const { className, children, anchorRef, href, ...otherProps } = props

  return (
    <>
      {href && (
        <Link href={href} prefetch={false}>
          <a className={className} ref={anchorRef} {...otherProps}>
            {children}
          </a>
        </Link>
      )}
    </>
  )
}
