import React, { CSSProperties, useContext } from 'react'
import styled from 'styled-components'
import { deviceCssQuery } from '@/styling/breakpoints'
import { SocialLinksContext } from '../../context/SocialLinksContext'
import { BaseImage } from '../BaseImage'

const StyledSocialLinks = styled.div<{ $distance?: string; $autoFit?: boolean }>(
  (props) => `
    display: grid;


    ${
      !props.$autoFit &&
      `
      grid-auto-flow: column;
      grid-auto-columns: max-content;
    `
    }

    align-items: center;

    @media screen and ${deviceCssQuery.xs} {
      grid-gap: ${props.$distance || '30px'};
      ${
        props.$autoFit &&
        `grid-template-columns: repeat(auto-fit, ${props.$distance || '30px'});`
      }
    }

    @media screen and ${deviceCssQuery.md} {
      grid-gap: ${props.$distance || '44.25px'};
      ${
        props.$autoFit &&
        `grid-template-columns: repeat(auto-fit, ${props.$distance || '44.25px'});`
      }

    }
  `,
)

const StyledLink = styled.a<{ filter: CSSProperties['filter'] }>`
  transition: 150ms;
  position: relative;
  width: 24px;
  height: 24px;

  img {
    filter: ${(props) => props.filter || 'none'};
    object-position: 0 -24px;
    user-select: none;
  }

  @media (hover: hover) {
    &:hover {
      transform: scale(1.2);
      transition: 150ms;
      img {
        filter: none;
        object-position: 0 0;
      }
    }
  }
`

type TSocialLinks = {
  distance?: string
  className?: string
  filter?: CSSProperties['filter']
  autoFit?: boolean
}

export const SocialLinks: React.FC<TSocialLinks> = (props) => {
  const { distance, className, filter, autoFit } = props
  const socialLinksData = useContext(SocialLinksContext)

  return (
    <StyledSocialLinks className={className} $distance={distance} $autoFit={autoFit}>
      {socialLinksData.map((item) => (
        <StyledLink
          href={item.url}
          key={item.id}
          target="_blank"
          rel="noopener noreferrer nofollow"
          filter={filter}
        >
          <BaseImage
            draggable={false}
            src={item.icon}
            alt={item.alt}
            objectFit="cover"
            layout="fill"
            loading="lazy"
          />
        </StyledLink>
      ))}
    </StyledSocialLinks>
  )
}
