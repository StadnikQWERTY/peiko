import styled from 'styled-components'
import { Z_INDEX } from '@/styling/z-index'
import { deviceCssQuery } from '@/styling/breakpoints'
import { TOP_BAR_MENU_MOBILE_HEIGHT, TOP_BAR_MENU_DESKTOP_HEIGHT } from '@/constants/menu'

export const Panel = styled.div`
  width: 100%;
  z-index: ${Z_INDEX.MENU};
  background: ${(props) => props.theme.palette.primaryBg};
  border-bottom: 1px solid ${(props) => props.theme.palette.secondary};
  box-sizing: border-box;

  transition: 200ms;
`

export const ContentContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;

  @media screen and ${deviceCssQuery.xs} {
    height: ${TOP_BAR_MENU_MOBILE_HEIGHT}px;
  }

  @media screen and ${deviceCssQuery.md} {
    height: ${TOP_BAR_MENU_DESKTOP_HEIGHT}px;
  }
`

export const MenuIconContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;

  @media screen and ${deviceCssQuery.xs} {
    border-left: 1px solid ${(props) => props.theme.palette.secondary};
    width: 84px;
  }

  @media screen and ${deviceCssQuery.md} {
    border-left: unset;
    width: unset;
  }
`

export const LogoContainer = styled.div`
  max-width: 80px;
`
