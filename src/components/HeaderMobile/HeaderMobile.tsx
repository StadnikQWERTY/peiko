import React from 'react'
import { MenuIcon } from '@/components/icons/MenuIcon'
import { ROUTES } from '@/routes'
import { SectionContainer } from '@/components/sections/SectionContainer'
import { useBreakpoints } from '@/hooks/useBreakpoints'
import { BaseLink } from '@/components/BaseLink'
import { PeikoLogo } from '@/components/PeikoLogo'
import * as Styled from './styled'

type TTopMenu = {
  isOpened?: boolean
  onChangeOpeningState: (newValue: boolean) => void
  className?: string
  onLogoClick?: () => void
}

export const HeaderMobile: React.FC<TTopMenu> = ({
  isOpened,
  onChangeOpeningState,
  className,
  onLogoClick,
}) => {
  const { isMobile } = useBreakpoints()

  const content = (
    <Styled.ContentContainer>
      <Styled.LogoContainer>
        <BaseLink
          href={ROUTES.HOME}
          onClick={() => {
            if (isOpened) {
              onChangeOpeningState(!isOpened)
            }
            if (onLogoClick) {
              onLogoClick()
            }
          }}
        >
          <PeikoLogo />
        </BaseLink>
      </Styled.LogoContainer>

      <Styled.MenuIconContainer>
        <MenuIcon opened={isOpened} onClick={() => onChangeOpeningState(!isOpened)} />
      </Styled.MenuIconContainer>
    </Styled.ContentContainer>
  )

  const contentStyled = isMobile ? { paddingRight: 0 } : {}

  return (
    <Styled.Panel className={className}>
      <SectionContainer desktopMenuExist={false} contentStyles={contentStyled}>
        {content}
      </SectionContainer>
    </Styled.Panel>
  )
}
