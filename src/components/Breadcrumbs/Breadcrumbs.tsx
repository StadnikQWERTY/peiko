import React, { CSSProperties } from 'react'
import styled from 'styled-components'
import { BreadcrumbJsonLd } from 'next-seo'
import useTranslation from 'next-translate/useTranslation'
import { Text } from '@/components/Text'
import { useBreakpoints } from '@/hooks/useBreakpoints'
import { BASE_APP_URL } from '@/constants/publicInfo'
import { deviceCssQuery } from '@/styling/breakpoints'
import { getRouteLocale } from '@/utils/get-route-locale'
import { removeTrailingSlash } from '@/utils/remove-trailing-slash'
import { BaseLink } from '../BaseLink'

const BackSvg: React.FC<{ className?: string }> = (props) => {
  const { className } = props
  return (
    <svg
      width="16"
      height="17"
      viewBox="0 0 16 17"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      className={className}
    >
      <path d="M10 12.5L6 8.5L10 4.5" strokeWidth="1.5" strokeLinecap="square" />
    </svg>
  )
}

const StyledBackSvg = styled(BackSvg)`
  stroke: ${(props) => props.theme.palette.secondaryLightText} !important;
  width: 16px;
  height: 17px;
`

const BreadcrumbsWrapper = styled.ul<{
  justifyContent?: CSSProperties['justifyContent']
}>`
  margin: 0;
  padding: 0;
  display: flex;
  text-align: center;

  @media screen and ${deviceCssQuery.xs} {
    justify-content: center;
  }

  @media screen and ${deviceCssQuery.md} {
    justify-content: ${(props) => props.justifyContent || 'center'};
  }
`

const Breadcrumb = styled.li`
  margin: 0 12px;
  text-decoration-line: underline;
  text-decoration-thickness: 1px;
  text-underline-offset: 1px;
  text-decoration-color: ${(props) => props.theme.palette.secondaryLightText};

  &::marker {
    color: ${(props) => props.theme.palette.lightText};
  }

  &:last-child {
    @media screen and ${deviceCssQuery.xs} {
      text-decoration-line: underline;
    }

    @media screen and ${deviceCssQuery.md} {
      text-decoration-line: none;
    }

    margin: 0 0 0 12px;
  }

  a:visited {
    color: ${(props) => props.theme.palette.secondaryLightText};
  }

  a {
    text-decoration-color: ${(props) => props.theme.palette.secondaryLightText};
  }

  &:first-child {
    list-style-type: none;
    margin: 0 12px 0 0;
  }
`

const LinkItem = styled(Text)<{ notActive?: boolean }>`
  display: flex;
  align-items: center;
  justify-content: center;
  gap: 8px;
  text-transform: uppercase;
  letter-spacing: 0.2em;
  color: ${(p) =>
    !p.notActive ? p.theme.palette.secondaryLightText : p.theme.palette.lightText};
  a {
    color: inherit;
  }
`

interface IBreadcrumb {
  href: string
  name: string
}

interface OwnProps {
  breadcrumbs: IBreadcrumb[]
  justifyContent?: CSSProperties['justifyContent']
}

export const Breadcrumbs: React.FC<OwnProps> = (props) => {
  const { breadcrumbs, justifyContent } = props
  const { isMobile } = useBreakpoints()
  const { lang } = useTranslation()

  const filteredBreadcrumbs = isMobile
    ? [
        breadcrumbs[
          breadcrumbs.length >= 2 ? breadcrumbs.length - 2 : breadcrumbs.length - 1
        ],
      ]
    : breadcrumbs

  const breadCrumbJsonLdData = breadcrumbs.map((breadcrumb, index) => {
    const { name, href } = breadcrumb
    return {
      position: index + 1,
      name,
      item: removeTrailingSlash(`${BASE_APP_URL}${getRouteLocale(lang)}${href}`),
    }
  })

  const breadcrumbsElements = filteredBreadcrumbs.map((breadcrumb, index) => {
    const { href, name } = breadcrumb
    return (
      <Breadcrumb key={name} onClick={(e) => e.stopPropagation()}>
        {index === filteredBreadcrumbs.length - 1 && filteredBreadcrumbs.length > 1 ? (
          <LinkItem variant="h4" notActive>
            {isMobile && <StyledBackSvg />}
            {name}
          </LinkItem>
        ) : (
          <BaseLink href={href}>
            <LinkItem variant="h4">
              {isMobile && <StyledBackSvg />}
              {name}
            </LinkItem>
          </BaseLink>
        )}
      </Breadcrumb>
    )
  })

  return (
    <>
      <BreadcrumbJsonLd itemListElements={breadCrumbJsonLdData} />
      <BreadcrumbsWrapper justifyContent={justifyContent}>
        {breadcrumbsElements}
      </BreadcrumbsWrapper>
    </>
  )
}
