import React, { FC } from 'react'
import { Text } from '@/components/Text'
import List from '@/components/List/List'
import styled from 'styled-components'
import { IArticlePageItem } from '@/requests/blog/types'

import { deviceCssQuery } from '@/styling/breakpoints'
import { customParser } from '@/utils/customParser'

const Comparison = styled.div`
  display: grid;

  @media screen and ${deviceCssQuery.xs} {
    grid-template-columns: 1fr;
  }

  @media screen and ${deviceCssQuery.lg} {
    grid-template-columns: 1fr 1fr;
  }
`

const ComparisonItem = styled.div`
  display: grid;
  padding: 32px;
  &:last-of-type {
    background: #bd4818;
    filter: invert(1);
  }
`

interface OwnProps {
  items: IArticlePageItem[]
  header?: string
  headerTag?: string
  beforeDesc?: string
  afterDesc?: string
}

export const ComparisonBlock: FC<OwnProps> = (props) => {
  const { items, afterDesc, beforeDesc, header, headerTag } = props
  return (
    <>
      {header && (
        <Text
          variant="h3ForBlog"
          tag={(headerTag as keyof JSX.IntrinsicElements | undefined) || 'h2'}
        >
          {header}
        </Text>
      )}
      {items.length && (
        <>
          {beforeDesc && (
            <Text variant="paragraphForBlog">{customParser(beforeDesc || '')}</Text>
          )}
          <Comparison>
            {items?.map((item, index) => {
              const formatTheses = item.theses?.map((item) => ({ name: item })) || []
              return (
                <ComparisonItem key={item.title}>
                  <Text variant="h3">{item.title}</Text>
                  <br />
                  <List list={formatTheses} dotColor={index ? '#14161D' : undefined} />
                </ComparisonItem>
              )
            })}
          </Comparison>
          {afterDesc && (
            <Text variant="paragraphForBlog">{customParser(afterDesc || '')}</Text>
          )}
        </>
      )}
    </>
  )
}
