import React from 'react'
import styled from 'styled-components'
import { Text } from '@/components/Text'
import { VolumetricArrowIcon } from '@/components/icons/VolumetricArrowIcon'
import { defaultColorConfig } from '@/styling/theme'
import Link from 'next/link'
import { deviceCssQuery } from '@/styling/breakpoints'
import { useBreakpoints } from '@/hooks/useBreakpoints'

const LinksContainer = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
`

const TextHover = styled(Text)`
  @media screen and ${deviceCssQuery.xs} {
    font-size: 14px;
  }

  @media screen and ${deviceCssQuery.lg} {
    font-size: 20px;
  }
`

const VolumetricArrowIconLeft = styled(VolumetricArrowIcon)`
  transform: scaleX(-1);
`

const LinksItem = styled.div`
  cursor: pointer;
  display: flex;
  align-items: center;
  justify-content: start;

  &:last-of-type {
    justify-content: end;
  }

  & ${TextHover} {
    transition: color 300ms;
  }

  @media screen and ${deviceCssQuery.xs} {
    gap: 14px;
    letter-spacing: 0.2em;
    text-transform: uppercase;
    flex-direction: column;
    align-items: start;

    &:last-of-type {
      flex-direction: column-reverse;
      align-items: end;
      text-align: end;

      justify-content: start;
    }

    & ${TextHover} {
      color: ${(props) => props.theme.palette.primaryText};
    }

    &:hover ${TextHover} {
      color: ${(props) => props.theme.palette.primary};
    }
  }

  @media screen and ${deviceCssQuery.md} {
    align-items: center;
    gap: 28px;
    flex-direction: row;
    &:last-of-type {
      text-align: end;
      align-items: center;
      flex-direction: row;

      justify-content: end;
    }
  }
`

interface IChangePageData {
  url: string
  name: string
}

interface OwnProps {
  next?: IChangePageData | null
  prev?: IChangePageData | null
}

export const PageSwitcher: React.FC<OwnProps> = (props) => {
  const { next, prev } = props
  const { isDesktop } = useBreakpoints()
  return (
    <LinksContainer>
      {prev?.url && prev?.name ? (
        <Link href={prev.url}>
          <LinksItem>
            <div>
              <VolumetricArrowIconLeft
                color={defaultColorConfig.primary}
                width={isDesktop ? 56 : 28}
                height={isDesktop ? 56 : 28}
                opacity={1}
                isMobile={!isDesktop}
              />
            </div>
            <TextHover variant={isDesktop ? 'h2' : 'h4'}>{prev.name}</TextHover>
          </LinksItem>
        </Link>
      ) : (
        <div />
      )}

      {next?.url && next?.name ? (
        <Link href={next.url}>
          <LinksItem>
            <TextHover variant={isDesktop ? 'h2' : 'h4'}>{next.name}</TextHover>
            <div>
              <VolumetricArrowIcon
                color={defaultColorConfig.primary}
                width={isDesktop ? 56 : 28}
                height={isDesktop ? 56 : 28}
                opacity={1}
                isMobile={!isDesktop}
              />
            </div>
          </LinksItem>
        </Link>
      ) : (
        <div />
      )}
    </LinksContainer>
  )
}
