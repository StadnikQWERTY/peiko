import React from 'react'
import { PEIKO_CONTACTS, PEIKO_LINK_CONTACTS } from '@/constants/publicInfo'
import { Text, TText } from '@/components/Text'

type TPhoneLink = {
  className?: string
  textProps?: Omit<TText, 'children'>
  phoneVariant?: 'first' | 'second'
  phone?: string
  onClick?: () => void
}

export const PhoneLink: React.FC<TPhoneLink> = (props) => {
  const { className, textProps, phoneVariant = 'first', phone, onClick } = props

  const getPhone = () => {
    if (phone) return phone
    if (phoneVariant === 'first') return PEIKO_CONTACTS.PHONE
    if (phoneVariant === 'second') return PEIKO_CONTACTS.PHONE_SECOND
    return PEIKO_CONTACTS.PHONE
  }

  const getLink = () => {
    if (phone) return `tel: ${phone}`
    if (phoneVariant === 'first') return PEIKO_LINK_CONTACTS.PHONE
    if (phoneVariant === 'second') return PEIKO_LINK_CONTACTS.PHONE_SECOND
    return PEIKO_LINK_CONTACTS.PHONE
  }

  return (
    <a href={getLink()} className={className}>
      <Text variant="h3" {...textProps} onClick={onClick}>
        {getPhone()}
      </Text>
    </a>
  )
}
