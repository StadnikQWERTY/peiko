import React from 'react'
import styled from 'styled-components'
import { deviceCssQuery } from '@/styling/breakpoints'
import { ServiceBlock } from '@/components/DevelopmentServices/components/ServiceBlock'
import { ISubService } from '@/requests/types'

const Services = styled.section`
  display: grid;

  @media screen and ${deviceCssQuery.xs} {
    grid-template-columns: 100%;
  }

  @media screen and ${deviceCssQuery.lg} {
    grid-template-columns: 1fr 1fr;
    margin-left: unset;
  }
`

const ServiceBlockContainer = styled(ServiceBlock)`
  @media screen and ${deviceCssQuery.xs} {
    margin-bottom: -1px;
  }

  @media screen and ${deviceCssQuery.lg} {
    margin-left: -1px;
  }
`

interface OwnProps {
  services: ISubService[]
  onClick?: () => void
}

export const DevelopmentServices: React.FC<OwnProps> = (props) => {
  const { services, onClick } = props

  let count = 0

  return (
    <Services>
      {services.map((service) => {
        const isEmpty = (Object.keys(service) as Array<keyof typeof service>).every(
          (item) => {
            if (item === 'id') return true
            return !service[item]
          },
        )

        if (isEmpty) return null

        count += 1

        return (
          <ServiceBlockContainer
            onClick={onClick}
            key={service.id}
            service={service}
            counter={count}
          />
        )
      })}
    </Services>
  )
}
