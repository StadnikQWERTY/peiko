import styled from 'styled-components'
import { deviceCssQuery } from '@/styling/breakpoints'
import { Text } from '@/components/Text'

export const Body = styled.div`
  display: grid;
  grid-gap: 8px;
  grid-auto-rows: max-content;
`

export const TextHover = styled(Text)``

export const BodyTitle = styled.div`
  display: flex;
`

export const TitleArrowContainer = styled.div`
  margin-left: auto;
`

export const Root = styled.button`
  cursor: pointer;
  position: relative;
  display: grid;
  align-content: flex-start;
  border: 1px solid ${(props) => props.theme.palette.secondary};
  text-align: left;

  @media screen and ${deviceCssQuery.xs} {
    padding: 28px 12px;
    grid-gap: 24px;
    grid-template-columns: 100%;
  }

  @media screen and ${deviceCssQuery.md} {
    padding: 32px;
    grid-gap: 32px;
    grid-template-columns: 100%;
  }

  & .service-block-text {
    transition: color 200ms;
  }

  &:hover ${TextHover}, &:hover .service-block-text {
    color: ${(props) => props.theme.palette.primary};
    transition: color 300ms;
  }
`
