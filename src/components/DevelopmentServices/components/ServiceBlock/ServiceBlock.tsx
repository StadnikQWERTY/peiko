import React from 'react'
import styled, { css } from 'styled-components'
import { useRouter } from 'next/router'
import Link from 'next/link'
import { ArrowIcon } from '@/components/icons/ArrowIcon'
// import { Hashtags } from '@/components/Hashtags'
import { Text } from '@/components/Text'
import { IServicePageService } from '@/requests/services/types'
import { ROUTES } from '@/routes'
import { FontWeight } from '@/styling/fonts'
import { deviceCssQuery } from '@/styling/breakpoints'
import * as Styled from './styled'

type TServiceBlock = {
  className?: string
  service: IServicePageService
  counter: number
  onClick?: () => void
}

// const HashtagsWrapper = styled.div``

const StyledLink = styled.a(
  (props) => css`
    color: ${props.theme.palette.primaryText};
    font-weight: ${FontWeight.SemiBold};
    margin: 0 9.33px 0 0;
    @media screen and ${deviceCssQuery.xs} {
      font-size: 18px;
      line-height: 27px;
    }

    @media screen and ${deviceCssQuery.md} {
      font-size: 20px;
      line-height: 150%;
    }
  `,
)

const Numbering = styled(Text)`
  color: ${(props) => props.theme.palette.primary};
  margin: 0;
`

function pad(d: number) {
  return d < 10 ? `0${d.toString()}` : d.toString()
}

export const ServiceBlock: React.FC<TServiceBlock> = (props) => {
  const { className, service, counter, onClick } = props

  const router = useRouter()

  const link = service.uri && `${ROUTES.SERVICE}/${service.uri}`

  const handleRootClick = (e: React.SyntheticEvent<HTMLButtonElement>) => {
    if (onClick) {
      onClick()
    }
    if (!link) return
    const parent = (e.target as HTMLButtonElement).parentNode as HTMLLinkElement
    if (parent.tagName === 'A') return
    router.push(link)
  }

  return (
    <Styled.Root onClick={handleRootClick} role="link" className={className}>
      <Styled.Body>
        <Numbering variant="paragraph" tag="p">
          {pad(counter)}/
        </Numbering>
        <Styled.BodyTitle>
          {!link && (
            <Text tag="h3" variant="h3" className="service-block-text" margin="4px 0">
              {service.name}
            </Text>
          )}

          {link && (
            <Link href={link}>
              <StyledLink href={link} className="service-block-text">
                {service.name}
              </StyledLink>
            </Link>
          )}
          <Styled.TitleArrowContainer>
            <ArrowIcon />
          </Styled.TitleArrowContainer>
        </Styled.BodyTitle>
        {service.description && (
          <Text variant="paragraph" tag="p" margin={!link ? '0' : undefined}>
            {service.description}
          </Text>
        )}
        {
          //   service.tags && (
          //   <HashtagsWrapper>
          //     <Hashtags generateLink={ROUTES.CASES} tags={service.tags} />
          //   </HashtagsWrapper>
          // )
        }
      </Styled.Body>
    </Styled.Root>
  )
}
