import React from 'react'

type TCheckMarkIcon = {
  className?: string
}

export const CheckMarkIcon: React.FC<TCheckMarkIcon> = (props) => {
  const { className } = props

  return (
    <svg
      width="11"
      height="8"
      viewBox="0 0 11 8"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      className={className}
    >
      <path
        d="M3.6673 5.89073L1.47196 3.69539L0.529297 4.63806L3.6673 7.77606L10.1386 1.30473L9.19596 0.362061L3.6673 5.89073Z"
        fill="white"
      />
    </svg>
  )
}
