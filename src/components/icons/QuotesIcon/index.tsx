import React from 'react'
import styled, { css } from 'styled-components'

type TQuotesIconProps = {
  className?: string
  position: 'left' | 'right'
}

const SvgIcon = styled.svg<{ position: TQuotesIconProps['position'] }>`
  ${(p) =>
    p.position === 'left'
      ? css`
          align-self: flex-start;
          justify-self: flex-start;
        `
      : css`
          align-self: flex-end;
          justify-self: flex-end;
          transform: rotateX(180deg);
        `}
`

export const QuotesIcon: React.FC<TQuotesIconProps> = ({ className, position }) => (
  <SvgIcon
    width="20"
    height="16"
    viewBox="0 0 20 16"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    className={className}
    position={position}
  >
    <path
      d="M0 16V8.6385C0 6.63537 0.583017 4.83255 1.74905 3.23005C2.91508 1.62754 4.436 0.55086 6.31179 0V16H0ZM13.7643 16V8.6385C13.7643 6.63537 14.3219 4.83255 15.4373 3.23005C16.6033 1.62754 18.1242 0.55086 20 0V16H13.7643Z"
      fill="url(#paint0_linear_5060_2660)"
    />
    <defs>
      <linearGradient
        id="paint0_linear_5060_2660"
        x1="0"
        y1="0"
        x2="22.6119"
        y2="10.2225"
        gradientUnits="userSpaceOnUse"
      >
        <stop stopColor="#45C0EB" />
        <stop offset="1" stopColor="#308FEA" />
      </linearGradient>
    </defs>
  </SvgIcon>
)
