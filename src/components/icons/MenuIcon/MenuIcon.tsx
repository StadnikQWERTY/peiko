import React from 'react'
import styled from 'styled-components'
import useTranslation from 'next-translate/useTranslation'

const animationSpeed = '300ms'

const StyledButton = styled.button`
  display: block;
  width: 36px;
  height: 36px;
  padding: 0;
`

const StyledLine = styled.div<{ $rotate?: number }>`
  width: 36px;
  height: 1px;
  background: ${(props) => props.theme.palette.primaryText};
  transform: rotate(${(props) => props.$rotate || 0}deg);
  transition: ${animationSpeed} all;
`

const StyledFirstLine = styled(StyledLine)<{ $opened?: boolean }>`
  margin-bottom: ${(props) => (props.$opened ? 0 : '10px')};
  transition: ${animationSpeed} all;
`

const StyledSecondLine = styled(StyledLine)<{ $opened?: boolean }>`
  margin-top: ${(props) => (props.$opened ? 0 : '10px')};
  transition: ${animationSpeed} all;
`

type TMenuIcon = {
  opened?: boolean
  onClick?: () => void
}

export const MenuIcon: React.FC<TMenuIcon> = (props) => {
  const { opened, onClick } = props
  const { t } = useTranslation('aria-label')

  return (
    <StyledButton onClick={onClick} aria-label={t('menu-button')}>
      <StyledFirstLine $rotate={opened ? 45 : 0} $opened={opened} />
      <StyledSecondLine $rotate={opened ? -45 : 0} $opened={opened} />
    </StyledButton>
  )
}
