import React from 'react'

type TFileIcon = {
  className?: string
}

export const FileIcon: React.FC<TFileIcon> = (props) => {
  const { className } = props
  return (
    <svg
      width="10"
      height="13"
      viewBox="0 0 10 13"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      className={className}
    >
      <path
        d="M8.02144 12.5H0.333984V4.35321L2.68994 1.97632H8.02144V12.5ZM0.887716 11.9415H7.47281V2.53484H2.91934L0.887758 4.58447L0.887716 11.9415Z"
        fill="white"
      />
      <path
        d="M9.66831 11.0238H8.10981V10.4652H9.11466V1.05857H4.56608L3.7344 1.90008L3.3418 1.5039L4.33686 0.5H9.66835L9.66831 11.0238Z"
        fill="white"
      />
      <path
        d="M3.08237 4.74922H0.609375V4.19074H2.52879V2.25427H3.08243L3.08237 4.74922Z"
        fill="white"
      />
    </svg>
  )
}
