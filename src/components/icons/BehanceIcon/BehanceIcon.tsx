import React from 'react'
import { TBaseIcon } from '@/components/icons/utils/types'
import styled, { css } from 'styled-components'

type TBehanceIcon = TBaseIcon

const activeStyle = css`
  fill: #1c5cf6;
`

const StyledRoot = styled.svg`
  path {
    fill: white;
  }

  @media (hover: hover) {
    &:hover path {
      ${activeStyle}
    }
  }

  @media (hover: none) {
    &:active path {
      ${activeStyle}
    }
  }
`

export const BehanceIcon: React.FC<TBehanceIcon> = (props) => {
  const { className } = props

  return (
    <StyledRoot
      width="20"
      height="13"
      viewBox="0 0 20 13"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      className={className}
    >
      <path d="M8.07158 5.8119C8.07158 5.8119 9.96426 5.66769 9.96426 3.38979C9.96426 1.11173 8.4158 0 6.45436 0H0V12.7301H6.45436C6.45436 12.7301 10.3945 12.8578 10.3945 8.97276C10.3945 8.9728 10.5663 5.8119 8.07158 5.8119ZM5.98978 2.26263H6.45436C6.45436 2.26263 7.33169 2.26263 7.33169 3.58714C7.33169 4.9115 6.81573 5.10351 6.23048 5.10351H2.84387V2.26263H5.98978ZM6.27197 10.4676H2.84387V7.06559H6.45436C6.45436 7.06559 7.76199 7.048 7.76199 8.81385C7.76199 10.3029 6.78532 10.4563 6.27197 10.4676ZM15.6494 3.23891C10.8794 3.23891 10.8836 8.1302 10.8836 8.1302C10.8836 8.1302 10.5563 12.9964 15.6494 12.9964C15.6494 12.9964 19.8935 13.2452 19.8935 9.61126H17.7108C17.7108 9.61126 17.7836 10.9798 15.7222 10.9798C15.7222 10.9798 13.5391 11.1299 13.5391 8.765H19.9663C19.9663 8.76496 20.6696 3.23891 15.6494 3.23891ZM13.5151 7.06559C13.5151 7.06559 13.7817 5.10347 15.6979 5.10347C17.6137 5.10347 17.5897 7.06559 17.5897 7.06559H13.5151ZM18.0981 2.31758H12.9808V0.749966H18.0981V2.31758Z" />
    </StyledRoot>
  )
}
