import React, { FC } from 'react'

export const CheckIcon: FC<{ isCheck: string }> = ({ isCheck }) => {
  if (isCheck === '1') {
    return (
      <svg
        width="24"
        height="24"
        viewBox="0 0 24 24"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M9.99997 15.586L6.70697 12.293L5.29297 13.707L9.99997 18.414L19.707 8.70697L18.293 7.29297L9.99997 15.586Z"
          fill="url(#paint0_linear_3901_17287)"
        />
        <defs>
          <linearGradient
            id="paint0_linear_3901_17287"
            x1="5.29297"
            y1="7.29297"
            x2="21.3842"
            y2="14.8359"
            gradientUnits="userSpaceOnUse"
          >
            <stop stopColor="#60BE79" />
            <stop offset="1" stopColor="#35934E" />
          </linearGradient>
        </defs>
      </svg>
    )
  }

  return (
    <svg
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M6.14653 16.1465L7.56054 17.5605L17.2675 7.85354L15.8535 6.43954L6.14653 16.1465Z"
        fill="url(#paint0_linear_3901_17290)"
      />
      <path
        d="M15.8535 17.5605L17.2675 16.1465L7.56051 6.43949L6.14651 7.85349L15.8535 17.5605Z"
        fill="url(#paint1_linear_3901_17290)"
      />
      <defs>
        <linearGradient
          id="paint0_linear_3901_17290"
          x1="2"
          y1="12"
          x2="17.9162"
          y2="4.53869"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#E2702E" />
          <stop offset="1" stopColor="#DF4528" />
        </linearGradient>
        <linearGradient
          id="paint1_linear_3901_17290"
          x1="2"
          y1="12"
          x2="17.9162"
          y2="4.53869"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#E2702E" />
          <stop offset="1" stopColor="#DF4528" />
        </linearGradient>
      </defs>
    </svg>
  )
}
