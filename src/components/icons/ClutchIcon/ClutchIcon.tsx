import React from 'react'
import styled from 'styled-components'
import { TBaseIcon } from '@/components/icons/utils/types'

type ClutchIcon = TBaseIcon

const StyledRoot = styled.svg`
  path.dot {
    fill: white;
  }

  @media (hover: hover) {
    &:hover path.dot {
      fill: #ef4335;
    }
  }

  @media (hover: none) {
    &:active path.dot {
      fill: #ef4335;
    }
  }
`

export const ClutchIcon: React.FC<ClutchIcon> = (props) => {
  const { className } = props

  return (
    <StyledRoot
      width="18"
      height="20"
      viewBox="0 0 18 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      className={className}
    >
      <path
        d="M14.069 14.0816C13.0345 14.898 11.5862 15.5102 10.1379 15.5102C6.82759 15.5102 4.55172 13.0612 4.55172 9.79592C4.55172 6.53061 6.82759 4.28571 10.1379 4.28571C11.5862 4.28571 13.0345 4.69387 14.069 5.71428L14.6897 6.32653L18 3.2653L17.1724 2.65306C15.3103 1.0204 12.8276 0 10.1379 0C4.34483 0 0 4.28571 0 10C0 15.7143 4.34483 20 10.1379 20C12.8276 20 15.3103 18.9796 17.1724 17.3469L18 16.7347L14.6897 13.4694L14.069 14.0816Z"
        fill="white"
      />
      <path
        className="dot"
        d="M9.93059 13.2631C11.7588 13.2631 13.2409 11.8012 13.2409 9.99778C13.2409 8.19437 11.7588 6.73242 9.93059 6.73242C8.10233 6.73242 6.62024 8.19437 6.62024 9.99778C6.62024 11.8012 8.10233 13.2631 9.93059 13.2631Z"
        fill="white"
      />
    </StyledRoot>
  )
}
