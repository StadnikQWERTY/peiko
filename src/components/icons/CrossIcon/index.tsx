import React, { FC } from 'react'

const CrossIcon: FC<{ className?: string }> = (props) => {
  const { className } = props
  return (
    <svg
      className={className}
      width="10"
      height="9"
      viewBox="0 0 10 9"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path d="M1 0.5L9 8.5M1 8.5L9 0.5" stroke="#B1C1D1" />
    </svg>
  )
}

export default CrossIcon
