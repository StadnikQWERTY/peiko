import { CSSProperties, FC } from 'react'

interface OwnProps {
  color?: CSSProperties['color']
}

const ClockItem: FC<OwnProps> = (props) => {
  const { color } = props
  return (
    <svg
      width="16"
      height="19"
      viewBox="0 0 16 19"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M7.9987 15.1667C4.3187 15.1667 1.33203 12.1867 1.33203 8.50004C1.33203 4.82004 4.3187 1.83337 7.9987 1.83337C11.6854 1.83337 14.6654 4.82004 14.6654 8.50004C14.6654 12.1867 11.6854 15.1667 7.9987 15.1667ZM10.1254 10.9734C10.2054 11.02 10.292 11.0467 10.3854 11.0467C10.552 11.0467 10.7187 10.96 10.812 10.8C10.952 10.5667 10.8787 10.26 10.6387 10.1134L8.26536 8.70002V5.62002C8.26536 5.34002 8.03869 5.12002 7.76536 5.12002C7.49202 5.12002 7.26536 5.34002 7.26536 5.62002V8.98669C7.26536 9.16002 7.35869 9.32002 7.51202 9.41335L10.1254 10.9734Z"
        fill={color || 'white'}
      />
    </svg>
  )
}

export default ClockItem
