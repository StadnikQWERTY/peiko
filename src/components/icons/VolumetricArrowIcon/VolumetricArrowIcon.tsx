import React from 'react'

type TVolumetricArrowIcon = {
  className?: string
  opacity?: number
  color?: string
  width?: number
  height?: number
  isMobile?: boolean
}

export const VolumetricArrowIcon: React.FC<TVolumetricArrowIcon> = (props) => {
  const { className, opacity, color, width, height, isMobile } = props

  if (isMobile) {
    return (
      <svg
        width="30"
        height="30"
        viewBox="0 0 30 30"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        className={className}
      >
        <path
          d="M8.84324 8.55621L8.84686 1.10521L28.8187 1L29 21.2514L21.8329 21.5414L21.4545 13.7085L6.15768 29L1 23.8441L16.2968 8.5526L8.84324 8.55621Z"
          stroke={color || '#E4E8F4'}
          opacity={opacity || 0.5}
        />
      </svg>
    )
  }

  return (
    <svg
      width={width || 102}
      height={height || 102}
      viewBox="0 0 102 102"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      className={className}
    >
      <path
        opacity={opacity || 0.5}
        d="M29.0116 27.9865L29.0245 1.37574L100.353 1L101 73.3263L75.4034 74.3621L74.0518 46.3874L19.4203 101L1 82.5861L55.6316 27.9736L29.0116 27.9865Z"
        stroke={color || '#E4E8F4'}
      />
    </svg>
  )
}
