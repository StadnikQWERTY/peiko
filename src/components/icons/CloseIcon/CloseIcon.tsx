import React from 'react'

type TCloseIcon = {
  className?: string
  onClick?: () => void
}

export const CloseIcon: React.FC<TCloseIcon> = (props) => {
  const { className, onClick } = props
  return (
    <svg
      width="10"
      height="9"
      viewBox="0 0 10 9"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      className={className}
      onClick={onClick}
    >
      <path d="M1 0.5L9 8.5M1 8.5L9 0.5" stroke="white" />
    </svg>
  )
}
