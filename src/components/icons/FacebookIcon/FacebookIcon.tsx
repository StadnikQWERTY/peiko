import React from 'react'
import { TBaseIcon } from '@/components/icons/utils/types'
import styled, { css } from 'styled-components'

type TFacebookIcon = TBaseIcon

const activeStyle = css`
  fill: #3c5a9a;
`

const StyledRoot = styled.svg`
  path {
    fill: white;
  }

  @media (hover: hover) {
    &:hover path {
      ${activeStyle}
    }
  }

  @media (hover: none) {
    &:active path {
      ${activeStyle}
    }
  }
`

export const FacebookIcon: React.FC<TFacebookIcon> = (props) => {
  const { className } = props

  return (
    <StyledRoot
      width="11"
      height="20"
      viewBox="0 0 11 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      className={className}
    >
      <path d="M7.13988 19.9999V10.8769H10.383L10.8686 7.32157H7.13988V5.05157C7.13988 4.02223 7.44264 3.32068 9.00606 3.32068L11 3.31981V0.139943C10.655 0.0967723 9.47146 0 8.0945 0C5.21969 0 3.25153 1.65682 3.25153 4.69964V7.32167H0V10.877H3.25143V20L7.13988 19.9999Z" />
    </StyledRoot>
  )
}
