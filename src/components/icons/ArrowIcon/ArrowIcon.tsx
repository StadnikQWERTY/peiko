import React from 'react'
import styled from 'styled-components'

type TArrowIcon = {
  className?: string
  color?: string
}

const StyledRoot = styled.svg`
  transition: 300ms color;
`

export const ArrowIcon: React.FC<TArrowIcon> = ({ className, color = '#41B6E6' }) => (
  <StyledRoot
    width="11"
    height="11"
    viewBox="0 0 11 11"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    className={className}
  >
    <path
      d="M10.1704 1.37219L1.3316 10.211M10.1704 1.37219L3.07009 1.4016M10.1704 1.37219L10.1412 8.47266"
      stroke={color}
      strokeLinecap="square"
    />
  </StyledRoot>
)
