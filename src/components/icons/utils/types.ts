export type TBaseIcon = {
  className?: string
  color?: string
}
