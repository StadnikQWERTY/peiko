import React from 'react'
import styled from 'styled-components'

type TProps = {
  className?: string
  color?: string
}

const StyledRoot = styled.svg`
  transition: 300ms color;
`

export const ArrowDownIcon: React.FC<TProps> = ({ className, color = '#41B6E6' }) => (
  <StyledRoot
    width="16"
    height="16"
    viewBox="0 0 16 16"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    className={className}
  >
    <path d="M12 6L8 10L4 6" stroke={color} strokeWidth="1.5" strokeLinecap="square" />
  </StyledRoot>
)
