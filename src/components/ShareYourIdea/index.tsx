import React, { FC } from 'react'
import { Text } from '@/components/Text'
import { Spacer } from '@/components/Spacer/Spacer'
import LongButton from '@/components/LongButton/LongButton'
import { useSetContactUs } from '@/features/contact-us'
import useTranslation from 'next-translate/useTranslation'

const ShareYourIdea: FC = () => {
  const { setContactUsVisibility } = useSetContactUs()
  const { t: tCommon } = useTranslation('common')
  const { t: tServices } = useTranslation('services')
  return (
    <>
      <Text variant="h2" align="center">
        {tServices('shareYourIdea')}
      </Text>
      <Spacer lg={{ padding: '25px 0' }} xs={{ padding: '25px 0' }}>
        <Text variant="paragraph" align="center">
          {tServices('contactUsText')}
        </Text>
      </Spacer>
      <LongButton
        xs={{ minWidth: 'auto', paddingLeft: '24px', paddingRight: '24px' }}
        onClick={() => setContactUsVisibility(true)}
      >
        {tCommon('contactUs')}
      </LongButton>
    </>
  )
}

export default ShareYourIdea
