import React from 'react'
import styled from 'styled-components'
import { deviceCssQuery } from '@/styling/breakpoints'
import { ArrowTextButton, TProps } from '@/components/buttons/ArrowTextButton'

export const Button = styled(ArrowTextButton)`
  padding: 16px 24px;
  width: 100%;
  background: ${(props) => props.theme.palette.primary};

  @media screen and ${deviceCssQuery.md} {
    width: fit-content;
  }
`

export const PrimaryButton: React.FC<TProps> = ({ children, ...props }) => (
  <Button {...props}>{children}</Button>
)
