import styled, { css, keyframes, DefaultTheme } from 'styled-components'
import { FontWeight } from '@/styling/fonts'
import { Z_INDEX } from '@/styling/z-index'
import { GET_IN_TOUCH_FLOAT_BUTTON_HEIGHT } from '@/constants/menu'
import { ButtonProps } from './Button'

const wave = keyframes`
  0% {
    box-shadow: 0 0 0 0 rgba(65, 182, 230, 0.7);
  }
  70% {
    box-shadow: 0 0 0 14px rgba(65, 182, 230, 0);
  }
  100% {
    box-shadow: 0 0 0 0 rgba(65, 182, 230, 0);
  }
`

const waveAnimationDecalration = css`
  animation: ${wave} 2s infinite;
`

const variantStyles = (theming: DefaultTheme) => ({
  primary: css`
    font-weight: ${FontWeight.Medium};
    background-color: ${theming.palette.primary};
    color: ${theming.palette.white};
    border: 1px solid ${theming.palette.primary};

    & svg {
      fill: ${theming.palette.white};
    }

    & svg > path {
      transition: stroke 0.15s ease-in-out;
      stroke: ${theming.palette.white};
    }

    &:hover {
      color: ${theming.palette.primary};
      background-color: transparent;
    }

    &:hover svg {
      fill: ${theming.palette.primary};
    }

    &:hover svg > path {
      stroke: ${theming.palette.primary};
    }
  `,

  secondary: css`
    font-weight: ${FontWeight.SemiBold};
    border: 1px solid ${theming.palette.primary};
    color: ${theming.palette.primary};

    & svg {
      fill: ${theming.palette.primary};
    }

    & svg > path {
      transition: stroke 0.15s ease-in-out;
      stroke: ${theming.palette.primary};
    }

    &:hover {
      color: ${theming.palette.white};
      background-color: ${theming.palette.primary};
    }

    &:hover svg {
      fill: ${theming.palette.white};
    }

    &:hover svg > path {
      stroke: ${theming.palette.white};
    }
  `,

  float: css`
    position: fixed;
    display: block;
    bottom: 0;
    left: 0;
    right: 0;
    width: 100%;
    border-radius: 0px;
    height: ${GET_IN_TOUCH_FLOAT_BUTTON_HEIGHT}px;
    font-weight: ${FontWeight.Medium};
    z-index: ${Z_INDEX.FLOAT_BUTTON};
    color: ${theming.palette.white};
    background-color: ${theming.palette.primary};

    & svg {
      fill: ${theming.palette.white};
    }

    & svg > path {
      stroke: ${theming.palette.white};
    }
  `,
})

const sizeStyles = {
  small: css`
    font-size: 14px;
    padding: 13px 16px;
    line-height: 16px;
    letter-spacing: 0.03em;
  `,
  medium: css`
    font-size: 16px;
    padding: 17px 32px;
    line-height: 20px;
    letter-spacing: 0.07em;
  `,
}

type StyledButtonProps = ButtonProps

export const Button = styled.button<StyledButtonProps>`
  position: relative;
  border: none;
  display: inline-flex;
  border-radius: 50px;
  width: ${({ fullWidth }) => (fullWidth ? '100%' : 'fit-content')};
  cursor: pointer;
  text-align: center;
  text-transform: uppercase;
  transition-property: color, background-color, border-color;
  transition-duration: 0.15s;
  transition-timing-function: ease-in-out;
  ${({ size }) => size && sizeStyles[size]};
  ${({ theme, variant }) => variant && variantStyles(theme)[variant]};
  pointer-events: ${({ loading }) => (loading ? 'none' : 'auto')};

  ${({ waveAnimation }) => waveAnimation && waveAnimationDecalration};

  &:disabled {
    cursor: not-allowed;
    border-color: ${({ theme }) => theme.palette.secondary};
    background: ${({ theme }) => theme.palette.secondary};
    color: ${({ theme }) => theme.palette.lightText};

    & svg {
      fill: ${({ theme }) => theme.palette.lightText};
    }

    & svg > path {
      stroke: ${({ theme }) => theme.palette.lightText};
    }
  }
`

export const ButtonContent = styled.span<{ $loading?: boolean }>`
  position: relative;
  display: inline-flex;
  align-items: center;
  gap: 7px;
  opacity: ${({ $loading }) => ($loading ? 0 : 1)};
`

export const LoaderContainer = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: ${Z_INDEX.BUTTON_LOADER};
`
