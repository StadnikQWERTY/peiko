import React from 'react'
import { ArrowIcon } from '@/components/icons/ArrowIcon'
import { Loader } from '@/components/Loader'
import { useTheme } from 'styled-components'
import * as Styled from './Button.styled'

export type ButtonVariant = 'primary' | 'secondary' | 'float'
export type ButtonSize = 'small' | 'medium'

export interface ButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {
  variant?: ButtonVariant
  size?: ButtonSize
  loading?: boolean
  endIcon?: React.ReactNode
  startIcon?: React.ReactNode
  waveAnimation?: boolean
  fullWidth?: boolean
}

export const Button = React.forwardRef<HTMLButtonElement, ButtonProps>((props, ref) => {
  const {
    variant = 'primary',
    size = 'medium',
    loading,
    children,
    startIcon,
    endIcon = <ArrowIcon />,
    waveAnimation,
    fullWidth,
    ...otherProps
  } = props

  const { palette } = useTheme()

  return (
    <Styled.Button
      ref={ref}
      variant={variant}
      size={size}
      loading={loading}
      waveAnimation={waveAnimation}
      fullWidth={fullWidth}
      {...otherProps}
    >
      <Styled.ButtonContent $loading={loading}>
        {startIcon}
        {children}
        {endIcon}
      </Styled.ButtonContent>
      {loading && (
        <Styled.LoaderContainer>
          <Loader color={variant === 'primary' ? palette.white : palette.primary} />
        </Styled.LoaderContainer>
      )}
    </Styled.Button>
  )
})
