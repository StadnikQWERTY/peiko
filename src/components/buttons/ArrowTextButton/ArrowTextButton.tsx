import React, { ButtonHTMLAttributes } from 'react'
import styled, { css } from 'styled-components'
import { FontWeight } from '@/styling/fonts'
import { Z_INDEX } from '@/styling/z-index'
import { ArrowIcon } from '@/components/icons/ArrowIcon'
import { Loader } from '@/components/Loader'

const StyledBottomLine = styled.div`
  display: flex;
  position: absolute;
  height: 1px;
  width: 0;
  background-color: ${(props) => props.theme.palette.primary};
  transition: 200ms;
  left: 0;
  bottom: 0;
  z-index: ${Z_INDEX.BUTTON_LINE};
`

export const StyledButtonContent = styled.div`
  display: grid;
  align-items: center;
  grid-auto-flow: column;
  grid-auto-columns: max-content;
  grid-gap: 7.33px;

  font-size: 16px;
  font-weight: ${FontWeight.Medium};
  line-height: 19.5px;
  text-transform: uppercase;
  color: ${(props) => props.theme.palette.primary};

  transition: 350ms;
`
export const StyledButton = styled.button<{ $lineAnimated?: boolean }>`
  display: flex;
  justify-content: center;
  cursor: pointer;
  position: relative;

  ${(props) =>
    props.$lineAnimated &&
    css`
      padding: 24px 0;
    `}

  ${(props) =>
    props.$lineAnimated &&
    css`
      & ${StyledBottomLine} {
        width: 100%;
        margin-left: auto;
        transition: 200ms;
      }
    `}

  ${(props) =>
    props.$lineAnimated &&
    css`
      &:hover ${StyledButtonContent} {
        transform: translateY(-2px);
        transition: 350ms;
      }
    `}
`

const LoaderContainer = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0;
  z-index: ${Z_INDEX.BUTTON_LOADER};
`

export type TProps = {
  className?: string
  children: React.ReactNode
  lineAnimated?: boolean
  color?: string
  onClick?: () => void
  type?: ButtonHTMLAttributes<unknown>['type']
  loading?: boolean
}

export const ArrowTextButton = React.forwardRef<HTMLButtonElement, TProps>(
  (props, ref) => {
    const {
      className,
      children,
      lineAnimated = false,
      color,
      onClick,
      type,
      loading,
    } = props

    const handleClick = () => {
      if (!loading && onClick) {
        onClick()
      }
    }

    return (
      <StyledButton
        ref={ref}
        className={className}
        type={type}
        $lineAnimated={lineAnimated}
        onClick={handleClick}
      >
        <StyledButtonContent>
          {children} <ArrowIcon color={color} />
        </StyledButtonContent>

        <StyledBottomLine />

        {loading && (
          <LoaderContainer>
            <Loader />
          </LoaderContainer>
        )}
      </StyledButton>
    )
  },
)

ArrowTextButton.displayName = 'ArrowTextButton'
