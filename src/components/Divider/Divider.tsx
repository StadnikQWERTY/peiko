import React from 'react'
import styled, { css } from 'styled-components'

type TDivider = {
  vertical?: boolean
}

const StyledDivider = styled.div<TDivider>`
  height: 1px;
  background: ${(props) => props.theme.palette.secondary};
  width: 100%;

  ${(props) =>
    props.vertical &&
    css`
      height: 100%;
      width: 1px;
    `}
`

export const Divider: React.FC<TDivider> = (props) => <StyledDivider {...props} />
