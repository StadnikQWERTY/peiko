/* eslint-disable @typescript-eslint/no-var-requires */
const axios = require('axios')
const fs = require('fs')
const i18nConfig = require('./i18n.js')

let siteDomain = ''
let apiUrl = ''

const { defaultLocale } = i18nConfig
const getLang = (lang) => (defaultLocale === lang ? '' : `/${lang}`)

const generateSiteMap = async () => {
  const apiInstance = axios.create({
    baseURL: `${apiUrl}/api`,
    headers: {
      'Content-Type': 'application/json',
    },
  })

  // get slugs from server
  let slugs = {}

  const getSlugs = async () => {
    try {
      const [{ data: blog }, { data: caseData }, { data: service }] = await Promise.all([
        apiInstance.get('/blog/uris'),
        apiInstance.get('/cases/uris'),
        apiInstance.get('/services/uris'),
      ])

      slugs = {
        blog: blog.data.map((item) => ({ ...item, slug: item.uri })),
        case: caseData.data.map((item) => ({ ...item, slug: item.uri })),
        service: service.data.map((item) => ({ ...item, slug: item.uri })),
      }
    } catch (e) {
      console.warn(e)
      process.exit(0)
    }
  }
  await getSlugs()

  // create item
  const createItem = ({ route, slug, lang }) => {
    const pageId = slug ? `/${slug}` : ''
    return `
       <url>
           <loc>${`${siteDomain}${getLang(lang)}${route}${pageId}`}</loc>
       </url>`
  }

  // sitemap filtered by locales
  const sitemapLocales = {}

  // create static routes
  const generateStaticRoutes = (pages) =>
    pages.forEach((page) => {
      const regex = /(pages)|(src)|(.js)|(.tsx)|(.md)|(index)/gi
      let route = page.replace(regex, '')
      route = route.replace(/\/$/, '')

      i18nConfig.locales.forEach((lang) => {
        if (!sitemapLocales[lang]) {
          sitemapLocales[lang] = []
        }
        const item = createItem({ route, lang })
        sitemapLocales[lang].push(item)
      })
    })

  // create dinamic routes
  const generateDinamicRoutes = ({ pages, route }) => {
    if (!pages) return ''

    return pages.forEach(({ locale, slug, noidex, ...page }) => {
      if (noidex) return ''
      if (!sitemapLocales[locale]) {
        sitemapLocales[locale] = []
      }
      const item = createItem({ ...page, route, lang: locale, slug })
      sitemapLocales[locale].push(item)
    })
  }

  // render

  generateStaticRoutes(['', '/blog', '/about', '/services', '/cases'])
  generateDinamicRoutes({ pages: slugs.blog, route: '/blog/article' })
  generateDinamicRoutes({ pages: slugs.case, route: '/case' })
  generateDinamicRoutes({ pages: slugs.service, route: '/service' })

  // render common sitemap
  const renderCommonSitemap = () => {
    const renderItem = (lang) => `
    <sitemap>
      <loc>${siteDomain}/sitemap_${lang}.xml</loc>
    </sitemap>`

    const sitemap = `<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
  ${Object.keys(sitemapLocales)
    .map((lang) => renderItem(lang))
    .join('')}
</sitemapindex>`

    fs.writeFileSync('public/sitemap.xml', sitemap)
  }

  // render locales sitemaps
  const renderSitemapLocales = () => {
    Object.keys(sitemapLocales).forEach((lang) => {
      const sitemap = `<?xml version="1.0" encoding="UTF-8"?>
    <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
      ${sitemapLocales[lang].join('')}
    </urlset>`

      fs.writeFileSync(`public/sitemap_${lang}.xml`, sitemap)
    })
  }

  renderCommonSitemap()
  renderSitemapLocales()

  process.exit(0)
}

// read anv variables
try {
  const envString = fs.readFileSync('./.env.local', 'utf8')
  const rows = envString.split('\n')

  rows.forEach((item) => {
    const values = item.split('=')
    if (values[0] === 'NEXT_PUBLIC_APP_URL') {
      const [, val] = values
      siteDomain = val
    }
    if (values[0] === 'NEXT_PUBLIC_API_REST_URL') {
      const [, val] = values
      apiUrl = val
    }
  })
} catch (e) {
  console.warn(e)
  process.exit(0)
}

generateSiteMap()

module.exports = generateSiteMap
