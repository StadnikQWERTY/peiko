## Install

- Install globaly **node.js**
- Install dependencies: **yarn install**

## Commands

- Start locale dev server: **yarn dev**
- prod build: **yarn build**
- Check eslint: **yarn lint**
- Check typescript: **yarn tsc**
- Run prettier: **yarn prettier**
- Format code with prettier & eslint: **yarn format**

## Config files

- Build configuration in file: **next.config.js**
