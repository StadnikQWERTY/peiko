/** @type {import('next').NextConfig} */
const nextTranslate = require('next-translate')

const { IMAGES_DOMAIN } = process.env

const withBundleAnalyzer = require('@next/bundle-analyzer')({
  enabled: process.env.ANALYZE === 'true',
})

module.exports = () =>
  withBundleAnalyzer(
    nextTranslate({
      reactStrictMode: false,
      typescript: {
        ignoreBuildErrors: true,
      },
      images: {
        domains: [IMAGES_DOMAIN],
      },
      compiler: {
        styledComponents: true,
      },
      webpack(config) {
        return config
      },
    }),
  )
