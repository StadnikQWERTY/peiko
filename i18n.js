module.exports = {
  locales: ['en', 'ru'],
  defaultLocale: 'en',
  localeDetection: false,
  logBuild: false,
  pages: {
    '*': [
      'common',
      'validation',
      'form',
      'contact-us',
      'services',
      'cases',
      'aria-label',
      'error',
      'alt',
      'header',
    ],
    '/': ['home', 'it-partners', 'clutch-award'],
    '/about': ['about', 'it-partners'],
    '/service/[service]': ['clutch-award'],
  },
}
